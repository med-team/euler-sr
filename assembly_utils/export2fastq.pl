#!/usr/bin/env perl
if ($#ARGV < 1) {
		print "usage: export2fastq.pl in_export.txt out.fastq\n";
		exit(0);
}

$in = shift @ARGV;
$out = shift @ARGV;

open(IN, "$in") or die "cannot open export file $in\n";
open(OUT, ">$out") or die "cannot open fasta file $out\n";


while(<IN>) {
		$line = $_;
		@vals = split(/\s+/, $line);
		$valstr = join(",", @vals);
		$title = "$vals[0]:$vals[1]:$vals[2]:$vals[3]:$vals[4]:$vals[5]$vals[6]$vals[7]";
		print OUT "@$title\n";
		print OUT "$vals[8]\n";
		print OUT "+$title\n";
		print OUT "$vals[9]\n";
}
		
