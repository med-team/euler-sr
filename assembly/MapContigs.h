/***************************************************************************
 * Title:          MapContigs.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  03/16/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef MAP_CONTIGS_H_
#define MAP_CONTIGS_H_
#include "SortedTupleList.h"
#include "ReadPos.h"
#include "DNASequence.h"
#include <list>

// class that is an ungapped alignment.
class Block {
 public:
	int refPos;
	int qryPos;
	int length;
	int strand;
};

typedef std::list<Block> BlockList;

void MapContig(DNASequence &contig,
							 SimpleSequenceList &sequences,
							 std::vector<CountedReadPos> &refPositions,
							 int tupleSize, int minBlockLength,
							 BlockList &blocks);

#endif
