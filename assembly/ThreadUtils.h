#ifndef THREAD_UTILS_H_
#define THREAD_UTILS_H_

#include "IntervalGraph.h"
#include "ThreadPath.h"
#include "mctypes.h"
#include <string>
#include <vector>

int CollectBranchSequences(IntervalGraph &graph, int branchLength, int vertex, int maxBranches,
													 std::vector<char*> &branchSequences, 
													 std::vector<int> &branchLengths, 
													 string curSeq, int &curSeqIndex, int depth);


int StorePathSequences(IntervalGraph &graph, 
											 int edgeIndex,int edgePos, 
											 std::string curSequence,int searchLength, 
											 std::vector<std::string> &sequences);

int Thread(IntervalGraph &graph,int edgeIndex, int intvIndex, 
					 const char* read, int readLength,
					 ThreadPath &minThreadPath,
					 int maxScore, int maxThreadLength, 
					 DNASequence &perfectRead,
					 std::vector<int> &scoreList, int scoreListLength, FloatMatrix &scorMat,
					 int readIndex, int allowGaps);

void AppendReverseComplements(DNASequenceList &sequences);

void FindBestMatch(DNASequence &read, std::vector<std::string> &pathSequences, 
									 int & id_best, int & delta_best, int &delta_best2);

int ThreadRecursively(IntervalGraph &graph,
											int edgeIndex, int edgePos,
											const char*curSeq,int curSeqLength,
											ThreadPath &curThreadPath, int curThreadScore,
											ThreadPath &minThreadPath, int &minThreadScore, 
											int maxScore, int maxDepth, DNASequence &perfectRead, 
											std::vector<int> &scoreList, int scoreListLength, int depth, 
											FloatMatrix &scoreMat, int printNext, int readIndex, int allowGaps);


int ThreadRecursivelyBandedAlign(DNASequence &read,
																 IntervalGraph &graph,
																 int edgeIndex, int edgePos,
																 char* curPathSeq, int curPathSeqLength,
																 ThreadPath &curThreadPath, int curThreadScore,
																 ThreadPath &minThreadPath, int minThreadScore,
																 int curDepth, int maxDepth, map<int,int> &nTraverse, string spacing);


void ThreadToSeq(IntervalGraph &graph,
								 ThreadPath &path,
								 std::string &sequence);

#endif
