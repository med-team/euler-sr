/***************************************************************************
 * Title:          ReadIntervals.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/18/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef READ_INTERVALS_H_
#define READ_INTERVALS_H_

#include "DeBruijnGraph.h"
#include "ReadPaths.h"
#include "BEdge.h"
#include <algorithm>
#include <string>
#include <list>

using namespace std;
class EdgeInterval {
 public:
  int edge : 32;
  int edgePos : 32;
  int length : 32;
  int readPos : 32;
  EdgeInterval() {
    edge = -1;
    edgePos  = -1;
    readPos  = -1;
    length = 0;
    
  }
  EdgeInterval& operator=(const EdgeInterval &ei) {
    edge    = ei.edge;
    edgePos = ei.edgePos;
    readPos = ei.readPos;
    length  = ei.length;
		return *this;
  }
  void Print(std::ostream &out) {
    std::cout << "edge: " << edge << " edgePos: " << edgePos 
							<< " readPos: " << readPos << " length: " << length;
  }
};

// Read intervals are for edges to map which reads pass through them.

class BareReadInterval {
 public:
  int read : 32;
  int readPos : 32;
	int edgePos : 32;
  int length : 32;
	BareReadInterval(int readP, int readPosP, int edgePosP, int lengthP) {
		read    = readP;
		readPos = readPosP;
		edgePos = edgePosP;
		length  = lengthP;
	}
	BareReadInterval() {
		read = readPos = edgePos = length = -1;
	}
	BareReadInterval operator=(BareReadInterval ri) {
		read = ri.read;
		readPos = ri.readPos;
		edgePos = ri.edgePos;
		length  = ri.length;
		return *this;
	}
};
typedef std::vector<BareReadInterval> BareReadIntervalList;
typedef std::pair<BareReadInterval*, int> BareReadIntervalIndex;
typedef std::list<BareReadIntervalIndex> BareReadIntervalIndexList;

class ReadInterval : public BareReadInterval {
 public:
	//  
	// There is comfortably enough space for 
	// 12 flags here.
	//
	unsigned int markedForDeletion:1;
	unsigned int traversed:1;
	unsigned int detached:1;										 
											 
	int pathPos:27; /* 
										 This could probably be 10-12. 
										 The path length is bounded by
										 the genome length (assuming an
										 interval at each position),
										 but is typically much shorter.
									*/
	ReadInterval(int edgeP, int readP, int readPosP, 
							 int edgePosP, int lengthP, int pathPosP) : 
	BareReadInterval(readP, readPosP, edgePosP, lengthP) {
		//		edge = edgeP;
		pathPos = pathPosP;
	}

  ReadInterval() {
		//    edge    = -1;
    read    = -1;
    readPos = -1;
    edgePos = -1;
    length  = -1;
		pathPos = -1;
		traversed = 0;
		markedForDeletion = 0;
		detached = 0;
  }
  void Print(std::ostream &out) {
		//    out << " edge: " << edge << " edgePos " << edgePos << " read " << read 
    out << "edgePos " << edgePos << " read " << read 
				<< " read pos " << readPos << " len: " << length;
  }

  ReadInterval &operator=(const EdgeInterval &ei) {
		//    edge    = ei.edge;
    readPos = ei.readPos;
    edgePos = ei.edgePos;
    length  = ei.length;
    return *this;
  }    

  ReadInterval operator=( ReadInterval ri) {
		//    edge    = ri.edge;
    read    = ri.read;
    readPos = ri.readPos;
    edgePos = ri.edgePos;
    length  = ri.length;
		pathPos = ri.pathPos;
		markedForDeletion = ri.markedForDeletion;
		traversed = ri.traversed;
		detached  = ri.detached;
    return *this;
  }
	
	int IsMarkedForDeletion() {
		return markedForDeletion == 1;
	}
	int MarkForDeletion() {
		markedForDeletion = 1;
		return markedForDeletion;
	}
};

class CompareBareReadIntervalIndicesByRead {
 public:
	BareReadIntervalList* readIntervalPtr;
	int operator()(const int &a, const int &b) {
		if ((*readIntervalPtr)[a].read != (*readIntervalPtr)[b].read) 
			return (*readIntervalPtr)[a].read < (*readIntervalPtr)[b].read;
		if ((*readIntervalPtr)[a].readPos != (*readIntervalPtr)[b].readPos) 
			return (*readIntervalPtr)[a].readPos < (*readIntervalPtr)[b].readPos;
	}
};


class CompareReadIntervalsByReadPos {
 public:
  int operator()(const ReadInterval &a, const ReadInterval &b) {
    // first sort on edges, if either edge is marked as don't care
		// skip the comparison
		//    if (a.edge != b.edge and a.edge != -1 and b.edge !=-1) return a.edge < b.edge;
    
    // next sort on reads
    if (a.read != b.read) return a.read < b.read;

    // the intervals are from the same read, sort according to the
    // order of the reads
    
    return a.readPos < b.readPos;

    // the edges are equal
    if (a.edgePos != b.edgePos) return a.edgePos < b.edgePos;
  }
};
	

class CompareBareReadIntervalsByEdgePos {
 public:
	int operator()(const BareReadInterval &a, const BareReadInterval &b) const {
    // next sort on reads
    if (a.read != b.read) return a.read < b.read;

    // the intervals are from the same read, sort according to the
    // order of the reads
		if (a.readPos != b.readPos) return a.readPos < b.readPos;

    // the edges are equal
    return a.edgePos < b.edgePos;
	}
};

class CompareBareReadIntervalsByReadPos {
 public:
	int operator()(const BareReadInterval &a, const BareReadInterval &b) const {
		if (a.read != b.read) return a.read < b.read;
		
		if (a.edgePos != b.edgePos) return a.edgePos < b.edgePos;
		
		return a.readPos < b.readPos;
	}
};

class CompareReadIntervals {
 public:
  int operator()(const ReadInterval &a, const ReadInterval &b) {
    // first sort on edges, if either edge is marked as don't care
		// skip the comparison
		//    if (a.edge != b.edge and a.edge != -1 and b.edge !=-1) return a.edge < b.edge;
    
		// next sort on reads
    if (a.read != b.read) return a.read < b.read;

    // the edges are equal
    if (a.edgePos != b.edgePos) return a.edgePos < b.edgePos;

    // the intervals are from the same read, sort according to the
    // order of the reads
    
    return a.readPos < b.readPos;
  }
};

class EdgeIntervalList {
 public:
  static int tupleSize;
  std::vector<EdgeInterval> edgeIntervals;
  EdgeIntervalList() {
    edgeIntervals.resize(0);
  }
  
  int IncrementEdgeInterval(int edge, int edgePos, int readPos, int &edgeMultiplicity) {
    int cur = 0;
    cur  = edgeIntervals.size() - 1;
    if (edgeIntervals.size() > 0 and 
				edgeIntervals[cur].edge == edge and 
				edgeIntervals[cur].edgePos + edgeIntervals[cur].length - tupleSize == edgePos - 1) {
      edgeIntervals[cur].length++;
    }
    else {
      ++cur;
      edgeIntervals.resize(cur + 1);
      edgeIntervals[cur].edge    = edge;
      edgeIntervals[cur].edgePos = edgePos;
      edgeIntervals[cur].length  = tupleSize;
      edgeIntervals[cur].readPos = readPos;
      ++edgeMultiplicity;
    }
		return cur;
  }
};


typedef std::vector<ReadInterval> ReadIntervalList;
typedef std::vector<BareReadInterval> BareReadIntervalList;
typedef std::vector<BareReadIntervalList> BareReadIntervalListList;
typedef std::vector<EdgeIntervalList> EdgeIntervalListList;

int IncrementReadIntervalList(int edge, int edgePos, int read, int prevPos, int readPos,
															ReadIntervalList &readIntervals, int tupleSize,
															int prevEdge);

class IntervalEdge : public BEdge {
 public:
  ReadIntervalList *intervals;
  IntervalEdge() : BEdge() {}
	void Init() {
		seq.seq = NULL;
		seq.length = 0;
		intervals = new ReadIntervalList;
	}
  void Nullify() {
		intervals = new ReadIntervalList;
    BEdge::Nullify();
  }
  IntervalEdge &operator=(IntervalEdge edge) {
    // copy parent type information
    BEdge::operator=(edge);
		//    intervals->clear();
    // copy over an entire list of intervals
    intervals = edge.intervals;
		return *this;
  }
	void Clear() {
		if (intervals != NULL) {
			intervals->clear();
			std::vector<ReadInterval>().swap(*intervals);
			delete intervals;
			intervals = NULL;
		}
		((BEdge*)this)->Nullify();
	}
};

void PathReadPosToIntervalIndex(std::vector<BareReadIntervalList> &edgeReadIntervals,
																std::vector<std::vector<int> > &edgeIntervalIndices,
																PathIntervalList &paths,
																std::vector<int> &pathLengths);

typedef std::vector<IntervalEdge> IntervalEdgeList;


// sure, this is a pretty strict templatization, but I'm not
// sure how else to do it when I'm using subclasses
template<typename E>
void PrintReadIntervals(std::vector<E> &edges,
												std::string &readIntervalFileName) {

  std::ofstream readIntervalOut;
  openck(readIntervalFileName, readIntervalOut, std::ios::out);

  int v, e;
  int edgeIndex;
  int intervalIndex;
  intervalIndex = 0;
  for (edgeIndex = 0; edgeIndex < edges.size(); edgeIndex++ ) {
    if (edges[edgeIndex].IsNullified() == 1)
      continue;
    readIntervalOut << "EDGE " << edgeIndex 
										<< " Length " << edges[edgeIndex].length
										<< " Multiplicity " << edges[edgeIndex].intervals->size()
										<< std::endl;
    for (intervalIndex = 0; intervalIndex < edges[edgeIndex].intervals->size();
				 intervalIndex++) {
      readIntervalOut << "INTV" 
											<< " " << (*edges[edgeIndex].intervals)[intervalIndex].read 
											<< " " << (*edges[edgeIndex].intervals)[intervalIndex].readPos 
											<< " " << (*edges[edgeIndex].intervals)[intervalIndex].length
											<< " " << (*edges[edgeIndex].intervals)[intervalIndex].edgePos << std::endl;
    }
  }
  readIntervalOut.close();
}


template<typename V, typename E>
int StoreIntervalPathForward(E &edge, int intervalIndex, 
														 std::vector<V> &vertices, std::vector<E> &edges,
														 int vertexLength, std::vector<int> &path) {
  /* 
     Given an edge and a read interval on that edge, search
     back for the entire length of the path.  Add each edge that gets traced back 
     to 'path'.
  */

  int edgeOut;
  int destVertex;
  int nextEdge;
  int curEdge = edge.index;
  int nextIntervalIndex;

  /*
    std::cout << "edge: " << edge.index << " interval: " << intervalIndex 
    << " re:" << edge.intervals[intervalIndex].read
    << " rp:" << edge.intervals[intervalIndex].readPos
    << " le:" << edge.intervals[intervalIndex].length
    << " ep:" << edge.intervals[intervalIndex].edgePos << std::endl;
  */
  do {
    destVertex = edges[curEdge].dest;
    // Look through each in edge for the continuation of this read path
    nextIntervalIndex = -1;
    for (edgeOut = vertices[destVertex].FirstOut();
				 edgeOut < vertices[destVertex].EndOut();
				 edgeOut = vertices[destVertex].NextOut(edgeOut)) {
      nextEdge = vertices[destVertex].out[edgeOut];
      //  std::cout << "  search succ edge " << nextEdge << std::endl;
      nextIntervalIndex = GetSuccEdgeReadIntvIndex((*edges[curEdge].intervals)[intervalIndex],
																									 edges[nextEdge], vertexLength);
      if (nextIntervalIndex != -1) {
				path.push_back(nextEdge);
				break;
      }
    }
    curEdge = nextEdge;
    intervalIndex = nextIntervalIndex;
  }
  while (intervalIndex != -1 and edgeOut < 4);
  return path.size();
}

template<typename E>
int GetSuccEdgeReadIntvIndex(ReadInterval &intv, E &edge, int vertexLength) {
  int succIntvIndex;
  int intervalEnd;
	succIntvIndex = GetReadFirstIndex(intv.read, edge);
  if (succIntvIndex >= 0) {
    while ((*edge.intervals)[succIntvIndex].read == intv.read) {
      intervalEnd = intv.readPos + intv.length;
      if (intervalEnd - vertexLength == (*edge.intervals)[succIntvIndex].readPos )
				return succIntvIndex;
      succIntvIndex++;
    }
  }
  // no succeeding read index found
  return -1;
}



template<typename V, typename E>
	int StoreIntervalPathReverse(E &edge, int intervalIndex, 
															 std::vector<V> &vertices, std::vector<E> &edges,
															 int vertexLength, std::vector<int> &path) {
  /* 
     Given an edge and a read interval on that edge, search
     back for the entire length of the path.  Add each edge that gets traced back 
     to 'path'.
  */
  int edgeIn;
  int srcVertex;
  int prevEdge, curEdge;
  int prevIntervalIndex;
  curEdge = edge.index;
  if ((*edge.intervals)[intervalIndex].readPos == 0)
    return 0;

  do {
    srcVertex = edges[curEdge].src;
    prevIntervalIndex = -1;
    // Look through each in edge for the continuation of this read path
    for (edgeIn = vertices[srcVertex].FirstIn();
				 edgeIn < vertices[srcVertex].EndIn();
				 edgeIn = vertices[srcVertex].NextIn(edgeIn)) {
      prevEdge = vertices[srcVertex].in[edgeIn];
      prevIntervalIndex = GetPrecEdgeReadIntvIndex((*edges[curEdge].intervals)[intervalIndex],
																									 edges[prevEdge], vertexLength);
      if (prevIntervalIndex != -1) {
				path.push_back(prevEdge);
				break;
      }
    }
    curEdge = prevEdge;
    intervalIndex = prevIntervalIndex;
  }
  while (prevIntervalIndex != -1 and
				 edgeIn < 4 and
				 (*edges[curEdge].intervals)[intervalIndex].readPos != 0);
  return path.size();
}

template<typename E>
int GetPrecEdgeReadIntvIndex(ReadInterval &intv, E &edge, int vertexLength) {
  int precIntvIndex;
  int intervalEnd;
  //stopfn(intv.edgePos);
  precIntvIndex = GetReadFirstIndex(intv.read, edge);

  if (precIntvIndex >= 0) {
    while ((*edge.intervals)[precIntvIndex].read == intv.read) {
      intervalEnd = (*edge.intervals)[precIntvIndex].readPos + 
				(*edge.intervals)[precIntvIndex].length;
      if (intervalEnd - vertexLength == intv.readPos )
				return precIntvIndex;
      precIntvIndex++;
    }
  }
  // no preceeding read index found
  return -1;
}



int CountEdges(std::string &readIntervalFileName);

template<typename E>
int BinaryPrintReadIntervals(std::string &intervalFileName,
														 std::vector<E> &edges) {
	std::ifstream intvIn;
	openck(intervalFileName, intvIn, std::ios::in | std::ios::binary);
	int numEdges;
	intvIn.read((char*) &numEdges, sizeof(int));
	int e;
	for (e = 0; e < numEdges; e++) {
		intvIn.read((char*) &edges[e].length, sizeof(int));
		intvIn.read((char*) &edges[e].multiplicity, sizeof(int));
		(*edges[e].intervals).resize(edges[e].multiplicity);
		intvIn.read((char*) &((*edges[e].intervals)[0]), sizeof(ReadInterval) * edges[e].multiplicity);
	}
	return 1;
}


template<typename E>
int ReadReadIntervals(std::string &readIntervalFileName,
											std::vector<E> &edges) {
  std::ifstream readIntervalIn;
  openck(readIntervalFileName, readIntervalIn, std::ios::in);
  std::string word;
  int value;
  int curEdge = -1;
  int curIntv;
	int maxReadIndex = -1;
	char *linePtr;
	int read, readPos, length, edgePos;
	string line;
	int edgeLength, multiplicity;
  while(readIntervalIn) {
    if (! (readIntervalIn >> word)) break;
    if (word == "EDGE") {
			if (curEdge % 10000 ==9999) {
				std::cout << "edge: " << curEdge + 1 << std::endl;
			}
      ++curEdge;
      if (! (readIntervalIn >> value)) { 
				std::cout << "Should have edge number " << std::endl;
				exit(1);
      }
      if (! (readIntervalIn >> word)) {
				std::cout << "Should have 'Length' token " << std::endl;
				exit(1);
      }
      if (word != "Length" ){ 
				std::cout << "Badly formatted interval file " << word 
									<< " should be 'Length'"<<std::endl;
				exit(1);
      }
      if (!(readIntervalIn >> edges[curEdge].length)) {
				std::cout << "Should specify length " << std::endl;
				exit(1);
      }
      if (!(readIntervalIn >> word)) {
				std::cout << "Should have 'Multiplicity' token" << std::endl;
				exit(1);
      }
      if (word != "Multiplicity") {
				std::cout << "Badly formatted interval file " << word
									<< " should be 'Multiplicity'" << std::endl;
				exit(1);
      }
      readIntervalIn >> edges[curEdge].multiplicity;
      (*edges[curEdge].intervals).resize(edges[curEdge].multiplicity);
      curIntv = 0;
    }
    else if (word == "INTV") {
      assert(curIntv < edges[curEdge].multiplicity);
			int read, readPos, length, edgePos;
			readIntervalIn >> read;
			readIntervalIn >> readPos;
			readIntervalIn >> length;
			readIntervalIn >> edgePos;
      (*edges[curEdge].intervals)[curIntv].read = read;
			(*edges[curEdge].intervals)[curIntv].readPos = readPos;
			(*edges[curEdge].intervals)[curIntv].length = length;
			(*edges[curEdge].intervals)[curIntv].edgePos = edgePos;
			/*			
      readIntervalIn >> (*edges[curEdge].intervals)[curIntv].read
										 >> (*edges[curEdge].intervals)[curIntv].readPos 
										 >> (*edges[curEdge].intervals)[curIntv].length
										 >> (*edges[curEdge].intervals)[curIntv].edgePos;
			*/
			assert((*edges[curEdge].intervals)[curIntv].length >= 0);
			if ((*edges[curEdge].intervals)[curIntv].read > maxReadIndex)
				maxReadIndex = (*edges[curEdge].intervals)[curIntv].read;
      curIntv++;
    }
    else {
      std::cout << "Badly formatted interval file: " << word << std::endl;
      exit(1);
    }
  }
	if (maxReadIndex < 0) {
		std::cout << "Error, did not read in any paths, halting assembly." << std::endl;
		exit(0);
	}
	return maxReadIndex;
}

void SetReadPathIntervals(ReadIntervalList &readIntervals,
													PathIntervalList &paths,
													PathLengthList &pathLengths);

void PrintReadIntervals(ReadIntervalList &readIntervals,
												SimpleSequenceList &edges,
												std::vector<int> &mult,
												std::string &readIntervalFileName);

void StoreAllReadIntervals(ReadPositions &dbEdges, int dbEdgeSize, 
													 SimpleSequenceList &edges, 
													 std::string &sequenceListFileName, 
													 std::vector<BareReadIntervalList> &edgeReadIntervals,
													 PathIntervalList &paths,
													 std::vector<int> &pathLengths,
													 int skipGapped);

void StoreReadIntervals(EdgeIntervalListList &edgeIntervals,
												ReadIntervalList &readIntervals);

void StoreEdgeInterval(ReadPositions &overlaps, int overlapSize,
											 SimpleSequenceList &edges,
											 SimpleSequence &sequence,
											 EdgeIntervalList &edgeInterval,
											 std::vector<int> &mult);


void StoreEdgeIntervals(ReadPositions &overlaps, int ovelapSize,
												SimpleSequenceList &edges,
												SimpleSequenceList &sequences,
												EdgeIntervalListList &edgeIntervals,
												PathIntervalList &paths,
												PathLengthList &pathLengths,
												std::vector<int> &mult);

void EdgeToReadIntervals(EdgeIntervalListList &edgeIntervals,
												 ReadIntervalList &readIntervals);


void SortReadIntervalsByReadPos(ReadIntervalList &list);
void SortReadIntervals(ReadIntervalList &list);
void SortBareReadIntervalsByReadPos(BareReadIntervalList &list);
void SortBareReadIntervalsByEdgePos(BareReadIntervalList &list);
void SortReadIntervalIndicesByRead(BareReadIntervalList &list, 
																	 std::vector<int> &listIndices);


class CompareReadIntervalRead {
 public:
  int operator()(const ReadInterval &read1, const int readIndex) {
    return read1.read < readIndex;
  }
};


template<typename E>
int TraceReadIntervalReverse(E &curEdge, int readInterval, E &prevEdge, int vertexSize) {
  int prevIntvIndex;
  prevIntvIndex = GetReadFirstIndex((*(curEdge.intervals))[readInterval].read, prevEdge);
  if (prevIntvIndex == -1)
    return -1;

  // Now try and match the end of this segment of the read interval with 
  // the previous read interval.

  while (prevIntvIndex < (*prevEdge.intervals).size() and
				 (*prevEdge.intervals)[prevIntvIndex].read == (*curEdge.intervals)[readInterval].read) {
    if ((*prevEdge.intervals)[prevIntvIndex].readPos +
				(*prevEdge.intervals)[prevIntvIndex].length - vertexSize ==
				(*curEdge.intervals)[readInterval].readPos)
      return prevIntvIndex;
    ++prevIntvIndex;
  }
  return -1;
}

template<typename E>
int TraceReadIntervalForward(E &curEdge, int readInterval, E &nextEdge, int vertexSize) {
  int nextIntvIndex;
  /*
    std::cout << "tracing next " << readInterval << " " << curEdge.index  
    << " " << nextEdge.index << std::endl;
  */
	// Locate the first time this read occurs in this edge.  The read intervals
	// should be sorted according to read index on the edge, so if this read maps to the edge 
	// several times the intervals will all be contiguous.

  nextIntvIndex = GetReadFirstIndex((*curEdge.intervals)[readInterval].read, nextEdge);
  //  std::cout << " next: " << nextIntvIndex << std::endl;
  if (nextIntvIndex == -1)
    return -1;

  // Now try and match the end of this segment of the read interval with 
  // the nextious read interval.
  int curEnd = (*curEdge.intervals)[readInterval].readPos + 
    (*curEdge.intervals)[readInterval].length;
  while (nextIntvIndex < (*nextEdge.intervals).size() and
				 (*nextEdge.intervals)[nextIntvIndex].read == (*curEdge.intervals)[readInterval].read) {
    if (curEnd == (*nextEdge.intervals)[nextIntvIndex].readPos + vertexSize)
      return nextIntvIndex;
    ++nextIntvIndex;
  }
  return -1;
}

template<typename E>
int GetReadFirstIndex(int read, E &edge) {
  int first, last, cur;
  ReadIntervalList::iterator readPos;
  CompareReadIntervalRead comp;
  int readIndex;
  readPos = std::lower_bound(edge.intervals->begin(), edge.intervals->end(), read, comp);
  if (readPos == edge.intervals->end())
    return -1;
  else {
    readIndex = readPos - edge.intervals->begin();
    if ((*edge.intervals)[readIndex].read == read)
      return readIndex;
    else
      return -1;
  }
}


int  CountEdgeIntervals(EdgeIntervalListList &edgeIntervals);

class EdgeMappedReadPos : public ReadPos {
 public:
  int edge;
  int edgePos;
  EdgeMappedReadPos() : ReadPos() {
    edge = -1;
    edgePos  = -1;
  }
  friend std::ostream &operator<<(std::ostream &out, EdgeMappedReadPos &rp) {
    out << (ReadPos&) rp << " " << rp.edge << " " << rp.edgePos;
    return out;
  }
  friend std::istream &operator>>(std::istream &in, EdgeMappedReadPos &rp) {
    in >> (ReadPos&) rp >> rp.edge >> rp.edgePos;
    return in;
  }
};

typedef std::vector<EdgeMappedReadPos> EdgeMappedReadPositions;



#endif
