/***************************************************************************
 * Title:          EstimateErrorDistribution.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  12/07/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "utils.h"
#include "Spectrum.h"
#include "SimpleStats.h"
#include "IntegralTuple.h"
#include <cmath>
#include <math.h>
#include <map>
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;


float ExpPdf(float x,float lambda);
float Factorial(int f);
float GaussPdf(float mu, float simasq, float x);
float PoissonPdf(int k, float lambda);
int   FindDistribIntersection(float lambda, float mu, float sigmasq);

void PrintUsage() {
		std::cout << "usage: estErrorDist spectrumFile [-numSamples] " << std::endl
							<< " [-ig g] intial guess for separating exponential from gaussian" << std::endl
							<< " [-delta d]  convergence parameter d " << std::endl
				      << " [-printAll] Print all estimated parameters: exponential error, and mean and var. of gaussian correct."
              << std::endl;
		std::cout << " [-verbose]  Print lots of output" << std::endl;
		std::cout << " [-printCoverage] Print the estimated coverage." << std::endl;
		std::cout << " [-printHistogram] Print a histogram of word counts." << std::endl;
		std::cout << " [-binary] Read spectrum as binary" << std::endl;
}
int main(int argc, char* argv[]) {

	std::string spectrumFileName;
	if (argc < 2 ) {
		PrintUsage();
		exit(1);
	}

	spectrumFileName = argv[1];

	int numSamples = 100000;

	int argi = 2;
	int expMax = 2;
	float delta = 0.00001;
	int verbose = 0;
  int printAll = 0;
	int printHistogram = 0;
	int printCoverage = 0;
	int printMaxError = 1;
	int maxIts = 100;
	int binarySpectrum = 0;
	while (argi < argc) {
		if (strcmp(argv[argi], "-numSamples")== 0) {
			numSamples = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-maxIts")== 0) {
			maxIts = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-ig")== 0) {
			expMax = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-delta") == 0) {
			delta = atof(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-verbose") == 0) {
			verbose = 1;
		}
    else if (strcmp(argv[argi], "-printAll") == 0) {
      printAll = 1;
    }
		else if (strcmp(argv[argi], "-printHistogram") == 0) {
			printHistogram = 1;
		}
		else if (strcmp(argv[argi], "-printCoverage") == 0) {
			printCoverage = 1;
			printMaxError = 0;
		}
		else if (strcmp(argv[argi], "-binary") == 0) {
			binarySpectrum = 1;
		}
		else {
			PrintUsage();
			std::cout << "bad option: " << argv[argi] << std::endl;
			exit(1);
		}
		++argi;
	}

	std::ifstream spectrumIn;
	if (binarySpectrum == 0) 
		openck(spectrumFileName, spectrumIn);
	else 
		openck(spectrumFileName, spectrumIn, std::ios::in| std::ios::binary);
	

	int numTuples;
	if (binarySpectrum == 0) 
		spectrumIn >> numTuples;
	else {
		spectrumIn.read((char*) &numTuples, sizeof(int) );
	}

	std::string tuple;
	int mult;
	std::vector<int> spectMult;
	int i;
	int numGenerated = 0;
	std::map<int, int> histogram;

	// Simply read the entire spectrum.
	if (numTuples < numSamples) {
		//		spectrumIn.close();
		spectMult.resize(numTuples);
		if (binarySpectrum == 0) {
			i = 0;
			while(spectrumIn) {
				spectrumIn >> tuple >> spectMult[i];
				if (histogram.find(spectMult[i]) == histogram.end()) {
					histogram[spectMult[i]] = 1;
				}
				i++;
			}
		}
		else {
			CountedIntegralTuple countedTuple;
			for (i = 0; i < numTuples; i++) { 
				spectrumIn.read((char*) &countedTuple, sizeof(CountedIntegralTuple));
				spectMult[i] = countedTuple.count;
				if (histogram.find(spectMult[i]) == histogram.end()) {
					histogram[spectMult[i]] = 1;
				}
			}
		}
		numGenerated = numTuples;
	}
	else {
		//		SeedRandom();
		std::vector<int> sample;
		sample.resize(numSamples);
		int minIndex, maxIndex;
		float pSample;
		
		// Generate *roughly* numSamples indices.
		pSample = (numSamples * 1.0) / (numTuples);
		for (i = 0; i < numTuples and numGenerated < numSamples; i++) {
			if (Uniform() < pSample) { 
				sample[numGenerated] = i;
				numGenerated++;
			}
		}
		int curTuple = 0;
		std::string line;
		int mult;
		spectMult.resize(numGenerated);
		if (binarySpectrum == 0) {
			for (i = 0; i < numTuples and curTuple < numGenerated; i++) {
				while (sample[curTuple] > i) {
					std::getline(spectrumIn, line);
					i++;
				}
				spectrumIn >> tuple >> spectMult[curTuple];
				if (histogram.find(spectMult[curTuple]) == histogram.end()) {
					histogram[spectMult[curTuple]] = 1;
				}
				else {
					histogram[spectMult[curTuple]]++;
				}
				curTuple++;
			}
		}
		else {
			CountedIntegralTuple countedTuple;
			for (i = 0; i < numTuples and curTuple < numGenerated; i++) {
				while (sample[curTuple] > i) {
					spectrumIn.read((char*) &countedTuple, sizeof(CountedIntegralTuple));
					i++;
				}
				//spectrumIn >> tuple >> spectMult[curTuple];
				spectrumIn.read((char*) &countedTuple, sizeof(CountedIntegralTuple));
				spectMult[curTuple] = countedTuple.count;
				if (histogram.find(countedTuple.count) == histogram.end()) {
					histogram[countedTuple.count] = 1;
				}
				else {
					histogram[countedTuple.count]++;
				}
				curTuple++;
			}
		}
	}
	std::map<int,int>::iterator mapit;
	int begin = 99999999;
	int best  = -1;
	int bestIndex = -1;
	int second = -1;
	int prev = -1;
	int hump = 0;
	if (histogram.begin() != histogram.end()) {
		best = histogram.begin()->second;
		bestIndex = 1;
		prev = best;
	}
	if (printHistogram) {
		for (mapit = histogram.begin(); mapit != histogram.end(); ++mapit) {
			std::cout << mapit->first << " " << mapit->second << " " << best << std::endl;
			if (!hump and prev < mapit->second) {
				hump = 1;
			}
			if (! hump and best > mapit->second) {
				best = mapit->second;
				bestIndex = mapit->first;
			}
			prev = mapit->second;
		}
		std::cout << "best: " << bestIndex << std::endl;
	}
	//	exit(0);
	
	
	// Initial guesses.  The esponential process does not generate 
	// anything above 3, count the frequencies and means of 
	// exponential and gaussian process.

	
	int numExp, numGauss;
	int sumExp = 0; 
	int sumGauss = 0;
	long sumSq = 0;
	numExp = 0; numGauss = 0;
	for (i = 0; i < numGenerated; i++) {
		if (spectMult[i] <= expMax) {
			numExp++;
			sumExp += spectMult[i];
		}
		else {
			numGauss++;
			sumGauss += spectMult[i];
			sumSq += spectMult[i] * spectMult[i];
		}
	}
	int window[3];
	std::map<int,int>::iterator histEnd, histIt;;
	histEnd = histogram.end();
	int h = 0;
	if (verbose) {
		for (histIt = histogram.begin(); histIt != histEnd; histIt++) {
			std::cout << (*histIt).second << " ";
		}
		std::cout << std::endl;
	}

	if (numGauss == 0) {
		std::cout << 1 << std::endl;
		exit(0);
	}
	if (numExp == 0) {
		std::cout << 1 << std::endl;
		exit(0);
	}


	float expMean = (1.0*sumExp) /  numExp;
	float gaussMean = (1.0*sumGauss) / numGauss;
	float expLambda;
	float gaussVar, gaussStddev;
	//	std::cout << "expMean: " << expMean << std::endl;
	expLambda = 1/expMean;

	gaussVar =  sumSq / numGauss - gaussMean * gaussMean ;
	gaussStddev = sqrt(gaussVar);
	float mixGauss = (1.0*numGauss) / numGenerated;
	float mixExp   = 1.0 - mixGauss;

	int curIndex = 0;
	
	for (i = 0; i < numGenerated; i++) {
		if (spectMult[i] < (4*gaussStddev + gaussMean)) {
			spectMult[curIndex] = spectMult[i];
			curIndex++;
		}
	}

	numGenerated = curIndex;
	
	std::vector<float> pExp;
	pExp.resize(numGenerated);

	// Initialize the probablity of being 
	// generated by an error process.
	int numExcluded = 0;
	for (i = 0; i < numGenerated; i++) {
		if (spectMult[i] <= expMax) 
			pExp[i] = 1.0;
		else
			pExp[i] = 0.0;
	}

	if (verbose) {
		std::cout << "initialized to: ";
		std::cout << "0 " << expLambda << " " << gaussMean << " " << gaussVar 
							<< " " << mixExp << std::endl;
	}
	int converged = 0;

	float expLambdaHat, gaussVarHat, gaussMeanHat;

	float expResp;
	int iter = 1;
	float ex, ga;
	
	while (! converged ) {
		//
		// Compute expectations.
		//
		for (i = 0; i < numGenerated; i++) { 
			//			ex = PoissonPdf(spectMult[i], expLambda);
			ex = ExpPdf(spectMult[i],expLambda);
			ga = GaussPdf(gaussMean, gaussVar, spectMult[i]);
			if (pExp[i] >= 0) {
				if (((ga == 0) or 
						 (ga + ex == 0) or 
						 ((mixExp *ex + (1-mixExp) *ga) == 0)) and
						spectMult[i] > gaussMean) {
					// prevent some overflow conditions.
					pExp[i] = 0;
				}
				else {
					/*
					pExp[i] = (mixExp) * PoissonPdf(spectMult[i], expLambda)/ 
						((mixExp) * PoissonPdf(spectMult[i], expLambda) + 
						 (1 - mixExp) * GaussPdf(gaussMean, gaussVar, spectMult[i]));
					*/
					pExp[i] = (mixExp) * ExpPdf(spectMult[i], expLambda)/ 
						((mixExp) * ExpPdf(spectMult[i], expLambda) + 
						 (1 - mixExp) * GaussPdf(gaussMean, gaussVar, spectMult[i]));
					
					if (std::isnan(pExp[i])) {
						std::cout << "Unable to compute expectation for " << spectMult[i] <<  " " 
											<< mixExp << " " << expLambda<< std::endl;
						std::cout << ExpPdf(spectMult[i], expLambda) << " " 
											<<  GaussPdf(gaussMean, gaussVar, spectMult[i]) << " " << ex + ga << std::endl;
						std::cout << "Make sure you are running on the entire spectrum, including" << std::endl
											<< "frequency-1 tuples, since the exponential distribution uses these."
											<< std::endl;
						exit(1);
					}
				}
			}
			/*			std::cout << spectMult[i] << " " 
								<< ex << " " << ga << " " << " " << mixExp 
								<< " " <<  pExp[i] << std::endl;
			*/
		}

		//
		// Compute maximization.
		//
		expResp = 0;
		for (i = 0; i < numGenerated; i++) {
			if (pExp[i] >= 0) 
				expResp += pExp[i];
		}

		// exponential parameter

		expMean = 0;
		for (i = 0; i < numGenerated; i++) {
			expMean += pExp[i]*spectMult[i];
		}
		expLambdaHat = expResp / expMean;
		
		// gaussian parameter mu

		float sumGauss = 0;
		for (i = 0; i < numGenerated; i++) {
			sumGauss += (1-pExp[i]) * spectMult[i];
		}
		gaussMeanHat = sumGauss / (numGenerated - expResp);

		// gaussian parameter sigma squared

		float gaussSumSq = 0;
		for (i = 0; i < numGenerated; i++) {
			gaussSumSq += (1-pExp[i]) * (spectMult[i] - gaussMean)* (spectMult[i]-gaussMean);
		}
		gaussVarHat = gaussSumSq / (numGenerated - expResp);

		mixExp = expResp / numGenerated;
		if (verbose) {
			std::cout << iter << " " 
								<< expLambdaHat << " "
								<< gaussMeanHat << " "
								<< gaussVarHat << " "
								<< mixExp << std::endl;
		}
		if (iter == maxIts) 
			converged = 1;
		if (fabs(expLambda - expLambdaHat) < delta or
				fabs(gaussMean - gaussMeanHat) < delta or
				fabs(gaussVar - gaussVarHat) < delta) {
			converged = 1;
		}
		expLambda = expLambdaHat;
		gaussMean = gaussMeanHat;
		gaussVar  = gaussVarHat;
		++iter;
	}


	int threshIsect;
	threshIsect = FindDistribIntersection(expLambda, gaussMean, gaussVar);
	if (printMaxError) {
   	std::cout << threshIsect << std::endl;
  }
	else if (printCoverage) {
		std::cout << gaussMean << std::endl;
	}
	if (printAll) {
		std::cout << expLambda << " " << gaussMean << " " << gaussVar << " " << mixExp << std::endl;
	}
//	std::ofstream pExpOut;
//	pExpOut.open("pExpOut.txt");
//	for(i = 0;i < numGenerated; i++) {
//		pExpOut << pExp[i] << " " << spectMult[i] << std::endl;
//	}
//	pExpOut.close();
}


float Factorial(int f) {
	float res = f;
	f--;
	while (f > 1) {
		res *= f;
		f--;
	}
	return res;
}

float PoissonPdf(int k, float lambda) {
	if (k < 11)
		return exp(k*log(lambda))*exp(-lambda)/Factorial(k);
	else
		return 0.0;
}



float ExpPdf(float x,float lambda) {
	return lambda * exp(-x*lambda);
}

float GaussPdf(float mu, float sigmasq, float x) {
	float expval = exp(-((x - mu)*(x-mu))/(2*sigmasq));
	//	assert(expval != 0.0);
	return 1/sqrt(sigmasq*2*M_PI) * exp(-((x - mu)*(x-mu))/(2*sigmasq));
}

int FindDistribIntersection(float lambda, float mu, float sigmasq) {

	float a, b, c;

	a = 1;
	b = -2*mu-2*lambda*sigmasq;
	c = 2*sigmasq*log(lambda) + sigmasq*log(2*M_PI*sigmasq) + mu*mu;

	if (b*b - 4*a*c  < 0) {
		return -1;
	}

	else 
		return (int) floor( (-b - sqrt(b*b-4*a*c))/2*a);
}

