#ifndef PATH_BRANCH_H_
#define PATH_BRANCH_H_

#include <map>

class PathBranch {
 public:
	typedef std::map<int, PathBranch*> BranchMap;
	BranchMap branches;
	int count;
	PathBranch() {
		count = 0;
	}
};

void DeletePathTree(PathBranch &branch);
#endif
