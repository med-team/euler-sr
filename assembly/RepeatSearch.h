/***************************************************************************
 * Title:          RepeatSearch.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef FSDRTR_H_
#define FSDRTR_H_

int FindShortestDistanceBetweenTwoRepeats(TVertexList &vertices, TEdgeList &edges);
int SearchForRepeat(TVertexList &vertices, TEdgeList &edges, 
		    int curEdge, int curSearchLength, int maxSearchLength,
		    std::map<int, int> &distMap,
		    int &repeatEdge, int &len1, int &len2);


#endif
