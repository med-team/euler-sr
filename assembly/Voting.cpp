/***************************************************************************
 * Title:          Voting.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifdef _OPENMP
#include <omp.h>
#endif


#include "Voting.h"

int InitVotingMatrix(DNASequence &read, IntMatrix &votes) {
	if (votes.size() < read.length) {
		CreateMatrix(votes, read.length, 9);
	}
	else {
		int i, j;
		for (i = 0; i < votes.size(); i++ ){
			std::fill(votes[i].begin(), votes[i].end(), 0);
		}
	}
}

int InitSolidVector(DNASequence &read, IntVector &solid) {
	if (read.length > solid.size()) {
		solid.resize(read.length);
	}
	std::fill(solid.begin(), solid.end(), 0);
}

int PrepareSequence(DNASequence &read) {
	int p;
	for (p = 0; p < read.length; p++ ){ 
		read.seq[p] = toupper(read.seq[p]);
		if (!(read.seq[p] == 'A' ||
					read.seq[p] == 'C' ||
					read.seq[p] == 'T' || 
					read.seq[p] == 'G'))
			return 0;
	}
	return 1;
}


int SolidifySequence(DNASequence &read, 
										 CountedIntegralTupleDict &spectrum, int tupleSize, int minMult,
										 IntMatrix &votes, IntVector &solid,
										 int minVotes, Stats &stats, int numSearch, 
										 int DoDeletion, int DoInsertion, int earlyEnd) {
	
	int s;
	std::deque<int> history;
	int changeMade;
	int startPos, fixPos;
	fixPos = -1;
	int iter = 0;
	int numFixed = 0;
	do {
		//		std::cout << endl << endl << read.namestr << " iter: " << iter << std::endl;
		if (fixPos > 0)
			startPos = fixPos;
		else 
			startPos = 0;

		for (s = 1; s <= numSearch; s++) {
			InitVotingMatrix(read, votes);
			InitSolidVector(read, solid);
			VoteSequence(read, spectrum, tupleSize, minMult, startPos, 
									 votes, solid, s, 0, DoDeletion, DoInsertion, earlyEnd, history);
			//			PrintMatrix(votes, std::cout, 2);
			if (FixSequence(read, spectrum, votes, solid, tupleSize, 
											minMult, minVotes, stats, s, fixPos)) {
				++numFixed;
				return numFixed;
			}
		}
		//		std::cout << "fp: " << fixPos << std::endl;
		++iter;
	} while (fixPos > 0);
	return 0;
}

int CheckSolid(DNASequence &seq, CountedIntegralTupleDict &spectrum, int tupleSize, int minMult) {
	int p;
	CountedIntegralTuple tuple;
	int index;
	for (p = 0; p < seq.length - tupleSize +1; p++ ) {
		if (tuple.StringToTuple(&(seq.seq[p]))) {
			if (((index = spectrum.DictLookupBinaryTuple(tuple)) == -1) or
					minMult > spectrum.list[index].count ) {
				return 0;
			}
		}
		else {
			return 0;
		}
	}
	return 1;
}


int VoteHistory(IntMatrix &votes, std::deque<int> &history) {
	int histPos, histMut;
	std::deque<int>::iterator histIt;
	// 
	for (histIt = history.begin(); histIt != history.end(); histIt++) {
		histPos = *histIt;
		++histIt;
		histMut = *histIt;
		votes[histPos][unmasked_nuc_index[histMut]]++;
	}
}

int VoteSequence(DNASequence &seq, CountedIntegralTupleDict &spectrum, int tupleSize,
								 int minMult, int startPos,
								 IntMatrix &votes, IntVector &solid, 
								 int numSearch, int runFast,
								 int checkInsertions, int checkDeletions, int earlyEnd,
								 std::deque<int> &history) {
	DNASequence dnaseq;
	dnaseq.seq    = seq.seq;
	dnaseq.length = seq.length;
	dnaseq._ascii = 1;
	int p;
	CountedIntegralTuple tempTuple, mutatedTuple;
	int tupleIndex;
	int end;
	if (earlyEnd) {
		end=  min(seq.length - tupleSize + 1, earlyEnd - tupleSize + 1);
	}
	else {
		end = seq.length - tupleSize + 1;
	}
	static char nextNuc[256];

 	nextNuc['G'] = 'A';
	nextNuc['A'] = 'C';
	nextNuc['C'] = 'T';
	nextNuc['T'] = 'G';													
	nextNuc['g'] = 'A';
	nextNuc['a'] = 'C';
	nextNuc['c'] = 'T';
	nextNuc['t'] = 'G';
	int numCast = 0;
	for (p = 0; p < seq.length; p++) {
		assert(seq.seq[p] >= (int) 'A' and
					 seq.seq[p] <= (int) 'Z');
	}
	
	for (p = startPos; p < end; p++ ) {
		
		if (tempTuple.StringToTuple(&seq.seq[p])) {
			std::string tupleString, mutatedString;
			//			tempTuple.ToString(tupleString);
			
			if ((tupleIndex = spectrum.DictLookupBinaryTuple(tempTuple)) != -1 and
					spectrum.list[tupleIndex].count >= minMult ) {
			solid[p] = 1;
			}
			else {
				// Cast votes for mutations
				int vp;
				int nucIndex;
				char mutNuc;
				unsigned char un;
				int mut;
				int histPos, histMut;
				for (vp = 0; vp < tupleSize - 1; vp++) {
					mutNuc = nextNuc[seq.seq[p + vp]];
					char start = seq.seq[p + vp];
					MutateTuple(tempTuple, (unsigned char) numeric_nuc_index[mutNuc], vp, mutatedTuple);

					for (mut = 0; mut < 3; mut++ ) {
						if ((tupleIndex = spectrum.DictLookupBinaryTuple(mutatedTuple) != -1)) {
							if (spectrum.list[tupleIndex].count >= minMult) {
								votes[vp + p][unmasked_nuc_index[mutNuc]]++;
								++numCast;
							}
						}
						mutNuc = nextNuc[mutNuc];
						MutateTuple(tempTuple, numeric_nuc_index[mutNuc], vp, mutatedTuple);
					}
				}
			}
		}
	}
	return numCast;
}

int FixSequence(DNASequence &seq, 
								//								T_Spectrum &spectrum,
								CountedIntegralTupleDict &spectrum,
								IntMatrix &votes, IntVector &alreadySolid,
								int tupleSize, int minMult, int voteThreshold, Stats &stats, int numSearch,
								int &fixPos) {
	// numSearch is the number of mutations to search for to fix a read.
	
	// At first, no changes are made to the sequence
  fixPos = 0;
	int p, m;
	int numAboveThreshold = 0;
	int maxVotes = 0;
	int allGood  = 1;
	for (p = 0; p < seq.length - tupleSize + 1; p++ ) {
		if (alreadySolid[p] == 0) {
			allGood = 0;
			break;
		}
	}
	if (allGood) {
		// no need to fix this sequence
		/*		std::cout << "seq: " << seq.namestr << " is all good" << std::endl;*/
		return 1;
	}

	int s;
	//	PrintMatrix(votes, std::cout, 3);
	for (p = 0; p < votes.size(); p++) { 
		for (m = 0; m < votes[p].size(); m++) {
			if (votes[p][m] >= voteThreshold) 
				numAboveThreshold++;
			if (votes[p][m] >= maxVotes) {
				// Make room for the next vote
				maxVotes = votes[p][m];
			}
		}
	}
	
	//	std::cout << "max votes: " << maxVotes << std::endl;
	// Make sure there aren't multiple possible fixes
	std::vector<int> maxPos, maxMod;
	std::vector<int> tiePos;
	int numTies = -1;
	for (p = 0; p < votes.size(); p++) { 
		for (m = 0; m < votes[p].size(); m++) {
			if (votes[p][m] == maxVotes) {
				numTies++;
				maxPos.push_back(p);
				maxMod.push_back(m);
			}
		}
	}
	int mod, pos;
	
	if (numAboveThreshold > 0 ) {
		if (numTies < numSearch or 
				(maxPos.size() > 1 and maxPos[0] != maxPos[1])) {
			// Found at least one change to the sequence
			unsigned char prev, cur;
			for (s = 0; s < numSearch and s < maxPos.size(); s++) {
				mod = maxMod[s];
				pos = maxPos[s];
				fixPos = pos;
				if (mod < 4) {
					prev = seq.seq[pos];
					cur = nuc_char[mod];
					//					cout << "making modification " << seq.seq[pos] << " " << pos << " " << nuc_char[mod] << endl;
					seq.seq[pos] = nuc_char[mod];
					stats.numMut++;
				}
				else if (mod == 4) {
					int i;
					for (i = pos; i < seq.length; i++ ) {
						seq.seq[i] = seq.seq[i+1];
					}
					seq.length--;
					stats.numDel++;
				}
				else if (mod > 5) {
					seq.length++;
					unsigned char* newSeq = new unsigned char[seq.length];
					assert(pos > 0);
					memcpy(newSeq, seq.seq, pos);
					int i;
					for (i = pos + 1; i < seq.length; i++) {
						newSeq[i] = seq.seq[i-1];
					}
					newSeq[pos] = nuc_char[mod-5];
					delete[] seq.seq;
					seq.seq = newSeq;
					stats.numIns++;
				}
			}
			int solidRes = CheckSolid(seq, spectrum, tupleSize, minMult);

			return solidRes;
		} 
		else {
			// Bookkeeping for sequences that have ties.
			stats.numTies++;
			return 0;
		}
	}
	else {

		stats.numNotSolid++;
		return 0;
	}
}

int TrimSequence(DNASequence &seq, CountedIntegralTupleDict &spectrum,	int tupleSize, 
									int minMult, int &seqStart, int &seqEnd, int maxTrim, int printMap) {
	int i;
	seqStart = 0;
	//	typename T_Spectrum::TupleType tempTuple;
	CountedIntegralTuple tuple;
	int lastInvalid = -1;
	for (i = 0; (i < seq.length - tupleSize + 1) and (i < tupleSize) ; i++ ){ 
		if (nucToIndex[seq.seq[i]] >= 4)
			lastInvalid = i;
	}

	for (i = 0; i < seq.length - tupleSize + 1; i++ ){ 
		
		if ((i > lastInvalid) and
				tuple.StringToTuple(&seq.seq[i]) and
				spectrum.DictLookupBinaryTuple(tuple) != -1) {
			break;
		}
		if (nucToIndex[seq.seq[i+tupleSize-1]] >= 4)
			lastInvalid = i + tupleSize - 1;
		// Not solid yet, advance
		seqStart++;
	}
	seqEnd = seq.length;
	
	for (i = seqStart + 1; i < seq.length - tupleSize; i++ ) {
		if (nucToIndex[seq.seq[i+tupleSize-1]] >= 4) {
			// don't include the last tuple.
			i--;
			break;
		}
		if (tuple.StringToTuple(&seq.seq[i])) {
			if (spectrum.DictLookupBinaryTuple(tuple) == -1) {
				break;
			}
		}
	}

	// 
	// i is set to the position after the last solid tuple.  Since seqEnd - seqStart 
	// should be the length of the solid sequence this includes the length
	// of the tuple.
	//
	seqEnd = i + tupleSize;

	if (seqStart > maxTrim)
		return 0;
	
	if (seq.length - seqEnd > maxTrim)
		return 0;

	if (printMap) {
		std::stringstream mapstrm;
		mapstrm << " mapstart= "<< seqStart << " mapend=" << seqEnd;
		seq.namestr += mapstrm.str();
	}
	
	int s;
	int newLength = seqEnd - seqStart;
	for (s = 0; s < newLength; s++ ) {
		seq.seq[s] = seq.seq[s + seqStart];
	}
	seq.length = newLength;
	return 1;
}
	
int FindSolidSubsequence(DNASequence &seq, CountedIntegralTupleDict &spectrum, 
												 int tupleSize, int minMult, int &seqStart, int &seqEnd) {
	int i;
	int solidSubsequence = 1;
	CountedIntegralTuple tuple;
	for (i = seqStart; i < seqEnd - tupleSize + 1; i++) {
		if (!tuple.StringToTuple(&seq.seq[i]) or
				spectrum.DictLookupBinaryTuple(tuple) == -1) {
			solidSubsequence = 0;
			break;
		}
	}
	return solidSubsequence;
}
