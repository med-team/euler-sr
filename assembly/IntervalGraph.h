/***************************************************************************
 * Title:          IntervalGraph.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  12/07/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef BRANCHING_GRAPH_H_
#define BRANCHING_GRAPH_H_

#include "BVertex.h"
#include "BEdge.h"
#include "DeBruijnGraph.h"
#include "ReadIntervals.h"
#include "ReadPaths.h"
#include "SimpleSequence.h"
#include "graph/GraphAlgo.h"
#include "graph/MSTAlgo.h"
#include "ThreadPath.h"
#include "BinomialHeap.h"
#include <map>
#include <list>


using namespace std;

class TVertex : public BVertex {
 public:
  unsigned char toDelete : 1;
  unsigned char traversed: 1;

  // Ok, this is probably not the best coding style, but I
  // need another code for flagging, and I can probably have up to 32 flags
  // without changing the space requirements.

  unsigned char flagged2: 1;
  TVertex() : BVertex() { 
    marked  = GraphVertex::NotMarked;
    flagged = GraphVertex::NotMarked;
    flagged2 = GraphVertex::NotMarked;
  }
  char IsMarked() { return marked;}
  void Mark() {marked = GraphVertex::Marked;}
  void Unmark() {
    marked  = GraphVertex::NotMarked;
    flagged = GraphVertex::NotMarked;
    traversed = GraphVertex::NotMarked;
    flagged2 = GraphVertex::NotMarked;
  }
	int vertexSize;
};

class TEdge : public IntervalEdge {
 public:

  TEdge() : IntervalEdge() {
    marked       = GraphEdge::NotMarked;
    mst          = GraphAlgo::MSTOut;
    flagged      = GraphEdge::NotMarked;
    balPreferred = GraphEdge::NotMarked;
		guarded      = GraphEdge::NotMarked;
    // This is used to record if an edge is likely to be erroneous.
    suspect      = GraphEdge::NotMarked;
    // Sometimes it is desirable to mark an edge as preferred
    // so that if there are operations that should be done in order
    // to preserve the balancedness of a graph, the proper order may be maintained.
    // Normally this isn't a problem, but if there is a tie for preference in 
    // processing order, then use balPreferred to break the tie.
    // What that means that when the operation is first performed to the graph,
    // the balPreferred should be set (so that later when the balaned edge is processed
    // preference will be set.
    balPreferred = GraphEdge::NotMarked;
  }
	int IsShort(int minLength) {
		return (guarded == GraphEdge::NotMarked and
						length < minLength);
	}

	int IsLowCoverage(int coverage) {
		return (guarded == GraphEdge::NotMarked and
						(int) intervals->size() < coverage);
	}
	int CountReadsStartingInEdge() {
		unsigned int i;
		int numStarts = 0;
		for (i = 0; i < intervals->size(); i++) {
			if ((*intervals)[i].readPos == 0)
				numStarts++;
		}
		return numStarts;
	}
	
	int CountReadsCoveringEdge() {
		unsigned int i;
		int numCover = 0;
		for (i = 0; i < intervals->size(); i++) {
			if ((*intervals)[i].length == length) 
				numCover++;
		}
		return numCover;
	}

	void MarkIntervalForRemoval(int intervalIndex) {
		assert(intervalIndex < (int) intervals->size());
		if ((*intervals)[intervalIndex].markedForDeletion == 0) {
			multiplicity--;
			(*intervals)[intervalIndex].markedForDeletion = 1;
		}
	}

	int IsUnexpectedlyLowCoverage(float readsPerNucleotide, 
																float maxStdDev, 
																int absoluteCutoff,
																int acceptedLength) {
		if ((int) intervals->size() < absoluteCutoff)
			return 1;

		if (length >= acceptedLength) 
			return 0;
		
		if (intervals->size() > 6)
					return 0;
		float expIntervals;
		expIntervals = readsPerNucleotide * length;
		// Assume intervals are distributed as the sum of a Poisson 
		float stddevIntervals = sqrt(expIntervals * readsPerNucleotide);

		float normStdDev;
		int numCoveringReads; 
    numCoveringReads = intervals->size();

		normStdDev = (expIntervals - numCoveringReads) / stddevIntervals;
		if (normStdDev > maxStdDev) {
			/*		std::cout << index << " # intv " << numCoveringReads
				<< " length: " << length 
				<< " exp  " << expIntervals
				<< " stddev: " << normStdDev << std::endl;*/
			return 1;
		}
		else {
			/*			std::cout << "edge is OK! " << length<< " " 
							<< numCoveringReads << " " << expIntervals << std::endl;
			*/
			return 0;
		}
	}

  char IsMarked() {return marked;}
  void Mark() {marked = GraphEdge::Marked;}
  void Unmark() {
    marked       = GraphEdge::NotMarked;
    flagged      = GraphEdge::NotMarked;
    traversed    = GraphEdge::NotMarked;
    balPreferred = GraphEdge::NotMarked;
		guarded      = GraphEdge::NotMarked;
  }
  // This plus one more field inherited from GraphEdge
  // Will need to upgrade to an integer field soon
  unsigned char toDelete     : 1;
  unsigned char traversed    : 1;
  unsigned char balPreferred : 1;
  unsigned char suspect      : 1;
	unsigned char guarded      : 1;
  TEdge &operator=(TEdge edge) {
    IntervalEdge::operator=(edge);
    this->marked       = edge.marked;
    this->flagged      = edge.flagged;
    this->traversed    = edge.traversed;
    this->balPreferred = edge.balPreferred;
    this->suspect      = edge.suspect;
		this->guarded      = edge.guarded;
		return *this;
  }
	void Clear() {
		((IntervalEdge*)this)->Clear();
	}
};

typedef std::vector<TVertex> TVertexList;
typedef std::vector<TEdge> TEdgeList;



class IntervalGraph {
 public:
  TVertexList vertices;
  TEdgeList edges;
  int vertexSize;
	int containsIntervals;
  std::vector<SimpleSequence> *edgeSeqPtr;
  IntervalGraph() {
    edgeSeqPtr = NULL;
    vertexSize = -1;
    isBalanced = 1;
		containsIntervals = 1;
  }
	int GetBalancedPathIndex(int p) {
		if (p % 2 == 0) 
			return p+1;
		else 
			return p - 1;
	}
  void CalcDMST(); 
	void PathToThread(int p, ThreadPath &path);
  int FindAlternatePaths();

  int RemoveAllPathsThroughEdge(int edge);

  int RemovePath(std::vector<int> &pathEdges,
								 std::vector<int> &pathIndices);

  int TracePath(int curEdge, int curIndex, 
								std::vector<int> &pathEdges,
								std::vector<int> &pathIndices);

  void MergeSimplePath(int vertex, int toEdge, int fromEdge);

  void ReadIntervalGraph(std::string &bGraphFileName, 
												 std::string &intervalFileName,
												 std::string pathName = "", int skipIntervals = 0);
  
  void PrintIntervalGraph(std::string &bGraphName,
													std::string &intervalName);

  void RemoveAllSimpleBulges(int minBulgeSize);
  int RemoveSmallComponents(int size, 
														std::string &readsFile, std::string &componentReadsFile);

  void Erode(int minEdgeLength);
  void RemoveAllButMST();
	int CondenseSimplePaths();

  int TracePathReverse(int curEdge, int curIndex, int &prevEdge, int& prevIndex);
  int TracePathForwards(int curEdge, int curIndex, int &nextEdge, int& nextIndex);
	int RemoveEmptyEdges();
	int RemoveEmptyVertices();
  int RemoveLowCoverageEdges(float lowCoverageStddev, int absoluteCutoff);
  int FindShortContainedDAG(int sourceVertex, int maxPathLength, 
														std::set<int> &dagVertices,
														std::set<int> &dagEdges,
														std::vector<int> &shortestPath);

  void Unmark();
  void RemoveBalPreference();
  void Unflag();
  void Untraverse();
  void RemoveBalPreferred();
  void Unsuspect();
  void InitializeFlags();
  void IncrementOptimalPathCount(int source, 
																 std::vector<int> &sinks, 
																 std::vector<int> &edgeTraversalCount);

  int FindHighestMultiplicityPath(int begin, int end, std::vector<int> &path);
	int CalcEdgeMultiplicityStats(float &avgMultip);
  int MarkSuspectEdges(int polyNucLength);
  int RemoveSuspectBulges();
  void FindHighestScoringPathSourceToSink(int source, int sink, std::vector<int> &edgeTraversalCount);
  void TraceSourceToSinkPaths(std::vector<int> &edgeTraversalCount);
  void FindSourcesAndSinks(std::vector<int> &sources,
													 std::vector<int> &sinks);
	void RemoveBulges(int bulgeLength, int useDMST=1);
	void RemoveWhirls(int whirlLength);
	int StorePathForwards(int curEdge, int curIndex,
												std::vector<int> &pathEdges,
												std::vector<int> &pathIndices);

	int StorePathReverse(int curEdge, int curIndex,
											 std::vector<int> &pathEdges,
											 std::vector<int> &pathIndices);

	void DiscardGappedPaths();
	void RemoveMarkedPathIntervals();
	int  RemoveTruncatedPathIntervals();
	void GrowIntervalsOnSimplePath(int edgeIndex);
	int CollectSimplePathIntervals(int startEdge, int numNewIntervals);
	void MarkIntervalForRemoval(int edgeIndex, int intervalIndex) {
		assert(edgeIndex >= 0);
		assert(intervalIndex >=0);
		if (IsIntervalMarkedForRemoval(edgeIndex, intervalIndex))
			return;
		int readIndex =	(*edges[edgeIndex].intervals)[intervalIndex].read;
		int pathPos   =	(*edges[edgeIndex].intervals)[intervalIndex].pathPos;
		// Clear the path
		assert(pathPos < pathLengths[readIndex]);
		paths[readIndex][pathPos].edge = -1;
		paths[readIndex][pathPos].index = -1;
		
		// Clear the edge interval
		(*edges[edgeIndex].intervals)[intervalIndex].markedForDeletion = 1;
		edges[edgeIndex].multiplicity--;
	}
	PathLengthList pathLengths;
	PathIntervalList paths;

	unsigned int IsIntervalMarkedForRemoval(int edgeIndex, int intervalIndex) {
		if (edgeIndex >= 0 and intervalIndex >= 0)
			return (*edges[edgeIndex].intervals)[intervalIndex].markedForDeletion;
		else
			return 1;
	}
	int RemoveMarkedIntervals();
	int RemoveMarkedIntervalsNoPaths();
	void SetMaxReadIndex(int mri) {maxReadIndex=  mri;}
	int IsForwardRead(int read) {
		return (read % 2 == 0);
	}
	int GetCompReadIndex(int read) {
		if (read % 2 == 0) 
			return read + 1;
		else 
			return read - 1;
	}

	int RemovingEdgeCutsGraph(int e) {
		// The edge cuts the 
		/*
	std::cout << vertices[edges[e].src].OutDegree() << " "
						<< vertices[edges[e].src].InDegree() << " "
						<< vertices[edges[e].dest].OutDegree() << " "
						<< vertices[edges[e].dest].InDegree() << std::endl;
		*/
		return ((vertices[edges[e].src].OutDegree() == 1 and 
						 vertices[edges[e].src].InDegree() != 0) or
						(vertices[edges[e].dest].InDegree() == 1 and
						 vertices[edges[e].dest].OutDegree() != 0));
	}
	int CheckPathContinuity();
	int CheckAllPathsBalance(int fatal=0);
	int CheckAllPathsContinuity(int fatal=0);
	int CheckBalance();
	int CheckGraphStructureBalance();				
	int CheckPathContinuity(int p);
	int CheckPathBalance(int p, int balp);

	void ProtectEdges(std::string &protectedEdgeFileName,
										int edgeTupleLength);

  int SearchForDirectedCycle(int sourceVertex, int curVertex, std::set<int> &visitedEdges,
														 int curPathLength, int maxCycleLength);

  int SearchForUndirectedCycle2(int sourceVertex, int curVertex, std::set<int> &curPath, 
																int curPathLength, int maxCycleLength, int isUndirected,
																std::set<int> &visitedEdges, std::set<int> &visitedVertices);
	

  void SkipOutEdge(int vertex, int inEdge, int outEdge);
  void Prune(std::vector<int> &verticesToRemove,
						 std::vector<int> &edgesToRemove);

	int CountReadsContainedInEdge(int edge);
	int CountReadsPassingThroughEdge(int edge);
	int CountReadsExtendingIntoEdge(int edge, int limit);

	void RemoveLowPathEdges(int minPaths, int minExtend);

	int ReplacePathRangeForward(int readIndex,
															int intvEdge, int intvIndex,
															int pathStart, int pathEnd,
															std::vector<int> &newEdges);

	int ReplacePathRangeReverse(int readIndex,
															int intvEdge, int intvIndex,
															int pathStart, int pathEnd,
															std::vector<int> &newEdges);

	int ReplacePathRange(int readIndex, 
											 int intvEdge, int intvIndex,
											 int pathStart, int pathEnd,
											 std::vector<int> &newEdges);
	void PrintPath(int p, std::ostream &pathOut);
	void PrintPathReverse(int p, std::ostream &pathOut);
	void UpdateAllPathIndices();
	void UpdatePathIndices(int edge);
	int CalculateReadLength(int read);

	void RemoveEdgeAndMarkIntervalsForRemoval(int edge,
																						std::vector<int> &removedVertices);

	int MarkIntervalsOnPathForRemoval(int path);
  void RemoveEdge(int edge, std::vector<int> &removedVertices);
	void MarkPathForRemoval(int path);
	int IsEdgeInDisjoint(int edge, int minSpanningPaths);
	int IsEdgeOutDisjoint(int edge, int minSpanningPaths);
	int CutDisjointEdges(int minSpanningPaths);
  void DisconnectEdgesAtSource(std::vector<int> &edgeList);
	void DisconnectEdgesAtDest(std::vector<int> &edgeList);
	int isBalanced;
	int PathToSequence(vector<int> &path, SimpleSequence &seq);
	int ComputePathLength(vector<int> &path);
	int GetPathLength(int path);
	int ComputeIntervalPositions(list<int> &path, vector<int> &positions);
	int AlignmentBoundariesToPath(int *alignment, int alignmentLength,
																int start, int end,
																vector<int> &path, vector<int> &edgeStarts, 
																vector<int> &pathEdges, 
																vector<int> &pathEdgeStarts, vector<int> &pathEdgeLengths);
	int ListToVector(list<int> &l, vector<int> &v);
	int LookupAlignedPosition(int edge, int edgePos, int *alignment, int seqLength, 
														vector<int> &path,
														vector<int> &edgeStarts, int traversal);

	int LocateEdgeOnPath(int edge, list<int> &path);


	void MergeOutEdges(int vertex,
										 int toEdge, int fromEdge);

	void AppendIntervals(int toEdge, int fromEdge);
	int CountIntervalsOnSimplePath(int edge);
	void MoveIntervals(int toEdge, int fromEdge, int toEdgeIntvStartIndex, int lengthOffset);
	void MoveIntervals(int toEdge, int fromEdge);

	void MergeInEdges(int vertex,
										int toEdge, int fromEdge);

	void RemovePath(int path);
	void MarkPathIntervalsForRemoval(int path);
	void RemoveUnlinkedEdges();
	void ErodeShortEndIntervals(int minIntvLength);
	void RemoveErasedPaths();
	void CondenseEdgeLists();
	void SortAllEdgeIntervalsByReadPos();
	void Free();
	int TraceBackEdges(int startVertex, int endVertex, int backEdges[], 
										 std::vector<int> &path);
	// The maximum read index among all read intervals
	int ExtractMin(set<int> &keyQueue, map<int,int> &values, int &minKey, int &minValue);
	int maxReadIndex;
	int VertexMapToEdgePath(map<int,int> &vertexPaths, int startVertex,
													list<int> &edgePath, int dir);

	void AssignVertexSizes();

	int SearchTwoDirectedCycle(int redVertex, int blackVertex, int maxLength,
														 BinomialHeapNode<int,int>* redNodeList[],
														 BinomialHeapNode<int,int>* blackNodeList[],
														 int redDistList[],
														 int blackDistList[],
														 int redInv[], int blackInv[], int invocation,
														 int &minVertex, int &minCycleLength,
														 int redPath[], int blackPath[]);

	void AddOutEdgeLengths(int curVertex, int lengthToCurVertex, int maxLength,
												 BinomialHeap<int,int> &lengthPQueue,
												 BinomialHeapNode<int,int> *nodeRef[],
												 int invocations[], int inv,
												 int useFlagged, int path[]);

	int MarkPathRangeForRemoval(int pathIndex, int start, int end);

	int FindMinimalMatchedArrivalLength(map<int,int> &setA, map<int,int> &setB, 
																			int &minElement, int&minLength);
	int  DoesPathRepeat(int readIndex, int pathPos);
	void ProtectEdges(ReadPositions &protectedPositions, 
										SimpleSequenceList &protectedEdges, 
										int edgeTupleLength);

	int FormComplimentPath(std::vector<int> &origPath,
												 std::vector<int> &compPath);

						
	void PrintPaths(std::string pathFile);
	int NumForwardReads() {
		return ((maxReadIndex+1)/2);
	}
	void SetMultiplicities();

	void RemoveEdgeList(std::vector<int> &edgesToRemove);
	void RemoveVertexList(std::vector<int> &verticesToRemove);

	int MarkPathForRemoval(std::vector<int> &pathEdges,
												 std::vector<int> &pathIndices);

	void RemoveEdgeAndMarkPathsForRemoval(int edge,
																				std::vector<int> &removedVertices);

	void RemoveEdgeAndMarkIntervalsForRemoval(std::vector<int> &edgeList,
																						std::vector<int> &removedVertices);


	int MakeRoomForEdges(int readIndex, int intvEdge, int intvIndex,
											 int pathStart, int pathEnd,
											 std::vector<int> &newEdges);

	void CopyNewEdges(int readIndex, int intvEdge, int intvIndex,
										int pathStart, std::vector<int> &newEdges);
	void PrintImbalancedPaths(int p, int balp);
	void MarkPathsThroughEdgeForRemoval(int edgeIndex);
	void MarkIntervalsInEdgeForRemoval(std::vector<int> &edgeIndices);
	void MarkIntervalsInEdgeForRemoval(int edgeIndex);
	int RouteRemovedIntervals(int maxSearchLength);
	void SplicePathRange(int readIndex,
											 int spliceStart, int spliceEnd);
	int SearchAlternatePath(int curPathEdge, int nextPathEdge, 
													std::list<int> &altPathEdges,
													int maxSearchLength,
													int curPathLength = 0);

	int StoreAlternatePath(int curPathEdge, int nextPathEdge, 
												 std::vector<int> &altPathEdges,
												 int maxSearchLength);
	void DeleteReadInterval(int readIndex, int pathPos);
	void UntraverseReadIntervals();
	void AssignPathOrderToEdges();

	void AssignIntervalPathOrder();
	void AssignEdgesToIntervals();
	void SortAllEdgeIntervalsByEdgePos();
  void RemoveEdges(std::vector<int> &edgesToRemove, std::vector<int> &orphanedVertices);
  void MarkBalancedEdges();
	int RemoveBulgingEdges(int bulgeLength, int useDMST, int iter = 0);
	int SearchForUndirectedCycle(int sourceVertex, int cycleEndVertex, int maxCycleLength);

  template<typename Container>
		void FlagEdgeSet(Container &edgeSet) {
		std::set<int>::iterator edgeIt;
    for (edgeIt = edgeSet.begin(); edgeIt != edgeSet.end(); ++edgeIt) {
      edges[*edgeIt].flagged = GraphEdge::Marked;
    }
  }


  template<typename Container>
		void TraverseEdgeSet(Container &edgeSet) {
		std::set<int>::iterator edgeIt;
    for (edgeIt = edgeSet.begin(); edgeIt != edgeSet.end(); ++edgeIt) {
      edges[*edgeIt].traversed = GraphEdge::Marked;
      vertices[edges[*edgeIt].src].traversed = GraphVertex::Marked;
      vertices[edges[*edgeIt].dest].traversed = GraphVertex::Marked;
    }
  }

  template<typename Container>
		void Unflag2(Container &vertexSet) {
		std::set<int>::iterator vertexIt;
    for (vertexIt = vertexSet.begin(); vertexIt != vertexSet.end(); ++vertexIt) {
      vertices[*vertexIt].flagged2 = GraphVertex::NotMarked;
    }
  }
  template<typename Container>
		void UntraverseEdgeSet(Container &edgeSet) {
		std::set<int>::iterator edgeIt;
    for (edgeIt = edgeSet.begin(); edgeIt != edgeSet.end(); ++edgeIt) {
      edges[*edgeIt].flagged = GraphEdge::NotMarked;
      vertices[edges[*edgeIt].src].traversed = GraphVertex::NotMarked;
      vertices[edges[*edgeIt].dest].traversed = GraphVertex::NotMarked;
    }
  }
  int IsSetCyclic(std::set<int> &vertexSet, int curVertex, std::list<int> &path);
  int IsDAGSuspect(std::set<int> &dagEdges);
  int PruneDAG(std::set<int> &dagVertices, 
							 std::set<int> &dagEdges,
							 std::vector<int> &path,
							 std::vector<int> &verticesToDelete,
							 std::vector<int> &edgesToDelete);

  int IsEdgeSuspect(int e, int polyNucLength);
  void RerouteCompoundEdgeIntervals(int vertex, int inEdge, int outEdge);
  void ConcatenateEdgeSequence(int vertex, int inEdge, int outEdge);
  void RerouteSimplePathIntervals(int vertex, int inEdge, int outEdge);
  int  RemoveSimpleBulges(int minBulgeSize);
  int  GetOutEdgeIndex(int vertex, int edge);
  int  GetInEdgeIndex(int vertex, int edge);
  int  RemoveSimpleBulge(int vertex, int e1, int e2, int &edgeIndex);
  int  RemoveSimpleBulgeLowerMult(int vertex, int e1, int e2, int &edgeIndex);
  void MergeEdgeIntervals(int source, int dest);

  void ClearMST();

  int FindRouteToMaxPath(int curVertex, 
												 std::vector<int> &optPathTraversals, 
												 std::vector<int> &vertexPath);

  int FindVertexOnMaxPath(int curVertex, 
													std::list<int> &bestPath,
													std::list<int> &curPath, 
													std::vector<int> &optPathTraversals);

  int VertexOnMaxPath(int curVertex, std::vector<int> &optPathTraversals);
  int EdgeOnMaxPath(int edge, std::vector<int> &optPathTraversals);
  int VertexOnAlternatePath(int curVertex, std::vector<int> &optPathTraversals);
  int FindAlternatePaths(int sourceVertex, 
												 std::vector<int> &optPathTraversals, 
												 std::vector<int> &vertexPath,
												 std::vector<int> &verticesToDelete,
												 std::vector<int> &edgesToDelete);
	void DeleteEdgeReadIntervals(int edge);
	void DeleteEdgeListReadIntervals(std::vector<int> &edgeList);
	void UpdatePathEdges(std::vector<int> &edgesToRemove);
  int VertexSetContained(std::set<int> &dag, int source);
  void CollectVertices(int curVertex, int destVertex, std::set<int> &dagVertices);
  void CollectEdges(int lastVertex, std::set<int> &vertexSet, std::set<int> &edgeSet);
	int SearchForCycle(int sourceVertex, int prevVertex, int curVertex, int curPathLength, int maxPathLength,
										 std::string padding = "");
};

class RemoveAllButMSTFunctor {
 public:
  TVertexList *vertices;
  TEdgeList   *edges;
  std::vector<int> erodedVertexList;
  std::vector<int> erodedEdgeList;
  void operator()(int vertexIndex) {
    std::cout << "THIS ISN't IMPLEMENTED" << std::endl;
    exit(0);
    int edgeIndex, balancedIndex;
    int outEdgeIndex;
    for (outEdgeIndex = (*vertices)[vertexIndex].FirstOut();
				 outEdgeIndex < (*vertices)[vertexIndex].EndOut();
				 outEdgeIndex = (*vertices)[vertexIndex].NextOut(outEdgeIndex)) {
      edgeIndex = (*vertices)[vertexIndex].out[outEdgeIndex];
      balancedIndex = (*edges)[edgeIndex].balancedEdge;
      if ((*edges)[edgeIndex].mst != GraphAlgo::MSTIn) {
				assert((*edges)[balancedIndex].mst != GraphAlgo::MSTIn);
				erodedEdgeList.push_back(edgeIndex);
      }
    }
  }
};


class ErodeLeavesFunctor {
 public:
  TVertexList *vertices;
  TEdgeList   *edges;
	IntervalGraph *graph;
  std::vector<int> erodedVertexList;
  std::vector<int> erodedEdgeList;
  int minLength;
  void operator()(int vertexIndex);
  void Clear() {
    erodedVertexList.clear();
    erodedEdgeList.clear();
  }
};

void FormGraphFileNames(std::string &base, 
												std::string &bgraph, std::string &graph,
												std::string &intv, 
												std::string &path, std::string &edge);


void ReadIntervalGraph(std::string &base, 
											 IntervalGraph &graph, int &vertexSize, int skipIntervals = 0);


void WriteIntervalGraph(std::string &base,
												IntervalGraph &graph, int vertexSize = 1);



#endif
