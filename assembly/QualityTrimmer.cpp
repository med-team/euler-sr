/***************************************************************************
 * Title:          QualityTrimmer.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  12/09/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "SeqReader.h"
#include "DNASequence.h"
#include "utils.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <cmath>

using namespace std;
void PrintUsage() {
  cout << "usage: qualityTrimmer [-fasta inFastA -qual inQual][-fastq inFastQ] -outFasta out  " << std::endl;
  cout << "  -minQual qual   Use quality values in 'inQual' to trim the 'inFasta'" << std::endl
				<< " file. " << std::endl;
  cout << "  -span span      Trim off any region that does not average 'qual' " << std::endl
				<< "                  over a span of 'span' " << std::endl; 
	cout << "  -maxTrim trim   If more than 'trim' are removed, then discard the read." << std::endl;
	cout << "  -type    illumina|sanger  Use either Illumina or Sanger scaling (Sanger=none)"<<endl
 			 << "                  of quality values." << endl;
	cout << "  -trimFront N    Automatically trim N bases from the front." << endl;
	cout << "  -trimEnd   N    Automatically trim N bases from the end." << endl;
}


int ReadQualityValues(std::istream &in, std::string &qualTitle, std::vector<int> &qualValues) {
  std::string line;
  if (!in ) {
    return 0;
  }
  if (in.peek() == '>') {
    in.get();
    in >> qualTitle;
    std::getline(in, line);
  }
  qualValues.clear();
  int qualValue;
  
  while (in and in.peek() != '>') {
    std::getline(in, line);
    std::stringstream lineStrm(line);
    // Parse the line and append values in it
    while (lineStrm >> qualValue) { 
      qualValues.push_back(qualValue);
      qualValue = -1;
    }
    if (in.eof()) 
      return 1;
    line = "";
  }
  return 1;
}

int TrimRead(DNASequence &origRead, std::vector<int> &qualValues,
						 int minQual, int span, int minFrontTrim, int minEndTrim,
						 DNASequence &newRead, int &trimFront, int &trimEnd) {
  // copy the title for completeness 
	//  newRead.namestr = origRead.namestr;
  int p;
  int s;
  float totQual;
  int firstSolidPos, lastSolidPos;
	//  DNASequence maskedSeq;
	//  maskedSeq = origRead;
  firstSolidPos = -1;
  lastSolidPos  = -1;
  int qualSpan;
	int sum;
	int p1;
  // Find the first valid start position (this excludes parts
  // that just have 1 or 2 masked nucleotides)
  int start, end;
  for (start = minFrontTrim; start < origRead.length; start++) {
				if (qualValues[start] >=minQual) break;
  }
  for (end = origRead.length-1+ minEndTrim; end >= 0; end--) {
      if (qualValues[end] >= minQual) break;
  }
	for (p = 0; p < origRead.length - 1 - span; p++) {
		sum = 0;
		for (p1 = 0; p1 < span; p1++) {
			sum += qualValues[p+p1];
		}
		qualValues[p] = sum / span;
	}
	firstSolidPos = -1; lastSolidPos = -1;
	for (p = start; p < origRead.length; p++ ) {
		if (qualValues[p] >= minQual) {
			firstSolidPos = p;
			break;
		}
	}
	for (p = end; p > firstSolidPos; p--) {
		if (qualValues[p] > minQual) {
			lastSolidPos = p;
			break;
		}
	}
	if (lastSolidPos != (origRead.length - 1))
		lastSolidPos -= span;


	
  // Now produce new sequence that has the bad ends of the read trimmed off
  if (firstSolidPos == -1 or lastSolidPos == -1 or firstSolidPos >= lastSolidPos) {
    // The read is just plain bad, exit now
    return 0;
  }
  else {
    int trimmedLength;
    trimmedLength = lastSolidPos - firstSolidPos;
		trimFront = firstSolidPos;
		trimEnd   = lastSolidPos;
		newRead._ascii = 1;
		newRead.seq    = origRead.seq + trimFront;
		newRead.length = trimEnd - trimFront + 1;
    return 1;
  }
}

int main(int argc, char* argv[]) {
  
  std::string seqName, qualName, outName, qualType;
  int minQual, span;
  minQual = 12, span=10;

  if (argc < 4) {
    PrintUsage();
    exit(0);
  }
  int argi;
  argi = 1;
  int maxTrim = 0;
	int fileType;
	int fastA = 1;
	int fastQ = 2;
	seqName  = "";
	qualName = "";
	outName  = "";
	qualType = "";
	int minTrimFront = 0;
	int minTrimEnd   = 0;
  while (argi < argc) {
		if (strcmp(argv[argi], "-fasta") == 0) {
			seqName = argv[++argi];
			fileType = fastA;
		}
		else if (strcmp(argv[argi], "-qual") == 0) {
			qualName = argv[++argi];
		}
		else if (strcmp(argv[argi], "-outFasta") == 0) {
			outName = argv[++argi];
		}
		else if (strcmp(argv[argi], "-fastq") == 0) {
			seqName = argv[++argi];
			fileType = fastQ;
		}
    else if (strcmp(argv[argi], "-minQual") == 0) {
      minQual = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-span") == 0) {
      span = atoi(argv[++argi]);
    }
		else if (strcmp(argv[argi], "-maxTrim") == 0) {
			maxTrim = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-type") == 0) {
			qualType = argv[++argi];
			if (qualType != "illumina" and
					qualType != "sanger") {
				PrintUsage();
				cout << "Type '"<< qualType << "' must be either 'illumina' or 'sanger'" << endl;
				exit(1);
			}
		}
		else if (strcmp(argv[argi], "-trimFront") == 0) {
			minTrimFront = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-trimEnd") == 0) {
			minTrimEnd = -atoi(argv[++argi]);
		}
    else {
      PrintUsage();
			std::cout << "bad option: " << argv[argi] << std::endl;
      exit(1);
    }
    ++argi;
  }
	if (seqName == outName) {
		std::cout << "Error! input and output file names are the same." << std::endl;
		exit(0);
	}
	if (seqName == ""|| outName == "") {
		PrintUsage();
		std::cout << "Missing input or output name." << std::endl;
		exit(0);
	}
	if (qualName == "" and fileType == fastA) {
		std::cout << "You must specify a quality file if the input is FASTA" << std::endl;
		exit(0);
	}

  std::ofstream seqOut;
  openck(outName, seqOut, std::ios::out);

	
  std::ifstream qualIn;
	if (fileType == fastA)
		openck(qualName, qualIn, std::ios::in);

  std::ifstream seqIn;
  openck(seqName, seqIn, std::ios::in);
	std::vector<int> qualValues;
	//	int *qualValuesPtr;
	//	int qualValuesLength, qualValuesBufLength;
	//	qualValuesBufLength = 0;
	//	qualValuesLength = 0;
	//  std::vector<int> qualValues;
  std::string qualTitle;
  DNASequence seq, trimmedSeq;
  int nDiscards;
  int totalReads;
  nDiscards = totalReads = 0;
  int amountTrimmed;
  amountTrimmed = 0;
  int nReads = 0;
	seq._masked = 0;
	trimmedSeq._masked = 0;

	int illmnQualScale[256];
	int i;
	for (i = 0; i < 256; i++) {
		illmnQualScale[i] = (int) 10*log(1+ std::pow(10, ((double)i-64)/10.0))/log(10) + 0.499;
	}

  while (true) {
		if (fileType == fastA) {
			if (!SeqReader::GetSeq(seqIn, seq, SeqReader::noConvert))
				break;
			if (!ReadQualityValues(qualIn, qualTitle, qualValues)) {
				std::cout << "error reading quality values for sequence " << seq.namestr << std::endl;
				return 1;
			}
			/*
			if (qualValues.size() != seq.length) {
				std::cout << "error, there are " << qualValues.size() << " values for " << qualTitle
									<< " but the read " << seq.namestr << " is of length " << seq.length << std::endl;
				return 1;
			}
			*/
		}
		else if (fileType == fastQ) {
			std::string title, seqString, qualString, qualTitle;
			// Get rid of '@'
			if (seqIn.get() == EOF)
				break;
			// Read the title.
			if (!std::getline(seqIn, title)) break;
			// discard newline
			if (seqIn.get() == EOF) break;
			// Read the sequence.
			if (!std::getline(seqIn, seqString)) break;
			
			//			seq.Reset(seqString.size());
			if (seq.length < seqString.size()) {
				seq.Reset(seqString.size());
			}
			seq.length = seqString.size();
			memcpy(seq.seq, seqString.c_str(), seqString.size());
			seq.namestr = title.substr(0);
			// Discard the quality title.
			if (!std::getline(seqIn, qualTitle)) break;
			// Read the quality string.
			if (!std::getline(seqIn, qualString)) break;
			
      if (qualValues.size() < qualString.size()) {
				qualValues.resize(qualString.size());
			}
			int qualValuesLength = qualString.size();
			int i;
			//			int qualLength = qualValues.size();
			char * qualPtr = (char*) qualString.c_str();

			// Guess the fastq type: illumina or sanger
			int sanger = 1;
			int illumina = 2;
			int type = sanger;
			
			// Guess the quality type if necessary
			if (qualType == "") {
				for (i = 0; i < qualValuesLength; i++ ){
					if (qualPtr[i] > 74) {
						type = illumina;
						break;
					}
				}
			}
			else if (qualType == "illumina") {
				type = illumina;
			}
			for (i = 0; i < qualValuesLength; i++ ){
				if (type == illumina) {
					qualValues[i] =  illmnQualScale[qualPtr[i]]; //10*log(1+ std::pow(10, (qualPtr[i]-64)/10.0))/log(10) + 0.499;
				}
				else {
					qualValues[i] = qualPtr[i] - 33;
				}
			}

			//			std::cout << std::endl;
		}
		PrintStatus(nReads, 100000);
    nReads++;
    totalReads++;
		//    std::cout << "trimming " << seq.namestr << std::endl;

		int trimFront, trimEnd;
		trimFront = 0;
		trimEnd = 0;
		
    if (TrimRead(seq, qualValues, minQual, span, minTrimFront, minTrimEnd, 
								 trimmedSeq, trimFront, trimEnd)) {
			std::stringstream trimKW;
			if (maxTrim == 0 or (seq.length - trimEnd) + trimFront < maxTrim) {
				/*if (trimFront >= 0 or trimEnd >= 0) {
					trimKW << " qualFront=" << trimFront << " qualEnd=" << trimEnd;
					trimmedSeq.namestr += trimKW.str();
				}
				*/
				//seq.PrintSeq(seqOut);
				seqOut << ">" << seq.namestr << endl;
				trimmedSeq.PrintSeq(seqOut);
				seqOut << std::endl;
				amountTrimmed += seq.length - trimmedSeq.length;
			}
			else if (maxTrim > 0 and trimEnd + trimFront > maxTrim) {
				++nDiscards;
			}
    }
    else {
      ++nDiscards;
    }
  }
  std::cout << std::endl;
  std::cout << "discarded a total of " << nDiscards << " / " << totalReads << std::endl;
  std::cout << "trimmed a total of " << amountTrimmed << " nucleotides " << std::endl;
  return 0;
}
