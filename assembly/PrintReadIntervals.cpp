/***************************************************************************
 * Title:          PrintReadIntervals.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  09/16/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <iostream>
#include <string>
#include <vector>
#include "DeBruijnGraph.h"
#include "ReadIntervals.h"
#include "SeqReader.h"
#include "SeqUtils.h"
#include "SimpleSequence.h"
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;




void PrintUsage() {
  std::cout << "usage: printReadIntervals overlapFileName edgeFile sequences "
	    << "vertexSize intervalFileName pathFileName" << std::endl;
}

int main(int argc, char* argv[] ) {

  std::string overlapFileName;
  std::string edgeFileName;
  std::string sequenceListFileName;
  std::string readIntervalFileName;
  std::string intervalFileName;
	std::string pathFileName;
  int vertexSize, overlapSize;

  if (argc < 5) {
    PrintUsage();
    exit(1);
  }

  int argi = 1;
  overlapFileName = argv[argi++];
  edgeFileName       = argv[argi++];
  sequenceListFileName = argv[argi++];
  vertexSize = atoi(argv[argi++]);
  readIntervalFileName = argv[argi++];
	pathFileName = argv[argi++];
  overlapSize = vertexSize + 1;
	
	std::string reportFileName = sequenceListFileName + ".report";
	std::ofstream reportFile;
	openck(reportFileName, reportFile, std::ios::app);
	BeginReport(argc, argv, reportFile);
  SimpleSequenceList sequences, edges;
  // Get the edges (don't care about their reverse complement, since it should be here
  ReadSimpleSequences(edgeFileName, edges);
	
	// Try skipping these for now... it may be faster
	//  ReadSimpleSequences(sequenceListFileName, sequences);
	//  AppendReverseComplements(sequences);
  
  // Read in the vertex list
  std::ifstream overlapIn;
  ReadPositions overlaps;
  openck(overlapFileName, overlapIn, std::ios::in);
  overlapIn >> overlaps;
  overlapIn.close();

	//  std::vector<int> mult;
	//  EdgeIntervalListList edgeIntervals;
	//  EdgeIntervalList::tupleSize = overlapSize;
	PathIntervalList paths;
	PathLengthList pathLengths;
	std::vector<BareReadIntervalList> edgeReadIntervals;
	//	std::vector<BareReadIntervalIndexList> edgeReadIntervalIndices;
	edgeReadIntervals.resize(edges.size());

	std::cout << "storing read intervals. " << std::endl;
	int skipGapped = 1;
	StoreAllReadIntervals(overlaps, overlapSize, edges, sequenceListFileName, 
												edgeReadIntervals, paths, pathLengths, skipGapped);
	
	std::cout << "sorting edge read interval lists." << edgeReadIntervals.size() << endl;
	int e, i;
	for (e = 0; e < edgeReadIntervals.size(); e++) {
		SortBareReadIntervalsByReadPos(edgeReadIntervals[e]);
	}
	std::cout << "done, printing\n";
  std::ofstream readIntervalOut;
  openck(readIntervalFileName, readIntervalOut, std::ios::out);

  int edgeIndex;
  int intervalIndex;
  intervalIndex = 0;
	int ri;
  for (edgeIndex = 0; edgeIndex < edges.size();edgeIndex++) {
    readIntervalOut << "EDGE " << edgeIndex 
										<< " Length " << edges[edgeIndex].length
										<< " Multiplicity " << edgeReadIntervals[edgeIndex].size()
										<< std::endl;
		cout << edgeReadIntervals[edgeIndex].size() << endl;
		for (ri = 0; ri < edgeReadIntervals[edgeIndex].size(); ri++) {
      readIntervalOut << "INTV " << edgeReadIntervals[edgeIndex][ri].read 
											<< " " << edgeReadIntervals[edgeIndex][ri].readPos 
											<< " " << edgeReadIntervals[edgeIndex][ri].length
											<< " " << edgeReadIntervals[edgeIndex][ri].edgePos << std::endl;
    }
  }
  readIntervalOut.close();
	std::vector<std::vector<int> > edgeIntervalIndices;
	edgeIntervalIndices.resize(edgeReadIntervals.size());
	for (e = 0; e < edgeReadIntervals.size(); e++) {
		SortBareReadIntervalsByEdgePos(edgeReadIntervals[e]);
		edgeIntervalIndices[e].resize(edgeReadIntervals[e].size());
		for (i = 0; i < edgeIntervalIndices[e].size(); i++ ) {
			edgeIntervalIndices[e][i] = i;
		}
		SortReadIntervalIndicesByRead(edgeReadIntervals[e], edgeIntervalIndices[e]);
	}

	PathReadPosToIntervalIndex(edgeReadIntervals, edgeIntervalIndices, paths, pathLengths);
	WriteReadPaths(pathFileName, paths, pathLengths);
	EndReport(reportFile);
	return 0;
}


