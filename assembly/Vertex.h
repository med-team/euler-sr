#ifndef VERTEX_H_
#define VERTEX_H_

#define ALPHABET_SIZE 4
#include "ReadPos.h"

class Vertex : public ReadPos {
 public:
  int out[ALPHABET_SIZE];
  int in[ALPHABET_SIZE];
  int FirstOut() {
    int e;
    for (e = 0; e < ALPHABET_SIZE; e++ ) {
      if (out[e] >= 0) 
				return out[e];
    }
  }
  int FirstOutIndex() {
    int e;
    for (e = 0; e < ALPHABET_SIZE; e++) 
      if (out[e] >= 0)
				return e;
    return e;
  }
  int FirstInIndex() {
    int e;
    for (e = 0; e < ALPHABET_SIZE; e++) 
      if (in[e] >= 0)
				return e;
    return e;
  }

  int FirstIn() {
    int e;
    for (e = 0; e < ALPHABET_SIZE; e++) 
      if (in[e] >= 0)
				return in[e];
  }
    
  int InDegree() {
    int e, d;
    d = 0;
    for (e = 0; e < ALPHABET_SIZE; e++ )
      (in[e] >= 0) and d++;
    return d;
  }

  int OutDegree() {
    int e, d;
    d = 0;
    for (e = 0; e < ALPHABET_SIZE; e++ )
      (out[e] >= 0) and d++;
    return d;
  }

  int Singleton() {
		int totalDegree = 0;
		int i,o;
		for (o = 0; o < ALPHABET_SIZE; o++ ) {
			(out[o] != -1) ? totalDegree++ : totalDegree;
		}
		for (i = 0; i < ALPHABET_SIZE; i++) {
			(in[i] != -1) ? totalDegree++ : totalDegree;
		}
		return totalDegree == 0;
  }

  int IsBranch() {
    return (InDegree() != 1 or OutDegree() != 1);
  }

  int DegreeOneOutEdge() {
    // if there is a singe out edge, return the vertex that it goes to
    int e, e1, d;
    e1 = -1;
    d  = 0;
    for (e = 0; e < ALPHABET_SIZE ; e++ ) {
      if (out[e] >= 0) {
				e1 = e;
				d++;
      }
    }
    if (d == 1) 
      return e1;
    else
      return -1;
  }
    
  Vertex &operator=(const Vertex& val) {
		out[0] = val.out[0]; 
		out[1] = val.out[1];
		out[2] = val.out[2];
		out[3] = val.out[3];
		in[0]  = val.in[0];
		in[1]  = val.in[1];
		in[2]  = val.in[2];
		in[3]  = val.in[3];
    pos = val.pos;
    read = val.read;
    return *this;
  }
  Vertex() : ReadPos(-1,-1) {
    out[0] = out[1] = out[2] = out[3] = -1;
    in[0] = in[1] = in[2] = in[3] = -1;
  }
};


typedef std::vector<Vertex> VertexList;

#endif
