/***************************************************************************
 * Title:          SortIntegralTupleList.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <iostream>
#include "IntegralTuple.h"

#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;

void PrintUsage() {
	std::cout << "usage: sortIntegralTupleList tupleList" << std::endl;
	std::cout << "     -printCount   Both read in and print out the count of each tuple" << std::endl;
	std::cout << "     -minMult M    Discards tuples that occur less than M times." << std::endl
						<< std::endl;
	exit(0);
}

int main(int argc, char *argv[]) {
	
	std::string tupleListName;
	int argi = 1;
	if (argc < 2) {
		PrintUsage();
	}
	tupleListName = argv[argi++];
	int readCount = 0;
	int minMult   = 0;
	while(argi < argc) {
		if (strcmp(argv[argi], "-printCount") == 0) {
			readCount = 1;
		}
		else if (strcmp(argv[argi], "-minMult") == 0) {
			minMult = atoi(argv[++argi]);
		}
		else {
			PrintUsage();
			std::cout << "bad option: " << argv[argi] << std::endl;
		}

		argi++;
	}
	
	std::ifstream listIn;
	std::ofstream listOut;
	openck(tupleListName, listIn, std::ios::in | std::ios::binary);
	int nTuples;
	//	listIn.read((char*) &nTuples, sizeof(int));

	if (readCount) {
		CountedIntegralTuple* tupleList;
		/*		CountedIntegralTuple *tupleList = new CountedIntegralTuple[nTuples];
		listIn.read((char*) tupleList, 
		sizeof(CountedIntegralTuple)*nTuples);

		listIn.close();
		*/
		ReadBinaryTupleList(tupleListName, &tupleList, nTuples, minMult);

		std::sort(tupleList, tupleList + nTuples);
		openck(tupleListName, listOut, std::ios::out | std::ios::binary);
		listOut.write((const char*) &nTuples, sizeof(int));
		listOut.write((const char*) tupleList, sizeof(CountedIntegralTuple)*nTuples);
	}
	else {
		IntegralTuple *tupleList;
		ReadBinaryTupleList(tupleListName, &tupleList, nTuples);
		std::sort(tupleList, tupleList + nTuples);
		openck(tupleListName, listOut, std::ios::out | std::ios::binary);
		listOut.write((const char*) &nTuples, sizeof(int));
		listOut.write((const char*) tupleList, sizeof(IntegralTuple)*nTuples);
	}
	return 0;
}
