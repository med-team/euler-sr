/***************************************************************************
 * Title:          HashedSpectrum.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/05/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef HASHED_SPECTRUM_H_
#define HASHED_SPECTRUM_H_

#include "Spectrum.h"
#include "ReadPos.h"
#include "SimpleSequence.h"
#include "PagedList.h"
#include "hash/PagedHashTable.h"

class HashValueFunctor {
public:
  int hashLength;
  SimpleSequenceList *sequences;

  int MaxHashValue() {
    return 1 << (hashLength*2);
  }
	int operator=(HashValueFunctor &hashFunction) {
		hashLength = hashFunction.hashLength;
		sequences  = hashFunction.sequences;
	}
  int operator()(CountedReadPos &p) {
    int hashValue = 0;
    int i;
    int nucValue;
    for (i = 0; i < hashLength; i++ ) {
      nucValue = unmasked_nuc_index[(*sequences)[p.read].seq[p.pos + i]];
      if ( nucValue < 4) {
				hashValue <<=2;
				hashValue += nucValue;
      }
      else {
				return -1;
      }
    }
    return hashValue;
  }
};


typedef PagedHashTable<CountedReadPos, 15, HashValueFunctor, UpdateFunctor> ReadPosHashTable;


class HashedSpectrum  {
 public:

	int hashLength;
	HashedSpectrum(HashValueFunctor &calcHashValue);
	HashValueFunctor hashFunction;
	ReadPosHashTable hashTable;
	int SetHashSize(int size) {
		HashedSpectrum::hashLength = size;
	}
	
	
	int HashSequence(SimpleSequenceList &seqListPtr, int curSeq,
									 int trimFront = 0,
									 int trimEnd   = 0);

	void StoreSpectrum(SimpleSequenceList &seqList, 
										 int trimFront = 0, int trimEnd   = 0);

	void HashToReadPositionList(SimpleSequenceList &sequences,
															ReadPositions &readPos );
};


#endif
