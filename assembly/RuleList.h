#ifndef RULE_LIST_H_
#define RULE_LIST_H_


#include <string>
#include <vector>
#include <regex.h>
using namespace std;
class Rule {
public:
	std::string regexpStr;
	int cloneLength;
	int cloneVar;
	int type;
	string forward, reverse;
	regex_t compRegex;
};


typedef std::vector<Rule> RuleList;

void ParseRuleFile(std::string &ruleFile, std::vector<Rule> &rules);

int GetReadRule(RuleList &rules, std::string &readTitle, int &readType);

#endif
