/***************************************************************************
 * Title:          Tuple.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/16/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef TUPLE_H_
#define TUPLE_H_

#include "DNASequence.h"
#include "SeqUtils.h"

#include <sstream>
class Tuple : public std::string {
 public:
	static int tupleSize;
	int Valid() {
		int i;
		for ( i = 0; i < this->size(); i++) {
			if (numeric_nuc_index[(*this)[i]] >= 4)
				return 0;
		}
		return 1;
	}
	Tuple(const char* st) {
		std::string(s);
	}
	Tuple() {}
	int GetMult() {
		return 1;
	}
	std::istream &ReadLine(std::istream &in, int minMult = 0) {
		in >> (*this);
		std::string line;
		std::getline(in, line);
		std::stringstream linestrm(line);
		if (tupleSize == 0) {
			tupleSize = this->size();
		}
		int mult;
		linestrm >> mult;
		return in;
	}
	const char *ToString() {
		return (const char*) this->c_str();
	}
	int IncrementMult() {
		return 0;
	}
	Tuple &operator=(std::string &t) {
		*((std::string*)this) = t;
		return *this;
	}
	int GetHashValue(unsigned int &hashValue) {
		int i;
		if (this->size() == 0) {
			return -1;
		}
		hashValue = 0;
		// method 1
		for (i = 0; i < tupleSize; i++) {
			hashValue <<=2;
			if (unmasked_nuc_index[(*this)[i]] >= 4)
				return -1;
			else
				hashValue += unmasked_nuc_index[(*this)[i]];
		}
		return 1;
	}
};

template <typename T>
void Concatenate( T pre, unsigned char nuc, T& result) {
  // concatenate nuc to pre
  int length = pre.size();
	std::string resultStr;
  resultStr = "";
  resultStr.insert(0,(const char*) pre.ToString() + 1,length-1);
  resultStr.push_back(nuc_char[nuc]);
	result.assign((char*) resultStr.c_str());
  //  std::cout << "concatendated " << pre << " " << nuc_char[nuc] << "  to make: " << result << std::endl;
}

void InsertTuple(char* tuple, int tupleLen, int pos, unsigned char nuc);
void DeleteTuple(char* tuple, int tupleLen, int pos, unsigned char fill);
void MutateTuple(char* tuple, int pos, unsigned char nuc);


#endif
