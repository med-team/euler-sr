/***************************************************************************
 * Title:          Tuple.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "Tuple.h"

int Tuple::tupleSize = 0;

void MutateTuple(char* tuple, int pos, unsigned char nuc) {
	tuple[pos] = nuc;
}
void DeleteTuple(char* tuple, int tupleLen, int pos, unsigned char fill) {
	int i;
	for (i = pos; i < tupleLen -1; i++ ){
		tuple[i] = tuple[i+1];
	}
	tuple[i] = fill;
}

void InsertTuple(char* tuple, int tupleLen, int pos, unsigned char nuc) {
	int i;
	for (i = tupleLen - 1; i > pos; i--) {
		tuple[i] = tuple[i-1];
	}
	tuple[i] = nuc;
}




