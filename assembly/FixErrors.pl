#!/usr/bin/env perl

BEGIN {
		unshift(@INC, $ENV{"EUSRC"} . "/assembly/");
}

use RunCmd;

if ($#ARGV < 1) {
  print "usgae: FixErrors.pl reads.fasta tupleSize\n";
  exit(0);
}
$readsFile = shift @ARGV;
$tupleSize = shift @ARGV;
$fixedFile = "$readsFile.fixed";


$srcDir = $ENV{"EUSRC"} . "/assembly/" . $ENV{"MACHTYPE"};
$countSpectrumCmd = "$srcDir/integralCountSpectrum $readsFile $tupleSize $readsFile.spect -printCount -binary";
$sortSpectrumCmd = "$srcDir/sortIntegralTupleList $readsFile.spect -printCount -minMult 2";
$fixErrorsCmd = "$srcDir/fixErrors $readsFile $readsFile.spect $tupleSize $readsFile.fixed -minMult 2 -maxScore 6 -startScore 4 -edgeLimit 4 -stepScore 2 -minVotes 2";


RunCmd::RunCommand($countSpectrumCmd);
RunCmd::RunCommand($sortSpectrumCmd);
RunCmd::RunCommand($fixErrorsCmd);
