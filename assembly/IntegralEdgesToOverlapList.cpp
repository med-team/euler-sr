/***************************************************************************
 * Title:          IntegralEdgesToOverlapList.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  09/16/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <string>
#include "DeBruijnGraph.h"
#include "SeqReader.h"
#include "IntegralTuple.h"
#include "IntervalGraph.h"
int IntegralTuple::tupleSize = 0;

int main(int argc, char* argv[]) {
  std::string edgeFileName;
  int overlapLength;
  std::string overlapPosFileName;
 
  if (argc < 4) {
    std::cout << "usgae: integralEdgesToOverlapList edgeFileName vertexSize "
							<< "overlapListFile " << std::endl;
    exit(1);
  }

  int argi = 1;
  edgeFileName = argv[argi++];
  overlapLength = atoi(argv[argi++]) + 1;
  overlapPosFileName = argv[argi++];

  // every overlap should be unique, so there is no reason to have 
  // any special data structure to find unique overlaps.


  int numOverlaps = 0;
  int s, e, p;

  SimpleSequenceList edges;
  ReadSimpleSequences(edgeFileName, edges);

  for (e = 0; e < edges.size(); e++ ){ 
    numOverlaps += edges[e].length - overlapLength + 1;
  }
	
  
  EdgePosIntegralTupleList readPositions;

  readPositions.resize(numOverlaps);
	EdgePosIntegralTuple::tupleSize = overlapLength;
  int ovp = 0;
  for (e = 0; e < edges.size(); e++ ) {
    for (p = 0; p < edges[e].length - overlapLength + 1; p++) {
			readPositions[ovp].StringToTuple(&edges[e].seq[p]);
      readPositions[ovp].edge = e;
      readPositions[ovp].pos  = p;
      ovp++;
    }
  }
	std::sort(readPositions.begin(), readPositions.end());

  
  std::ofstream overlapOut;
  openck(overlapPosFileName, overlapOut, std::ios::out | std::ios::binary);
	//int numOverlaps = readPositions.size();
	overlapOut.write((char*) &numOverlaps, sizeof(int));
	overlapOut.write((char*) &readPositions[0], sizeof(EdgePosIntegralTuple) * numOverlaps);
  overlapOut.close();

  return 0;
}
