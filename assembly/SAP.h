#ifndef SAP_H_
#define SAP_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "DNASequence.h"
#include "IntegralTuple.h"
#include "SeqReader.h"
#include "SeqUtils.h"
#include "utils.h"
#include "mctypes.h"
#include "FixErrorsStats.h"

using namespace std;


class SEdge {
public:
	// prevNuc, prevLevel, and prevPosition serve as the
	// back pointers in the dynamic programming table (3-d grid).
  char prevNuc;
  unsigned char prevLevel;
	unsigned char solidStretch;
	
  int prevPosition;
	
	// The current tuple
	CountedIntegralTuple tuple;

	// The score of this point in the lattice.
  int score;
  static int tupleSize;
  SEdge() {
    prevNuc = 0;
    prevLevel = 0;
    prevPosition = 0;
		//    tuple = MultTuple("");
    score = 0;
		solidStretch = 0;
  }
  SEdge& operator=(const SEdge& rhs) {
    prevNuc   = rhs.prevNuc;
    prevLevel = rhs.prevLevel;
    prevPosition = rhs.prevPosition;
		tuple = rhs.tuple;
    score = rhs.score;
		solidStretch = rhs.solidStretch;
    return *this;
  }

};

class FixParams {
public:
  int maxGap;
  int gapOpen;
  int gapExtend;
  int misMatch;
  int scoreThreshold;
  int span;
  int maxTrim;
  int edgeLimit;
	int startScore;
	int stepScore;
	int maxScore;
};

typedef std::map<CountedIntegralTuple, SEdge> Cell;
typedef Cell::iterator EdgeIterator;


typedef Cell* Column;
typedef Column* Grid;
typedef std::vector<Grid> Cube;
void ReverseSeq(DNASequence &seq, DNASequence &rev);
void PatchSeq(DNASequence &seq, int pos, DNASequence &patch, int replaceLength);
void CreateGrid(int dim, Grid& grid);
void DeleteGrid(int dim, Grid &grid);

int FindSolidPosition(DNASequence &seq, 
											CountedIntegralTupleDict &spectrum,
											int span, int &lastSolidPos, int& pos);

int StoreEdge(int prevPos, int prevLevel, 
							int prevNuc, CountedIntegralTuple prevTuple, 
							Cell& cell, CountedIntegralTuple tuple, int score, 
							int prevScore, int prevSolid);

int FindMinimumScoreEdge(Cube &matrix, FixParams &p, EdgeIterator &edgeIt, 
												 int &minN, int &minK, int& numMin, int &minScore, 
												 CountedIntegralTuple &minTuple, int &minTrim, int &trim);

int Backtrack(Cube &matrix, int pos, int level, int nuc, 
							CountedIntegralTuple &tuple, 
							DNASequence &seq, Stats &fixStats, FixParams &params,
							int &firstEdit, int &end);

int SolidifyRead(DNASequence &seq,
								 //								 CountedIntegralTupleList &spectrum,
								 CountedIntegralTupleDict &spectrum,
								 IntMatrix &scoreMat,
								 FixParams &params, Stats &fixStats, int &readWasModified);

int SolidifyUntilFixed(DNASequence &seq,
											 CountedIntegralTupleList &origEnumSeq, int origEnumPos,
											 //											 CountedIntegralTupleList &spectrum,
											 CountedIntegralTupleDict &spectrum,
											 IntMatrix &scoreMat,
											 int tupleSize, FixParams &params,
											 DNASequence &fixedSeq, int &replaceLength, 
											 Stats &fixStats,
											 int &lastEdit, int &end);

#endif
