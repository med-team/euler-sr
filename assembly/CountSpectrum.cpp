/***************************************************************************
 * Title:          CountSpectrum.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  09/16/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "DNASequence.h"
#include "SeqReader.h"
#include "SeqUtils.h"
#include "utils.h"
#include "DeBruijnGraph.h"
#include "HashedSpectrum.h"
#include "ListSpectrum.h"
#include "MultTuple.h"
#include <vector>
#include <iostream>
#include <fcntl.h>
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;


void PrintUsage() {
	std::cout << "usage: countspectrum reads kmer_out " << std::endl;
	std::cout << "  [-tupleSize tupleSize ] (20)  The size of tuple for which to count frequency." 
						<< std::endl;
	std::cout << "  [-noPrintCount|-printPos]     Print tuples as a list of indices into the read list." 
						<< std::endl;
  std::cout << "                                This can be a more space-efficient way of representing tuples." 
						<< std::endl;
	std::cout << "  [-trimFront T] Trim first T characters from each read before" 
						<< std::endl << "                  counting the spectrum" << std::endl;
	std::cout << "  [-trimEnd   T] Trim T characters from the end of each read"
						<< std::endl << "                  before ocunting the spectrum. " << std::endl;
	std::cout << "    [-sort]   Write tuples in sorted order." << std::endl;   
	std::cout << "  [-lockFile file]  Wait on locked file 'file' to read input." << std::endl;
	std::cout << "  This is convenient when runnign on a grid that has an NFS bottle neck." 
						<< std::endl;
}

int main(int argc, char* argv[]) {
  if (argc < 3) {
		PrintUsage();
    exit(0);
  }
  std::string readsFile = argv[1];
  std::string tupleOutFile = argv[2];
  int argi = 3;
  SimpleSequenceList seqList;
  HashValueFunctor hashFunction;
  int tupleSize = 20;
  int printCount = 1;
  int printPosition = 0;
  int DoSort = 0;
	int trimFront = 0;
	int trimEnd   = 0;
	int sortTuples= 0;
	int doLock = 0;
	std::string lockFileName = "";
  hashFunction.hashLength = 11;

  while (argi < argc) {
    if (strcmp(argv[argi], "-tupleSize") == 0 or
				strcmp(argv[argi], "-vertexSize") == 0) {
      ++argi;
      tupleSize = atoi(argv[argi]);
    }
    else if (strcmp(argv[argi], "-noPrintCount") == 0) {
      printCount = 0;
    }
    else if (strcmp(argv[argi], "-printPos") == 0) {
      printPosition =1;
    }
    else if (strcmp(argv[argi], "-sort") == 0) {
      DoSort = 1;
    }
		else if (strcmp(argv[argi], "-trimFront") == 0){ 
			trimFront = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-trimEnd") == 0) {
			trimEnd   = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-sort") == 0) {
			sortTuples = 1;
		}
		else if (strcmp(argv[argi], "-lockFile") == 0) {
			doLock = 1;
			lockFileName = argv[++argi];
		}
		else if (strcmp(argv[argi], "-hashLength") == 0) {
			hashFunction.hashLength = atoi(argv[argi++]);
		}
		else {
			PrintUsage();
			std::cout << "bad option: " << argv[argi] << std::endl;
			exit(1);
		}
    ++argi;
  }

  if (tupleSize < hashFunction.hashLength) 
    hashFunction.hashLength = tupleSize;


  CountedReadPos::hashLength = tupleSize;
  CountedReadPos::sequences  = &seqList;
	hashFunction.sequences     = &seqList;


  std::ifstream seqIn;

  DNASequence read, rc;
  int curRead = 0;
  SimpleSequence simpleSeq;
  int storeSeq;
  int seqNumber = 0;
	HashedSpectrum spectrum(hashFunction);
	int lockFileDes;
	struct flock fldes;
	if (doLock) {
		WaitLock(lockFileName, lockFileDes);
	}

  ReadSimpleSequences(readsFile, seqList);
	
	if (doLock) {
		ReleaseLock(lockFileDes);
	}
  AppendReverseComplements(seqList);
	spectrum.hashFunction.sequences = &seqList;
	spectrum.SetHashSize(tupleSize);
	spectrum.StoreSpectrum(seqList, trimFront, trimEnd );

  // now print the hash
  std::ofstream tupleOut;
  openck(tupleOutFile, tupleOut, std::ios::out);
  ReadPosHashTable::iterator it, end;
  
  int i;
  CountedReadPos readPos;
  int nReadPos = 0;
  // Step 1, count the number of read positions.
	ReadPosHashTable::HashPage *page;
	ReadPosHashTable::Data *data, *pageEnd;
  for (i = 0; i < hashFunction.MaxHashValue(); i++ ) {
		page = spectrum.hashTable.table[i].head;
		while (page != NULL) {
			pageEnd = &(*page).page[page->size];
			for (data = &(*page).page[0]; data != pageEnd; ++data) {
				++nReadPos;
			}
			page  = page->next;
		}
	}
	/*    end = spectrum.hashTable.table[i].End();
    it  = spectrum.hashTable.table[i].Begin();
    while (it < end) {
      ++nReadPos;
      ++it;
    }
		}*/

	if (sortTuples) {
		ListSpectrum<MultTuple> tupleList;
		tupleList.Resize(nReadPos);
		int index =0;
		MultTuple::tupleSize = tupleSize;
		for (i = 0; i < hashFunction.MaxHashValue(); i++ ) {
			page = spectrum.hashTable.table[i].head;
			while (page != NULL) {
				pageEnd = &(*page).page[page->size];
				for (data = &(*page).page[0]; data != pageEnd; ++data) {

					/*				
										end = spectrum.hashTable.table[i].End();
										it  = spectrum.hashTable.table[i].Begin();
										while (it < end) {
					*/
					tupleList[index].copy((char*) &seqList[data->read].seq[data->pos], tupleSize);
					++index;
				}
				page = page->next;
			}
		}
		std::sort(tupleList.tupleList.begin(), tupleList.tupleList.end());
		tupleOut.close();
		tupleOut.clear();
		if (doLock) {
			WaitLock(lockFileName, lockFileDes);
		}
		tupleList.Write(tupleOutFile);
	}
	else {
		tupleOut << nReadPos << std::endl;
		if (doLock) {
			WaitLock(lockFileName, lockFileDes);
		}
		for (i = 0; i < hashFunction.MaxHashValue(); i++ ) {
			page = spectrum.hashTable.table[i].head;
			while (page != NULL) {
				pageEnd = &(*page).page[page->size];
				for (data = &(*page).page[0]; data != pageEnd; ++data) {

					//					end = spectrum.hashTable.table[i].End();
					//			it  = spectrum.hashTable.table[i].Begin();
					//			while (it < end) {
					if (printPosition) {
						tupleOut << data->read << " " << data->pos << std::endl;
					}
					else {
						PrintTuple(seqList, *data, tupleSize, tupleOut); 
						if (printCount) {
							tupleOut << " " << data->count;
						}
						tupleOut << std::endl;
					}
				}
				page = page->next;
			}
		}
	}
	if (doLock){ 
		ReleaseLock(lockFileDes);
	}
}
