/***************************************************************************
 * Title:          FixErrorsVoting.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/17/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "SimpleSequence.h"
#include "SeqReader.h"
#include "SeqUtils.h"
#include "utils.h"
#include "hash/HashUtils.h"
#include "BufferedSeqReader.h"
#include "FixErrorsStats.h"

#include <vector>
#include <iostream>
#include <iomanip>
#include <ext/hash_map>
#include <map>
#include <deque>
using namespace std;
char nextNuc[256];

#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;

void PrintUsage() {
	std::cout << "fixErrorsVoting   Fix errors in reads using spectral alignment with " << std::endl
						<< "                  a voting approach instead of a dynamic programming. " << std::endl
						<< "                  This has the benefit of being able to fix point " << std::endl
						<< "                  mutations or single indels in very small reads (<25 nt),"<< std::endl
						<< "                  assuming there is no more than one or two errors" << std::endl
						<< "                  per read." << std::endl;
	std::cout << "   Usage: fixErrorsVoting seqFile spectrumFile tupleSize outputFile [options] " << std::endl
						<< "     -minMult  m  Only consider tuples with multiplicity above m"<<std::endl
						<< "                    to be solid." << std::endl
						<< "     -minVotes v  Require at least 'v' separate votes to fix any position."<<std::endl
						<< "                  A vote is cast for a position p, nucleotide n, if a"<<std::endl
						<< "                  change at (p,n) makes a tuple t change from below m" <<std::endl
						<< "                  to above." << std::endl;
	std::cout << "     -maxTrim  x  Trim at most x nucleotides off the ends of a read. If "<<std::endl
						<< "                  more than x nucleotides need to be trimmed, the read is unfixable."
						<< std::endl;
	std::cout << "     -deletions   Search for single deletions. " << std::endl;
	std::cout << "     -insertions  Search for single insertions. " << std::endl;
	std::cout << "     -search s    Try up to 's' changes to make a tuple solid. " << std::endl;
	std::cout << "     -compare file For benchmarking purposes, the correct reads are given "<< std::endl
						<< "                  in file 'file'.  Read those and copare the results."<<std::endl;
	std::cout << "     -discardFile file Print all reads that do not pass the threshold to 'file'" 
						<< std::endl;
	std::cout << "     -map mapFile Print portions of retained reads to 'mapFile'."<< std::endl;
	std::cout << "     -spectrum [concise|full].  Use either a concise or full spectrum." <<std::endl
						<< "                    The concise spectrum must be on words of size less than 16" << std::endl
						<< "                    and resets all multiplicities greater than 3 to 3."<<std::endl
						<< "                    The full spectrum may be of any length, and stores " << std::endl
						<< "                    the exact multiplicity of all k-mers." << std::endl;
	std::cout << "     -catchall M   Run a round of error correction with low (minMult M minVotes 2\n" 
						<< "                  maxTrim 3 " << std::endl;
	std::cout << "     -fast        Loose some sensitivity for speed." << std::endl;
	std::cout << "     -earlyEnd N    Stop fixing errors after N nucleotides, just trim after that." << std::endl;
	std::cout << "     -map       Print coordinates of the mapped reads in the fasta header." << endl;

}


void PrintMap(DNASequence &seq, int start, int end, std::ofstream &out);

template<typename T_Spectrum>
int TrimSequence(DNASequence &seq, T_Spectrum &spectrum, int tupleSize, int minMult,
								int &seqStart, int &seqEnd, int maxTrim, int printMap);

template<typename T_Spectrum> 
int FindSolidSubsequence(DNASequence &seq, T_Spectrum &spectrum,
										 int tupleSize, int minMult, int &seqStart, int &seqEnd);

template <typename T_Spectrum>
int FixSequence(DNASequence &seq, T_Spectrum &spectrum,
								IntMatrix &votes, IntVector &alreadySolid,
								int tupleSize, int voteThreshold, Stats &stats, int searchSize, int &changeMade);

int PrepareSequence(DNASequence &read);
int InitVotingMatrix(DNASequence &read, IntMatrix &votes);
int InitSolidVector(DNASequence &read, IntVector &solid);

template <typename T_Spectrum>
int VoteSequence(DNASequence &seq, T_Spectrum &spectrum, int tupleSize, int minMult, int startPos,
								 IntMatrix &votes, IntVector &solid, 
								 int numSearch,
								 int checkInsertions, int checkDeletions, int earlyEnd,
								 std::deque<int> &history);

template <typename T_Spectrum>
int SolidifySequence(DNASequence &read, T_Spectrum &spectrum, int tupleSize, int minMult,
										 IntMatrix &votes, IntVector &solid,
										 int minVotes, Stats &stats, int numSearch, int DoDeletion, int DoInsertion, int earlyEnd);

void AddMap(DNASequence &seq, int mapStart, int mapEnd) { 
	std::stringstream mapstrm;
	mapstrm << " mapstart="<< mapStart << " mapEnd=" << mapEnd;
	seq.namestr += mapstrm.str();
}

void AddIndex(DNASequence &seq, int index) {
	std::stringstream strm;
	strm << " index=" << index;
	seq.namestr += strm.str();
}

int runFast = 0;

int main(int argc, char* argv[]) {
 	nextNuc['G'] = 'A';
	nextNuc['A'] = 'C';
	nextNuc['C'] = 'T';
	nextNuc['T'] = 'G';

	int argi;
	if (argc < 4) {
		PrintUsage();
		exit(1);
	}
	argi = 1;
	std::string readsFile = argv[argi++];
	std::string spectrumFileName = argv[argi++];
	int tupleSize = atoi(argv[argi++]);
	IntegralTuple::tupleSize = tupleSize;
	std::string outputFileName  = argv[argi++];
	std::string discardFileName = "";
	std::string spectrumType = "full";
	std::string compareFile;
	std::string mapFileName;
	int printMap;
	int minMult, minVotes;
	int doInsertion, doDeletion;
	int doTrim;
	int maxTrim;
	int printAll;
	int maxMods;
	int catchall;
	int catchallMult = 0;
	printMap    = 0;
	doTrim      = 0;
	doInsertion = 0;
	doDeletion  = 0;
	minMult     = 3;
	minVotes    = 4;
	maxTrim     = -1;
	printAll    = 0;
	catchall    = 0;
	maxMods     = 99999;
	int numSearch = 1;
	int earlyEnd  = 0;
	std::string reportFileName = readsFile + ".report";
	std::ofstream reportOut;
	openck(reportFileName, reportOut, std::ios::ate);
	BeginReport(argc, argv, reportOut);
	while (argi < argc) {
    if (strcmp(argv[argi], "-minMult") == 0 ) {
      ++argi;
      minMult = atoi(argv[argi]);
    }
    else if (strcmp(argv[argi], "-minVotes") == 0 ) {
      ++argi;
      minVotes = atoi(argv[argi]);
    }
    else if (strcmp(argv[argi], "-discardFile") == 0 ) {
      ++argi;
      discardFileName = argv[argi];
    }
		else if (strcmp(argv[argi], "-compare") == 0 ) {
			compareFile = argv[++argi];
		}
		else if (strcmp(argv[argi], "-search") == 0) {
			numSearch = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-deletions") == 0) {
			doDeletion = 1;
		}
		else if (strcmp(argv[argi], "-insertions") == 0) {
			doInsertion = 1;
		}
		else if (strcmp(argv[argi], "-trim") == 0) {
			doTrim = 1;
		}
		else if (strcmp(argv[argi], "-printMap") == 0) {
			printMap = 1;
			//			mapFileName = argv[++argi];
		}
		else if (strcmp(argv[argi], "-maxTrim") == 0) {
			doTrim = 1;
			maxTrim = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-spectrumType") == 0){ 
			spectrumType = argv[++argi];
		}
		else if (strcmp(argv[argi], "-printAll") == 0){ 
			printAll = 1;
		}
		else if (strcmp(argv[argi], "-maxMods") == 0) {
			maxMods = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-catchall") == 0) {
			catchall = 1;
			catchallMult = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-fast") == 0) {
			runFast = 1;
		}
		else if (strcmp(argv[argi], "-earlyEnd") == 0) {
			earlyEnd = atoi(argv[++argi]);
		}
		else {
			PrintUsage();
			std::cout << "bad option: " << argv[argi] << std::endl;
			exit(0);
		}
		argi++;
	}


	doDeletion = 0;
	doInsertion = 0;
  std::ifstream seqIn;
  std::ofstream seqOut, discardOut, mapOut;
  openck(outputFileName, seqOut, std::ios::out);
	openck(readsFile, seqIn, std::ios::in);
	
  if (discardFileName != "") 
    openck(discardFileName, discardOut, std::ios::out);

	std::cout << "reading tuples >= " << minMult << std::endl;
	std::vector<CountedIntegralTuple> spectrum;
	int spectrumSize;
	if (catchall == 1)
		ReadMultBoundedBinaryTupleList(spectrumFileName, 2, spectrum);
	else
		ReadMultBoundedBinaryTupleList(spectrumFileName, minMult, spectrum);

	std::cout << "Done. Read " << spectrum.size() << " tuples." << std::endl;

	int r;
	IntMatrix votes;
	IntVector solid;
	Stats stats;
	int numDiscarded = 0;
	std::stringstream mapstrm;
	DNASequence read;
	//	for (r = 0; r < reads.size(); r++ ){
	int numFixed = 0;
	r = -1;
	std::cout << "************************************************** readIndex\tnumFixed\tnumDiscarded\tnMut\tnDel\tnIns\tnTies\tnNotSolid"<<std::endl;
	while (SeqReader::GetSeq(seqIn, read, SeqReader::noConvert)) {

		if (r % 100000 == 0 and r > 0) {
			std::cout << ".";
			std::cout.flush();
		}
		if (r % 5000000 == 0 and r > 0) {
			std::cout << "\t" << r << "\t" << numFixed << "\t" << numDiscarded << "\t" << stats.numMut << "\t" 
								<< stats.numDel << "\t" <<stats.numIns << "\t";
			std::cout << stats.numTies << "\t" << stats.numNotSolid << std::endl;
		}
		r++;
		int discardSeq = 0;
		if (!PrepareSequence(read)) {
			//			std::cout << "sequence : " << read.namestr << " is bad!" << std::endl;
			discardSeq = 1;
		}
		else {
			DNASequence original = read;
			int numChanges = 0;
			AddIndex(read, r);

			if ((numChanges = 
					 SolidifySequence(read, spectrum, tupleSize, minMult,
														votes, solid, minVotes, stats, numSearch, 
														doDeletion, doInsertion, earlyEnd)) != 0) {
				if (numChanges > maxMods) {
					discardSeq = 1;
					read = original;
				}
				else {
					if (printMap) {
						AddMap(read, 0, read.length);
					}
					discardSeq = 0;
				}
				original.Reset();
			}
			else {
				// Try trimming the sequence to get something that works.
				int trimStart, trimEnd;
				// Find the locations of the first solid positions.
				if (doTrim and 
						TrimSequence(read, spectrum, tupleSize, minMult,
												 trimStart, trimEnd, maxTrim, printMap)) {
					
					// If there is space for one solid tuple (trimStart < trimEnd - ts+1)
					// and the subsequence between the trimmed ends is ok, print the
					// trimmed coordinates.
					discardSeq = 0;
				}
				else {
					// Either too much was trimmed,
					// or the sequence was not solid between the trimming.
					// Consider this sequence unfixable.
					discardSeq = 1;  
				}
			}
		}
		
		if (discardSeq) {
			if (catchall) {
				int numChanges;
				// Try and fix this read with super low error correction parameters.
				numChanges = SolidifySequence(read, spectrum, tupleSize, minMult,
																			votes, solid, catchallMult, stats, numSearch, 
																			doDeletion, doInsertion, earlyEnd);
				if (numChanges > maxMods)
					discardSeq = 1;
			
				else {
					int trimStart, trimEnd;
					int readStart = 0;
					int readEnd   = read.length;
					// 
					// The read may be perfectly fixed, only a prefix may be fixed, or none of it may be fixed.
					// If none of it was fixed, discard the sequence.
					// Otherwise, try and trim the sequence down to the fixed parts.
					//
					if (FindSolidSubsequence(read, spectrum, tupleSize, minMult, readStart, readEnd)) {
						discardSeq = 0;
					}
					else if (TrimSequence(read, spectrum, tupleSize, minMult,
																trimStart, trimEnd, maxTrim, printMap)) {
						discardSeq = 0;
					}
				}
			}
		}
		if (discardSeq and ! printAll) {
			++numDiscarded;
			if (discardFileName !="") {
				read.PrintlnSeq(discardOut);
				discardOut << std::endl;
			}
		}
		else {
			read.PrintlnSeq(seqOut);
			++numFixed;
		}
		read.Reset();
	}
	
	std::cout << std::cout << stats << endl;
	std::cout << "discarded " << numDiscarded << std::endl;
	EndReport(reportOut);
	return 0;
}


template <typename T_Spectrum>
int SolidifySequence(DNASequence &read, T_Spectrum &spectrum, int tupleSize, int minMult,
										 IntMatrix &votes, IntVector &solid,
										 int minVotes, Stats &stats, int numSearch, 
										 int DoDeletion, int DoInsertion, int earlyEnd) {
	
	int s;
	std::deque<int> history;
	int changeMade;
	int startPos, fixPos;
	fixPos = -1;
	int iter = 0;
	int numFixed = 0;
	do {
		//		std::cout << "iter: " << iter << std::endl;

		if (fixPos > 0)
			startPos = fixPos;
		else 
			startPos = 0;

		for (s = 1; s <= numSearch; s++) {
			InitVotingMatrix(read, votes);
			InitSolidVector(read, solid);
			VoteSequence(read, spectrum, tupleSize, minMult, startPos, 
									 votes, solid, s, DoDeletion, DoInsertion, earlyEnd, history);
			//			PrintMatrix(votes, std::cout, 2);
			++numFixed;
			if (FixSequence(read, spectrum, votes, solid, tupleSize, 
											minMult, minVotes, stats, s, fixPos)) 
				return numFixed;
		}
		//		std::cout << "fp: " << fixPos << std::endl;
		++iter;
	} while (fixPos > 0);
	return 0;
}
 
int InitVotingMatrix(DNASequence &read, IntMatrix &votes) {
	if (votes.size() < read.length) {
		CreateMatrix(votes, read.length, 9);
	}
	else {
		int i, j;
		for (i = 0; i < votes.size(); i++ ){
			std::fill(votes[i].begin(), votes[i].end(), 0);
		}
	}
}

int InitSolidVector(DNASequence &read, IntVector &solid) {
	if (read.length > solid.size()) {
		solid.resize(read.length);
	}
	std::fill(solid.begin(), solid.end(), 0);
}

int PrepareSequence(DNASequence &read) {
	int p;
	for (p = 0; p < read.length; p++ ){ 
		read.seq[p] = toupper(read.seq[p]);
		if (!(read.seq[p] == 'A' ||
					read.seq[p] == 'C' ||
					read.seq[p] == 'T' || 
					read.seq[p] == 'G'))
			return 0;
	}
	return 1;
}

template <typename T_Spectrum>
int CheckSolid(DNASequence &seq, T_Spectrum &spectrum, int tupleSize, int minMult) {
	int p;
	CountedIntegralTuple tuple;
	int index;
	for (p = 0; p < seq.length - tupleSize +1; p++ ) {
		if (tuple.StringToTuple(&(seq.seq[p]))) {
			if (((index = LookupBinaryTuple(spectrum, tuple)) == -1) or
					minMult > spectrum[index].count ) {
				return 0;
			}
		}
		else {
			return 0;
		}
	}
	return 1;
}


int VoteHistory(IntMatrix &votes, std::deque<int> &history) {
	int histPos, histMut;
	std::deque<int>::iterator histIt;
	// 
	for (histIt = history.begin(); histIt != history.end(); histIt++) {
		histPos = *histIt;
		++histIt;
		histMut = *histIt;
		votes[histPos][unmasked_nuc_index[histMut]]++;
	}
}

template <typename T_Spectrum>
int VoteSequence(DNASequence &seq, T_Spectrum &spectrum, int tupleSize,
								 int minMult, int startPos,
								 IntMatrix &votes, IntVector &solid, 
								 int numSearch,
								 int checkInsertions, int checkDeletions, int earlyEnd,
								 std::deque<int> &history) {

	DNASequence dnaseq;
	dnaseq.seq    = seq.seq;
	dnaseq.length = seq.length;
	dnaseq._ascii = 1;
	int p;
	/*
		std::cout << "fixing seq "  << seq.namestr << " ";
		seq.PrintSeq(std::cout); std::cout << std::endl;
	*/
	CountedIntegralTuple tempTuple, mutatedTuple;
	int tupleIndex;
	int end;
	if (earlyEnd) {
		end=  min(seq.length - tupleSize + 1, earlyEnd - tupleSize + 1);
	}
	else {
		end = seq.length - tupleSize + 1;
	}

	for (p = startPos; p < end; p++ ) {

		if (tempTuple.StringToTuple(&seq.seq[p])) {
			std::string tupleString, mutatedString;
			//			tempTuple.ToString(tupleString);
			
			if ((tupleIndex = LookupBinaryTuple(spectrum, tempTuple)) != -1 and
					spectrum[tupleIndex].count >= minMult ) {
			solid[p] = 1;
				/*				std::cout << "solid: " << p << std::endl;*/
			}
			else {
				// Cast votes for mutations
				int vp;
				int nucIndex;
				unsigned char mutNuc;
				unsigned char un;
				int mut;
				int histPos, histMut;
				int last = 0;
				if (runFast)
					vp = -3;
				else
					vp = 0;
				while(!last) {
					if (runFast) vp+=4;
					else vp++;
					// adjust the last round if necessary.
					if (vp >= tupleSize - 1) {
						vp = tupleSize - 2;
						last = 1;
					}
					mutNuc = nextNuc[seq.seq[p + vp]];
					char start = seq.seq[p + vp];
					MutateTuple(tempTuple, (unsigned char) numeric_nuc_index[mutNuc], 
											vp, mutatedTuple);

					for (mut = 0; mut < 3; mut++ ) {
						//												mutatedTuple.ToString(mutatedString);
												//												std::cout<< tupleString << " " << mutatedString << std::endl;
						if (tupleIndex = LookupBinaryTuple(spectrum, mutatedTuple) != -1) {
							if (spectrum[tupleIndex].count >= minMult) {
								//VoteHistory(votes, history);
								votes[vp + p][unmasked_nuc_index[mutNuc]]++;
							}
						}
						mutNuc = nextNuc[mutNuc];
						MutateTuple(tempTuple, numeric_nuc_index[mutNuc], vp, mutatedTuple);
					}
				}
			}
		}
	}
}

template <typename T_Spectrum>
int FixSequence(DNASequence &seq, 
								T_Spectrum &spectrum,
								IntMatrix &votes, IntVector &alreadySolid,
								int tupleSize, int minMult, int voteThreshold, Stats &stats, int numSearch,
								int &fixPos) {
	// numSearch is the number of mutations to search for to fix a read.
	
	// At first, no changes are made to the sequence
  fixPos = 0;
	int p, m;
	int numAboveThreshold = 0;
	int maxVotes = 0;
	int allGood  = 1;
	for (p = 0; p < seq.length - tupleSize + 1; p++ ) {
		if (alreadySolid[p] == 0) {
			allGood = 0;
			break;
		}
	}
	if (allGood) {
		// no need to fix this sequence
		/*		std::cout << "seq: " << seq.namestr << " is all good" << std::endl;*/
		return 1;
	}

	int s;
	//	PrintMatrix(votes, std::cout, 3);
	for (p = 0; p < votes.size(); p++) { 
		for (m = 0; m < votes[p].size(); m++) {
			if (votes[p][m] > voteThreshold) 
				numAboveThreshold++;
			if (votes[p][m] >= maxVotes) {
				// Make room for the next vote
				maxVotes = votes[p][m];
			}
		}
	}
	
	//	std::cout << "max votes: " << maxVotes << std::endl;
	// Make sure there aren't multiple possible fixes
	std::vector<int> maxPos, maxMod;
	std::vector<int> tiePos;
	int numTies = -1;
	for (p = 0; p < votes.size(); p++) { 
		for (m = 0; m < votes[p].size(); m++) {
			if (votes[p][m] == maxVotes) {
				numTies++;
				maxPos.push_back(p);
				maxMod.push_back(m);
			}
		}
	}
	int mod, pos;
	
	if (numAboveThreshold > 0 ) {
		if (numTies < numSearch or 
				(maxPos.size() > 1 and maxPos[0] != maxPos[1])) {
			// Found at least one change to the sequence
			unsigned char prev, cur;
			for (s = 0; s < numSearch and s < maxPos.size(); s++) {
				mod = maxMod[s];
				pos = maxPos[s];
				fixPos = pos;
				if (mod < 4) {
					prev = seq.seq[pos];
					cur = nuc_char[mod];
					seq.seq[pos] = nuc_char[mod];
					stats.numMut++;
				}
				else if (mod == 4) {
					int i;
					for (i = pos; i < seq.length; i++ ) {
						seq.seq[i] = seq.seq[i+1];
					}
					seq.length--;
					stats.numDel++;
				}
				else if (mod > 5) {
					seq.length++;
					unsigned char* newSeq = new unsigned char[seq.length];
					assert(pos > 0);
					memcpy(newSeq, seq.seq, pos);
					int i;
					for (i = pos + 1; i < seq.length; i++) {
						newSeq[i] = seq.seq[i-1];
					}
					newSeq[pos] = nuc_char[mod-5];
					delete[] seq.seq;
					seq.seq = newSeq;
					stats.numIns++;
				}
			}
			int solidRes = CheckSolid(seq, spectrum, tupleSize, minMult);

			return solidRes;
		} 
		else {
			// Bookkeeping for sequences that have ties.
			stats.numTies++;
			return 0;
		}
	}
	else {
		//		std::cout << "none above threshold. " << std::endl;
		stats.numNotSolid++;
		return 0;
	}
}

template <typename T_Spectrum>
int TrimSequence(DNASequence &seq, T_Spectrum &spectrum,	int tupleSize, 
									int minMult, int &seqStart, int &seqEnd, int maxTrim, int printMap) {
	int i;
	seqStart = 0;
	//	typename T_Spectrum::TupleType tempTuple;
	CountedIntegralTuple tuple;
	for (i = 0; i < seq.length - tupleSize + 1; i++ ){ 
		if (tuple.StringToTuple(&seq.seq[i]) and
				LookupBinaryTuple(spectrum, tuple) != -1) {
			break;
		}
		// Not solid yet, advance
		seqStart++;
	}
	seqEnd = seq.length;
	for (i = seqStart + 1; i < seq.length - tupleSize; i++ ) {
		if (tuple.StringToTuple(&seq.seq[i])) {
			if (LookupBinaryTuple(spectrum, tuple) == -1) {
				break;
			}
		}
	}

	// 
	// i is set to the position after the last solid tuple.  Since seqEnd - seqStart 
	// should be the length of the solid sequence this includes the length
	// of the tuple.
	//
	seqEnd = i + tupleSize;

	if (seqStart > maxTrim)
		return 0;
	
	if (seq.length - seqEnd > maxTrim)
		return 0;

	if (printMap) {
		std::stringstream mapstrm;
		mapstrm << " mapstart= "<< seqStart << " mapend=" << seqEnd;
		seq.namestr += mapstrm.str();
	}
	
	int s;
	int newLength = seqEnd - seqStart;
	for (s = 0; s < newLength; s++ ) {
		seq.seq[s] = seq.seq[s + seqStart];
	}
	seq.length = newLength;
	return 1;
}
	
template <typename T_Spectrum>
int FindSolidSubsequence(DNASequence &seq, T_Spectrum &spectrum, 
												 int tupleSize, int minMult, int &seqStart, int &seqEnd) {
	int i;
	int solidSubsequence = 1;
	CountedIntegralTuple tuple;
	for (i = seqStart; i < seqEnd - tupleSize + 1; i++) {
		if (!tuple.StringToTuple(&seq.seq[i]) or
				LookupBinaryTuple(spectrum, tuple) == -1) {
			solidSubsequence = 0;
			break;
		}
	}
	return solidSubsequence;
}
		

	
void PrintMap(DNASequence &seq, int start, int end, std::ofstream &out) {
	out << ">" << seq.namestr << std::endl;
	out << start << " " << end << std::endl;
}
