/***************************************************************************
 * Title:          FilterIlluminaReads.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "SeqReader.h"
#include "utils.h"

using namespace std;

void PrintUsage() {
	cout << "usage: filterIlluminaReads in out [-max[ACTG] pct] [-minTrim trim] [-v]" << endl;
	cout << "    -maxC C (0.85)  Trim the ends of reads if they are more than C%"<<endl;
	cout << "    -maxA A (0.85)  Trim the ends of reads if they are more than A%"<<endl;
	cout << "    -maxG G (0.85)  Trim the ends of reads if they are more than G%"<<endl;
	cout << "    -maxT T (0.85)  Trim the ends of reads if they are more than T%"<<endl;
	cout << "    -maxN N       Trim a read if it has one nucleotide more than N% in the suffix" <<endl;
	cout << "    -minTrim T (25) only trim if the run is longer than T" << endl;
	cout << "    -v            Be verbose with trimming" << endl;
}

int main(int argc, char* argv[]) {

	string inFile, outFile;

	ifstream in;
	ofstream out;
	if (argc < 3) {
		PrintUsage();
		exit(1);
	}
	
	inFile =  argv[1];
	outFile = argv[2];
	if (inFile == outFile) {
		cout << "ERROR: the input file and output file cannot be the same." << endl;
		exit(1);
	}
	int argi = 3;
	float maxC = 0.85;
	float maxA = 0.85;
	float maxG = 0.85;
	float maxT = 0.85;
	int verbose = 0;
	int minTrim = 25;
	while (argi < argc) {
		if (strcmp(argv[argi], "-maxC") == 0) {
			maxC = atof(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-maxA") == 0) {
			maxA = atof(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-maxG") == 0) {
			maxG = atof(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-maxT") == 0) {
			maxT = atof(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-maxN") == 0) {
			float trimPct = atoi(argv[++argi]);
			maxT = maxA = maxC = maxG = trimPct;
		}		
		else if (strcmp(argv[argi], "-minTrim") == 0){ 
			minTrim = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-v") == 0) {
			verbose = 1;
		}
		++argi;
	}
	openck(inFile, in, std::ios::in);
	openck(outFile, out, std::ios::out);

	DNASequence read;
	while(SeqReader::GetSeq(in, read, SeqReader::noConvert)) {
		int nA, nC, nT, nG;
		nT = nG = nA = nC = 0;
		int p = read.length - 1;
		int trim = 0;

		// Don't try and trim reads that are too short.
		if (read.length < minTrim) {
			read.PrintlnSeq(out);
			continue;
		}
		// Compute the pct A/C in the suffix.

		int end = read.length - minTrim;
		for (p = read.length - 1; p >= end; p--) {
			switch(toupper(read.seq[p])) {
			case 'A':
				nA++;
				break;
			case 'C':
				nC++;
				break;
			case 'T':
				nT++;
				break;
			case 'G':
				nG++;
				break;
			}
		}

		if ((float)nA / minTrim > maxA) {
			// This triggers a trim, continue moving back while the read is 
			trim = minTrim;
			while (p >= 0 and toupper(read.seq[p]) == 'A') {
				p--;
				trim++;
			}
			if (verbose) {
				cout << "trimming A " << trim << endl;
				read.PrintlnSeq(cout);
			}
			read.length -= trim;
		}
		else if ((float)nC / minTrim > maxC) {
			trim = minTrim;
			while (p >= 0 and toupper(read.seq[p]) == 'C') {
				p--;
				trim++;
			}
			if (verbose) {
				cout << "trimming C " << trim << endl;
				read.PrintlnSeq(cout);
			}
			read.length -= trim;
		}
		else if ((float)nT / minTrim > maxT) {
			trim = minTrim;
			while (p >= 0 and toupper(read.seq[p]) == 'T') {
				p--;
				trim++;
			}
			if (verbose) {
				cout << "trimming T " << trim << endl;
				read.PrintlnSeq(cout);
			}
			read.length -= trim;
		}
		else if ((float)nG / minTrim > maxG) {
			trim = minTrim;
			while (p >= 0 and toupper(read.seq[p]) == 'G') {
				p--;
				trim++;
			}
			if (verbose) {
				cout << "trimming G " << trim << endl;
				read.PrintlnSeq(cout);
			}
			read.length -= trim;
		}
		if (read.length > 0)
			read.PrintlnSeq(out);
	}
	return 0;
}	
