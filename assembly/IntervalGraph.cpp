/***************************************************************************
 * Title:          IntervalGraph.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  12/07/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "HashedSpectrum.h"
#include "IntervalGraph.h"
#include "DeBruijnGraph.h"
#include "graph/GraphAlgo.h"
#include "graph/MSTAlgo.h"
#include "Spectrum.h"
#include "align/alignutils.h"
#include "AlignmentPrinter.h"
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <functional>
#include <iostream>
#include <queue>

using namespace std;

void FormGraphFileNames(std::string &base, 
												std::string &bgraph, std::string &graph,
												std::string &intv, 
												std::string &path, std::string &edge) {
	bgraph = base + ".bgraph";
	graph  = base + ".graph";
	intv   = base + ".intv";
	path   = base + ".path";
	edge   = base + ".edge";
}



void ReadIntervalGraph(std::string &baseIn, 
											 IntervalGraph &graph, int &vertexSize, int skipIntervals) {

	std::string bGraphFileName, graphFileName,
		intervalFileName, pathFileName, edgeFileName;

	FormGraphFileNames(baseIn, bGraphFileName, graphFileName,
										 intervalFileName, pathFileName, edgeFileName);

	//	graph.vertexSize    = vertexSize;

	graph.ReadIntervalGraph(bGraphFileName, intervalFileName, pathFileName, skipIntervals);

	vertexSize = graph.vertexSize;

	ReadSequences(edgeFileName, graph.edges);
}

void WriteIntervalGraph(std::string &baseOut,
												IntervalGraph &graph, int vertexSize) {

	std::string bGraphFileName, graphFileName,
		intervalFileName, pathFileName, edgeFileName;

	FormGraphFileNames(baseOut, bGraphFileName, graphFileName,
										 intervalFileName, pathFileName, edgeFileName);

	graph.PrintIntervalGraph(bGraphFileName, intervalFileName);

  PrintGraph(graph.vertices, graph.edges, graphFileName);
  PrintEdges(graph.vertices, graph.edges, edgeFileName);
	WriteReadPaths(pathFileName, graph.paths, graph.pathLengths);
}


void IntervalGraph::CalcDMST() {
  GraphAlgo::CalcDirectedMST(vertices, edges);
}


void IntervalGraph::ErodeShortEndIntervals(int minIntvLength) {
	int e;
	int numEroded = 0;
	int changeMade = 0;
	do {
		changeMade = 0;
		for (e = 0; e < edges.size(); e++ ){ 
			ReadIntervalList *intervals = edges[e].intervals;
			int numIntervals = intervals->size();
			int i;
			int pathIndex;
			int src = edges[e].src;
			int srcLen = vertices[src].vertexSize;
			for (i = 0; i < numIntervals; i++ ) {
				// Is this interval the last on the path?
				pathIndex = (*intervals)[i].read;
				if ((*intervals)[i].markedForDeletion){
					continue;
				}
				// This path is harmless if it corresponds to 1 or fewer edges.
				if (pathLengths[pathIndex] <= 1) {
					continue;
				}
				if (((*intervals)[i].pathPos == pathLengths[pathIndex]-1) and
						(*intervals)[i].length - srcLen < minIntvLength) {
					// This interval is too short, remove it.

					edges[e].MarkIntervalForRemoval(i);
					pathLengths[pathIndex]--;

					// Erode the reverse complement.
					int rcPathIndex;
					if (pathIndex % 2 == 0) {
						rcPathIndex = pathIndex + 1;
					}
					else {
						rcPathIndex = pathIndex - 1;
					}
					int rcIntvEdge  = paths[rcPathIndex][0].edge;
					int rcIntvIndex = paths[rcPathIndex][0].index;
					edges[rcIntvEdge].MarkIntervalForRemoval(rcIntvIndex);
					int p;
					for (p = 0; p < pathLengths[rcPathIndex] - 1; p++) {
						paths[rcPathIndex][p] = paths[rcPathIndex][p+1];
						(*edges[paths[rcPathIndex][p].edge].intervals)[paths[rcPathIndex][p].index].pathPos = p;
					}
					pathLengths[rcPathIndex]--;
				
					++numEroded;
					changeMade = 1;
				}
			}
		}

		cout << "eroded: " << numEroded << " paths. " << endl;
		RemoveMarkedIntervals();
	}
	while (changeMade);
}

void IntervalGraph::PrintIntervalGraph(std::string &bGraphName,
																			 std::string &intervalName) {
	std::ofstream bGraphOut;
	openck(bGraphName, bGraphOut, std::ios::out);
  PrintBGraph(vertices, edges, vertexSize, bGraphOut);
	bGraphOut << vertexSize;
	//	SortAllEdgeIntervalsByEdgePos();
	PrintReadIntervals(edges, intervalName);
	//	SortAllEdgeIntervalsByReadPos();
} 

int IntervalGraph::CalculateReadLength(int read) {
  int pathPos;
	
  pathPos = 0;
  int readLength = 0;
  int edgeIndex;
  int intvIndex;
  int destVertex;
  for (pathPos = 0; pathPos < pathLengths[read]; pathPos++) {
    edgeIndex = paths[read][pathPos].edge;
    intvIndex = paths[read][pathPos].index;
    destVertex = edges[edgeIndex].dest;
    readLength+= (*edges[edgeIndex].intervals)[intvIndex].length - vertices[destVertex].vertexSize;
  }
  /* 
     The last interval goes up to and including the last vertex, so the loop
     over-subtracts by one vertex.  Add that back now.
  */
  readLength += vertices[destVertex].vertexSize;
  return readLength;
}

int IntervalGraph::CheckGraphStructureBalance() {
  int v, e;
  int destV, balV, balE;
  for (e = 0; e < edges.size(); e++) {
    destV = edges[e].dest;
    balE  = edges[e].balancedEdge;
    balV  = edges[balE].src;
    if (edges[balE].balancedEdge != e) {
      std::cout << "EDGE BALANCE ERROR: " << e << " != " << edges[balE].balancedEdge << std::endl;
    }
    else {
      if (destV >= 0 and 
					(vertices[destV].InDegree() != vertices[balV].OutDegree() or
					 vertices[destV].OutDegree() != vertices[balV].InDegree())) {
				std::cout << "Graph is not complementary. " << std::endl;
				std::cout << "vertex " << destV << " " << vertices[destV].InDegree() 
									<< " " << vertices[destV].OutDegree() << " versus: " << balV <<"  "
									<< vertices[balV].InDegree() << " "
									<< vertices[balV].OutDegree() << std::endl;
				return 0;
      }
			/*
				if (edges[e].length != edges[balE].length) {
				cout << "Graph is not complementary! " << endl;
				cout << e << " length: " << edges[e].length << " balE: " << edges[balE].length << endl;
				return 0;
				}
			*/
    }
  }
	return 1;
}

void IntervalGraph::AssignPathOrderToEdges() {
  int p, e;
  for (p = 0; p < paths.size(); p++ ) {
    for (e = 0; e < pathLengths[p]; e++ ) {
			assert(paths[p][e].edge < edges.size());
			assert(paths[p][e].index < edges[paths[p][e].edge].intervals->size());
      (*edges[paths[p][e].edge].intervals)[paths[p][e].index].pathPos = e;
    }
  }
}
	
int IntervalGraph::CheckAllPathsBalance(int fatal) {
  int p, pos;
  int edge, intv, balEdge, balIntv;

  for (p = 0; p < paths.size(); p+=2 ) {
    int balp = p + 1;
    if (CheckPathBalance(p, balp) == 0) {
      PrintImbalancedPaths(p, balp);
      assert(!fatal);
    }
  }
	return 1;
}

void IntervalGraph::PrintImbalancedPaths(int p, int balp) {
  std::cout << "path: " << p << " is not a mirror image of " << balp << std::endl;
  std::cout << "     ";
  PrintPath(p, std::cout);
  std::cout << "bal: ";
  PrintPathReverse(balp, std::cout);
}


int IntervalGraph::CheckPathContinuity(int p) {
  int pos;
  int edge, index;
  int nextEdge, nextIndex;
  for (pos = 0; pos < pathLengths[p]-1; pos++ ) {
    edge = paths[p][pos].edge;
    index = paths[p][pos].index;
    nextEdge = paths[p][pos+1].edge;
    nextIndex = paths[p][pos+1].index;
    if (edge == -1 or nextEdge == -1)
      continue;
    if ((*edges[edge].intervals)[index].readPos +
				(*edges[edge].intervals)[index].length -
				vertices[edges[edge].dest].vertexSize != 
				(*edges[nextEdge].intervals)[nextIndex].readPos) {
			/*      std::cout << "path: " << p << " is off by " <<
				((*edges[edge].intervals)[index].readPos +
				 (*edges[edge].intervals)[index].length -
				 vertices[edges[edge].dest].vertexSize) -
				(*edges[nextEdge].intervals)[nextIndex].readPos << std::endl;
			*/
      return 0;
    }
		int dest = edges[edge].dest;
		if (nextEdge != edge and vertices[dest].LookupOutIndex(nextEdge) == -1) {
			cout << "path: " << p << " is not continuous" << endl;
			return 0;
		}
  }
  return 1;
}

int IntervalGraph::CheckAllPathsContinuity(int fatal) {
  int p;
  for (p = 0; p < paths.size(); p++) {
    if (!CheckPathContinuity(p)) 
			return 0;
  }
	return 1;
}

int IntervalGraph::CheckPathBalance(int p, int balp) {
  int pos;
  int edge, intv, balEdge, balIntv;
  if (pathLengths[p] > 0) {
    if (pathLengths[balp] != pathLengths[p]) {
      return 0;
    }
    else {
      for (pos = 0; pos < pathLengths[p]; pos++) {
				edge = paths[p][pos].edge;
				intv = paths[p][pos].index;
				balEdge = paths[balp][pathLengths[balp] - pos - 1].edge;
				balIntv = paths[balp][pathLengths[balp] - pos - 1].index;
				assert(edge < 0 or intv < 0 or  (*edges[edge].intervals)[intv].length >= 0);
				assert(balEdge < 0 or intv < 0 or (*edges[balEdge].intervals)[balIntv].length >= 0);
				if (edge < 0 and balEdge >= 0 or
						edge >= 0 and balEdge < 0) {
					return 0;
				}
				if (edge >= 0 and balEdge >= 0 and 
						(*edges[edge].intervals)[intv].length != 
						(*edges[balEdge].intervals)[balIntv].length) {
					return 0;
				}
				if (edge >= 0 and balEdge >= 0) {
					if (edge != edges[balEdge].balancedEdge) {
						//						cout << "edge mismatch: " << p << " " << pos << " " << edge << " " << balEdge << " " << edges[balEdge].balancedEdge << endl;
					}
				}
      }
    }
  }
  // No imbalances detected, return 0K
  return 1;
}
	

void IntervalGraph::PrintPath(int p, std::ostream &pathOut) {
  int pos, edge, intv;
  pathOut << std::setw(8) <<  p << " len: " << pathLengths[p] << endl;
  for (pos = 0; pos < pathLengths[p]; pos++) {
    edge = paths[p][pos].edge;
    intv = paths[p][pos].index;
    if (edge != -1)
      pathOut << " (" << p << ", " << pos << "), e " << edge << ", i " << edges[edge].index << ", I " 
							<< intv 
							<< ", R " << setw(8) << (*edges[edge].intervals)[intv].readPos 
							<< ", E " << setw(8) << (*edges[edge].intervals)[intv].edgePos 
							<< ", L " << setw(8) << (*edges[edge].intervals)[intv].length << ", v "
							<< vertices[edges[edge].dest].vertexSize << ") " << endl;
    else
      pathOut << " -1 (0) ";
  }
  pathOut << std::endl;
}


void IntervalGraph::PrintPathReverse(int p, std::ostream &pathOut) {
  int pos, edge, intv;
  pathOut << std::setw(8) << p << " len: " << pathLengths[p] << endl;
  int len = pathLengths[p];
  for (pos = 0; pos < pathLengths[p]; pos++) {
    edge = paths[p][len - pos - 1].edge;
    intv = paths[p][len - pos - 1].index;
    if (edge != -1)
      pathOut << " (" << p << ", " << pos << "), e " << edge << ", i " << edges[edge].index << ", I " 
							<< intv 
							<< ", R "  << setw(8) << (*edges[edge].intervals)[intv].readPos 
							<< ", E "  << setw(8) << (*edges[edge].intervals)[intv].edgePos 
							<< ", L "  << setw(8) << (*edges[edge].intervals)[intv].length << ", v "
							<< vertices[edges[edge].dest].vertexSize << ") " << endl;

    else
      pathOut << " -1 (0) ";
  }
  pathOut << std::endl;
}

void IntervalGraph::PrintPaths(std::string pathName) {
  std::ofstream pathOut;
  openck(pathName, pathOut, std::ios::out);
  int p;
  int pos;
  int edge, intv;
  for (p = 0; p < paths.size(); p+=2 ) {
    if (pathLengths[p] > 0) {
      pathOut << "     ";
      PrintPath(p, pathOut);
    }
    int balp = p+1;
    if (pathLengths[balp] > 0) {
      pathOut << "bal: ";
      PrintPath(balp, pathOut);
    }
  }
  pathOut.close();
}

void IntervalGraph::AssignVertexSizes() {
	int v;
	for (v = 0; v < vertices.size(); v++) {
		vertices[v].vertexSize = vertexSize;
	}
}

void IntervalGraph::ReadIntervalGraph(std::string &bGraphName,
																			std::string &intervalName, 
																			std::string pathName, 
																			int skipIntervals) {
  // The interval graph is a combination of graph and intervals
  std::ifstream in;
  openck(bGraphName, in, std::ios::in);
  ReadVertexList(in, vertices);
  ReadEdgeList(in, edges);
	int graphVertexSize;
	if ((in >> graphVertexSize)) {
		vertexSize = graphVertexSize;
	}
	//  ReadBGraph(bGraphName, vertices, edges);

	AssignVertexSizes();
  CheckVertices(vertices, edges);
  CheckEdges(vertices, edges);

	if (skipIntervals) {
		CheckBalance();
		InitializeFlags();
		return;
	}

  maxReadIndex = ReadReadIntervals(intervalName, edges);

  // All the methods here work with intervals sorted by read pos
  // but other code uses them sorted by edge pos
  // Restore the order when writing the graph
	// SortAllEdgeIntervalsByReadPos();
	
  // Assign each read interval its order in the path it is part of
  if (pathName != "") {
    ReadReadPaths(pathName, paths, pathLengths);
		std::cout << "Done reading graph." << std::endl;
    AssignPathOrderToEdges();
  }
  else {
    AssignIntervalPathOrder();
  }

  //	AssignEdgesToIntervals();
  CheckBalance();
  assert(CheckAllPathsBalance(1));
  CheckGraphStructureBalance();
  InitializeFlags();
}


void IntervalGraph::MergeEdgeIntervals(int source, int dest) {

  int sourceLength = edges[source].length;
  int destLength   = edges[dest].length;

  int i;
  int numDestIntervals = edges[dest].intervals->size();

  edges[dest].intervals->resize(edges[dest].intervals->size() + 
																edges[source].intervals->size());

  int destPos;

  // Copy over the read intervals
  for (i = 0; i < edges[source].intervals->size(); i++ ) {
    (*edges[dest].intervals)[i + numDestIntervals] = (*edges[source].intervals)[i];
    destPos = i + numDestIntervals;
  }
	//  SortReadIntervalsByReadPos(*edges[dest].intervals);
  int read, pathPos;
  for (i = 0; i < (*edges[dest].intervals).size(); i++) {
    /*
      The read and pathPos do not change when merging
      the edge intervals, but the original edge, and position
      on the edge do.  Reset all of them since the order
      changes when sorting.
    */
    read = 		(*edges[dest].intervals)[i].read;
    pathPos = (*edges[dest].intervals)[i].pathPos;
    paths[read][pathPos].edge  = dest;
    paths[read][pathPos].index = i;
  }
}

int IntervalGraph::GetOutEdgeIndex(int vertex, int edge) {
  int e;
  for (e = 0; e < 4; e++ ) {
    if (vertices[vertex].out[e] == edge)
      return e;
  }
  return 4;
}

int IntervalGraph::GetInEdgeIndex(int vertex, int edge) {
  int e;
  for (e = 0; e < 4; e++ ) {
    if (vertices[vertex].in[e] == edge)
      return e;
  }
  return 4;
}

void IntervalGraph::RemoveAllSimpleBulges(int minBulgeSize) {
  int nRemoved = 0;
  int removeBulgeIter = 0;
  do {
    nRemoved = RemoveSimpleBulges(minBulgeSize);
    ++removeBulgeIter;
    std::cout << "remove bulge iter: " << removeBulgeIter << " " << nRemoved << std::endl;
  }
  while (nRemoved > 0);

}

int IntervalGraph::RemoveSimpleBulges(int minBulgeSize) {
  
  // Remove simple bulges, directed cycles of length 2.  This was 
  // mainly written as practice for merging intervals, to see if it 
  // would work, and to eek out some better results for component size.
  int v, a, b;
  std::vector<int> removedEdges;
  int removedEdge;
  Unflag();
  int edgeA, edgeB;
  int balancedEdge;
  int edgeIndex;
  
  for (v = 0; v < vertices.size(); v++ ){
		// Find which edges go into the same position
		//    if (vertices[v].OutDegree() == 2) {
		for (a = 0; a < 3 ; a++ ){
			for (b = a + 1; b < 4 ; b++ ) {
				if (vertices[v].out[a] != -1 and
						vertices[v].out[b] != -1 and
						edges[vertices[v].out[a]].dest == 
						edges[vertices[v].out[b]].dest) {
					if (edges[vertices[v].out[a]].length < minBulgeSize and
							edges[vertices[v].out[b]].length < minBulgeSize) {
						// There is a simple bulge.  Need to check if there is a preference on 
						// which one to remove.  There is a preference when the balanced
						// edge of one of the edges has already been removed.
						if (edges[vertices[v].out[a]].flagged == GraphEdge::Marked) {
							removedEdge = RemoveSimpleBulge(v, vertices[v].out[b], vertices[v].out[a], edgeIndex);
							removedEdges.push_back(removedEdge);
						}
						else if (edges[vertices[v].out[b]].flagged == GraphEdge::Marked) {
							removedEdge = RemoveSimpleBulge(v, vertices[v].out[a], vertices[v].out[b], edgeIndex);
							removedEdges.push_back(removedEdge);
						}
						else {
							// There is no preference on which edge to merge into the other.
							// Keep the edge of higher multiplicity
							if (edges[vertices[v].out[a]].balancedEdge == vertices[v].out[b]) {
								// The bulge involves two balanced edges.  Since we'll be deleting
								// the balance, we mark the balance of the originals to themselves 
								// so that the bookkeeping is correct (no edges remain without balanced
								// edges).
								edges[vertices[v].out[a]].balancedEdge = vertices[v].out[a];
								edges[vertices[v].out[b]].balancedEdge = vertices[v].out[b];
							}
							// Remove the edge of lower multiplicity
							removedEdge = RemoveSimpleBulgeLowerMult(v, vertices[v].out[a], vertices[v].out[b], edgeIndex);
							removedEdges.push_back(removedEdge);
							balancedEdge = edges[removedEdge].balancedEdge;
							// std::cout << "removing bulged edge: " << removedEdge << " and giving preference to " << balancedEdge << std::endl;
							edges[balancedEdge].flagged = GraphEdge::Marked;
							//  std::cout << "removing edge " << removedEdge << " and giving preference to " << balancedEdge << std::endl;
						}
					}
				}
			}
		}
		//    }
	}
	// Now do a sanity check on the removed edges.  For every edge 
	// in the removed set, it's balanced edge should have been removed.

	int e, e2;
	std::sort(removedEdges.begin(), removedEdges.end());
	for (e = 0; e < removedEdges.size(); e++) {
		removedEdge = removedEdges[e];
		balancedEdge = edges[removedEdge].balancedEdge;
		if (std::binary_search(removedEdges.begin(), removedEdges.end(),
													 balancedEdge) == 0) {
			std::cout << "error, removed " << removedEdge << " but not " 
								<< edges[removedEdge].balancedEdge << std::endl;
		}
	}

	std::cout << "Simple bulge removal removed: " << removedEdges.size() << " edges " << std::endl;
	std::vector<int> nullVertices;
	/* 
		 Remove the bulges from the graph.
		 At this point in time the paths are marked for deletion, but they are not 
		 removed so that they may be re-routed.
	*/

	Prune(nullVertices, removedEdges);

	/* 
		 For now re-route before condensing simple paths.  If it were done afterwards, there 
		 would be gapped intervals being joined together. That's not so bad, but just not 
		 yet figured out.
	*/
	//	RouteRemovedIntervals();
	SortAllEdgeIntervalsByReadPos();
	UpdateAllPathIndices();
	CondenseSimplePaths();
	assert(CheckAllPathsBalance(1));
	return removedEdges.size();
}



int IntervalGraph::IsEdgeSuspect(int e, int polyNucLength) {
	// edge e 
	int p;
	if (polyNucLength > edges[e].length)
		return 0;

	int polyNucPos = 0;
	int lastPolyNuc = -1;
	int polyNucFound = 0;
	for (p = 1; p < edges[e].length; p++ ) {
		if (edges[e].seq.seq[polyNucPos] != edges[e].seq.seq[p])
			polyNucPos = p;
		else if (p - polyNucPos >= polyNucLength) {
			polyNucFound = 1;
			lastPolyNuc = p;
		}
		if (p > vertexSize ) {
			// There exists a k-mer here that does not have 
			// a homopolymer in it.  There fore this edge is trusted
			if (lastPolyNuc < p - vertexSize)
				return 0;
		}
	}
	return 1;
}

int IntervalGraph::LookupAlignedPosition(int edge, int edgePos, int *alignment, int seqLength, 
																				 vector<int> &path,
																				 vector<int> &edgeStarts, int traversal) {
	int nTraversal = 0;
	int pathLength = edgeStarts.size();
	vector<int>::iterator pathIt, pathEnd;
	pathEnd = path.end();
	int pathPos;
	for(pathPos = 0, pathIt = path.begin();
			pathIt != pathEnd; ++pathIt, ++pathPos) {
		if (*pathIt == edge) {
			++nTraversal;
			if (nTraversal == traversal) {
				// Found the proper path index
				break;
			}
		}
	}
	assert(pathPos < edgeStarts.size());

	int alignPos = edgeStarts[pathPos];
	alignPos += edgePos;
	assert(alignPos < seqLength);
	return alignPos;
}

int IntervalGraph::AlignmentBoundariesToPath(int *alignment, int alignmentLength,
																						 int start, int end,
																						 vector<int> &path, vector<int> &edgeStarts, 
																						 vector<int> &pathEdges, 
																						 vector<int> &pathEdgeStarts, vector<int> &pathEdgeLengths) {
	// Find the edge containing the start
	int edgeIndex;
	int startEdgeIndex = 0;
	int endEdgeIndex = 0;
	int startEdgePos =  0;
	int startEdgePathLength = 0;
	startEdgeIndex = -1;

	if (start >= edgeStarts[edgeStarts.size() - 1]) {
		startEdgeIndex = edgeStarts.size() - 1;
	}
	else {
		for (edgeIndex = 0; edgeIndex < edgeStarts.size(); edgeIndex++) {
			if (start < edgeStarts[edgeIndex]) {
				startEdgeIndex = edgeIndex - 1;
				break;
			}
		}
	}

	startEdgePos = start - edgeStarts[startEdgeIndex];
	endEdgeIndex = startEdgeIndex;
	/*	if (end > edgeStarts[edgeStarts.size() - 1] + edges[path[path.size()-1]].length) {
		endEdgeIndex = edgeStarts.size() - 1;
		}
		else {
	*/
	for (edgeIndex = startEdgeIndex; edgeIndex < edgeStarts.size(); edgeIndex++) {
		if (end < edgeStarts[edgeIndex] + edges[path[edgeIndex]].length) {
			endEdgeIndex = edgeIndex;
			break;
		}
	}
	//	}

	int pathLength = endEdgeIndex - startEdgeIndex + 1;
	assert(pathLength > 0);
	pathEdges.resize(pathLength);
	pathEdgeStarts.resize(pathLength);
	pathEdgeLengths.resize(pathLength);

	// Store the edges corresponding to this path.
	int i;
	for (i = 0; i < pathLength; i++) {
		pathEdges[i] = path[startEdgeIndex + i];
	}

	// Now store the boundaries corresponding to this path.
	pathEdgeStarts[0] = start - edgeStarts[startEdgeIndex];
	if (pathLength == 1) {
		pathEdgeLengths[0] = end - start + 1;
	}
	else {
		pathEdgeLengths[0] = edges[path[startEdgeIndex]].length  - pathEdgeStarts[0];

		for (i = 1; i < pathLength; i++) {
			// This is an internal interval, so it starts at the beginning of the edge.
			pathEdgeStarts[i] = 0;
			pathEdgeLengths[i] = edges[path[startEdgeIndex + i]].length;
		}
		// Fix the end length if the interval ends before the end of the edge.
		pathEdgeLengths[pathLength-1] = (end - edgeStarts[endEdgeIndex] + 1);
	}
	return pathLength;
}


int IntervalGraph::MarkSuspectEdges(int polyNucLength) {
	int e;
	Unsuspect();
	if (vertexSize <= 0) {
		std::cout << "ERROR, the vertex size needs to be set to mark " 
							<< "suspect edges! " << std::endl;
		exit(0);
	}

	//  std::cout << "marking suspect edges!! " << std::endl;
	for (e = 0; e < edges.size(); e++ ) {
		if (IsEdgeSuspect(e, polyNucLength)) {
			edges[e].suspect = GraphEdge::Marked;
			/*
				std::cout << " edge : " << e << " is suspect " << std::endl;
			*/
		}
		else
			edges[e].suspect = GraphEdge::NotMarked;
	}
}

int IntervalGraph::RemoveSimpleBulgeLowerMult(int vertex, int e1, int e2, int &edgeIndex) {

	// Merge the edge of lower multiplicity into the edge of higher.
	int sourceEdge, destEdge;
	int destVertex;
	destVertex = edges[e1].dest;

	sourceEdge = e1;
	destEdge   = e2;

	// check the lower mult. edge
	if (edges[e2].multiplicity > edges[e1].multiplicity) {
		sourceEdge = e2;
		destEdge = e1;
	}

	RemoveSimpleBulge(vertex, destEdge, sourceEdge, edgeIndex);

	return sourceEdge;
}

int IntervalGraph::RemoveSimpleBulge(int vertex, int destEdge, int sourceEdge, int &edgeIndex) {

	// Merge the edge of lower multiplicity into the edge of higher.
	int destVertex;
	destVertex = edges[destEdge].dest;

	MergeEdgeIntervals(sourceEdge, destEdge);

	//  RemoveEdge(sourceEdge);
	// Unlink the out edge
	edgeIndex = GetOutEdgeIndex(vertex, sourceEdge);
	vertices[vertex].out[edgeIndex] = -1;

	// Unlink the in edge
	edgeIndex  = GetInEdgeIndex(destVertex, sourceEdge);
	vertices[destVertex].in[edgeIndex] = -1;

	/* 
		 Remove this for now since we'll just merge the edge intervals since 
		 the source and dest edges do not change.
		 MarkIntervalsInEdgeForRemoval(sourceEdge);
	*/
	return sourceEdge;
}

class CountAndStoreVertices {
public:
	int size;
	int length;
	int maxSize;
	std::vector<TEdge> *edges;
	std::vector<TVertex> *vertices;
	std::vector<int> componentIndices, edgeIndices;

	void operator()(int vertexIndex) {
		int e;
		size++;
		if (maxSize == -1 and componentIndices.size() > maxSize) {
			return;
		}
		componentIndices.push_back(vertexIndex);
		int edgeIndex;
		int srcIndex;
		int destIndex;
		for (e = (*vertices)[vertexIndex].FirstIn(); 
				 e < (*vertices)[vertexIndex].EndIn(); 
				 e = (*vertices)[vertexIndex].NextIn(e) ) {
			assert((*vertices)[vertexIndex].in[e] != -1);
			edgeIndex = (*vertices)[vertexIndex].in[e];
			srcIndex = (*edges)[edgeIndex].src;
			if (edgeIndex >= 0 and
					(*edges)[edgeIndex].marked == GraphEdge::NotMarked ) {
				edgeIndices.push_back(edgeIndex);
				length += (*edges)[edgeIndex].length;
				(*edges)[edgeIndex].marked = GraphEdge::Marked;
			}
		}

		for (e = (*vertices)[vertexIndex].FirstOut(); 
				 e < (*vertices)[vertexIndex].EndOut(); 
				 e = (*vertices)[vertexIndex].NextOut(e) ) {
			assert((*vertices)[vertexIndex].out[e] != -1);
			edgeIndex = (*vertices)[vertexIndex].out[e];
			if (edgeIndex >= 0 and
					(*edges)[edgeIndex].marked == GraphEdge::NotMarked) {
				edgeIndices.push_back(edgeIndex);
				length += (*edges)[edgeIndex].length;
				(*edges)[edgeIndex].marked = GraphEdge::Marked;
			}
		}
	}
};

void IntervalGraph::RemoveBalPreferred() {
	int e;
	for (e = 0; e < edges.size(); e++) 
		edges[e].balPreferred = GraphEdge::NotMarked;
}

void IntervalGraph::Unsuspect(){
	int e;
	for (e = 0; e < edges.size(); e++) 
		edges[e].suspect = GraphEdge::NotMarked;
}

void IntervalGraph::Unflag() {
	int v, e;
	for (v = 0; v < vertices.size(); v++ )
		vertices[v].flagged = GraphVertex::NotMarked;
	for (e = 0; e < edges.size(); e++)
		edges[e].flagged = GraphEdge::NotMarked;
}

void IntervalGraph::Untraverse() {
	int v, e;
	for (v = 0; v < vertices.size(); v++ )
		vertices[v].traversed = GraphVertex::NotMarked;
	for (e = 0; e < edges.size(); e++)
		edges[e].traversed = GraphEdge::NotMarked;
}

void IntervalGraph::Unmark() {
	int v, e;
	for (v = 0; v < vertices.size(); v++ )
		vertices[v].marked = GraphVertex::NotMarked;
	for (e = 0; e < edges.size(); e++)
		edges[e].marked = GraphEdge::NotMarked;
}

void IntervalGraph::InitializeFlags() {
	int v, e;
	for (v = 0; v < vertices.size(); v++ )
		vertices[v].Unmark();
	for (e = 0; e < edges.size(); e++)
		edges[e].Unmark();
}

int IntervalGraph::RemoveSmallComponents(int length, 
																				 std::string &readsFile, std::string &componentReadsFile) {

	int v, cv, ce;
	CountAndStoreVertices countSize;
	countSize.vertices = &vertices;
	countSize.edges    = &edges;
	countSize.maxSize  = length;
	Unmark();
	std::vector<int> verticesToRemove, edgesToRemove;
	int component = 0;
	for (v = 0; v < vertices.size(); v++ ) {
		if (vertices[v].marked == GraphVertex::NotMarked ) {
			countSize.size = 0;
			countSize.length = 0;
			countSize.componentIndices.clear();
			countSize.edgeIndices.clear();
			TraverseDFS(vertices, edges, v, countSize);
			if (countSize.length < length) {
				// Need to push back everything in the component
				for (cv = 0; cv < countSize.componentIndices.size(); cv++) {
					verticesToRemove.push_back(countSize.componentIndices[cv]);
				}
				for (ce = 0; ce < countSize.edgeIndices.size(); ce++) {
					edgesToRemove.push_back(countSize.edgeIndices[ce]);
				}
			} 
			++component;
		}
	}
	std::cout << "component removal removed: " << verticesToRemove.size() << " vertices  / "
						<< vertices.size() << " and : " 
						<< edgesToRemove.size() << " edges / " 
						<< edges.size() << std::endl;

	int e, i;
	MarkIntervalsInEdgeForRemoval(edgesToRemove);
	SimpleSequenceList reads;
	std::ofstream compReadsOut;
	if (readsFile != "") {
		std::cout << "Printing small component reads to: " << componentReadsFile << std::endl;
		openck(componentReadsFile, compReadsOut, std::ios::out);
		ReadSimpleSequences(readsFile, reads);
		AppendReverseComplements(reads);
		int er;
		int read;
		std::stringstream titlestrm;
		for (er = 0; er < edgesToRemove.size(); er++ ) {
			e = edgesToRemove[er];
			for (i = 0; i < (*edges[e].intervals).size(); i++ ) {
				read = (*edges[e].intervals)[i].read;
				titlestrm.str("");
				titlestrm << read;
				if (pathLengths[read] > 0) {
					reads[read].PrintSeq(compReadsOut, titlestrm.str());
					pathLengths[read] = 0;
				}
			}
		}
		compReadsOut.close();
	}

	Prune(verticesToRemove, edgesToRemove);
	int numRemoved = RemoveMarkedIntervals();
	std::cout << "after removing components, removed " << numRemoved << " paths " << std::endl;


	for (e = 0; e < edges.size();e++ ){ 
		for (i = 0; i < (*edges[e].intervals).size(); i++) { 
			assert((*edges[e].intervals)[i].pathPos >= 0);
		}
	}
	assert(CheckAllPathsBalance(1));
	return verticesToRemove.size();
}


void IntervalGraph::RemoveEdges(std::vector<int> &edgesToRemove, std::vector<int> &orphanedVertices) {
	int e;
	for (e = 0; e < edgesToRemove.size(); e++ ){ 
		RemoveEdge(edgesToRemove[e], orphanedVertices);
	}
}

void IntervalGraph::ConcatenateEdgeSequence(int vertex, int inEdge, int outEdge) {

	// Copy the edge from outEdge into in edge
	// and update the edge lengths
	int oldEdgeLength = edges[inEdge].length;
	edges[inEdge].seq.length += edges[outEdge].seq.length - vertexSize;

	unsigned char* newSeq;
	// keep the old sequence
	newSeq = new unsigned char[edges[inEdge].seq.length];
	memcpy(newSeq, edges[inEdge].seq.seq, oldEdgeLength);

	// Make sure we are not about to write over memory 
	// starting before the end of the edge
	assert(edges[inEdge].seq.length > vertexSize);

	memcpy((unsigned char*) (newSeq + oldEdgeLength - vertexSize),
				 edges[outEdge].seq.seq, edges[outEdge].seq.length);

	delete[] edges[inEdge].seq.seq;
	edges[inEdge].seq.seq = newSeq;
	edges[inEdge].length = edges[inEdge].seq.length;
}

void IntervalGraph::MergeSimplePath(int vertex, int inEdge, int outEdge) {
	/*
		SortReadIntervalsByReadPos(edges[inEdge].intervals);
		SortReadIntervalsByReadPos(edges[outEdge].intervals);
	*/
	//  std::cout << "Merging paths " << inEdge << " <" << vertex << "- " << outEdge << std::endl;
	// do a couple of sanity checks.
	assert(vertices[vertex].InDegree() == 1);
	//  assert(vertices[vertex].OutDegree() == 1);

	// join or append read intervals if necessary
	// RerouteSimplePathIntervals(vertex, inEdge, outEdge);

	ConcatenateEdgeSequence(vertex, inEdge, outEdge);
	// Update the length of the edge, and the sequence

	SkipOutEdge(vertex, inEdge, outEdge);

	// Mark this edge as not used
	edges[outEdge].dest = -1;
	edges[outEdge].src  = -1;

	// Mark this vertex as not used.
	int inIndex, outIndex;
	inIndex  = vertices[vertex].LookupInIndex(inEdge);
	outIndex = vertices[vertex].LookupOutIndex(outEdge);
	assert(inIndex >= 0);
	//	assert(outIndex >= 0);
	vertices[vertex].in[inIndex]   = -1;
	if (outIndex >= 0)
		vertices[vertex].out[outIndex] = -1;

	//  SortReadIntervalsByReadPos(*edges[inEdge].intervals);
	//  UpdatePathIndices(inEdge);

	// Free up some memory.
	edges[outEdge].Clear();
	//	delete edges[outEdge].intervals;

	// Propagate some flags.
	if (edges[outEdge].guarded == GraphEdge::Marked)
		edges[inEdge].guarded = GraphEdge::Marked;
}

void IntervalGraph::UpdateAllPathIndices() {
	int e;
	for (e = 0; e < edges.size(); e++) 
		UpdatePathIndices(e);
}

void IntervalGraph::UpdatePathIndices(int edge) {
	/*
	 * Make path intervals point towards the correct index in an edge.
	 */
	int i;
	int read, pathPos;
	for (i = 0; i < edges[edge].intervals->size(); i++) { 
		if (!IsIntervalMarkedForRemoval(edge, i)) {
			read    = (*edges[edge].intervals)[i].read;
			pathPos = (*edges[edge].intervals)[i].pathPos;
			assert(read >= 0);
			assert(read < paths.size());
			assert(pathPos >= 0);
			assert(pathPos < pathLengths[read]);
			paths[read][pathPos].index = i;
		}
	}
}

void IntervalGraph::SortAllEdgeIntervalsByReadPos() {
	int e;
	for (e = 0; e < edges.size(); e++) {
		SortReadIntervalsByReadPos(*edges[e].intervals);
	}
}


void IntervalGraph::SortAllEdgeIntervalsByEdgePos() {
	int e;
	for (e = 0; e < edges.size(); e++) {
		SortReadIntervals(*edges[e].intervals);
	}
}

void IntervalGraph::SkipOutEdge(int vertex, int inEdge, int outEdge) {

	// Transform part of the graph of 
	//     the form  src--inEdge-->vertex--outedge-->dest
	//     to   --inEdge->dest
	//     
	// In edge now becomes the balance of outEdge->balancedEdge.
	// 
	// Unlink outEdge from graph
	int destVertex;
	int srcVertex;

	srcVertex  = edges[inEdge].src;
	destVertex = edges[outEdge].dest;

	// Fix the destination of the in edge so that 
	// it skips 'vertex'
	edges[inEdge].dest = destVertex;

	// Now fix the in edge from the dest vertex
	// have   vertex -- outEdge --> destVertex
	// we want the index of this.
	int inEdgeIndex = vertices[destVertex].LookupInIndex(outEdge);
	assert(inEdgeIndex != vertices[destVertex].EndIn());

	// point back past the skipped edge.
	vertices[destVertex].in[inEdgeIndex] = inEdge;

	// Fix the balanced edge of the skipped edge to reference the 
	// in edge.
	// We hope that only balanced operations will be performed on the graph

	int balOutEdge;
	balOutEdge = edges[outEdge].balancedEdge;

	edges[balOutEdge].balancedEdge = inEdge;

	int balInEdge;
	balInEdge = edges[inEdge].balancedEdge;
	if (edges[balInEdge].balancedEdge == -1) {
		// The in-edge references an edge that has been removed.
		// This can only happen if balanced edges have already been
		// skipped.  So, it's safe to make inedge point to the balanced
		// edge that the out edge uses, since it's already been merged with that.
		// I should draw an ascii diagram for this, but I don't want to now.
		edges[inEdge].balancedEdge = edges[outEdge].balancedEdge;
	}
	edges[outEdge].balancedEdge = -1;
}


int IntervalGraph::CondenseSimplePaths() {
	std::vector<int> removedVertices;
	std::vector<int> removedEdges;

	int vertex;
	int destVertex;
	int outEdge, outEdgeIndex;
	int followingVertex, followingEdge, followingEdgeIndex;
	int prevLength;

	// Phase 1, modify read intervals that are condensed.
	vertex = 0;
	for (vertex = 0; vertex < vertices.size(); vertex++) { 
		// Found a branching vertex, attempt to simplify paths from it.
		if (vertices[vertex].OutDegree() != 1 or vertices[vertex].InDegree() != 1) {
			for (outEdgeIndex = vertices[vertex].FirstOut();
					 outEdgeIndex < vertices[vertex].EndOut();
					 outEdgeIndex = vertices[vertex].NextOut(outEdgeIndex)) {
				outEdge    = vertices[vertex].out[outEdgeIndex];
				int destVertex0;
				destVertex = edges[outEdge].dest;
				prevLength = edges[outEdge].length;
				
				int beginSrc = edges[outEdge].src;
				followingEdge = outEdge;
				int lengthOffset, lengthOffset0, curIntv, curIntv0;
				lengthOffset = edges[outEdge].length - vertices[destVertex].vertexSize;
				curIntv = edges[outEdge].intervals->size();

				//
				// Count how many new intervals will be added.
				//
				int numSimplePathIntervals = CountIntervalsOnSimplePath(outEdge);
				if (numSimplePathIntervals > edges[outEdge].intervals->size()) {

					//
					// Only bother adding all the new intervals if more exist.
					//
					edges[outEdge].intervals->resize(numSimplePathIntervals);
					while (destVertex != beginSrc and 
								 vertices[destVertex].OutDegree() == 1 and
								 vertices[destVertex].InDegree() == 1) {

						// followingEdge is the next edge on the simple path
						followingEdgeIndex = vertices[destVertex].FirstOut();
						followingEdge      = vertices[destVertex].out[followingEdgeIndex];
						destVertex         = edges[followingEdge].dest;
						int numFollowingEdgeIntervals = edges[followingEdge].intervals->size();
						// Make the intervals on the following edge reference out edge
						MoveIntervals(outEdge, followingEdge, curIntv, lengthOffset);

						curIntv      += numFollowingEdgeIntervals;
						lengthOffset += edges[followingEdge].length - vertices[destVertex].vertexSize;
					}
					//					GrowIntervalsOnSimplePath(followingEdge);
				}
			}
		}
	}	

	// Phase 2. Remove all intervals that were marked as continuations
	// in the previous step.
	//	RemoveMarkedIntervals();
	//	RemoveMarkedPathIntervals();
	//	CheckBalance();
	// Phase 3. Combine intervals along the path into the original edge

	/*
	for (vertex = 0; vertex < vertices.size(); vertex++) { 
		// Found a branching vertex, attempt to simplify paths from it.
		if (vertices[vertex].OutDegree() != 1 or vertices[vertex].InDegree() != 1) {
			for (outEdgeIndex = vertices[vertex].FirstOut();
					 outEdgeIndex < vertices[vertex].EndOut();
					 outEdgeIndex = vertices[vertex].NextOut(outEdgeIndex)) {
				outEdge = vertices[vertex].out[outEdgeIndex];
				destVertex = edges[outEdge].dest;
				prevLength = edges[outEdge].length;
				int beginSrc = edges[outEdge].src;
				int numCombinedIntervals = 0;
				// First count the combined intervals.
				numCombinedIntervals = CollectSimplePathIntervals(outEdge, 0);
				// Then store them
				CollectSimplePathIntervals(outEdge, numCombinedIntervals);
			}
		}
	}	
	*/
	// 	CheckBalance();
	// Phase 4. Merge the edges along the simple path
	// This could be sped up as well.


	// Phase 5. Merge adjacent edges on the simple path.

	for (vertex = 0; vertex < vertices.size(); vertex++) {
		// If this vertex is the beginning of a path, try to simplify it.
		if (vertices[vertex].OutDegree() != 1 or vertices[vertex].InDegree() != 1) {
			for (outEdgeIndex = vertices[vertex].FirstOut();
					 outEdgeIndex < vertices[vertex].EndOut();
					 outEdgeIndex = vertices[vertex].NextOut(outEdgeIndex)) {
				outEdge = vertices[vertex].out[outEdgeIndex];
				destVertex = edges[outEdge].dest;
				prevLength = edges[outEdge].length;
				while (vertices[destVertex].OutDegree() == 1 and
							 vertices[destVertex].InDegree() == 1) {

					// Found an vertex --> destVertex --> some other vertex
					// Find out where this vertex goes.
					followingEdgeIndex = vertices[destVertex].FirstOut();
					followingEdge      = vertices[destVertex].out[followingEdgeIndex];
					followingVertex    = edges[followingEdge].dest;

					MergeSimplePath(destVertex, outEdge, followingEdge);

					// Remove this vertex and the outEdge
					removedVertices.push_back(destVertex);
					removedEdges.push_back(followingEdge);

					// Re-assign the destination edge
					destVertex = followingVertex;
				}
			}
		}
	}
	/*	int e;
		for (e = 0; e < edges.size(); e++ ){ 
		if (edges[e].balancedEdge != -1) {
		assert (edges[e].intervals->size() ==
		edges[edges[e].balancedEdge].intervals->size());
		}
		}
	*/
	//		CheckBalance();
	Prune(removedVertices, removedEdges);
	return removedEdges.size();
}

int IntervalGraph::CollectSimplePathIntervals(int startEdge, int numNewIntervals) {

	int destVertex = edges[startEdge].dest;
	int curIntv = edges[startEdge].intervals->size();
	int storeIntervals = 0;
	int prevNumIntervals = edges[startEdge].intervals->size();
	if (numNewIntervals > 0) {
		storeIntervals = 1;
		numNewIntervals += prevNumIntervals;
		edges[startEdge].intervals->resize(numNewIntervals );
	}
	int nextEdge, nextEdgeIndex;
	int firstSource = edges[startEdge].src;
	int edgeOffset  = edges[startEdge].length - vertices[destVertex].vertexSize;
	while (destVertex != firstSource and
				 vertices[destVertex].OutDegree() == 1 and
				 vertices[destVertex].InDegree() == 1) {
		nextEdgeIndex = vertices[destVertex].FirstOut();
		nextEdge      = vertices[destVertex].out[nextEdgeIndex];
		int nextIntv;
		int nextIntvPath, nextIntvPathPos;
		for (nextIntv = 0; nextIntv < edges[nextEdge].intervals->size(); nextIntv++ ) { 
			if (!IsIntervalMarkedForRemoval(nextEdge, nextIntv)) {
				if (storeIntervals) {
					// We should only be adding edges that start in the next edge.c
					//			assert((*edges[nextEdge].intervals)[nextIntv].pathPos == 0);
					assert(curIntv < numNewIntervals);
					(*edges[startEdge].intervals)[curIntv] = (*edges[nextEdge].intervals)[nextIntv];

					// Fix offsets
					(*edges[startEdge].intervals)[curIntv].edgePos += edgeOffset;
					nextIntvPath = (*edges[nextEdge].intervals)[nextIntv].read;
					nextIntvPathPos = (*edges[nextEdge].intervals)[nextIntv].pathPos;
					paths[nextIntvPath][nextIntvPathPos].edge = startEdge;
					++curIntv;
				}
				else {
					// Simply counting them this time.  Will store on the next pass
					// so that the array may be resized only once.
					++numNewIntervals;
				}
			}
		}
		destVertex = edges[nextEdge].dest;
		edgeOffset += edges[nextEdge].length - vertices[destVertex].vertexSize;
	}

	if (storeIntervals) {
		assert(curIntv == numNewIntervals);
		SortReadIntervalsByReadPos(*edges[startEdge].intervals);
		UpdatePathIndices(startEdge);
	} 

	return numNewIntervals;
}

void IntervalGraph::GrowIntervalsOnSimplePath(int edgeIndex) {
	int intv;
	int numIntervals = edges[edgeIndex].intervals->size();
	int pathIndex, pathPos;
	for (intv = 0; intv < numIntervals; intv++ ){ 
		if (!IsIntervalMarkedForRemoval(edgeIndex, intv)) {
			// search ahead for this path to be extended.
			pathIndex = (*edges[edgeIndex].intervals)[intv].read;
			pathPos   = (*edges[edgeIndex].intervals)[intv].pathPos;
			int p;
			int isSimplePath = 1;
			int destVertex = edges[edgeIndex].dest;
			int nextIntvEdge, nextIntvPos;

			for (p = pathPos + 1; p < pathLengths[pathIndex] and isSimplePath; p++ ) {
				nextIntvEdge = paths[pathIndex][p].edge;
				nextIntvPos  = paths[pathIndex][p].index;
				if (nextIntvEdge < 0 or nextIntvPos < 0)
					continue;
				if ((*edges[nextIntvEdge].intervals)[nextIntvPos].markedForDeletion)
					continue;

				assert(nextIntvEdge >= 0);
				assert(nextIntvPos >= 0);
				(*edges[edgeIndex].intervals)[intv].length += 
					(*edges[nextIntvEdge].intervals)[nextIntvPos].length - 
					vertices[destVertex].vertexSize;

				destVertex = edges[nextIntvEdge].dest;

				MarkIntervalForRemoval(nextIntvEdge, nextIntvPos);

				// Look to see of more of this paht should be extended
				if (vertices[destVertex].InDegree() == 1 and 
						vertices[destVertex].OutDegree() == 1) {
					isSimplePath = 1;
				}
				else {
					isSimplePath = 0;
				}
			}
		}
	}
}

void IntervalGraph::MarkPathsThroughEdgeForRemoval(int edgeIndex) {
	std::vector<int> pathEdges, pathIndices;
	int i;
	// Note: I check to see if an interval is traversed so that
	// I don't re-mark paths for removal.  There is nothing terribly
	// wrong with doing that, and so to save space, the traversed
	// flag may be removed.
	for (i = 0; i < edges[edgeIndex].intervals->size(); i++) {
		if ((*edges[edgeIndex].intervals)[i].read >= 0 and
				(*edges[edgeIndex].intervals)[i].traversed == 0) {
			// This edge has not yet been flagged for removal, do it now
			int readIndex = (*edges[edgeIndex].intervals)[i].read;
			MarkIntervalsOnPathForRemoval(readIndex);
			delete[] paths[readIndex];
			paths[readIndex] = NULL;
			pathLengths[readIndex] = 0;
			//      TracePath(edgeIndex, i, pathEdges, pathIndices);
			//      MarkPathForRemoval(pathEdges, pathIndices);
		}
	}
}

void IntervalGraph::MarkIntervalsInEdgeForRemoval(std::vector<int> &edgeIndices) {
	int e;
	for (e = 0; e < edgeIndices.size(); e++) {
		MarkIntervalsInEdgeForRemoval(edgeIndices[e]);
	}
}

void IntervalGraph::MarkIntervalsInEdgeForRemoval(int edgeIndex) {
	int i;
	int read;
	int pathPos;
	/* 
		 Given an edge, mark all intervals in it for removal.
	*/ 
	//	std::cout << "marking intervals in edge " << edgeIndex << std::endl;
	for (i = 0; i < edges[edgeIndex].intervals->size(); i++ ) { 
		MarkIntervalForRemoval(edgeIndex, i);
	}
}

void IntervalGraph::DiscardGappedPaths() {

	/* 
		 Discard paths that have gaps in them (a path is a series of edges and indexes
		 on the edge ( (e1,i1), (e2,i2), ... , (en,in) ).
		 The path is gapped if one of the pairs is cleared:
		 ((e1,i1), (-1,-1), ... (en,in)).

	*/
	int p, pi;
	int pathIsGapped;
	for (p = 0; p < paths.size(); p++) {
		pathIsGapped = 0;
		for (pi = 0; pi < pathLengths[p]; pi++) {
			/* Either both are not -1, or both are */
			if (paths[p][pi].edge == -1 and 
					paths[p][pi].index == -1) {
				pathIsGapped = 1;
				break;
			}
			else {
				assert(paths[p][pi].edge != -1 and paths[p][pi].index != -1);
			}
		}
		if (pathIsGapped) {
			/*
				std::cout << "path: " << p  << " of len: " << pathLengths[p] << " is gapped " << std::endl;
				for (pi = 0; pi < pathLengths[p]; pi++) {
				std::cout << paths[p][pi].edge << " " << paths[p][pi].index << ", ";
				}
				std::cout << std::endl;
			*/
			/*
				There exists a gap in this path.  There are a few options.
				- Remove this path from existence since it won't help resolving
				any equivalent transformations.
				- Keep the longest stretch of this path.  In some small
				cases this may help, but I don't anticipate that.

				for now,remove the entre path.
			*/

			std::vector<int> pathEdges, pathIndices;

			for (pi = 0; pi < pathLengths[p]; pi++) {
				if (paths[p][pi].edge != -1) {
					pathEdges.push_back(paths[p][pi].edge);
					pathIndices.push_back(paths[p][pi].index);
				}
				else {
					if (pathEdges.size() > 0) {
						/*
							std::cout << "removing subpath: " << std::endl;
							int pei;
							for (pei = 0; pei < pathEdges.size(); pei++) {
							std::cout << " " << pathEdges[pei] << " " << pathIndices[pei] << " ";
							}
							std::cout << std::endl;
						*/
						MarkPathForRemoval(pathEdges, pathIndices);
						pathEdges.clear();
						pathIndices.clear();
					}
				}
				paths[p][pi].edge = -1;
				paths[p][pi].index = -1;
			}
			if (pathEdges.size() > 0) {
				// process the last set of edges, if they exist
				/*
					std::cout << "removing subpath: " << std::endl;
					int pei;
					for (pei = 0; pei < pathEdges.size(); pei++) {
					std::cout << " " << pathEdges[pei] << " " << pathIndices[pei] << " ";
					}
					std::cout << std::endl;
				*/
				MarkPathForRemoval(pathEdges, pathIndices);
				pathEdges.clear();
				pathIndices.clear();
			}
			pathLengths[p] = 0;
		}
	}
	RemoveMarkedIntervals();
	//	RemoveLowCoverageEdges(2);
}

int IntervalGraph::DoesPathRepeat(int readIndex, int pathPos) {
	int p;
	assert(pathPos < pathLengths[readIndex]);
	for (p = 0; p < pathPos-1; p++) {
		if (paths[readIndex][p].edge == paths[readIndex][pathPos].edge)
			return 1;
	}
	return 0;
}


int IntervalGraph::RouteRemovedIntervals(int maxSearchLength) {
	int e, i, p;
	int readIndex;
	int curPathPos;
	int numRouted = 0;
	//	std::cout << "Rerouting removed intervals"  << std::endl;
	UntraverseReadIntervals();
	Untraverse();
	std::vector<int> pathEdges, pathIndices;
	int compReadIndex;
	for (e = 0; e < edges.size(); e++ ) {
		for (i = 0; i < edges[e].intervals->size(); i++) {
			if (! IsIntervalMarkedForRemoval(e, i)) {
				// and
				//					edges[e].intervals[i].traversed == 0) {
				readIndex = (*edges[e].intervals)[i].read;
				/*
					std::cout << "searching for alternate paths for " << e << " " 
					<< edges[e].index << " " << readIndex << std::endl;
					PrintPath(readIndex, std::cout);
				*/
				int compEdge, compIndex;
				int compPathPos, compPathNext;

				// If this graph is balanced, we process the complimentary path
				// at the same time as the forward path.  If this path is 
				// complimentary, wait until the forward path is reached.
				if (isBalanced and !IsForwardRead(readIndex))
					continue;

				compReadIndex = GetCompReadIndex(readIndex);
				// Try and trace this path
				curPathPos = (*edges[e].intervals)[i].pathPos;
				if (curPathPos < pathLengths[readIndex]-1) {
					// This is not the last edge on the path, so it may have been gapped.
					int curPathIndex, curPathEdge;
					assert(readIndex >= 0);
					assert(curPathPos >= 0);
					curPathIndex = paths[readIndex][curPathPos].index;
					curPathEdge =  paths[readIndex][curPathPos].edge;

					assert(curPathEdge != -1);
					assert(curPathIndex != -1);
					if (paths[readIndex][curPathPos+1].edge == -1) {
						// The path is missing some edges.  If it's possible 
						// to re-join this path back to the graph, do so.
						//						std::cout << e << " " << i << " has been deleted! " << std::endl;
						int nextPathPos, nextPathIndex, nextPathEdge;
						for (nextPathPos = curPathPos+2; 
								 nextPathPos < pathLengths[readIndex]; 
								 nextPathPos++ ) {
							if (paths[readIndex][nextPathPos].edge != -1)
								/* 
									 Found the continuation of the path that has
									 not been deleted.  Break now and try and link
									 the path at the previous undeleted pos to next. 
								*/
								break;
						}
						// Only route the paths where the curPathPos and nextPathPos are 
						// within the range 0 .. pathLengths-1, so that part of the 
						// path is known.
						if (nextPathPos < pathLengths[readIndex]) {
							nextPathIndex = paths[readIndex][nextPathPos].index;
							nextPathEdge  = paths[readIndex][nextPathPos].edge;
							// std::cout << "the path resumes at " << nextPathEdge << " " << nextPathIndex << std::endl;
							std::vector<int> altPathEdges;


							if (curPathEdge != nextPathEdge) {
								//std::cout << "searching path for " << readIndex << std::endl;
								int foundAlternatePath = 0;
								int advance = 0;
								int gappedLength;
								int pathRepeats = 0;
								for (advance = 0; advance < 3 
											 and !foundAlternatePath
											 and nextPathPos < pathLengths[readIndex]
											 and paths[readIndex][nextPathPos].edge != -1; nextPathPos++, advance++ ){ 
									if (DoesPathRepeat(readIndex, nextPathPos)) {
										pathRepeats = 1;
										continue;
									}
									nextPathIndex = paths[readIndex][nextPathPos].index;
									nextPathEdge  = paths[readIndex][nextPathPos].edge;
									gappedLength = (*edges[nextPathEdge].intervals)[nextPathIndex].readPos - 
										((*edges[curPathEdge].intervals)[curPathIndex].readPos + 
										 (*edges[curPathEdge].intervals)[curPathIndex].length -
										 vertices[edges[curPathEdge].dest].vertexSize);
									// Search for an alternate path that is of similar length to the original
									// path.  
									if (StoreAlternatePath(curPathEdge, nextPathEdge, 
																				 altPathEdges, (int)( gappedLength * 1.20) )) {
										if (pathRepeats) {
											//std::cout << "found alternate path for cyclic path: " << readIndex << std::endl;
										}
										foundAlternatePath = 1;
										break;
									}
								}
								if (foundAlternatePath) {
									/*std::cout << "found alternate path for " << readIndex << std::endl;*/
									if (isBalanced and IsForwardRead(readIndex)) {
										compReadIndex = GetCompReadIndex(readIndex);
										compPathPos   = pathLengths[compReadIndex] - nextPathPos - 1;
										compPathNext  = pathLengths[compReadIndex] - curPathPos - 1;
										compEdge      = paths[compReadIndex][compPathPos].edge;
										compIndex     = paths[compReadIndex][compPathPos].index;
									}						
									else {
										// reset compliment indices since the graph is not complementary
										// or the complement has already been processed
										compPathPos = compPathNext = compEdge = compIndex = -1;
									}

									int toRemove;
									int toRemoveEdge, toRemoveInterval;
									for (toRemove = curPathPos + 1; toRemove < nextPathPos; toRemove++ ) {
										toRemoveEdge     = paths[readIndex][toRemove].edge;
										toRemoveInterval = paths[readIndex][toRemove].index;
										if (toRemoveEdge >= 0) {
											MarkIntervalForRemoval(toRemoveEdge, toRemoveInterval);
										}
									}
									for (toRemove = compPathPos + 1; toRemove < compPathNext; toRemove++) {
										toRemoveEdge = paths[compReadIndex][toRemove].edge;
										toRemoveInterval = paths[compReadIndex][toRemove].index;
										if (toRemoveEdge >= 0) {
											MarkIntervalForRemoval(toRemoveEdge, toRemoveInterval);
										}
									}

									if (curPathPos+1 == nextPathPos) {
										std::cout << "ERROR: the positions in the read path should not be " << std::endl
															<< " adjacent when replacing a gapped path " << std::endl;
										assert(0);
									}
									if (altPathEdges.size() > 0) {
										numRouted++;
										int newLength, ne;
										newLength = 0;
										for (ne = 0; ne < altPathEdges.size(); ne++) {
											newLength += edges[altPathEdges[ne]].length - 
												vertices[edges[altPathEdges[ne]].dest].vertexSize;
										}

										ReplacePathRangeForward(readIndex,
																						curPathEdge, curPathIndex,
																						curPathPos+1, nextPathPos, altPathEdges);
										assert(CheckPathContinuity(readIndex));

										assert(isBalanced and compReadIndex >= 0);
										std::vector<int> compPathEdges;
										FormComplimentPath(altPathEdges, compPathEdges);
										ReplacePathRangeReverse(compReadIndex, compEdge, compIndex,
																						compPathPos + 1, compPathNext, compPathEdges);
										assert(CheckPathContinuity(compReadIndex));
										if (CheckPathBalance(readIndex, compReadIndex)==0) {
											std::cout << "Tried to replace path range " << compPathPos +1 
																<< " " << compPathNext - 1 << " " << compPathEdges.size() <<" ";
											int c;
											for (c = 0; c < compPathEdges.size(); c++) { std::cout <<" " << compPathEdges[c];}
											std::cout << std::endl;
											PrintImbalancedPaths(readIndex, compReadIndex);
										}

										int ap;
										for (ap = 0; ap < altPathEdges.size(); ap++) {
											assert(edges[altPathEdges[ap]].intervals->size() ==
														 edges[compPathEdges[compPathEdges.size() - ap - 1]].intervals->size());
										}
									}
									else {
										SplicePathRange(readIndex, curPathPos+1, nextPathPos);
										assert(isBalanced and compReadIndex >= 0);
										SplicePathRange(compReadIndex, compPathPos+1, compPathNext);
									}
								}
							}
							else {
								// The deleted section loops back to itself, so just delete the intervening 
								// section
								SplicePathRange(readIndex, curPathPos+1, nextPathPos);

								compReadIndex = GetCompReadIndex(readIndex);
								compPathPos   = pathLengths[compReadIndex] - nextPathPos - 1;
								compPathNext  = pathLengths[compReadIndex] - curPathPos - 1;
								compEdge      = paths[compReadIndex][compPathPos].edge;
								compIndex     = paths[compReadIndex][compPathPos].index;

								assert(isBalanced and compReadIndex >= 0);
								SplicePathRange(compReadIndex, compPathPos, compPathNext);
							}
						} // end if nextPathPos < end
					} // end if curPathPos+1 is gapped 
				} // end if cur path pos is not at end
			} // end checking if edge is deleted
		}
	}
	//SortAllEdgeIntervalsByReadPos();
	//UpdateAllPathIndices();
	return numRouted;
}



void IntervalGraph::SplicePathRange(int readIndex, int spliceStart, int spliceEnd) {

	//
	// Remove spliceStart ... spliceEnd entirely from a path. 
	// This will effectively remove a read from the graph.  Unfortunately, for now
	// the sequence will be a bit messed up, since I doin't actually splice the read
	// I just shift the readPos offsets back.  In the future, I should
	// shift the readPos offsets back, and actually modify the corresponding read.
	assert (spliceEnd > spliceStart);
	PathInterval *newPath;
	int splicedLength = spliceEnd - spliceStart;
	int newPathLength = pathLengths[readIndex] - splicedLength;
	int p;
	int edge, intv;
	// Calculate the length of the read that is deleted.
	// The read position is moved back for all subsequent reads.
	int deletedLength = 0;
	if (spliceEnd == spliceStart) {
		// can't really splice anything here.
		return;
	}
	/*
		if (spliceStart == 0) {
		if (spliceEnd < pathLengths[readIndex]) {
			int edge, index;
			edge = paths[readIndex][spliceEnd].edge;
			index = paths[readIndex][spliceEnd].index;
			if ( edge >= 0 and index >= 0) {
				deletedLength = (*edges[edge].intervals)[index].readPos;
			}
		}
	}
	else {
		if (spliceEnd < pathLengths[readIndex]) {
			assert(spliceStart>0);
			int startEdge  = paths[readIndex][spliceStart-1].edge;
			int startIntv  = paths[readIndex][spliceStart-1].index;
			int destVertex = edges[startEdge].dest;
			int resumeEdge = paths[readIndex][spliceEnd].edge;
			int resumeIntv = paths[readIndex][spliceEnd].index;

			if (startEdge == -1 or resumeEdge == -1) {
				// Something is wrong with this path, remove the entire thing.
				for (p = 0; p < pathLengths[readIndex]; p++) {
					edge = paths[readIndex][p].edge;
					intv = paths[readIndex][p].index;
					if (edge >= 0 && index >= 0 and
							!IsIntervalMarkedForRemoval(edge, intv)) {
						MarkIntervalForRemoval(edge, intv);
					}
				}
				return;
			}

			assert(resumeEdge >= 0);
			assert(resumeIntv >= 0);
			assert(startEdge >= 0);
			assert(startIntv >= 0);
			assert(spliceEnd < pathLengths[readIndex]);
			deletedLength = (*edges[resumeEdge].intervals)[resumeIntv].readPos -
				((*edges[startEdge].intervals)[startIntv].readPos + 
				 (*edges[startEdge].intervals)[startIntv].length - 
				 vertices[destVertex].vertexSize);
		}
		// else, if spliceEnd == readLengths[readIndex]-1, the entire end
		// of the path is deleted, so no readPos offsets are necessary
	}
	*/
	// First remove all intervals corresponding to spliced region.
	for (p = spliceStart; p < spliceEnd; p++) {
		edge = paths[readIndex][p].edge;
		intv = paths[readIndex][p].index;
		if (edge != -1 and intv != -1 and
				!IsIntervalMarkedForRemoval(edge, intv)) {
			MarkIntervalForRemoval(edge, intv);
		}
	}

	for (p = spliceEnd ; p < pathLengths[readIndex]; p++) {
		assert(newPathLength > 0);
		paths[readIndex][p - splicedLength] = paths[readIndex][p];
		edge = paths[readIndex][p].edge;
		intv = paths[readIndex][p].index;
		if (edge >= 0 and intv >= 0 and !IsIntervalMarkedForRemoval(edge, intv)) {
			(*edges[edge].intervals)[intv].pathPos = p - splicedLength;
			//		(*edges[edge].intervals)[intv].readPos -= deletedLength;
		}
	}

	if (newPathLength > 0) {
		//    paths[readIndex] = newPath;
	}
	else {
		paths[readIndex] = NULL;
	}
	pathLengths[readIndex] = newPathLength;
}

int IntervalGraph::FormComplimentPath(std::vector<int> &origPath,
																			std::vector<int> &compPath) {
	// Given orig path, find the path in the graph that
	// traversed all the complimentary edges
	compPath.resize(origPath.size());
	int e;
	for (e = 0; e < origPath.size(); e++) {
		compPath[origPath.size() - e - 1] = edges[origPath[e]].balancedEdge;
	}
}



int IntervalGraph::MakeRoomForEdges(int readIndex, int intvEdge, int intvIndex,
																		int pathStart, int pathEnd,
																		std::vector<int> &newEdges) {
	// This makes room for a range in a path.  The path from pathStart...pathEnd-1
	// gets replaced by newEdges.
	int newSpace;
	newSpace = newEdges.size() - (pathEnd - pathStart);
	int p, e;
	int curReadPos;
	PathInterval *newPath;
	if (newSpace != 0) {
		// Sanity check.  Make sure that some space is being allocated or deleted.
		if (pathLengths[readIndex] + newSpace == 0) {
			std::cout << "Allocating 0 space for a path, this shouldn't happen since we're adding" 
								<< std::endl;
			std::cout << "to paths. Check it out!" << std::endl;
			exit(0);
		}

		// Allocate the new path. This may be longer or shorter 
		// than the old path, depending on how much is being replaced.
		newPath = new PathInterval[pathLengths[readIndex] + newSpace];

		// Copy the old unmodified parts of the path into the newly allocated space.
		for (p = 0; p < pathStart; p++) {
			newPath[p] = paths[readIndex][p];
		}
		for (p = pathEnd; p < pathLengths[readIndex]; p++ ) {
			newPath[p + newSpace] = paths[readIndex][p];
			if (paths[readIndex][p].edge >= 0 && 
					paths[readIndex][p].index >= 0) 
				(*edges[paths[readIndex][p].edge].intervals)[paths[readIndex][p].index].pathPos += newSpace;
		}

		// The old path is not needed any more.
		delete[] paths[readIndex];
		paths[readIndex] = newPath;
	}
	return newSpace;
}

void IntervalGraph::CopyNewEdges(int readIndex, int intvEdge, int intvIndex,
																 int pathStart, std::vector<int> &newEdges) {

	// Copy edges from 'newEdges' into the path 'readIndex' starting at position
	// 'pathStart'. 
	// It is assumed that space has been made in the path for 'newEdges'.

	// Intervals are added to these edges, and initilalized as intervals that cover
	// the entire edge.  They might have to be fixed later.
	int e;
	ReadInterval newInterval;
	int curReadPos;
	curReadPos = (*edges[intvEdge].intervals)[intvIndex].readPos;
	int lastInterval;
	int prevEdgeLength, prevVertexSize;
	int prevEdge, prevInterval;
	prevEdge       = intvEdge;
	if (pathStart > 0) {
		prevEdge       = paths[readIndex][pathStart-1].edge;
		prevInterval   = paths[readIndex][pathStart-1].index;
		prevEdgeLength = (*edges[prevEdge].intervals)[prevInterval].length;
		prevVertexSize = vertices[edges[prevEdge].dest].vertexSize;
	}
	else {
		prevEdge = -1;
		prevInterval = -1;
		prevEdgeLength = 0;
		prevVertexSize = 0;
	}

	for (e = 0; e < newEdges.size(); e++ ) {
		/* 
			 Record which edge this interval is in, and which pos the edge
			 references the interval.

			 Assume the interval uses the entire length of the edge.  This 
			 may be truncated later if it needs to be in order to conform
			 to the positions of the rest of the path.
		*/
		newInterval.read    = readIndex;
		//    newInterval.edge    = -1; // Don't store edges for now.
		newInterval.pathPos = pathStart + e;
		newInterval.length  = edges[newEdges[e]].length;
		newInterval.readPos = curReadPos + prevEdgeLength - prevVertexSize;

		/* This path passes through the beginning of the edge. */
		newInterval.edgePos = 0;

		/* Just make sure something strange isn't happening with the new edge.*/
		assert(newInterval.length > 0);

		/* Add this interval to the edge. */
		edges[newEdges[e]].intervals->push_back(newInterval);
		edges[newEdges[e]].multiplicity++;
		/* 
			 We've added the interval to the edge, so its index is 
			 simply the last one.
		*/

		/* Make the path reference it's spot in the edge.*/
		paths[readIndex][e + pathStart].edge  = newEdges[e];
		paths[readIndex][e + pathStart].index = edges[newEdges[e]].intervals->size()-1;

		/* Store values to use on the next iteration.*/
		prevInterval = edges[newEdges[e]].intervals->size() - 1;
		prevEdge     = newEdges[e];
		prevEdgeLength = (*edges[prevEdge].intervals)[prevInterval].length;
		prevVertexSize = vertices[edges[prevEdge].dest].vertexSize;
		curReadPos     = newInterval.readPos;
	}
}

int IntervalGraph::ReplacePathRangeForward(int readIndex,
																					 int intvEdge, int intvIndex,
																					 int pathStart, int pathEnd,
																					 std::vector<int> &newEdges) {

	/*
		Given a path 'readIndex', replace a subpath with a different path.  
		The path from (pathStart ... pathEnd] is repalced by the path contained 
		in 'newEdges'.
		The rest of the path is filled in.
		If pathStart == pathEnd, this effectively inserts the path 
		before pathStart.

		After replacing the subpath, the lengths of the new path are updated
		so that the end position of the new subpath is 1- the beginning position
		of succeeding path interval.

		Since the path intervals are given, I don't think it's necessary to provide
		intvEdge and intvIndex as parameters, but that needs to be examined later.

		This can be tested with an assert for:
		paths[pathStart].edge == intvEdge  and
		paths[pathStart].index == intvIndex
	*/
	int origPathLength = pathLengths[readIndex];

	ReplacePathRange(readIndex, intvEdge, intvIndex, pathStart, pathEnd, newEdges);

	/*
		Now update the read and path positions.
	*/
	/*
		Update the values to be used on the next iteration.
	*/
	/* If this is the last new edge, offset the length so that 
		 the path reaches the next edge on the path.
	*/
	int lastNewEdge    = paths[readIndex][pathStart + newEdges.size()-1].edge;
	int lastNewIntv    = paths[readIndex][pathStart + newEdges.size()-1].index;
	int lastNewIntvLength = (*edges[lastNewEdge].intervals)[lastNewIntv].length;
	int lastVertexSize = vertices[edges[lastNewEdge].dest].vertexSize;
	int lastReadPos  = (*edges[lastNewEdge].intervals)[lastNewIntv].readPos;

	int nextEdge;
	int nextEdgeInterval;

	if (origPathLength < pathEnd-1) {
		nextEdge         = paths[readIndex][pathStart + newEdges.size()].edge;
		nextEdgeInterval = paths[readIndex][pathStart + newEdges.size()].index;
	}
	else {
		nextEdge = nextEdgeInterval = -1;
		// The entire end of the path was replaced, no need to modify the path length.
		return 1;
	}


	// IF the replaced path joins perfectly with the old path,
	// no interval length adjustments are necessary.
	if (nextEdge != -1 and lastReadPos + lastNewIntvLength - lastVertexSize == 
			(*edges[nextEdge].intervals)[nextEdgeInterval].readPos) {
		return 1;
	}


	// Otherwise, it is necessary to adjust the length of the intervals.


	int oldPathLength = (*edges[nextEdge].intervals)[nextEdgeInterval].readPos -
		((*edges[intvEdge].intervals)[intvIndex].readPos +
		 (*edges[intvEdge].intervals)[intvIndex].length - 
		 vertices[edges[intvEdge].dest].vertexSize);

	int newPathLength = 0;
	int e;
	for (e = 0; e < newEdges.size(); e++ ) {
		newPathLength += edges[newEdges[e]].length;
		newPathLength -= vertices[edges[newEdges[e]].dest].vertexSize;
	}

	int readPos;
	readPos      = (*edges[intvEdge].intervals)[intvIndex].readPos +
		(*edges[intvEdge].intervals)[intvIndex].length - 
		vertices[edges[intvEdge].dest].vertexSize;


	// now fix the lengths of the intervals
	int edge, intv;
	int edgeLength, intervalLength;
	float remainder, exactIntervalLength;
	remainder = 0;

	int endReadPos = (*edges[intvEdge].intervals)[intvIndex].readPos;
	for (e = 0; e < newEdges.size() ; e++) {
		edge = paths[readIndex][pathStart + e].edge;
		intv = paths[readIndex][pathStart + e].index;
		exactIntervalLength = ((((*edges[edge].intervals)[intv].length * 1.0 - 
														 vertices[edges[edge].dest].vertexSize)/ newPathLength) * 
													 oldPathLength + remainder);
		intervalLength = (int) floor(exactIntervalLength);

		// Account for accruing round-off error
		remainder = exactIntervalLength - intervalLength;

		// Fix the edge pos to 
		(*edges[edge].intervals)[intv].readPos = readPos;
		assert(readPos >= 0);
		(*edges[edge].intervals)[intv].readPos = readPos;
		(*edges[edge].intervals)[intv].length = intervalLength + vertices[edges[edge].src].vertexSize;
		readPos  += intervalLength;
		assert(readPos >= 0);
	}

	// the hope is that the read pos ends right where the original 
	// path continued.
	if (readPos != (*edges[nextEdge].intervals)[nextEdgeInterval].readPos) {
		(*edges[edge].intervals)[intv].length -= 
			(readPos - (*edges[nextEdge].intervals)[nextEdgeInterval].readPos);

	}
	return 1;
}

int IntervalGraph::ReplacePathRangeReverse(int readIndex,
																					 int intvEdge, int intvIndex,
																					 int pathStart, int pathEnd,
																					 std::vector<int> &newEdges) {
	ReplacePathRange(readIndex, intvEdge, intvIndex, pathStart, pathEnd, newEdges);

	/*
		Now update the read and path positions.
	*/
	/* Update the values to be used on the next iteration.*/
	/* If this is the last new edge, offset the length so that 
		 the path reaches the next edge on the path.
	*/
	int lastNewIntv     = paths[readIndex][pathStart+newEdges.size()-1].index;
	int lastNewEdge     = paths[readIndex][pathStart+newEdges.size()-1].edge;

	int nextEdge         = paths[readIndex][pathStart + newEdges.size()].edge;
	int nextEdgeInterval = paths[readIndex][pathStart + newEdges.size()].index;

	// IF the replace path joins perfectly with the old path,
	// no interval length adjustments are necessary.
	// The beginning of the spliced in section of the path is guaranteed
	// to fit with the end of the last part of the old path 
	// before the spliced in section.
	if ((*edges[lastNewEdge].intervals)[lastNewIntv].readPos + 
			(*edges[lastNewEdge].intervals)[lastNewIntv].length -
			vertices[edges[lastNewEdge].dest].vertexSize == 
			(*edges[nextEdge].intervals)[nextEdgeInterval].readPos) {
		return 1;
	}

	// Otherwise, it is necessary to adjust the length of the intervals.
	int oldPathLength = (*edges[nextEdge].intervals)[nextEdgeInterval].readPos -
		((*edges[intvEdge].intervals)[intvIndex].readPos + 
		 (*edges[intvEdge].intervals)[intvIndex].length - 
		 vertices[edges[intvEdge].dest].vertexSize);

	int newPathLength = 0;
	int e;
	for (e = 0; e < newEdges.size(); e++ ) {
		newPathLength += edges[newEdges[e]].length;
		newPathLength -= vertices[edges[newEdges[e]].dest].vertexSize;
	}
	int readPos;
	readPos      = (*edges[nextEdge].intervals)[nextEdgeInterval].readPos;

	// now fix the lengths of the intervals, starting at 
	// the END of the new path, so that the length used
	// corresponds to the beginning of the old path
	int edge, intv;
	int edgeLength, intervalLength;
	float remainder, exactIntervalLength;
	remainder = 0;
	int endReadPos = (*edges[intvEdge].intervals)[intvIndex].readPos;
	for (e = newEdges.size()-1; e >= 0 ; e--) {
		edge = paths[readIndex][pathStart + e].edge;
		intv = paths[readIndex][pathStart + e].index;
		exactIntervalLength = ((((*edges[edge].intervals)[intv].length * 1.0 - vertices[edges[edge].src].vertexSize)/ newPathLength) 
													 *oldPathLength + remainder);
		intervalLength = (int) floor(exactIntervalLength);

		// Account for accruing round-off error
		remainder = exactIntervalLength - intervalLength;
		readPos  -= intervalLength;

		// Fix the edge pos to 
		(*edges[edge].intervals)[intv].readPos = readPos;
		(*edges[edge].intervals)[intv].length  = intervalLength + vertices[edges[edge].src].vertexSize;
	}
	// back up the last read interval
	readPos -= (*edges[intvEdge].intervals)[intvIndex].length - vertices[edges[intvEdge].dest].vertexSize;
	// the hope is that the read pos ends right where the original 
	// path continued.
	if (readPos != (*edges[intvEdge].intervals)[intvIndex].readPos) {
		int diff = (*edges[intvEdge].intervals)[intvIndex].readPos- readPos;
		(*edges[edge].intervals)[intv].length -= diff;
		(*edges[edge].intervals)[intv].readPos += diff;
	}
	return 1;
}

int IntervalGraph::ReplacePathRange(int readIndex, 
																		int intvEdge, int intvIndex,
																		int pathStart, int pathEnd,
																		std::vector<int> &newEdges) {
	/* 
		 Replace part of a path starting at pathStart and ending at pathEnd
		 with the edges listed in newEdges.
		 Also, add the path intervals to the all edges in newEdges.
		 For now, assume that the path passes through the entire edge in newEdges.
	*/

	/*
		First fix the size of the path if need be.
	*/

	int newSpace;
	/* 
		 newSpace == 0, no new path needed.
		 < 0, must add space to the old path
		 > 0, must delete from the old path.
	*/
	newSpace = MakeRoomForEdges(readIndex, intvEdge, intvIndex,
															pathStart, pathEnd, newEdges);

	CopyNewEdges(readIndex, intvEdge, intvIndex, pathStart, newEdges);

	pathLengths[readIndex] += newSpace;

	return 1;
}


int IntervalGraph::StoreAlternatePath(int curPathEdge, int nextPathEdge, 
																			std::vector<int> &altPathEdges,
																			int maxSearchLength) {
	return 0;
	std::list<int> altPathEdgeList;
	if (SearchAlternatePath(curPathEdge, nextPathEdge, altPathEdgeList, maxSearchLength)) {
		std::list<int>::iterator listIt;
		altPathEdges.resize(altPathEdgeList.size());
		int i = 0;
		for (listIt = altPathEdgeList.begin();
				 listIt != altPathEdgeList.end();
				 listIt++) {
			altPathEdges[i] = *listIt;
			++i;
		}
		return 1;
	}
	else
		return 0;
}

int IntervalGraph::SearchAlternatePath(int curPathEdge, int nextPathEdge, 
																			 std::list<int> &altPathEdges,
																			 int maxSearchLength,
																			 int curPathLength) {
	/* Input:
		 curPathEdge, curPathIndex - the edge and index of a read interval on that edge.
		 nextPathEdge, nextPathIndex - the continuation of that path on some edge that
		 may or may not be adjacent to curPathEdge.
	*/


	if (curPathEdge == nextPathEdge) {
		return 1;
	}
	else {
		int outEdge, outEdgeIndex;
		int destVertex;
		destVertex = edges[curPathEdge].dest;
		for (outEdgeIndex = vertices[destVertex].FirstOut();
				 outEdgeIndex < vertices[destVertex].EndOut();
				 outEdgeIndex = vertices[destVertex].NextOut(outEdgeIndex)) {
			outEdge = vertices[destVertex].out[outEdgeIndex];

			// No long cycles.
			if (edges[curPathEdge].traversed == GraphEdge::Marked)
				continue;

			// No very short cycles.
			if (vertices[edges[curPathEdge].dest].traversed = GraphVertex::Marked)
				continue;

			if (edges[curPathEdge].dest == edges[nextPathEdge].src and // make sure this is reached from the right direction
					outEdge == nextPathEdge) {
				return 1;
			}
			else {
				/* The out edge is not the continuation of the path
					 we are looking for.  Continue with dfs for nextPathEdge.
				*/

				int nextEdgeLength = edges[outEdge].length - vertices[edges[outEdge].dest].vertexSize;
				/*
					std::cout << "sap " << altPathEdges.size() << " cur: " << curPathLength << " next " 
					<< nextEdgeLength << " max: " << maxSearchLength <<  " " 
					<< edges[outEdge].index << " " << std::endl;
				*/
				if (maxSearchLength > curPathLength  + nextEdgeLength) {
					altPathEdges.push_back(outEdge);
					edges[curPathEdge].traversed = GraphEdge::Marked;
					vertices[edges[curPathEdge].dest].traversed = GraphVertex::Marked;
					if (SearchAlternatePath(outEdge, nextPathEdge, altPathEdges, 
																	maxSearchLength, 
																	curPathLength +  nextEdgeLength)) {
						edges[curPathEdge].traversed = GraphEdge::NotMarked;
						return 1;
					}
					else {
						/* The search didn't find the nextPathEdge within maxSearchLength.
							 Don't use that edge.
						*/
						altPathEdges.pop_back();
						edges[curPathEdge].traversed = GraphEdge::NotMarked;
					}
				}
			}
		}
		/* 
			 All out edges have been tried, and no path works.
			 Return 0 for not found.
		*/
		return 0;
	}
}

void IntervalGraph::UntraverseReadIntervals() {
	int e, i;
	for (e = 0; e < edges.size(); e++) {
		for (i = 0; i < edges[e].intervals->size(); i++ ) {
			(*edges[e].intervals)[i].traversed = 0;
		}
	}
}

void IntervalGraph::DeleteEdgeListReadIntervals(std::vector<int> &edgeList) {
	int edge;
	for (edge = 0; edge < edgeList.size(); edge++) { 
		DeleteEdgeReadIntervals(edgeList[edge]);
	}
}

void IntervalGraph::DeleteEdgeReadIntervals(int edge) {
	int i;
	for (i = 0; i < edges[edge].intervals->size(); i++ ) {
		DeleteReadInterval((*edges[edge].intervals)[i].read,
											 (*edges[edge].intervals)[i].pathPos);
	}
}

void IntervalGraph::DeleteReadInterval(int readIndex, int pathPos) {
	PathInterval *newPath;
	/*
		std::cout << "deleting read interval " << readIndex << " " 
		<< pathPos << " " << pathLengths[readIndex] << std::endl;
	*/
	assert(pathLengths[readIndex] > 0);
	if (pathLengths[readIndex]-1 > 0) {
		newPath = new PathInterval[pathLengths[readIndex]-1];
		if (pathPos > 0)
			std::copy(&(paths[readIndex][0]), 
								&(paths[readIndex][pathPos]), 
								&(newPath[0]));
		if (pathPos < pathLengths[readIndex]-1) {
			std::copy(&(paths[readIndex][pathPos+1]), 
								&(paths[readIndex][pathLengths[readIndex]]),
								&(newPath[pathPos]));
		}
		/* Update the references from the edge back to the path. */
		int p;
		for (p = pathPos+1; p < pathLengths[readIndex]; p++) {
			if (!IsIntervalMarkedForRemoval(paths[readIndex][p].edge, paths[readIndex][p].index)) {
				(*edges[paths[readIndex][p].edge].intervals)[paths[readIndex][p].index].pathPos--;
				assert((*edges[paths[readIndex][p].edge].intervals)[paths[readIndex][p].index].pathPos >= 0);
			}
		}
		delete[] paths[readIndex];
		paths[readIndex] = newPath;
	}
	else {
		delete[] paths[readIndex];
		paths[readIndex] = NULL;
	}
	pathLengths[readIndex]--;
}

void IntervalGraph::RerouteSimplePathIntervals(int vertex, int inEdge, int outEdge) {

	assert(vertices[vertex].InDegree() == 1);

	// First extend all read intervals that pass through the in edge.
	// Read intervals are sorted by read than position on the read.

	int inInterval, outInterval;
	inInterval = 0;
	outInterval = 0;
	int numInIntervals  = edges[inEdge].intervals->size();
	int numOutIntervals = edges[outEdge].intervals->size();
	int numMerged = 0;
	int numOutIntervalsMarkedForRemoval = 0;

	int inPathPos;
	int readIndex;
	int nextEdge, nextEdgeIntv;
	for (inInterval = 0; inInterval < numInIntervals; inInterval++ ) {
		if (!IsIntervalMarkedForRemoval(inEdge, inInterval)) {

			inPathPos = (*edges[inEdge].intervals)[inInterval].pathPos;
			readIndex = (*edges[inEdge].intervals)[inInterval].read;
			if (inPathPos < pathLengths[readIndex]-1) {

				nextEdge     = paths[readIndex][inPathPos+1].edge;
				nextEdgeIntv = paths[readIndex][inPathPos+1].index;

				if (!IsIntervalMarkedForRemoval(nextEdge, nextEdgeIntv)) {
					assert(nextEdge == outEdge);
					assert((*edges[inEdge].intervals)[inInterval].readPos + 
								 (*edges[inEdge].intervals)[inInterval].length - 
								 vertices[edges[inEdge].dest].vertexSize == 
								 (*edges[nextEdge].intervals)[nextEdgeIntv].readPos);

					(*edges[inEdge].intervals)[inInterval].length += 
						(*edges[nextEdge].intervals)[nextEdgeIntv].length - 
						vertices[edges[inEdge].dest].vertexSize;
					assert((*edges[inEdge].intervals)[inInterval].length >= 0);
					++numMerged;

					// This out interval has been merged into the in interval.
					// We could remove it from the list, but that's too much
					// memory management.  Just simply mark the start pos of the 
					// out interval as 0
					int read, pathPos;

					// Flag this interval as being merged
					(*edges[nextEdge].intervals)[nextEdgeIntv].readPos = -1;

					read = (*edges[nextEdge].intervals)[nextEdgeIntv].read;
					pathPos = (*edges[nextEdge].intervals)[nextEdgeIntv].pathPos;

					MarkIntervalForRemoval(nextEdge, nextEdgeIntv);
					DeleteReadInterval(read, pathPos);
				}
			}
		}
	}

	// The intervals that remain in 'outEdge' are not part of 'inEdge'
	// They should be added to 'inEdge', with a starting position
	// to offset for the length of 'inEdge'.

	// Count the number of out intervals that will be skipped.
	int remainingOutIntervals = 0;
	for( outInterval = 0; outInterval < numOutIntervals; ++outInterval) {
		// if read pos < 0, this interval has already been merged
		if (IsIntervalMarkedForRemoval(outEdge, outInterval)) {
			numOutIntervalsMarkedForRemoval++;
		}
		else 
			remainingOutIntervals++;
	}

	assert(numOutIntervals == numOutIntervalsMarkedForRemoval + remainingOutIntervals);

	if (numInIntervals + numOutIntervals == 0) {
		edges[inEdge].intervals->clear();
	}
	else {
		edges[inEdge].intervals->resize(numInIntervals + remainingOutIntervals);
		outInterval = 0;
		int appendedIntervals = 0;
		int outIntervalRead, outIntervalPos;
		for( outInterval = 0; outInterval < numOutIntervals; ++outInterval) {
			// if read pos < 0, this interval has already been merged
			if ((*edges[outEdge].intervals)[outInterval].readPos >= 0) {
				if (!IsIntervalMarkedForRemoval(outEdge, outInterval)) {
					(*edges[inEdge].intervals)[numInIntervals + appendedIntervals].read =
						(*edges[outEdge].intervals)[outInterval].read;

					(*edges[inEdge].intervals)[numInIntervals + appendedIntervals].readPos =
						(*edges[outEdge].intervals)[outInterval].readPos;

					(*edges[inEdge].intervals)[numInIntervals + appendedIntervals].length =
						(*edges[outEdge].intervals)[outInterval].length;

					(*edges[inEdge].intervals)[numInIntervals + appendedIntervals].edgePos =
						edges[inEdge].length + (*edges[outEdge].intervals)[outInterval].edgePos - vertexSize;

					(*edges[inEdge].intervals)[numInIntervals + appendedIntervals].pathPos =
						(*edges[outEdge].intervals)[outInterval].pathPos;
					assert((*edges[inEdge].intervals)[numInIntervals + appendedIntervals].pathPos >= 0);

					/* 
						 Update the path to reference inEdge, and the new slot in inEdge.
					*/
					outIntervalRead = (*edges[outEdge].intervals)[outInterval].read;
					outIntervalPos  = (*edges[outEdge].intervals)[outInterval].pathPos;
					paths[outIntervalRead][outIntervalPos].edge  = inEdge;
					paths[outIntervalRead][outIntervalPos].index = numInIntervals + appendedIntervals;

					++appendedIntervals;
				}
				else {
					std::cout << "Caution!!! interval: " << outEdge << " " << outInterval 
										<< " is marked for removal, and I should have removed everything like that " 
										<< std::endl;
				}
			}
		}
		// sanity check
		//		assert(appendedIntervals == numOutIntervals - numMerged - numOutIntervalsMarkedForRemoval);
		assert(appendedIntervals == remainingOutIntervals);
	}
}

void IntervalGraph::UpdatePathEdges(std::vector<int> &edgesToRemove) {
	/* 
		 Input: a list of edges to remove.
		 Result: paths that reference an edge that is not removed
		 have that edge packed, so if there are edges 1, 2, and 3, 
		 and edge 2 is deleted, paths that reference edge 3 are updated
		 to reference 2.
	*/

	// Nothing needs to be done if nothing is removed.
	if (edgesToRemove.size() == 0) 
		return;

	int edge;
	int numRemovedEdges = 0;
	for (edge = 0; edge < edges.size(); edge++) {
		if (numRemovedEdges < edgesToRemove.size() and
				edge == edgesToRemove[numRemovedEdges]) {
			++numRemovedEdges;
		}
		else {
			int i;
			int read;
			int pathPos;
			for (i = 0; i < edges[edge].intervals->size(); i++ ) {
				if (!IsIntervalMarkedForRemoval(edge,i)) {
					read = (*edges[edge].intervals)[i].read;
					pathPos = (*edges[edge].intervals)[i].pathPos;
					assert(read >= 0);
					assert(read < paths.size());
					assert(pathPos < pathLengths[read]);
					assert(pathPos >= 0);
					assert(paths[read][pathPos].edge >= 0);
					assert(paths[read][pathPos].index >= 0);
					assert(paths[read][pathPos].edge == edge);
					assert(paths[read][pathPos].edge - numRemovedEdges >= 0);
					paths[read][pathPos].edge -= numRemovedEdges;
				}
			}
		}
	}
}

void IntervalGraph::Prune(std::vector<int> &verticesToRemove,
													std::vector<int> &edgesToRemove) {

	std::sort(edgesToRemove.begin(), edgesToRemove.end());
	std::sort(verticesToRemove.begin(), verticesToRemove.end());

	// Do a sanity check on the edges to remove. If any single edge
	// is in the to remove list, then it's balanced edge should be in as well
	int re;
	int balancedEdge;
	std::vector<int>::iterator reIt;
	for (re = 0; re < edgesToRemove.size(); re++ ){
		balancedEdge = edges[edgesToRemove[re]].balancedEdge;
		// We don't want to do a sanity check on edges that
		// do not have balanced parts.

		if (balancedEdge >= 0 and
				std::binary_search(edgesToRemove.begin(), edgesToRemove.end(), balancedEdge) == 0) {
			std::cout << "ERROR! Removing edge "<< edgesToRemove[re] << " but not balanced edge " 
								<< balancedEdge << std::endl;
		}
	}
	if (verticesToRemove.size() > 0) {
		/*
			std::cout << "Deleting " << verticesToRemove.size() 
			<< " vertices out of " << vertices.size()
			<< std::endl;
		*/
		RemoveVertexList(verticesToRemove);
		//    DeleteElementsFromList(vertices, verticesToRemove);
	}
	if (edgesToRemove.size() > 0) {
		RemoveEdgeList(edgesToRemove);
	}
	//	 RemoveTruncatedPathIntervals();
	assert(CheckEdges(vertices, edges));
	//  assert(CheckBalance());
}

void IntervalGraph::RemoveVertexList(std::vector<int> &verticesToRemove) {
	int v;
	int newV, removedV;

	newV = 0;
	removedV = 0;
	int inEdgeIndex, inEdge, outEdgeIndex, outEdge;
	int removedVertices = 0;
	for (v = 0; v < vertices.size(); v++) {
		if (removedV < verticesToRemove.size() and
				v == verticesToRemove[removedV]) {
			while (v < vertices.size() and 
						 removedV < verticesToRemove.size() and 
						 v == verticesToRemove[removedV]) {
				assert(removedV < verticesToRemove.size());
				vertices[v].out.clear();
				vertices[v].in.clear();
				++removedV;
			}
			++removedVertices;
		}
		else {
			vertices[newV] = vertices[v];
			for (inEdgeIndex = vertices[newV].FirstIn();
					 inEdgeIndex != vertices[newV].EndIn();
					 inEdgeIndex = vertices[newV].NextIn(inEdgeIndex)) {
				inEdge = vertices[newV].in[inEdgeIndex];
				edges[inEdge].dest = newV;
			}
			for (outEdgeIndex = vertices[newV].FirstOut();
					 outEdgeIndex != vertices[newV].EndOut();
					 outEdgeIndex = vertices[newV].NextOut(outEdgeIndex)) {
				outEdge = vertices[newV].out[outEdgeIndex];
				edges[outEdge].src = newV;
			}
			newV++;
		}
	}
	if (removedVertices < vertices.size())
		vertices.resize(vertices.size() - removedVertices);
	else
		vertices.clear();
}


void IntervalGraph::RemoveUnlinkedEdges() {
	vector<int> vlist, elist;
	int e;
	for (e = 0; e < edges.size(); e++ ){
		if (edges[e].src == -1) {
			assert(edges[e].dest == -1);
			int i;
			for (i = 0; i < (*edges[e].intervals).size(); i++) {
				if (!(*edges[e].intervals)[i].markedForDeletion) {
					//					cout << (*edges[e].intervals)[i].read << " in unlinked edge. ";
					int pi;
					int path = (*edges[e].intervals)[i].read;
					/*
					for (pi = 0; pi < pathLengths[path]; pi++ ){
						cout << paths[path][pi].edge << " ";
					}
					cout << endl;
					*/
					RemovePath((*edges[e].intervals)[i].read);
				}
			}
			elist.push_back(e);
		}
	}
	RemoveMarkedIntervalsNoPaths();
	Prune(vlist, elist);
}

void IntervalGraph::RemoveEdgeList(std::vector<int> &edgesToRemove) {
	int e, newE, removedE;
	newE = 0;
	removedE = 0;
	int src, srcOutIndex, dest, destInIndex;
	int i;
	int removedEdges = 0;
	for (e = 0; e < edges.size(); e++ ) {
		if (removedE < edgesToRemove.size() and
				e == edgesToRemove[removedE]) {
			++removedEdges;
			while (e < edges.size() and 
						 removedE < edgesToRemove.size() and
						 e == edgesToRemove[removedE]) {
				edges[e].Clear();
				removedE++;
			}
		}
		else {
			if (newE != e) {

				edges[newE] = edges[e];
				src = edges[newE].src;

				srcOutIndex = vertices[src].LookupOutIndex(e);
				assert(srcOutIndex >= 0);
				vertices[src].out[srcOutIndex] = newE;

				dest = edges[newE].dest;
				destInIndex = vertices[dest].LookupInIndex(e);
				assert(destInIndex >= 0);
				vertices[dest].in[destInIndex] = newE;

				int balE;
				balE = edges[newE].balancedEdge;
				if (balE != e) {
					edges[balE].balancedEdge = newE;
				}
				else {
					edges[newE].balancedEdge = newE;
				}

				// update the paths to index the new edge index
				ReadIntervalList *intvList = edges[newE].intervals;
				for (i= 0; i < intvList->size(); i++) { 
					ReadInterval *intvPtr = &((*intvList)[i]);
					if (! intvPtr->markedForDeletion) {
						assert(intvPtr->read >= 0);
						assert(intvPtr->pathPos >= 0);
						paths[intvPtr->read][intvPtr->pathPos].edge = newE;
					}
				}
			}
			++newE;
		}
	}
	if (removedEdges < edges.size()) {
		edges.resize(edges.size() - removedEdges);
	}
	else
		edges.clear();
}

void ErodeLeavesFunctor::operator()(int vertexIndex) {
	vertexIndex;
	int inDegree = (*vertices)[vertexIndex].InDegree();
	int outDegree = (*vertices)[vertexIndex].OutDegree();
	int destVertex, srcVertex;
	int e;
	int balancedIndex;
	int balancedDestVertex, balancedSrcVertex;
	int i;
	if ( outDegree == 0 and inDegree > 0) {
		int inEdge, inEdgeIndex;
		inEdge = (*vertices)[vertexIndex].FirstIn();
		int allShortSources = 1;
		while (inEdge < (*vertices)[vertexIndex].EndIn() and
					 allShortSources) {
			inEdgeIndex = (*vertices)[vertexIndex].in[inEdge];

			if (!(*edges)[inEdgeIndex].IsShort(minLength) or 
					(*vertices)[(*edges)[inEdgeIndex].src].marked == GraphVertex::Marked) {
				allShortSources = 0;
			}
			inEdge = (*vertices)[vertexIndex].NextIn(inEdge);
		}    
		if (allShortSources) {
			inEdge = (*vertices)[vertexIndex].FirstIn();
			while (inEdge < (*vertices)[vertexIndex].EndIn()) {
				inEdgeIndex = (*vertices)[vertexIndex].in[inEdge];
				srcVertex = (*edges)[inEdgeIndex].src;
				(*vertices)[srcVertex].marked = GraphVertex::Marked;
				if ((*edges)[inEdgeIndex].IsShort(minLength) or 
						(*edges)[inEdgeIndex].flagged == GraphEdge::Marked) {
					(*edges)[inEdgeIndex].flagged = GraphEdge::Marked;
					erodedEdgeList.push_back(inEdgeIndex);
					//					(*vertices)[srcVertex].EraseOutEdge(inEdgeIndex);
					if ((*vertices)[vertexIndex].flagged != GraphVertex::Marked)
						erodedVertexList.push_back(vertexIndex);

					(*vertices)[vertexIndex].flagged = GraphVertex::Marked;


					if (graph->isBalanced) {
						int balEdge = (*edges)[inEdgeIndex].balancedEdge;
						int balEdgeSrc = (*edges)[balEdge].src;
						int balEdgeDest = (*edges)[balEdge].dest;
						if ((*vertices)[balEdgeSrc].flagged != GraphVertex::Marked)
							erodedVertexList.push_back(balEdgeSrc);
						(*vertices)[balEdgeSrc].flagged = GraphVertex::Marked;
						// Only delete one edge from the dest per iteration.
						(*vertices)[balEdgeDest].marked = GraphVertex::Marked;

						(*edges)[balEdge].flagged = GraphEdge::Marked;
						erodedEdgeList.push_back(balEdge);

					}
				}
				inEdge = (*vertices)[vertexIndex].NextIn(inEdge);
			}
		}
	}

	if ( inDegree == 0 and outDegree > 0) {
		int outEdge, outEdgeIndex;
		int allShortSinks = 1;
		outEdge = (*vertices)[vertexIndex].FirstOut();
		while (outEdge < (*vertices)[vertexIndex].EndOut() and
					 allShortSinks ) {
			outEdgeIndex = (*vertices)[vertexIndex].out[outEdge];
			if (!(*edges)[outEdgeIndex].IsShort(minLength) or 
					(*vertices)[(*edges)[outEdgeIndex].dest].marked == GraphVertex::Marked) {
				allShortSinks = 0;
			}
			outEdge = (*vertices)[vertexIndex].NextOut(outEdge);
		}
		if (allShortSinks) {
			outEdge = (*vertices)[vertexIndex].FirstOut();
			while (outEdge < (*vertices)[vertexIndex].EndOut()) {
				outEdgeIndex = (*vertices)[vertexIndex].out[outEdge];
				destVertex = (*edges)[outEdgeIndex].dest;
				(*vertices)[destVertex].marked = GraphVertex::Marked;
				if ((*edges)[outEdgeIndex].IsShort(minLength) or
						(*edges)[outEdgeIndex].flagged == GraphEdge::Marked) {
					// Signal these edges and vertices as GONE!
					(*edges)[outEdgeIndex].flagged = GraphEdge::Marked;
					erodedEdgeList.push_back(outEdgeIndex);
					//					(*vertices)[destVertex].EraseInEdge(outEdgeIndex);
					if ((*vertices)[vertexIndex].flagged != GraphVertex::Marked) 
						erodedVertexList.push_back(vertexIndex);
					(*vertices)[vertexIndex].flagged = GraphVertex::Marked;

					if (graph->isBalanced) {
						int balEdge = (*edges)[outEdgeIndex].balancedEdge;
						int balEdgeDest = (*edges)[balEdge].dest;
						int balEdgeSrc  = (*edges)[balEdge].src;
						if ((*vertices)[balEdgeDest].flagged != GraphVertex::Marked)
							erodedVertexList.push_back(balEdgeDest);
						(*vertices)[balEdgeDest].flagged = GraphVertex::Marked;
						(*vertices)[balEdgeSrc].marked = GraphVertex::Marked;
						(*edges)[balEdge].flagged = GraphEdge::Marked;
						erodedEdgeList.push_back(balEdge);
					}
				}
				outEdge = (*vertices)[vertexIndex].NextOut(outEdge);
			}
		}

		// Do a sanity check on the balanced edges
		balancedIndex = (*edges)[outEdgeIndex].balancedEdge;
		balancedDestVertex = (*edges)[balancedIndex].dest;
		/*
			if (balancedIndex != outEdgeIndex and balancedDestVertex > vertexIndex)
			assert((*vertices)[balancedDestVertex].OutDegree() == 0 );
		*/


	}
	if (inDegree == 0 and outDegree == 0) {
		(*vertices)[vertexIndex].flagged = GraphVertex::Marked;
		erodedVertexList.push_back(vertexIndex);
	}
}


void IntervalGraph::RemoveEdgeAndMarkPathsForRemoval(int edge,
																										 std::vector<int> &removedVertices) {

	MarkPathsThroughEdgeForRemoval(edge);
	RemoveEdge(edge, removedVertices);
}


void IntervalGraph::RemoveEdgeAndMarkIntervalsForRemoval(std::vector<int> &edgeList,
																												 std::vector<int> &removedVertices) {
	int e;
	for (e = 0; e < edgeList.size(); e++ ) {
		MarkIntervalsInEdgeForRemoval(edgeList[e]);
		RemoveEdge(edgeList[e], removedVertices);
	}
}

void IntervalGraph::RemoveEdgeAndMarkIntervalsForRemoval(int edge,
																												 std::vector<int> &removedVertices) {

	MarkIntervalsInEdgeForRemoval(edge);
	RemoveEdge(edge, removedVertices);
}


void IntervalGraph::RemoveEdge(int edge, std::vector<int> &removedVertices) {
	int sourceVertex, destVertex;
	int outIndex, inIndex;
	sourceVertex = edges[edge].src;
	destVertex   = edges[edge].dest;
	// Detach this edge if necessary
	if (sourceVertex >= 0) {
		outIndex = vertices[sourceVertex].LookupOutIndex(edge);
		assert(outIndex >= 0);
		vertices[sourceVertex].out[outIndex] = -1;
	}

	// Detach this edge from dest if necessary
	if (destVertex >= 0) {
		inIndex  = vertices[destVertex].LookupInIndex(edge);
		assert(inIndex >= 0);
		vertices[destVertex].in[inIndex] = -1;
	}

	if (sourceVertex >= 0 and
			vertices[sourceVertex].OutDegree() == 0 and 
			vertices[sourceVertex].InDegree() == 0) {
		//    std::cout << "adding " << sourceVertex  << std::endl;
		removedVertices.push_back(sourceVertex);
	}

	if (destVertex >= 0 and 
			vertices[destVertex].InDegree() == 0 and
			vertices[destVertex].OutDegree() == 0) {
		//    std::cout << "adding " << destVertex << std::endl;
		removedVertices.push_back(destVertex);
	}
	edges[edge].src = -1;
	edges[edge].dest = -1;
	/*  RemoveAllPathsThroughEdge(edge);*/
}

void IntervalGraph::RemoveAllButMST() {
	int e;
	int v;
	GraphAlgo::CalcMST(vertices, edges);  
	std::vector<int> mstEdges;
	std::cout << "mst: ";
	for (e = 0; e < edges.size(); e++ ) {
		if (edges[e].mst == GraphAlgo::MSTIn and 
				edges[edges[e].balancedEdge].mst != GraphAlgo::MSTIn) {
			// Fix the graph to add back balanced edges
			edges[edges[e].balancedEdge].mst = GraphAlgo::MSTIn;
		}
		if (edges[e].mst == GraphAlgo::MSTIn) {
			mstEdges.push_back(e);
		}
	}
	std::cout << std::endl;
	if (CheckBalancedEdgeList(edges, mstEdges) ) {
		std::cout << "mst edges are BALANCED " << std::endl;
	}
	else {
		std::cout << "mst edges are NOT balanced " << std::endl;
	}

	std::vector<int> removedVertices, removedEdges;
	for (e = 0; e < edges.size(); e++ ) {
		if (edges[e].mst != GraphAlgo::MSTIn) {
			RemoveEdge(e, removedVertices);
			removedEdges.push_back(e);
		}
	}
	Prune(removedVertices, removedEdges);
	CondenseSimplePaths();
	Erode(60);
}



void IntervalGraph::Erode(int minEdgeLength) {
	ErodeLeavesFunctor erodeLeaves;

	erodeLeaves.vertices = &vertices;
	erodeLeaves.edges    = &edges;
	erodeLeaves.graph    = this;
	erodeLeaves.minLength = minEdgeLength;
	std::vector<int> erodedEdgeList, erodedVertexList;
	int iteration = 0;
	std::vector<int> skippedEdges, skippedVertices;
	int e;
	int numCondensedEdges = 0;
	do {
		++iteration;
		erodeLeaves.Clear();
		erodedVertexList.clear();
		erodedEdgeList.clear();
		Unmark();
		CheckGraphStructureBalance();
		TraverseRandomAccess(vertices, edges, erodeLeaves);
		int e, i;

		std::sort(erodeLeaves.erodedEdgeList.begin(),
							erodeLeaves.erodedEdgeList.end());
		int e2;

		assert(CheckEdges(vertices, edges));
		for (e = 0; e < erodeLeaves.erodedEdgeList.size(); e++ ) {
			if (e == 0 or 
					e > 0 and erodeLeaves.erodedEdgeList[e] != erodeLeaves.erodedEdgeList[e-1]) {
/*				cout << "removing edge (erode ): " << e << " " << edges[e].src << " -> " << edges[e].dest
						 << "  [" << vertices[edges[e].src].index << "] ->  [" 
						 << vertices[edges[e].dest].index << "] " << endl;
*/
				MarkIntervalsInEdgeForRemoval(erodeLeaves.erodedEdgeList[e]);
				//				 DeleteEdgeReadIntervals(erodeLeaves.erodedEdgeList[e]);
				RemoveEdge(erodeLeaves.erodedEdgeList[e], erodedVertexList);
			}
		}
		assert(CheckEdges(vertices, edges));
		assert(CheckGraphStructureBalance());
		std::cout << "removing " << erodeLeaves.erodedEdgeList.size() << " short edges." << std::endl;
		Prune(erodedVertexList, erodeLeaves.erodedEdgeList);
		assert(CheckEdges(vertices, edges));

		skippedVertices.clear();
		skippedEdges.clear();
		//    CheckBalance(edges);

		assert(CheckGraphStructureBalance());
		//		RemoveMarkedIntervals();
		//		 numCondensedEdges+= CondenseSimplePaths();

		// Do a sanity check.  I might turn this off in non-debugging mode.
		CheckBalance();
		//    ClearMST();
	} while (erodedEdgeList.size() > 0);
	/*
	if (containsIntervals)
		RemoveEmptyEdges();
	*/
	numCondensedEdges = CondenseSimplePaths();
	assert(CheckAllPathsBalance(1));
	std::cout << "End of erode, condensed " << numCondensedEdges << " edges." << std::endl;
	assert(CheckGraphStructureBalance());
	RemoveErasedPaths();
}

int IntervalGraph::CheckBalance() {
	int e;
	int balE;
	for (e = 0; e < edges.size(); e++ ) {
		balE = edges[e].balancedEdge;
		if (edges[balE].balancedEdge != e) {
			std::cout << "edges " << e <<" " << balE << " are not balanced " << std::endl;
			return 0;
		}
		else if (edges[balE].intervals->size() != edges[e].intervals->size()) {
			std::cout << "edges " << e << " (" << edges[e].index << ") " 
								<< balE << " ( " << edges[balE].index 
								<< ")  should have equal multiplicity " 
								<< edges[balE].intervals->size() << " " <<  edges[e].intervals->size() 
								<< " indices: " << edges[e].index << " " << edges[balE].index 
								<< std::endl;
			return 0;
		}
	}
	return 1;
}

void IntervalGraph::ClearMST() {
	int e;
	for (e = 0; e < edges.size(); e++ ) {
		edges[e].mst = GraphAlgo::MSTOut;
	}
}

int IntervalGraph::RemoveAllPathsThroughEdge(int edge) {
	int interval = 0;
	int prevSize;
	std::vector<int> pathEdges, pathIntervalIndices;
	//  std::cout << "removing paths through " << edge << std::endl;

	while (edges[edge].intervals->size() > 0)  {  
		// clean up from previous run
		pathEdges.clear();
		pathIntervalIndices.clear();

		// find the path that contains the 0'th interval in edges[e].intervals  
		prevSize = edges[edge].intervals->size();
		TracePath(edge, 0, pathEdges, pathIntervalIndices);  
		// remove it  
		MarkPathForRemoval(pathEdges,pathIntervalIndices);
		// sanity check, the size of the interval set shrank by at least 1
		assert(edges[edge].intervals->size() < prevSize);
	}
}


int IntervalGraph::TracePathReverse(int curEdge, int curIndex, int &prevEdge, int &prevIndex) {
	int inEdgeIndex, inEdge;
	prevEdge = -1;
	prevIndex = -1;
	int sourceVertex;
	sourceVertex = edges[curEdge].src;
	//  std::cout << "tracing path back through " << vertices[sourceVertex].InDegree() << std::endl;
	if (IsIntervalMarkedForRemoval(curEdge, curIndex))
		return 0;

	for (inEdgeIndex = vertices[sourceVertex].FirstIn();
			 inEdgeIndex < vertices[sourceVertex].EndIn();
			 inEdgeIndex = vertices[sourceVertex].NextIn(inEdgeIndex)) {
		inEdge = vertices[sourceVertex].in[inEdgeIndex];
		prevIndex = TraceReadIntervalReverse(edges[curEdge], curIndex, edges[inEdge], vertexSize);
		if ( prevIndex >= 0) {
			prevEdge = inEdge;
			return 1;
		}
	}
	return (inEdgeIndex < vertices[sourceVertex].EndIn());
}

int IntervalGraph::RemoveMarkedIntervalsNoPaths() {
	// Simply remove intervals marked for deletion.  This assumes
	// the paths corresponding to these are removed by another 
	// method.

	int e, i;
	int numRemoved;
	for (e = 0; e < edges.size(); e++) {
		numRemoved = 0;
		for (i = 0 ; i < edges[e].intervals->size(); i++ ) {
			if (IsIntervalMarkedForRemoval(e, i)) 
				numRemoved++;
			else {
				(*edges[e].intervals)[i-numRemoved] = (*edges[e].intervals)[i];
			}
		}
		if (numRemoved < (*edges[e].intervals).size())
			(*edges[e].intervals).resize(edges[e].intervals->size() - numRemoved);
		else
			edges[e].intervals->clear();
		UpdatePathIndices(e);
	}
}


int IntervalGraph::RemoveMarkedIntervals() {
	int e, i;
	int numRemoved;
	/*
		This is called after various paths in the graph have been
		marked for removal.  This removes those paths from the graph,
		and packs the remaining lists of paths.
	*/
	for (e = 0; e < edges.size(); e++) {
		numRemoved = 0;
		for (i = 0 ; i < edges[e].intervals->size(); i++ ) {
			if (IsIntervalMarkedForRemoval(e, i)) 
				numRemoved++;
			else {
				int pathPos = (*edges[e].intervals)[i].pathPos;
				assert(pathPos >= 0);
				paths[(*edges[e].intervals)[i].read][pathPos].index = i - numRemoved;
				(*edges[e].intervals)[i-numRemoved] = (*edges[e].intervals)[i];
			}
		}
		if (numRemoved < (*edges[e].intervals).size())
			(*edges[e].intervals).resize(edges[e].intervals->size() - numRemoved);
		else
			edges[e].intervals->clear();
	}
}

int IntervalGraph::IsEdgeInDisjoint(int edge, int minSpanningPaths) {
	int numIntv = edges[edge].intervals->size();
	int i;
	int numSpanningPaths = 0;
	for (i = 0; i < numIntv; i++ ) {
		if ((*edges[edge].intervals)[i].pathPos > 0)
			++numSpanningPaths;
	}
	if (numSpanningPaths >= minSpanningPaths)
		return 0;
	else
		return 1;
}

int IntervalGraph::IsEdgeOutDisjoint(int edge, int minSpanningPaths) {
	int numIntv = edges[edge].intervals->size();
	int i;
	int pathIndex, pathPos;
	int numSpanningPaths = 0;
	for (i = 0; i < numIntv; i++) {
		pathIndex = (*edges[edge].intervals)[i].read;
		pathPos   = (*edges[edge].intervals)[i].pathPos;
		if (pathPos < pathLengths[pathIndex] - 1)
			++numSpanningPaths;
	}
	if (numSpanningPaths >= minSpanningPaths)
		return 0;
	else
		return 1;
}

void IntervalGraph::RemovePath(int path) {
	MarkPathForRemoval(path);
	assert(paths[path] != NULL);
	pathLengths[path] = 0;
	delete[] paths[path];
	paths[path] = NULL;
}

void IntervalGraph::MarkPathIntervalsForRemoval(int path) {
	MarkPathForRemoval(path);
}

void IntervalGraph::MarkPathForRemoval(int path) {
	int i;
	for (i = 0; i < pathLengths[path]; i++ ){ 
		MarkIntervalForRemoval(paths[path][i].edge, paths[path][i].index);
	}
}


int IntervalGraph::MarkPathRangeForRemoval(int pathIndex, int start, int end) {

	int i;
	for (i = start; i < end; i++ ){ 
		(*edges[paths[pathIndex][i].edge].intervals)[paths[pathIndex][i].index].markedForDeletion = 1;
		//		MarkIntervalForRemoval(paths[pathIndex][i].edge, paths[pathIndex][i].index);
	}
	return 0;
}


int IntervalGraph::MarkPathForRemoval(std::vector<int> &pathEdges,
																			std::vector<int> &pathIndices) {
	int i;
	for (i = 0; i < pathEdges.size(); i++ ) {
		assert(pathIndices[i] < edges[pathEdges[i]].intervals->size());
		MarkIntervalForRemoval(pathEdges[i], pathIndices[i]);
	}
}

int IntervalGraph::MarkIntervalsOnPathForRemoval(int path) {
	int pi;
	for (pi = 0; pi < pathLengths[path]; pi++) {
		if (paths[path][pi].edge >= 0) {
			edges[paths[path][pi].edge].MarkIntervalForRemoval(paths[path][pi].index);
		}
	}
}


void IntervalGraph::RemoveMarkedPathIntervals() {
	int p, pi;
	for (p = 0; p < paths.size(); p++ ) {
		int curPi = 0;
		for (pi = 0; pi < pathLengths[p]; pi++ ){
			if (paths[p][pi].index != -1) {
				paths[p][curPi].index = paths[p][pi].index;
				paths[p][curPi].edge = paths[p][pi].edge;
				(*edges[paths[p][pi].edge].intervals)[paths[p][pi].index].pathPos = curPi;
				++curPi;
			}
		}				
		pathLengths[p] = curPi;
	}
}

int IntervalGraph::RemovePath(std::vector<int> &pathEdges,
															std::vector<int> &pathIndices) {
	int i, j;
	for (i = 0; i < pathEdges.size(); i++ ) {
		assert(pathIndices[i] < edges[pathEdges[i]].intervals->size());
		edges[pathEdges[i]].intervals->erase(edges[pathEdges[i]].intervals->begin() + pathIndices[i]);
		// Now fix any indices of intervals that pass through this edge again.  If they do
		// the size of the interval list is smaller, so we delete one earlier.

		// This is a slightly slow way of doing this, but oh well, these lists are small
		for (j = i+1; j < pathEdges.size(); j++ ) {
			if (pathEdges[i] == pathEdges[j] and pathIndices[j] > pathIndices[i])
				pathIndices[j]--;
		}
	}
}

int IntervalGraph::CutDisjointEdges(int minPathSpan) {
	int e;
	int numCut = 0;
	TVertexList newVertices;
	int appVertexIndex = vertices.size();
	int newVertexIndex = 0;
	TVertex tmpV;
	for (e = 0; e < edges.size(); e++) { 
		if (vertices[edges[e].src].InDegree() > 0) {
			if (IsEdgeInDisjoint(e, minPathSpan)) {
				// cut this edge from the source vertex
				vertices[edges[e].src].EraseOutEdge(e);
				edges[e].src = appVertexIndex;
				newVertices.push_back(tmpV);
				newVertices[newVertexIndex].AddOutEdge(e);
				cout << "cutting out edge " << e << " of length: " << edges[e].length << endl;
				++appVertexIndex;
				++newVertexIndex;
				++numCut;
			}
		}
		if (vertices[edges[e].dest].OutDegree() > 0) {
			if (IsEdgeOutDisjoint(e, minPathSpan)) {
				vertices[edges[e].dest].EraseInEdge(e);
				edges[e].dest = appVertexIndex;
				newVertices.push_back(tmpV);
				newVertices[newVertexIndex].AddInEdge(e);
				cout << "cutting in edge " << e << " of length " << edges[e].length << endl;
				++appVertexIndex;
				++newVertexIndex;
				++numCut;
			}
		}
	}
	int v;
	appVertexIndex = vertices.size();
	vertices.resize(vertices.size() + newVertices.size());
	for (v = 0; v < newVertices.size(); v++ ) {
		vertices[appVertexIndex] = newVertices[v];
		appVertexIndex++;
	}
	CondenseSimplePaths();
	return numCut;
}


void IntervalGraph::AssignIntervalPathOrder() {
	int e, i, p;
	std::vector<int> pathEdges, pathIndices;
	pathLengths.resize(maxReadIndex+ 1);
	std::fill(pathLengths.begin(), pathLengths.end(), 0);
	paths.resize(maxReadIndex+1);
	int readIndex;
	std::cout << "assigning path orders " << std::endl;
	for (e = 0; e < edges.size(); e++) {
		for (i = 0; i < edges[e].intervals->size(); i++) {
			if ((*edges[e].intervals)[i].pathPos == -1) {

				TracePath(e, i, pathEdges, pathIndices);

				assert(pathEdges.size() > 0);
				readIndex = (*edges[e].intervals)[i].read;
				pathLengths[readIndex] = pathEdges.size();
				paths[readIndex] = new PathInterval[pathEdges.size()];

				for (p = 0; p < pathEdges.size(); p++ ) {
					(*edges[pathEdges[p]].intervals)[pathIndices[p]].pathPos = p;
					paths[readIndex][p].edge = pathEdges[p];
					paths[readIndex][p].index = pathIndices[p];
				}

				pathEdges.clear();
				pathIndices.clear();
			}
		}
	}
	std::cout << "done." << std::endl;
}




int IntervalGraph::TracePath(int curEdge, int curIndex, 
														 std::vector<int> &pathEdges,
														 std::vector<int> &pathIndices) {
	int curEdgeCopy = curEdge;
	int curIndexCopy = curIndex;
	int nextEdge, nextIndex;
	int prevEdge = -1;
	int prevIndex = -1;
	int printInterval = 0;
	if (IsIntervalMarkedForRemoval(curEdge, curIndex))
		return 0;

	/* 
		 Starting at some random point in a path (all paths through an edge) 
		 follow the path backwards (to the beginning) and forwards (to the end).
		 Store the paths.
	*/		 
	StorePathReverse(curEdge, curIndex, pathEdges, pathIndices);
	curEdge = curEdgeCopy;
	curIndex = curIndexCopy;
	(*edges[curEdge].intervals)[curIndex].traversed = 1;
	pathEdges.push_back(curEdge);
	pathIndices.push_back(curIndex);

	StorePathForwards(curEdge, curIndex, pathEdges, pathIndices);

	if (printInterval) {
		int i;
		for (i = 0; i < pathEdges.size(); i++) {
			std::cout << pathEdges[i] << " " << pathIndices[i] << std::endl;
		}
	}
}

int IntervalGraph::CalcEdgeMultiplicityStats(float &intervalsPerNucleotide) {

	int totalLength; 		
	int totalIntervals;
	int e,i;
	totalIntervals = 0;
	totalLength    = 0;
	for (e = 0; e < edges.size(); e++) {
		totalIntervals += edges[e].multiplicity;
		totalLength    += edges[e].length;
	}
	intervalsPerNucleotide =  totalIntervals / (1.0*totalLength);
}

int IntervalGraph::RemoveEmptyEdges() {
	int e;
	std::vector<int> edgesToRemove, verticesToRemove;
	for (e = 0; e < edges.size() ; e++ ) {
		if (edges[e].intervals->size() == 0 and !RemovingEdgeCutsGraph(e) and 
				edges[edges[e].balancedEdge].intervals->size() and
				!RemovingEdgeCutsGraph(edges[e].balancedEdge)) {
			edgesToRemove.push_back(e);
			RemoveEdge(e, verticesToRemove);
		}
	}
	Prune(verticesToRemove, edgesToRemove);
	return edgesToRemove.size();
}

int IntervalGraph::RemoveEmptyVertices() {
	std::vector<int> verticesToRemove, blank;
	int v;
	for (v = 0; v < vertices.size(); v++ ){ 
		if (vertices[v].InDegree() == 0 and vertices[v].OutDegree() == 0) 
			verticesToRemove.push_back(v);
	}
	Prune(verticesToRemove, blank);
	return verticesToRemove.size();
}

int IntervalGraph::RemoveLowCoverageEdges(float lowCoverageStddev, int absoluteCutoff) {
	int e;
	std::vector<int> edgesToRemove, verticesToRemove;
	int removedAnEdge = 1;
	int iter = 0;
	int prevRemoveSize;
	/* 
		 Removing some edges lowers the multiplicity of others.  
		 Keep removing edges until none are removed (removedAnEdge==0).
	*/
	int srcVertex, destVertex;

	float avgMultip, stddevMultip;
	CalcEdgeMultiplicityStats(avgMultip);

	while (removedAnEdge) {
		edgesToRemove.clear();
		verticesToRemove.clear();
		++iter;
		do {
			UntraverseReadIntervals();
			prevRemoveSize = edgesToRemove.size();
			int removedEdge = 0;
			for (e = 0; e < edges.size(); e++ ) {
				if (edgesToRemove.size() > 0 and 
						removedEdge < edgesToRemove.size() and 
						e == edgesToRemove[removedEdge]) 
					++removedEdge;
				else {
					int lowCov = 1;
					if (edges[e].IsUnexpectedlyLowCoverage(avgMultip, 
																								 lowCoverageStddev, 
																								 absoluteCutoff,
																								 (int)(vertexSize*3))
							or edges[e].intervals->size() < lowCov) {
						// The edge doesn't have enough reads mapped to it.
						// Maybe remove it.

						// Don't keep empty edges
						/*
						if (edges[e].intervals->size() == 0) {
														edgesToRemove.push_back(e);
						}
						*/
						// Make sure that removing this edge doesn't
						// cut the graph in two
						if (!RemovingEdgeCutsGraph(e)  || edges[e].intervals->size() < lowCov) {
							// Check to see if this edge creates a short cycle. 
							// We trust those edges, and do special cycle-straightening later on.
							int doe, doi;
							int formsCycle = 0;
							for (doi = vertices[edges[e].dest].FirstOut();
									 doi < vertices[edges[e].dest].EndOut();
									 doi = vertices[edges[e].dest].NextOut(doi)) {
								doe = vertices[edges[e].dest].out[doi];
								if (edges[doe].dest == edges[e].src) {
									//std::cout << "edge " << e << " forms a short cycle"
									// << edges[e].length << std::endl;
									formsCycle = 1;
								}
							}
							if (!formsCycle) {
								edgesToRemove.push_back(e);
							}
						}
					}
				}
			}
			std::sort(edgesToRemove.begin(), edgesToRemove.end());
		} while (prevRemoveSize < edgesToRemove.size());
		std::cout << "Remove low coverage, iter: " << iter << " " 
							<< edgesToRemove.size() << " edges " << std::endl;

		for (e = 0; e < edgesToRemove.size(); e++) {
/*			cout << "removing edge (low cov): " << e << " " << edges[e].src << " -> " << edges[e].dest
					 << "  [" << vertices[edges[e].src].index << "] ->  [" 
					 << vertices[edges[e].dest].index << "] " << endl;	
*/
			RemoveEdgeAndMarkPathsForRemoval(edgesToRemove[e], verticesToRemove);
		}

		// Remove all intervals that were marked
		// when an edge was axed.  The paths have already been
		// removed, so don't bother removing those here.
		//		RemoveMarkedIntervalsNoPaths();
		Prune(verticesToRemove, edgesToRemove);
		//		assert(CheckPathContinuity());
		//    int numCondensed = CondenseSimplePaths();
		assert(CheckAllPathsBalance(1));

		//		UntraverseReadIntervals();
		// All the paths should have been removed already.
		//		RemoveMarkedIntervals();
		if (vertices.size() > 0) 
			Erode(vertices[0].vertexSize * 3);

		if (edgesToRemove.size() > 0)
			removedAnEdge = 1;
		else
			removedAnEdge = 0;
	}
	int numCondensed = CondenseSimplePaths();
	
	std::cout << "After removing low coverage edges, condensed " 
						<< numCondensed << " edges." << std::endl;
	return 1;
}


int IntervalGraph::TraceBackEdges(int startVertex, int endVertex, int backEdges[], 
																	std::vector<int> &path) {
	int curVertex = endVertex;
	while (curVertex != startVertex) {
		path.push_back(backEdges[curVertex]);
		curVertex = edges[backEdges[curVertex]].src;
	}
	// Reverse the order of the path.	
	int i;
	int middle = path.size() / 2;
	int length = path.size();
	int temp;
	for (i = 0; i < middle; i++) {
		temp = path[i];
		path[i] = path[length - 1 - i];
		path[length - 1 - i] = temp;
	}
	return path.size();
}

int IntervalGraph::StorePathReverse(int curEdge, int curIndex,
																		std::vector<int> &pathEdges,
																		std::vector<int> &pathIndices) {
	int prevEdge, prevIndex;
	do {
		if (IsIntervalMarkedForRemoval(curEdge, curIndex))
			break;
		TracePathReverse(curEdge, curIndex, prevEdge, prevIndex);
		if (prevEdge >= 0) {
			(*edges[prevEdge].intervals)[prevIndex].traversed = 1;
			pathEdges.insert(pathEdges.begin(),prevEdge);
			pathIndices.insert(pathIndices.begin(),prevIndex);
		}
		curEdge = prevEdge;
		curIndex = prevIndex;
	} while (curEdge >= 0 and curIndex >= 0);
	return pathEdges.size();
}

int IntervalGraph::StorePathForwards(int curEdge, int curIndex,
																		 std::vector<int> &pathEdges,
																		 std::vector<int> &pathIndices) {
	int nextEdge, nextIndex;
	do {
		// Don't try and add the current edge if it is already marked
		// for removal.
		if (IsIntervalMarkedForRemoval(curEdge, curIndex))
			break;

		TracePathForwards(curEdge, curIndex, nextEdge, nextIndex);
		if (nextEdge >= 0 and nextIndex >= 0) {
			(*edges[nextEdge].intervals)[nextIndex].traversed = 1;
			pathEdges.push_back(nextEdge);
			pathIndices.push_back(nextIndex);
		}
		curEdge = nextEdge;
		curIndex = nextIndex;
	} while (curEdge >= 0 and curIndex >= 0);
	return pathEdges.size();
}

int IntervalGraph::TracePathForwards(int curEdge, int curIndex, int &nextEdge, int &nextIndex) {
	int outEdgeIndex, outEdge;
	nextEdge = -1;
	nextIndex = -1;
	int destVertex;
	//  std::cout << "tracing path forwards through " << vertices[destVertex].OutDegree() << std::endl;
	// if the edge is a self-loop, it's possible the path continues on the same edge
	destVertex = edges[curEdge].dest;
	if (IsIntervalMarkedForRemoval(curEdge, curIndex))
		return 0;

	if (curIndex < edges[curEdge].intervals->size()-1 and
			(*edges[curEdge].intervals)[curIndex].read == (*edges[curEdge].intervals)[curIndex+1].read and
			(*edges[curEdge].intervals)[curIndex].readPos + 
			(*edges[curEdge].intervals)[curIndex].length - vertices[destVertex].vertexSize
			== (*edges[curEdge].intervals)[curIndex+1].readPos) {
		nextEdge = curEdge;
		nextIndex = curIndex + 1;
	}

	// the next index isn't the succeeding index, so it's in the next edge
	destVertex = edges[curEdge].dest;
	for (outEdgeIndex = vertices[destVertex].FirstOut();
			 outEdgeIndex < vertices[destVertex].EndOut();
			 outEdgeIndex = vertices[destVertex].NextOut(outEdgeIndex)) {
		outEdge = vertices[destVertex].out[outEdgeIndex];
		nextIndex = TraceReadIntervalForward(edges[curEdge], curIndex, edges[outEdge], vertices[destVertex].vertexSize);
		if (curEdge == outEdge and 
				nextIndex == curIndex) {
			std::cout << "TRACING stayed in the same spot, that's not good!\n";
			exit(1);
			nextEdge = -1;
			return 0;
		}
		if ( nextIndex >= 0) {
			nextEdge = outEdge;
			return 1;
		}
	}
	return outEdgeIndex < vertices[destVertex].EndOut();
}

template<typename V, typename E>
class FindSourcesAndSinksFunctor {
public:
	std::vector<int> sources;
	std::vector<int> sinks;
	std::vector<V> *vertices;
	std::vector<E> *edges;
	void Clear() {
		sources.clear();
		sinks.clear();
	}
	void operator()(int vertex) {
		int inDegree, outDegree;
		inDegree = (*vertices)[vertex].InDegree();
		outDegree = (*vertices)[vertex].OutDegree();
		if (inDegree == 1 and outDegree == 0)
			sinks.push_back(vertex);

		if (inDegree == 0 and outDegree == 1) 
			sources.push_back(vertex);

		return;
	}
};


void IntervalGraph::FindSourcesAndSinks(std::vector<int> &sources,
																				std::vector<int> &sinks) {

	Unmark();
	FindSourcesAndSinksFunctor<TVertex, TEdge> findSourceSink;
	findSourceSink.vertices = &vertices;
	findSourceSink.edges    = &edges;
	int vertex;
	for (vertex = 0; vertex < vertices.size(); vertex++ ) {
		if (vertices[vertex].marked == GraphVertex::Marked)
			continue;

		// reset any previous result of sources and sinks
		findSourceSink.Clear();
		TraverseDFS(vertices, edges, vertex, findSourceSink);
		sources.insert(sources.end(), 
									 findSourceSink.sources.begin(), findSourceSink.sources.end());
		sinks.insert(sinks.end(),
								 findSourceSink.sinks.begin(), findSourceSink.sinks.end());
	}
}


void IntervalGraph::TraceSourceToSinkPaths(std::vector<int> &edgeTraversalCount) {
	// reset the markings on the graph
	Unmark();

	int vertex, edge;

	// Initialize the edge traversal count
	edgeTraversalCount.resize(edges.size());
	for (edge = 0; edge < edgeTraversalCount.size(); edge++) edgeTraversalCount[edge] = 0;

	FindSourcesAndSinksFunctor<TVertex, TEdge> findSourceSink;
	findSourceSink.vertices = &vertices;
	findSourceSink.edges    = &edges;
	int source, sink;
	for (vertex = 0; vertex < vertices.size(); vertex++ ) {
		if (vertices[vertex].marked == GraphVertex::Marked)
			continue;

		// reset any previous result of sources and sinks
		findSourceSink.Clear();

		TraverseDFS(vertices, edges, vertex, findSourceSink);

		if (findSourceSink.sources.size() != findSourceSink.sinks.size()) {
			std::cout << "Warning: the graph does not have an even number of sources or sinks." << std::endl;
			std::cout << "The results are not well defined for this " << std::endl;
		}
	}
}


void IntervalGraph::IncrementOptimalPathCount(int source, 
																							std::vector<int> &sinks, 
																							std::vector<int> &edgeTraversalCount) {

	int sink;
	std::vector<int> shortestPathIndices;
	//  std::cout << "finding shortest paths from " << source << std::endl;
	SingleSourceMaximumPath(vertices, edges, source, shortestPathIndices);
	int curVertex;
	int edge;
	for (sink = 0; sink < sinks.size(); sink++) {
		curVertex = sinks[sink];
		int its = 0;
		while (curVertex != source and 
					 shortestPathIndices[curVertex] != -1) {
			assert(its < vertices.size());
			edge = vertices[curVertex].in[shortestPathIndices[curVertex]];
			edgeTraversalCount[edge]++;
			curVertex = edges[edge].src;
			its++;
		}
	}
}

void IntervalGraph::FindHighestScoringPathSourceToSink(int source, int sink, 
																											 std::vector<int> &edgeTraversalCount) {
	std::vector<int> shortestPathIndices;
	SingleSourceMaximumPath(vertices, edges, source, shortestPathIndices);

	TraceOptimalPath(vertices, edges, source, sink, shortestPathIndices, edgeTraversalCount);
}


void IntervalGraph::MarkBalancedEdges() {
	int edge;
	int balancedEdge;
	for (edge = 0; edge < edges.size(); edge++) {
		if (edges[edge].marked == GraphEdge::Marked) {
			balancedEdge = edges[edge].balancedEdge;
			if (balancedEdge >= 0) {
				edges[balancedEdge].marked = GraphEdge::Marked;
			}
		}
	}
}

int IntervalGraph::SearchForDirectedCycle(int sourceVertex, int curVertex, std::set<int> &visitedEdges,
																					int curPathLength, int maxCycleLength) {

	std::vector<int> sortedEdgeList;
	vertices[curVertex].GetSortedOutEdgeList(edges, sortedEdgeList);

	int outEdge, outEdgeIndex;
	int e;
	for (e =0 ; e < sortedEdgeList.size(); e++ ){
		outEdge = sortedEdgeList[e];
		if (visitedEdges.find(outEdge) != visitedEdges.end())
			continue;
		//		edges[outEdge].traversed = GraphEdge::Marked;
		if (curPathLength + edges[outEdge].length - vertices[edges[outEdge].src].vertexSize < maxCycleLength) {
			if (SearchForDirectedCycle(sourceVertex, edges[outEdge].dest, visitedEdges,
																 curPathLength + edges[outEdge].length - vertices[edges[outEdge].src].vertexSize, 
																 maxCycleLength)) {
				return 1;
			}
		}
	}
	return 0;
}


int IntervalGraph::ExtractMin(set<int> &keyQueue, map<int,int> &values, int &minKey, int &minValue) {
	set<int>::iterator queueIt, minIt;
	// Find the closest vertex.
	assert(keyQueue.size() > 0);
	assert(values.size() > 0);
	minValue = -1;
	for (queueIt = keyQueue.begin(); queueIt != keyQueue.end(); ++queueIt) {
		if (minValue == -1 or values[*queueIt] < minValue) {
			minValue = values[*queueIt];
			minKey   = *queueIt;
			minIt    = queueIt;
		}
	}
	// pop this element.
	keyQueue.erase(minIt);
	return keyQueue.size();
}




int IntervalGraph::SearchTwoDirectedCycle(int redVertex, int blackVertex, int maxLength,
																					BinomialHeapNode<int,int>* redNodeList[],
																					BinomialHeapNode<int,int>* blackNodeList[],
																					int redNodeLengths[],
																					int blackNodeLengths[],
																					int redInv[], int blackInv[], int invocation,
																					int &minVertex, int &minCycleLength,
																					int redPath[], int blackPath[]) {
	//
	// Initialize the queues, should this assume red != black?
	//
	BinomialHeap<int,int> redQueue;
	BinomialHeap<int,int> blackQueue;
	

	redNodeList[redVertex]             = redQueue.Insert(0, redVertex);
	blackNodeList[blackVertex]         = blackQueue.Insert(0, blackVertex);
	redNodeList[redVertex]->extRef     = &redNodeList[redVertex];
	blackNodeList[blackVertex]->extRef = &blackNodeList[blackVertex];

	std::set<int> emptyBlacklist;
	/*	std::map<int, BinomialHeapNode<int, int>* > redVertexSubset, blackVertexSubset;
	redVertexSubset[redVertex] = redNodeList[redVertex];
	blackVertexSubset[blackVertex] = blackNodeList[blackVertex];
	*/
	//	cout << "Searching for cycle from " << redVertex << " " << blackVertex << endl;
	int foundCycle = 0;
	BinomialHeapNode<int,int> *minRed = NULL, *minBlack = NULL;
	while (!redQueue.Empty() and
				 !blackQueue.Empty() and 
				 !foundCycle) {
		
		minRed   = redQueue.ExtractMin();
		minBlack = blackQueue.ExtractMin();

		// Record the distance to these nodes
		redNodeLengths[minRed->value] = minRed->key;
		blackNodeLengths[minBlack->value] = minBlack->key;

		// keep track of first visited vertices.
		redInv[minRed->value]     = invocation;
		blackInv[minBlack->value] = invocation;
		
		int cycleLength;
		assert(minRed != NULL);
		assert(minBlack != NULL);

		// Look to see if the search from the red vertex has 
		// crossed the search from the black vertex.
		if (blackInv[minRed->value] == invocation) {
			// The two searches have crossed, look to see if they form a cycle.
			cycleLength = minRed->key + blackNodeLengths[minRed->value];

			if (minCycleLength == -1 or cycleLength < minCycleLength) {
				minCycleLength = cycleLength;
				minVertex = minRed->value;
//				cout << "found a red->black cycle of length: " << minCycleLength << " " << minVertex << endl;
				foundCycle = 1;
				break;
			}
		}

		// Similar check from a red vertex.
		//

		if (redInv[minBlack->value] == invocation) {
			cycleLength = minBlack->key + redNodeLengths[minBlack->value];

			if (minCycleLength == -1 or cycleLength <minCycleLength) {
				minCycleLength = cycleLength;
				minVertex = minBlack->value;			
//				cout << "found a black->red cycle of length: " << minCycleLength << " " << minVertex << endl;
				foundCycle = 1;
				break;
			}
		}

		// The two paths have not crossed.
		// Add the edges emanating from each vertex to the queue.
		AddOutEdgeLengths(minRed->value, minRed->key, maxLength,
											redQueue, redNodeList, redInv, invocation, 0, redPath);

		AddOutEdgeLengths(minBlack->value, minBlack->key, maxLength,
											blackQueue, blackNodeList, blackInv, invocation, 0, blackPath);
		
		
		redNodeList[minRed->value] = NULL;
		blackNodeList[minBlack->value] = NULL;
		delete minRed; minRed = NULL;
		delete minBlack; minBlack = NULL;
	}
	//	cout << "search space: " << redVertexSubset.size() << " " <<  blackVertexSubset.size() << endl;
	// Free up the space searched in the graph.
	
	if (minRed != NULL)	delete minRed;
	if (minBlack != NULL) delete minBlack; 

	redQueue.Free();
	blackQueue.Free();
	/*	map<int, BinomialHeapNode<int, int>* >::iterator subsetIt, subsetEnd;
	subsetEnd = redVertexSubset.end();
	for (subsetIt = redVertexSubset.begin(); subsetIt != subsetEnd; ++subsetIt) {
		delete subsetIt->second;
	}
	subsetEnd = blackVertexSubset.end();
	for (subsetIt = blackVertexSubset.begin(); subsetIt != subsetEnd; ++subsetIt) {
		delete subsetIt->second;
	}																																		 
	*/

	if (foundCycle) {
		return 1;
	}
	else {
		// No cycles found.
		return 0;
	}
}


void IntervalGraph::AddOutEdgeLengths(int curVertex, int lengthToCurVertex,
																			int maxLength,
																			BinomialHeap<int,int> &lengthPQueue,
																			BinomialHeapNode<int,int> *nodeRef[],
																			int invocations[], int inv,
																			int useFlagged, int path[]) {

	// Now store or relax min edge lengths for edges reaching this vertex.
	int outEdge, outEdgeIndex;
	int dest = curVertex;
	for (outEdgeIndex = vertices[curVertex].FirstOut();
			 outEdgeIndex != vertices[curVertex].EndOut();
			 outEdgeIndex = vertices[curVertex].NextOut(outEdgeIndex)) {
		outEdge = vertices[curVertex].out[outEdgeIndex];

		// If only using marked edges (for example marked in the DMST)
		// check and continue if not marked.
		if (useFlagged and edges[outEdge].marked != GraphEdge::Marked)
			continue;
		dest = edges[outEdge].dest;

		// Only try and add edges if they are close enough.
		int arrivalLength = (lengthToCurVertex 
												 + edges[outEdge].length 
												 - vertices[edges[outEdge].dest].vertexSize);
		assert(arrivalLength >= 0);
		BinomialHeapNode<int,int> *newNode;

		// 
		// maxLength is the maximum cycle length.  Do not look
		// for a cycle if the length out of this edge will create
		// a cycle longer than maxLength.
		//

		if (arrivalLength < maxLength) {

			//
			// If the dest node has not been visited this search for a cycle
			// (denoted by 'inv'), add this node (and the time to get to it)
			// to the priority queue.
			//
			if (invocations[dest] != inv) {
				nodeRef[dest]   = lengthPQueue.Insert(arrivalLength, dest);
				//				graphVertexSubset[dest] = nodeRef[dest];
				nodeRef[dest]->extRef = &nodeRef[dest];
				path[dest]            = outEdge;
				assert(edges[outEdge].src != edges[outEdge].dest);
				invocations[dest]     = inv;
			}
			else if (nodeRef[dest] != NULL and 
							 nodeRef[dest]->key  > arrivalLength) {
				//
				// move this key in the tree.
				/*				cout << "decreasing " << dest << " (" << nodeRef[dest]->key << ") " 
						 << " to " << arrivalLength << " " << inv << endl; 
				*/
				/*
				cout << "decreasing key of " << nodeRef[dest] << " " 
						 << nodeRef[dest]->key << " " << nodeRef[dest]->value 
						 << " node inv "
						 << invocations[dest] << " inv " << inv << " " << dest << endl;
				*/
				assert(edges[outEdge].src != edges[outEdge].dest);				
				lengthPQueue.DecreaseKey(nodeRef[dest], arrivalLength);
				//				cout << "key of 2747 " << nodeRef[2747] << endl;
				path[dest]      = outEdge;
			}
		}
	}
}



int IntervalGraph::FindMinimalMatchedArrivalLength(map<int,int> &setA, map<int,int> &setB, int &minElement, int&minLength) {
	
	map<int,int>::iterator itA, endA, itB, endB;
	endA = setA.end();
	endB = setB.end();
	minLength = -1;
	minElement = -1;
	for (itA = setA.begin(); itA != endA; ++itA) {
		itB = setB.find((*itA).first);
		if (itB != endB) {
			if (minLength == -1 or ((*itA).second + (*itB).second) < minLength) {
				minLength = (*itA).second + (*itB).second;
				minElement = (*itA).first;
			}
		}
	}
	if (minLength == -1) {
		return 0;
	}
	else {
		return 1;
	}
}

int IntervalGraph::SearchForUndirectedCycle2(int sourceVertex, int curVertex, std::set<int> &curPath, 
																						 int curPathLength, int maxCycleLength, int isUndirected,
																						 std::set<int> &visitedEdges, std::set<int> &visitedVertices) {

	// Make sure the path is not following a cycle to an internal
	// vertex (that will be dealt with later).
  if (visitedVertices.find(curVertex) != visitedVertices.end())
    return 0;


	// This is visiting a new vertex in the graph.  Record
	// that so we know the path.
  visitedVertices.insert(curVertex);


	// Visit in edges accor
  int inEdge, inEdgeIndex;
  std::vector<int> sortedEdgeList;
  vertices[curVertex].GetSortedInEdgeList(edges, sortedEdgeList);
  int e;
  for (e = 0; e < sortedEdgeList.size(); e++ ) {
    inEdge = sortedEdgeList[e];

    // If this edge has already been visited (perhaps in the other
		// direction), don't traverse it.
    if (visitedEdges.find(inEdge) != visitedEdges.end())
      continue;

		//
		// Check to see if this in-edge completes a small cycle
		// 
    if (sourceVertex == edges[inEdge].src and 
				curPathLength + edges[inEdge].length - vertices[curVertex].vertexSize < maxCycleLength ) {
      return 1;
    }
		
		// No small cycle is found.  Record this edge as visited, and continue searching.
    visitedEdges.insert(inEdge);

		// Traverse this edge in the opposite orientation, if it is possible to find
		// a short cycle after traversing this edge.
    if (curPathLength + ( edges[inEdge].length - vertices[edges[inEdge].dest].vertexSize) < maxCycleLength) {
      curPath.insert(inEdge);
      if (SearchForUndirectedCycle2(sourceVertex, edges[inEdge].src, curPath, 
																		curPathLength + edges[inEdge].length - vertices[edges[inEdge].dest].vertexSize,
																		maxCycleLength, 1, visitedEdges, visitedVertices)) {
				return 1;
      }
    }
  }


	// Visit in edges accor
  int outEdge, outEdgeIndex;
	sortedEdgeList.clear();
  vertices[curVertex].GetSortedOutEdgeList(edges, sortedEdgeList);

  for (e = 0; e < sortedEdgeList.size(); e++ ) {
    outEdge = sortedEdgeList[e];

    // If this edge has already been visited (perhaps in the other
		// direction), don't traverse it.
    if (visitedEdges.find(outEdge) != visitedEdges.end())
      continue;

		//
		// Check to see if this in-edge completes a small cycle
		// 
    if (sourceVertex == edges[outEdge].dest and 
				curPathLength + edges[outEdge].length - vertices[curVertex].vertexSize < maxCycleLength ) {
      return 1;
    }
		
		// No small cycle is found.  Record this edge as visited, and continue searching.
    visitedEdges.insert(outEdge);

		// Traverse this edge in the opposite orientation, if it is possible to find
		// a short cycle after traversing this edge.
    if (curPathLength + ( edges[outEdge].length - vertices[edges[outEdge].dest].vertexSize) < maxCycleLength) {
      curPath.insert(outEdge);
      if (SearchForUndirectedCycle2(sourceVertex, edges[outEdge].dest, curPath, 
																		curPathLength + edges[outEdge].length - vertices[edges[outEdge].src].vertexSize,
																		maxCycleLength, 1, visitedEdges, visitedVertices)) {
				return 1;
      }
    }
  }
  return 0;
}


int IntervalGraph::SearchForUndirectedCycle(int sourceVertex, int cycleEndVertex, int maxCycleLength) {
  //	std::cout << "searching fora cycle from " << sourceVertex << " to " << cycleEndVertex << std::endl;
  // Initialize the priority queue to hold the source vertex
  RBTree<ScoredVertex<int> > queue;

  ScoredVertex<int> closestVertex;

  // Use a set to count the vertices that are visisted.   Although it 
  // uses extra space and runs in n log n time, it lets us not unmark the entire tree
  // after looking for a cycle, which should only check out a very small subgraph.

  closestVertex.vertex = sourceVertex;
  closestVertex.score  = 0;
  // prepare references to scores
  std::map<int, HeapScoredVertex*> references;
  std::set<int> traversedEdges;
  references[sourceVertex] = queue.Insert(closestVertex);


  while(queue.size() > 0) {
    if (queue.Pop(closestVertex) == 0) {
      std::cout << "ERROR working with queue " << std::endl;
      exit(0);
    }
		
    //    std::cout << "got vertex " << scoredVertex.vertex << std::endl;
    // First difference from normal Dijkstra: if there are any in edges
    // that are not already part of the dag.

    // Try and relax any of the edges going out of the current
    // vertex.
    
    int inEdgeIndex, inEdge;
    std::vector<int> sortedEdgeList;
    ScoredVertex<int> srcVertex;
    vertices[closestVertex.vertex].GetSortedInEdgeList(edges, sortedEdgeList);
    //		std::cout << "searching in edge list of length: " << sortedEdgeList.size() << std::endl;
    int in;
    for (in = 0; in < sortedEdgeList.size(); in++ ) {
      inEdge = sortedEdgeList[in];
      if (traversedEdges.find(inEdge) != traversedEdges.end())
				continue;
      traversedEdges.insert(inEdge);
      if (edges[inEdge].length + closestVertex.score < maxCycleLength) {
				if (edges[inEdge].src == cycleEndVertex) {
					/*
						std::cout << "found a cycle ending at edge " << inEdge << " " << edges[inEdge].src 
						<< " " << edges[inEdge].dest << " of length: " 
						<< edges[inEdge].length + closestVertex.score << std::endl;
					*/
					// CYCLE FOUND!
					return 1;
				}
				else {
					srcVertex.vertex = edges[inEdge].src;

					// It is necessary to update the distance in the queue if
					// the vertex doesn't exist in the queue, or it can be reached 
					// from 'inEdge' with less distance.
					if (references.find(srcVertex.vertex) == references.end() or 
							edges[inEdge].length + closestVertex.score < references[srcVertex.vertex]->data.score) {
						
						// it's possible this edge forms a cycle. update it in the queue
						if (references.find(srcVertex.vertex) != references.end()) {
							queue.Delete(references[srcVertex.vertex]);
						}
						
						srcVertex.score = edges[inEdge].length + closestVertex.score;
						/*
							std::cout << "vertex: " << srcVertex.vertex << " is reachable from " 
							<< srcVertex.score << std::endl;
						*/
						references[srcVertex.vertex] = queue.Insert(srcVertex);
						//						std::cout <<" queue size: " << queue.size() << std::endl;
					}
				}
      }
    }

    // process out edges

    int outEdgeIndex, outEdge;
    ScoredVertex<int> destVertex;
    sortedEdgeList.clear();
    vertices[closestVertex.vertex].GetSortedOutEdgeList(edges, sortedEdgeList);
    //		std::cout << "searching out edge list of length: " << sortedEdgeList.size() << std::endl;
    int out;
    for (out = 0; out < sortedEdgeList.size(); out++ ) {
      outEdge = sortedEdgeList[out];
      if (traversedEdges.find(outEdge) != traversedEdges.end())
				continue;
      traversedEdges.insert(outEdge);

      if (edges[outEdge].length + closestVertex.score < maxCycleLength) {
				if (edges[outEdge].dest == cycleEndVertex) {
					//					std::cout << "found a cycle of length: " << edges[outEdge].length + closestVertex.score << std::endl;
					// CYCLE FOUND!
					return 0;
				}
				else {
					destVertex.vertex = edges[outEdge].dest;

					// It is necessary to update the distance in the queue if
					// the vertex doesn't exist in the queue, or it can be reached 
					// from 'outEdge' with less distance.
					if (references.find(destVertex.vertex) == references.end() or 
							edges[outEdge].length + closestVertex.score < references[destVertex.vertex]->data.score) {
						
						// it's possible this edge forms a cycle. update it in the queue
						if (references.find(destVertex.vertex) != references.end()) {
							queue.Delete(references[destVertex.vertex]);
						}
						
						destVertex.score = edges[outEdge].length + closestVertex.score;
						/*						std::cout << "vertex: " << destVertex.vertex << " is reachable from " 
							<< destVertex.score << std::endl;
						*/
						references[destVertex.vertex] = queue.Insert(destVertex);
						//						std::cout <<" queue size: " << queue.size() << std::endl;
					}
				}
      }
    }
    // No cycle has been found from 'curVertex'.
  }
  return 0;
}

void IntervalGraph::RemoveWhirls(int whirlLength) {
	std::vector<int> edgesToRemove, verticesToRemove;
	int edge;
	for (edge = 0; edge < edges.size(); edge++) {
		if (edges[edge].src == edges[edge].dest and 
				edges[edge].length < whirlLength) {

			int i;
			for (i = 0; i < (*edges[edge].intervals).size(); i++ ){ 
				int path, pathPos;
				path = (*edges[edge].intervals)[i].read;
				pathPos = (*edges[edge].intervals)[i].pathPos;
				SplicePathRange(path, pathPos, pathPos+1);
			}
			RemoveEdge(edge, verticesToRemove);
			edgesToRemove.push_back(edge);
		}
	}
	//	std::cout << "Found " << edgesToRemove.size() << " whirls." << std::endl;
	//  RemoveEdgeAndMarkIntervalsForRemoval(edgesToRemove, verticesToRemove);
	Prune(verticesToRemove, edgesToRemove);
}

int IntervalGraph::VertexMapToEdgePath(map<int,int> &vertexPaths, int startVertex,
																			 list<int> &edgePath, int dir) {
	int pathLength = 0;
	map<int,int>::iterator mapIt, mapEnd;
	mapIt = vertexPaths.find(startVertex);
	while ((*mapIt).second != -1) {
		edgePath.push_back((*mapIt).second);
		if (dir > 0) {
			mapIt = vertexPaths.find(edges[(*mapIt).second].dest);
		}
		else {
			mapIt = vertexPaths.find(edges[(*mapIt).second].src);
		}
		assert(mapIt != vertexPaths.end());
		pathLength++;
	}
	return pathLength;
}




int IntervalGraph::RemoveBulgingEdges(int bulgeLength, int useDMST, int iter) {
  int edge;
  std::vector<int> edgesToRemove, orphanedVertices;
  std::set<int> edgesToRemoveSet;
  //	Untraverse();
  std::set<int> curPath, visitedEdges, visitedVertices;
  Untraverse();
  int numNotMarked = 0;

	map<int,int> srcVertexLengths, destVertexLengths, srcVertexPaths, destVertexPaths;

	int vertex;
	BinomialHeapNode<int, int> **redNodeList = new BinomialHeapNode<int,int>*[vertices.size()];
	BinomialHeapNode<int, int> **blackNodeList = new BinomialHeapNode<int,int>*[vertices.size()];
	cout << " allocating node lists of size " << vertices.size() << endl;
	int *redInv = new int[vertices.size()];
	int *blackInv = new int[vertices.size()];
	int *redPath = new int[vertices.size()];
	int *blackPath = new int[vertices.size()];
	int *redDistList = new int[vertices.size()];
	int *blackDistList = new int [vertices.size()];
	std::fill(redInv, redInv + vertices.size(), 0);
	std::fill(blackInv, blackInv + vertices.size(), 0);
	std::fill(redPath, redPath + vertices.size(), edges.size());
	std::fill(blackPath, blackPath + vertices.size(), edges.size());
	std::fill(redNodeList, redNodeList + vertices.size(), (BinomialHeapNode<int,int>*) NULL);
	std::fill(blackNodeList, blackNodeList + vertices.size(), (BinomialHeapNode<int,int>*) NULL);
	std::fill(redDistList, redDistList + vertices.size(), 0);
	std::fill(blackDistList, blackDistList + vertices.size(), 0);
	IntMatrix pathMat;
	FloatMatrix mismatchMat;
	FloatMatrix scoreMat;
	InitScoreMat(mismatchMat, 0, 1);
	int invocation = 0;
	for (vertex = 0; vertex < vertices.size(); vertex++) {
		if (vertices[vertex].OutDegree() > 1) {
			// Bulging edges may exist from this vertex.
			int outEdge, outEdgeIndex;
			int markedEdgeIndex, markedEdge, numMarkedEdges;
			numMarkedEdges = 0;
			/*			cout << "searching vertex of out degree: " 
				<< vertices[vertex].OutDegree() << endl;*/
			for (outEdgeIndex = vertices[vertex].FirstOut();
					 outEdgeIndex != vertices[vertex].EndOut();
					 outEdgeIndex = vertices[vertex].NextOut(outEdgeIndex)) {
				outEdge = vertices[vertex].out[outEdgeIndex];
				if ((useDMST and 
						 edges[outEdge].marked == GraphVertex::Marked)or 
						(!useDMST and
						 edges[outEdge].length < bulgeLength)) {
					markedEdge = outEdge;
					markedEdgeIndex = outEdgeIndex;
					numMarkedEdges++;
				}
			}
			
			if ((useDMST and numMarkedEdges != vertices[vertex].OutDegree()) or
					numMarkedEdges > 0) {


				for (outEdgeIndex = vertices[vertex].FirstOut();
						 outEdgeIndex != vertices[vertex].EndOut();
						 outEdgeIndex = vertices[vertex].NextOut(outEdgeIndex)) {
					outEdge = vertices[vertex].out[outEdgeIndex];
					
					// Don't remove bulging edges that are part of the dmst.
					if ((useDMST and edges[outEdge].marked == GraphEdge::Marked) or
							(!useDMST and edges[outEdge].length > bulgeLength))
						continue;

					if (edges[outEdge].dest == -1)
						continue;

					if (edges[outEdge].src == edges[outEdge].dest)
						continue;

					int nextOutEdgeIndex, nextOutEdge;

					for(nextOutEdgeIndex = vertices[vertex].NextOut(outEdgeIndex);
							nextOutEdgeIndex != vertices[vertex].EndOut();
							nextOutEdgeIndex = vertices[vertex].NextOut(nextOutEdgeIndex)) {
						nextOutEdge = vertices[vertex].out[nextOutEdgeIndex];

						if (edges[nextOutEdge].dest == -1)
							continue;

						if (edges[nextOutEdge].length >= bulgeLength) {
							continue;
						}
						if (edges[nextOutEdge].src == edges[nextOutEdge].dest)
							continue;
						markedEdge = nextOutEdge;
						

						//
						// If using the dMST, and this edge is marked as part of the dMST,
						// then it will not be merged with the outEdge, which is already marked
						// as part of the dMST.
						//
						if (useDMST and edges[markedEdge].marked == GraphEdge::Marked)
							continue;


						
						//					assert(outEdge != markedEdge);
						// Now look to see if a cycle leaves this edge.
						map<int,int> markedPathLengths, unmarkedPathLengths, markedPaths, unmarkedPaths;
						
						int minTwoDirCycleVertex = -1, minTwoDirCycleLength = -1;
						list<int> twoDirRedPath, twoDirBlackPath;
						

						//
						// Find an undirected cycle that is composed of n fowrward edges followed by m reverse
						// edges, where n,m>= 0, and n+m > 1.
						//
						
						++invocation;
						if (invocation == 0) {
							cout << "INVOCATION MUST BE RESET!" << endl;
							std::fill(redInv, redInv + vertices.size(), 0);
							std::fill(blackInv, blackInv + vertices.size(), 0);
							++invocation;
						}
						
						if (SearchTwoDirectedCycle(edges[markedEdge].dest, edges[outEdge].dest, 
																			 bulgeLength,
																			 redNodeList, blackNodeList, 
																			 redDistList, 
																			 blackDistList,
																			 redInv, blackInv, invocation,
																			 minTwoDirCycleVertex, minTwoDirCycleLength,
																			 redPath, blackPath)) {
							
							int minVertex, minVertexLength;
							
							minVertex = minTwoDirCycleVertex;
							minVertexLength = minTwoDirCycleLength;

							if (minVertex == vertex)
								continue;

							//
							// Trace out the two paths.
							//
							std::vector<int> redOptPath, blackOptPath;

							//
							// The first edges to the start vertex are initial
							// conditions for the search.
							//
							redPath[edges[markedEdge].dest] = markedEdge;
							blackPath[edges[outEdge].dest]  = outEdge;

							TraceBackEdges(vertex, minVertex, redPath, redOptPath);
							TraceBackEdges(vertex, minVertex, blackPath, blackOptPath);

							SimpleSequence redSequence, blackSequence;

							PathToSequence(redOptPath, redSequence);
							PathToSequence(blackOptPath, blackSequence);
							/*
								cout << redOptPath.size() << " " << blackOptPath.size() << endl;
								redSequence.PrintSeq(cout, "red");
								blackSequence.PrintSeq(cout, "black");
							*/

							DNASequence redDNA, blackDNA;
							redDNA.seq = redSequence.seq;
							redDNA.length = redSequence.length;
							blackDNA.seq = blackSequence.seq;
							blackDNA.length = blackSequence.length;
							int alignScore;
							int *locations = NULL;
							int band = 10;
							alignScore = BandedAlign(redDNA, blackDNA,
																			 0,1, 1, band,
																			 locations, scoreMat, pathMat, mismatchMat);
							if (alignScore < .10*redSequence.length and
									alignScore < .10*blackSequence.length and
									alignScore < band ) {
								cout << "alignScore: " << alignScore << " " << .10*redSequence.length << " " << .10*blackSequence.length << endl;
								redDNA.PrintlnSeq(cout);
								blackDNA.PrintlnSeq(cout);
							}

							delete[] redSequence.seq;
							delete[] blackSequence.seq;
							// The align score is to high, don't merge these edges.
							if (alignScore > .10*redSequence.length or
									alignScore > .10*blackSequence.length or
									alignScore >= band ) {
								//							cout << "skipping alignment." << endl;
								continue;
							}
							int outBal, nextOutBal, balVertex;
							nextOutBal = edges[outEdge].balancedEdge;
							outBal     = edges[markedEdge].balancedEdge;

							balVertex = edges[outBal].dest;
						
							if (nextOutBal == markedEdge or balVertex == vertex or
									outBal == outEdge or outBal == outEdge or 
									outEdge == nextOutBal) 
								// Don't try and merge these edges since the 
								// balance will be messed up.
								continue;
							// This is a whirl, don't remove it here.
							if (edges[outEdge].dest == vertex)
								continue;

							int cycleLength;
							cycleLength = ( edges[outEdge].length 
															- vertices[edges[outEdge].dest].vertexSize
															+ edges[markedEdge].length 
															- vertices[edges[markedEdge].dest].vertexSize
															+ minVertexLength );
							if (cycleLength > bulgeLength) {
								continue;
							}
							if (markedEdge != outBal and
									outEdge != nextOutBal and
									edges[nextOutBal].src != vertex and
									edges[outBal].src != vertex and
									edges[markedEdge].dest != balVertex and
									edges[outEdge].dest != balVertex) {
								int outEdgeDest = edges[outEdge].dest;
								cout << "merging to " << outEdge << " from " << markedEdge << " on vertex: " << vertex <<endl;
								MergeOutEdges(vertex, outEdge, markedEdge);
								edgesToRemoveSet.insert(markedEdge);
								edges[markedEdge].Clear();
							
								if (outEdgeDest != edges[markedEdge].dest)
									orphanedVertices.push_back(edges[markedEdge].dest);
							
								int nextOutBalSrc = edges[nextOutBal].src;
								cout << "merging in " << nextOutBal << " from " << outBal << " on vertex " << balVertex << endl;
								MergeInEdges(balVertex, nextOutBal, outBal);
								edgesToRemoveSet.insert(outBal);
								if (nextOutBalSrc != edges[outBal].src)
									orphanedVertices.push_back(edges[outBal].src);
								edges[outBal].Clear();
							}
						}
					}
				}
			}
		}
	}
	delete[] redNodeList;
	delete[] blackNodeList;
	delete[] redInv;
	delete[] blackInv;
	delete[] redPath;
	delete[] blackPath;
	delete[] redDistList;
	delete[] blackDistList;


  std::cout << "bulge removal found " << edgesToRemoveSet.size() << " edges in short bulges of " 
						<< numNotMarked << " edges in bulges of " << edges.size() << " total edges." << std::endl;
	int v;
	for (v = 0; v < vertices.size(); v++) {
		assert(vertices[v].CheckUniqueOutEdges());
		assert(vertices[v].CheckUniqueInEdges());
	}

  for (edge = 0; edge < edges.size(); edge++ ) {
    if (edgesToRemoveSet.find(edge) != edgesToRemoveSet.end()) {
      edgesToRemoveSet.insert(edges[edge].balancedEdge);
    }
  }
  // Turn the set into a vector
  edgesToRemove.insert(edgesToRemove.begin(), edgesToRemoveSet.begin(), edgesToRemoveSet.end());
  std::sort(edgesToRemove.begin(), edgesToRemove.end());
	
  Unflag();
  for (edge = 0; edge < edgesToRemove.size(); edge++) {
    edges[edgesToRemove[edge]].flagged = GraphEdge::Marked;
  }
	cout << "after re-routing intervals." << endl;
	int p, pi;
	/*
	for (p = 0; p < paths.size(); p++ ){ 
		for (pi = 0; pi < pathLengths[p]; pi++) {
			assert(paths[p][pi].edge != -1);
		}
	}
	*/

	// Unlink edges from the graph that form bulges.
	//  RemoveEdgeAndMarkIntervalsForRemoval(edgesToRemove, orphanedVertices);
	// Remove intervals that were deleted when re-routing intervals through bulges.
	//	RemoveMarkedIntervalsNoPaths();
  Prune(orphanedVertices, edgesToRemove);
	assert(CheckEdges(vertices,edges));
#ifdef NDEBUG
	for (p = 0; p < paths.size(); p++ ){ 
		for (pi = 0; pi < pathLengths[p]; pi++) {
			assert(paths[p][pi].edge != -1);
		}
	}
#endif

	//  int numRerouted = RouteRemovedIntervals(bulgeLength);
	// Remove intervals marked for deletion by the prune operation.
	int numZeroDegree = RemoveZeroDegreeVertices(vertices, edges);
	cout << "removed " << numZeroDegree << " zero degree vertices." << endl;
	RemoveMarkedIntervals();
	//
	// These aren't needed when the paths link intervals.
	//  SortAllEdgeIntervalsByReadPos();
	//  UpdateAllPathIndices();

  DiscardGappedPaths();
  assert(CheckGraphStructureBalance());
  assert(CheckAllPathsBalance(1));

	cout << "after discarding bulging edges." << endl;
	//	assert(CheckPathContinuity());
  int numCondensed = CondenseSimplePaths();
	assert(CheckGraphStructureBalance());
	assert(CheckBalance());
  std::cout << "After bulge removal, condensed " << numCondensed <<" edges " << std::endl;



  /* Do some post-processing of the graph to remove some untidy edges.*/
  Erode(bulgeLength/2);
	RemoveWhirls(bulgeLength);
  RemoveMarkedIntervals();
	CondenseEdgeLists();
  assert(CheckAllPathsBalance(1));	
  return edgesToRemove.size();
}

void IntervalGraph::CondenseEdgeLists() {
	int v;
	for (v = 0; v < vertices.size(); v++ ){ 
		vertices[v].CondenseEdgeLists();
	}
}	

int IntervalGraph::CheckPathContinuity() {
	int p, pi;
	for (p = 0; p < paths.size(); p++ ){ 
		for (pi= 0; pi < pathLengths[p]-1; pi++) {
			int curEdge = paths[p][pi].edge;
			int nextEdge = paths[p][pi+1].edge;
			int dest = edges[curEdge].dest;
			if (curEdge != nextEdge and vertices[dest].LookupOutIndex(nextEdge) == -1) {
				cout << "path: " << p << " is not continuous" << endl;
				return 0;
			}
		}
	}
	return 1;
}

int IntervalGraph::LocateEdgeOnPath(int edge, list<int> &path) {
	list<int>::iterator pathIt, pathEnd;
	int index = 0;
	int pathPos;
	pathIt = std::find(path.begin(), path.end(), edge);
	for (pathIt = path.begin(), pathEnd = path.end(); 
			 pathIt != pathEnd; ++pathIt, ++pathPos) {
		if (*pathIt == edge) {
			return pathPos;
		}
	}
	return -1;
}

int IntervalGraph::ListToVector(list<int> &l, vector<int> &v) {
	v.clear();
	v.insert(v.begin(), l.begin(), l.end());
	return v.size();
}

int IntervalGraph::ComputeIntervalPositions(list<int> &path, vector<int> &positions) {
	positions.resize(path.size());
	list<int>::iterator pathIt, pathEnd;
	int curPos = 0;
	int i = 0;
	for (pathIt = path.begin(); pathIt != path.end(); ++pathIt, ++i) {
		positions[i] = curPos;
		curPos += edges[*pathIt].length - vertices[edges[*pathIt].dest].vertexSize;
	}
}

int IntervalGraph::GetPathLength(int path) {
	int pathLength = 0;
	int i;
	int edge, intv;
	int dest;
	for (i = 0; i < pathLengths[path]-1; i++ ){
		edge = paths[path][i].edge;
		intv = paths[path][i].index;
		dest = edges[edge].dest;
		pathLength += (*edges[edge].intervals)[intv].length -
			vertices[dest].vertexSize;
	}
	edge = paths[path][i].edge;
	intv = paths[path][i].index;
	// last interval doesn't cross a vertex
	pathLength += (*edges[edge].intervals)[intv].length;
	return pathLength;
}

int IntervalGraph::ComputePathLength(vector<int> &path) {
	vector<int>::iterator pathIt, pathEnd;
	pathEnd = path.end();
	--pathEnd;
	int pathLength = 0;
	if (path.size() == 0) {
		return 0;
	}

	for (pathIt = path.begin(); pathIt != pathEnd; ++pathIt) {
		int dest = edges[*pathIt].dest;
		pathLength += edges[*pathIt].length - vertices[dest].vertexSize;
	}
	// The last edge is full-length (no overlapping vertices)
	pathLength += edges[*pathEnd].length;
	return pathLength;
}

int IntervalGraph::PathToSequence(vector<int> &path,
																	SimpleSequence &seq) {

	// First compute the length of the path.
	int pathLength = ComputePathLength(path);
	if (pathLength == 0) {
		seq.length = 0;
		return 0;
	}
	assert(pathLength > 0);
	seq.seq = new unsigned char[pathLength];
	seq.length = pathLength;
	
	vector<int>::iterator pathIt, pathEnd;
	pathEnd = path.end();
	--pathEnd;
	int curPos = 0;
	int segLength = 0; // segment length
	for (pathIt = path.begin(); pathIt != pathEnd; ++pathIt) {
		int dest, destLength;
		dest = edges[*pathIt].dest;
		destLength = vertices[dest].vertexSize;
		segLength = edges[(*pathIt)].length - destLength;
		memcpy((char*) &seq.seq[curPos], edges[(*pathIt)].seq.seq, segLength );
		curPos += segLength;
	}
	segLength = edges[(*pathEnd)].length;
	memcpy((char*) &seq.seq[curPos], edges[(*pathEnd)].seq.seq, segLength );
	return pathLength;
}

void IntervalGraph::SetMultiplicities() {
  int e;
  for (e = 0; e < edges.size(); e++) {
    edges[e].multiplicity = edges[e].intervals->size();
  }
}

void IntervalGraph::RemoveBulges(int bulgeLength, int useDMST) {
  int numRemovedEdges;
  int iter = 0;
  std::stringstream pathOutStrm;
	if (containsIntervals) {
		RemoveEmptyEdges();
	}
  do {
    Unmark();
    SetMultiplicities();
		if (useDMST) {
			cout << "Calculating the directed Maximal Spanning Tree" << endl;
			CalcDMST();
			//    std::string dmst = "dmst.dot";
			//    GVZPrintBGraph(vertices ,edges, dmst);
		  MarkBalancedEdges();
		}
		cout << "before removing bulges " << endl;
    numRemovedEdges = RemoveBulgingEdges(bulgeLength, useDMST, iter);
    pathOutStrm.str("");
    pathOutStrm << "removed."<<iter<< ".txt";
    assert(CheckAllPathsBalance(1));
		//    assert(CheckAllPathsContinuity(1));
    ++iter;
    std::cout << "Remove bulges iter: " << iter << " " << numRemovedEdges << " bulging edges " << std::endl;
  } while (numRemovedEdges != 0);
}


int IntervalGraph::RemoveTruncatedPathIntervals() {
  // Often paths will be truncated at their ends.
  // This function erases the truncated parts, and
  // udpates the indices in edges to reference
  // back to the paths accordingly (so if the first
  // two intervals of a path are deleted, the 
  // eges that contain later parts of the path must
  // have the pathPos values for the interval decremented
  // by 2.
  int p, pre, suf, pos;
  for (p = 0; p < paths.size(); p++ ) {
    if (pathLengths[p] > 0) {
      pre = 0;
      while (pre < pathLengths[p] and
						 paths[p][pre].edge == -1)
				pre++;
      // The entire path is removed, nothing to do
      if (pre == pathLengths[p]) {
				delete[] paths[p];
				paths[p] = NULL;
				pathLengths[p] = 0;
				continue;
      }
			
      suf = pathLengths[p] - 1;
      while (suf >= pre and paths[p][suf].edge == -1)
				suf--;
			
      if (suf - pre + 1 == pathLengths[p])
				continue;

      if (suf < pre) {
				delete[] paths[p];
				paths[p] = NULL;
				pathLengths[p] = 0;
      }
      // replace paths with the truncated path
      PathInterval *newPath = new PathInterval[suf-pre+1];
      for (pos = pre; pos <= suf; pos++) {
				newPath[pos - pre] = paths[p][pos];
      }
      delete[] paths[p];
      paths[p] = newPath;
			
      pathLengths[p] = suf - pre + 1;
      // now update the position in each edge
			
      for (pos = 0; pos < pathLengths[p]; pos++) {
				if (paths[p][pos].edge != -1) {
					(*edges[paths[p][pos].edge].intervals)[paths[p][pos].index].pathPos = pos;
				}
      }
    }			
  }
}

int IntervalGraph::SearchForCycle(int sourceVertex, int prevVertex, int curVertex, 
																	int curPathLength, int maxPathLength, std::string padding) {
  
  // End case, a cycle is found
  if (curVertex == sourceVertex) {
    std::cout << "search2 found cycle " << std::endl;
    return 1;
  }

  // For now explore all paths in the graph.  Of course this
  // will take a while when allowing large paths
  int outEdge, outEdgeIndex, inEdge, inEdgeIndex;
  for (outEdgeIndex = vertices[curVertex].FirstOut(); 
       outEdgeIndex < vertices[curVertex].EndOut();
       outEdgeIndex = vertices[curVertex].NextOut(outEdgeIndex)) {
    outEdge = vertices[curVertex].out[outEdgeIndex];
    if (edges[outEdge].dest != prevVertex and
				edges[outEdge].length + curPathLength - vertexSize < maxPathLength) {
      /*
				std::cout << padding << "searching " << curVertex << " - " << outEdge << " > " << edges[outEdge].dest
				<< " " << edges[outEdge].length << " " << curPathLength << std::endl;
      */
      if (SearchForCycle(sourceVertex, curVertex, edges[outEdge].dest, 
												 edges[outEdge].length + curPathLength - vertexSize, maxPathLength, padding + " "))
				return 1;
    }
  }
  for (inEdgeIndex = vertices[curVertex].FirstIn(); 
       inEdgeIndex < vertices[curVertex].EndIn();
       inEdgeIndex = vertices[curVertex].NextIn(inEdgeIndex)) {
    inEdge = vertices[curVertex].in[inEdgeIndex];
    if (edges[inEdge].src != prevVertex and
				edges[inEdge].length + curPathLength - vertexSize < maxPathLength) {
      /*
				std::cout << padding << "searching " << curVertex << " < " << inEdge << " - " << edges[inEdge].src
				<< " " << edges[inEdge].length << " " << curPathLength << std::endl;
      */
      if (SearchForCycle(sourceVertex, curVertex, edges[inEdge].src, 
												 edges[inEdge].length + curPathLength - vertexSize, maxPathLength, padding + " ")) {
				return 1;
      }
    }
  }

  // At the end of the function, no cycle found
  return 0;
}



void IntervalGraph::ProtectEdges(std::string &protectedEdgeFileName,
																 int edgeTupleLength) {
	//	std::cout << "Function ProtectEdges is deprecated." << std::endl;
  ReadPositions protPositions;
  HashValueFunctor calcHashValue;
  calcHashValue.hashLength = 10;
  HashedSpectrum hashTable(calcHashValue);

  SimpleSequenceList seqList;
  ReadSimpleSequences(protectedEdgeFileName, seqList);

  CountedReadPos::hashLength = edgeTupleLength;
  CountedReadPos::sequences  = &seqList;
  calcHashValue.sequences    = &seqList;
	
	hashTable.hashFunction = calcHashValue;
	hashTable.StoreSpectrum(seqList);
	//	StorSpectrum(seqList, hashTable, calcHashValue);
	//	HashToReadPositionList(hashTable, seqList, edgeTupleLength, protPositions);
	hashTable.HashToReadPositionList(seqList, protPositions);
	CompareTuples<SimpleSequenceList> comp;
  comp.sequencesPtr = &seqList;
  comp.length = edgeTupleLength;
	std::sort(protPositions.begin(), protPositions.end(), comp);

	std::cout << "there are: " << protPositions.size() << " protected positions." << std::endl;
  ProtectEdges(protPositions, seqList, edgeTupleLength);
}


void IntervalGraph::ProtectEdges(ReadPositions &protectedPositions, 
																 SimpleSequenceList &protectedEdges, 
																 int edgeTupleLength) {
  int e;
  int protIndex;
  int totalProtected = 0;
  int bale;
  int p;
  int guarded;
  for (e = 0; e < edges.size(); e++ ) {
    guarded = 0;
    for (p = 0; !guarded and p < edges[e].length - edgeTupleLength + 1; p++) {
      protIndex = LocateTuple(protectedEdges, protectedPositions, 
															edgeTupleLength, (char*) &(edges[e].seq.seq[p]));
      if (protIndex >= 0) {
				guarded = 1;
      }
    }

    if (guarded) {
      if (edges[e].guarded != GraphEdge::Marked) {
				edges[e].guarded = GraphEdge::Marked;
				//				std::cout << "protecting edge: " << e << std::endl;
				edges[e].multiplicity += 100;
				++totalProtected;
      }
      bale = edges[e].balancedEdge;
      if (edges[bale].guarded != GraphEdge::Marked) {
				//				std::cout << "protecting balance " << bale << std::endl;
				++totalProtected;
				edges[bale].multiplicity += 100;
      }
    }
  }
  std::cout << "protected: " << totalProtected << " / " << edges.size() << std::endl;
}

int IntervalGraph::CountReadsContainedInEdge(int edge){ 
  int i;
  int read, pos;
  int numContainedReads = 0;
  for (i = 0; i < edges[edge].intervals->size(); i++) {
    read = (*edges[edge].intervals)[i].read;
    pos  = (*edges[edge].intervals)[i].readPos;
		
    if (pathLengths[read] == 1) {
      numContainedReads++;
    }
  }
  return numContainedReads;
}

int IntervalGraph::CountReadsExtendingIntoEdge(int edge, int limit) {
  int i;
  int read, pos;
  int numExtendInto = 0;
  for (i = 0; i < edges[edge].intervals->size(); i++) {
    read = (*edges[edge].intervals)[i].read;
    pos  = (*edges[edge].intervals)[i].pathPos;
    if (pathLengths[read] == 1)
      continue;
    if (pos == 0 and 
				edges[edge].length - (*edges[edge].intervals)[i].edgePos > limit) {
      numExtendInto++;
    }
    else if (pos == pathLengths[read]-1 and
						 (*edges[edge].intervals)[i].length > limit) {
      numExtendInto++;
    }
  }
  return numExtendInto;
}


void IntervalGraph::RemoveLowPathEdges(int minPaths, int minExtend) {
  int e;
  std::vector<int> verticesToRemove, edgesToRemove;
  int npaths;
  for (e = 0; e < edges.size(); e++) { 
    npaths = CountReadsContainedInEdge(e) +
      CountReadsPassingThroughEdge(e) +
      CountReadsExtendingIntoEdge(e, minExtend);
    if (npaths < minPaths) {
      RemoveEdgeAndMarkPathsForRemoval(e, verticesToRemove);
      edgesToRemove.push_back(e);
    }
  }
  RemoveMarkedIntervals();
  Prune(verticesToRemove, edgesToRemove);
	RemoveEmptyEdges();
}

int IntervalGraph::CountReadsPassingThroughEdge(int edge) {
  int i;
  int read, pos;
  int numPassingThrough = 0;
  for (i = 0; i < edges[edge].intervals->size(); i++) {
    read = (*edges[edge].intervals)[i].read;
    pos  = (*edges[edge].intervals)[i].pathPos;

    if (pos > 0 and pos < pathLengths[read] - 1) {
      numPassingThrough++;
    }
  }
  return numPassingThrough;
}

void IntervalGraph::PathToThread(int p, ThreadPath &path) {
	path.clear();
	int i, pathEdge, pathInterval;
	int destVertex, destVertexLength, intvLength, intvPos;
	int readLength;
	for (i = 0; i < pathLengths[p] - 1; i++) {
		// Look up the edge, interval for this path interval
		pathEdge     = paths[p][i].edge;
		pathInterval = paths[p][i].index;
				
		// Find the length along this path interval
		destVertex       = edges[pathEdge].dest;
		destVertexLength = vertices[destVertex].vertexSize;
		intvLength       = (*edges[pathEdge].intervals)[pathInterval].length -
			destVertexLength;
		intvPos = (*edges[pathEdge].intervals)[pathInterval].edgePos;
		path.push_back(ThreadPathInterval(pathEdge, intvLength, intvPos));
		readLength += intvLength;
	}
	// The last interval includes the entire vertex length
	pathEdge     = paths[p][i].edge;
	pathInterval = paths[p][i].index;
	intvLength   = (*edges[pathEdge].intervals)[pathInterval].length;
	intvPos      = (*edges[pathEdge].intervals)[pathInterval].edgePos;
	readLength += intvLength;
	path.push_back(ThreadPathInterval(pathEdge, intvLength, intvPos));

}

void IntervalGraph::DisconnectEdgesAtSource(std::vector<int> &edgeList) {
	// Each edge in the list gets a new source vertex
	int newSrcIndex = vertices.size();
	vertices.resize(newSrcIndex + edgeList.size());

	int e;
	int origSrc;
		
	for (e = 0; e < edgeList.size(); e++) {
		origSrc = edges[edgeList[e]].src;
		int outEdgeIndex = vertices[origSrc].LookupOutIndex(edgeList[e]);
		vertices[origSrc].out[outEdgeIndex] = -1;
		vertices[newSrcIndex].AddOutEdge(edgeList[e]);
		edges[edgeList[e]].src = newSrcIndex;
		vertices[newSrcIndex].vertexSize = vertexSize;
		newSrcIndex++;
	}
}

void IntervalGraph::DisconnectEdgesAtDest(std::vector<int> &edgeList) {
	// Each edge in the list gets a new source vertex
	int newDestIndex = vertices.size();
	vertices.resize(newDestIndex + edgeList.size());

	int e;
	int origDest;
		
	for (e = 0; e < edgeList.size(); e++) {
		origDest = edges[edgeList[e]].dest;
		int inEdgeIndex = vertices[origDest].LookupInIndex(edgeList[e]);
		vertices[origDest].in[inEdgeIndex] = -1;
		vertices[newDestIndex].AddInEdge(edgeList[e]);
		edges[edgeList[e]].dest = newDestIndex;
		vertices[newDestIndex].vertexSize = vertexSize;
		newDestIndex++;
	}
}


int IntervalGraph::CountIntervalsOnSimplePath(int edge) {
	int edgeIndex, dest;

	dest = edges[edge].dest;
	int numIntervals = edges[edge].intervals->size();
	while (vertices[dest].InDegree() == 1 and
				 vertices[dest].OutDegree() == 1) {
		edgeIndex = vertices[dest].FirstOut();
		edge = vertices[dest].out[edgeIndex];
		dest = edges[edge].dest;
		numIntervals+= edges[edge].intervals->size();
	}
	return numIntervals;
}
		

void IntervalGraph::MoveIntervals(int toEdge, int fromEdge, int toEdgeIntvStartIndex, int lengthOffset) {
	ReadIntervalList *fromIntvList = edges[fromEdge].intervals,
		*toIntvList = edges[toEdge].intervals;

	int i;
	int read, index;
	
	int numFromIntervals = fromIntvList->size();
	int dest             = edges[toEdge].dest;

	// 
	// Modify the edge position of the from edge intervals
	// to be relative to the start of the toEdge.
	//
	for (i = 0; i < numFromIntervals; i++) {
		(*fromIntvList)[i].edgePos += lengthOffset;
	}

	//
	// Move the interval objects.
	//
	//	toIntvList->resize(toIntvList->size() + numFromIntervals);
	std::copy(fromIntvList->begin(), fromIntvList->end(), toIntvList->begin() + toEdgeIntvStartIndex );
	
	//
	// Update the paths to reference the new edge and 
	// new position in the edge.
	//
	for (i = 0; i < fromIntvList->size(); i++) {
		if ((*fromIntvList)[i].markedForDeletion == 0) {
			read  = (*fromIntvList)[i].read;
			index = (*fromIntvList)[i].pathPos;
			paths[read][index].edge = toEdge;
			paths[read][index].index += toEdgeIntvStartIndex;
		}
	}
	fromIntvList->clear();
	edges[fromEdge].multiplicity = 0;

}




void IntervalGraph::MoveIntervals(int toEdge, int fromEdge) {
	
	// Update the paths to use 'to' where 'from' is used.
	int fromIndex = 0;
	int numFromIntervals = edges[fromEdge].intervals->size();
	int fromPath, fromPathPos;
	ReadIntervalList *intvList = edges[fromEdge].intervals;
	for (fromIndex = 0; fromIndex < numFromIntervals; fromIndex++) {
		if (!(*intvList)[fromIndex].markedForDeletion) {
			fromPath = (*intvList)[fromIndex].read;
			fromPathPos = (*intvList)[fromIndex].pathPos;
			if (fromPath != -1 and fromPathPos != -1) {
				paths[fromPath][fromPathPos].edge = toEdge;
			}
		}
	}
		
	// Append the intervals from the from edge.
	int numToIntervals = edges[toEdge].intervals->size();
	int numIntervals = numToIntervals + numFromIntervals;
	edges[toEdge].intervals->resize(numIntervals);
	int i, f;
	for (f = 0, i = numToIntervals; i < numIntervals; i++, f++) {
		(*edges[toEdge].intervals)[i] = (*edges[fromEdge].intervals)[f];
	}

	SortReadIntervalsByReadPos(*edges[toEdge].intervals);
	UpdatePathIndices(toEdge);

	// Clear the intervals on the from edge.
	edges[fromEdge].intervals->clear();

	// Set the multiplicity of the to edge.
	numToIntervals = edges[toEdge].intervals->size();
	int mult = numToIntervals;
	for (i = 0; i < numToIntervals; i++ ){ 
		if ((*edges[toEdge].intervals)[i].markedForDeletion)
			--mult;
	}
	edges[toEdge].multiplicity = mult;
}


void IntervalGraph::MergeOutEdges(int vertex,
																	int toEdge, int fromEdge) {

	int toDest = edges[toEdge].dest;
	int fromDest = edges[fromEdge].dest;

	// Remove the in-edges if necessary
	if (edges[toEdge].dest != edges[fromEdge].dest) {
		// Merge parallel edges (remove simple bulge)
		
		// Update the connectivity so that the from edge
		// is no longer used.
		int fromOutEdge, fromOutEdgeIndex;
		for (fromOutEdgeIndex = vertices[fromDest].FirstOut();
				 fromOutEdgeIndex != vertices[fromDest].EndOut();
				 fromOutEdgeIndex = vertices[fromDest].NextOut(fromOutEdgeIndex)) {
			fromOutEdge = vertices[fromDest].out[fromOutEdgeIndex];
			edges[fromOutEdge].src = toDest;
			vertices[toDest].AddOutEdge(fromOutEdge);
			vertices[fromDest].out[fromOutEdgeIndex] = -1;
		}
		int fromDestInEdge, fromDestInEdgeIndex;
		for (fromDestInEdgeIndex = vertices[fromDest].FirstIn();
				 fromDestInEdgeIndex != vertices[fromDest].EndIn();
				 fromDestInEdgeIndex = vertices[fromDest].NextIn(fromDestInEdgeIndex)) {
			fromDestInEdge = vertices[fromDest].in[fromDestInEdgeIndex];
			if (fromDestInEdge == fromEdge)
				continue;
			else {
				vertices[edges[toEdge].dest].AddInEdge(fromDestInEdge);
				edges[fromDestInEdge].dest = edges[toEdge].dest;
				vertices[fromDest].in[fromDestInEdgeIndex] = -1;
			}
		}
	}

	// Transfer intervals.  Leave lengths intact.
	MoveIntervals(toEdge, fromEdge);

	// Unlink the vertex->fromEdge, since fromEdge is deleted.
	int fromEdgeIndex = vertices[vertex].LookupOutIndex(fromEdge);
	assert(fromEdgeIndex >= 0);
	vertices[vertex].out[fromEdgeIndex] = -1;

	
	
	// Remove the in-edges if necessary
	if (edges[toEdge].dest != fromDest) {
		// Free the adjacency lists in the dest vertex.
		vertices[fromDest].out.clear();
		// hack for releasing the memory in an stl vector.
		vector<int>().swap(vertices[fromDest].out);
		vertices[fromDest].in.clear();
		// hack for releasing the memory of an stl vector.
		vector<int>().swap(vertices[fromDest].in);
	}
	else {
		fromEdgeIndex = vertices[fromDest].LookupInIndex(fromEdge);
		assert(fromEdgeIndex >= 0);
		vertices[fromDest].in[fromEdgeIndex] = -1;
	}


	edges[fromEdge].src = -1;
	edges[fromEdge].dest = -1;
}


void IntervalGraph::MergeInEdges(int vertex,
																 int toEdge, int fromEdge) {


	// Store which edge is short or long.
	int toSrc = edges[toEdge].src;
	int fromSrc = edges[fromEdge].src;
	if (edges[toEdge].src != edges[fromEdge].src) {
		// Merge parallel edges (remove simple bulge)

		// Update the connectivity so that the from edge
		// is no longer used.
		int fromInEdge, fromInEdgeIndex;
		for (fromInEdgeIndex = vertices[fromSrc].FirstIn();
				 fromInEdgeIndex != vertices[fromSrc].EndIn();
				 fromInEdgeIndex = vertices[fromSrc].NextIn(fromInEdgeIndex)) {
			fromInEdge = vertices[fromSrc].in[fromInEdgeIndex];
			edges[fromInEdge].dest = toSrc;
			vertices[toSrc].AddInEdge(fromInEdge);
			vertices[fromSrc].in[fromInEdgeIndex] = -1;
		}

		int fromSrcEdgeOutEdge, fromSrcEdgeOutEdgeIndex;
		for (fromSrcEdgeOutEdgeIndex = vertices[fromSrc].FirstOut();
				 fromSrcEdgeOutEdgeIndex != vertices[fromSrc].EndOut();
				 fromSrcEdgeOutEdgeIndex = vertices[fromSrc].NextOut(fromSrcEdgeOutEdgeIndex)) {
			fromSrcEdgeOutEdge = vertices[fromSrc].out[fromSrcEdgeOutEdgeIndex];
			// This edge is simply removed.
			if (fromSrcEdgeOutEdge == fromEdge)
				continue;
			else {
				edges[fromSrcEdgeOutEdge].src = toSrc;
				vertices[toSrc].AddOutEdge(fromSrcEdgeOutEdge);
				vertices[fromSrc].out[fromSrcEdgeOutEdgeIndex] = -1;
			}
		}
	}
	MoveIntervals(toEdge, fromEdge);

	// Unlink the fromEdge.
	int fromEdgeIndex = vertices[vertex].LookupInIndex(fromEdge);
	assert(fromEdgeIndex >= 0);
	vertices[vertex].in[fromEdgeIndex] = -1;

	if (edges[toEdge].src != edges[fromEdge].src) {
		// fromSrc is merged with edges[toEdge].src, so 
		// the adjacency lists may be safely removed.
		vertices[fromSrc].in.clear();
		vector<int>().swap(vertices[fromSrc].in);
		vertices[fromSrc].out.clear();
		vector<int>().swap(vertices[fromSrc].out);
	}
	else {
		fromEdgeIndex = vertices[fromSrc].LookupOutIndex(fromEdge);
		assert(fromEdgeIndex >= 0);
		vertices[fromSrc].out[fromEdgeIndex] = -1;
	}
		
	//	fromEdgeIndex = vertices[fromSrc].LookupOutIndex(fromEdge);
	//	assert(fromEdgeIndex >= 0);
	//	vertices[fromSrc].out[fromEdgeIndex] = -1;
	edges[fromEdge].dest = -1;
	edges[fromEdge].src  = -1;
}


void IntervalGraph::RemoveErasedPaths() {
	int p;
	int pi, curP;
	
	for (p = 0; p < paths.size(); p++) {
		curP = 0;
		for (pi = 0; pi < pathLengths[p]; pi++) {
			if (paths[p][pi].edge != -1 and paths[p][pi].index != -1) {
				paths[p][curP].edge = paths[p][pi].edge;
				paths[p][curP].index = paths[p][pi].index;
				(*edges[paths[p][curP].edge].intervals)[paths[p][pi].index].pathPos = curP;
				++curP;
			}
		}
		pathLengths[p] = curP;
	}
	
}

void IntervalGraph::Free() {
	
	FreePaths(paths, pathLengths);
	int e;
	for (e = 0; e < edges.size(); e++) {
			edges[e].Clear();
	}
}
