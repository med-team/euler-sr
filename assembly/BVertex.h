#ifndef BVERTEX_H_
#define BVERTEX_H_

#include "ReadPos.h"
#include "graph/GraphAlgo.h"
#include "SeqUtils.h"
#include <iostream>
using namespace std;

#define ALPHABET_SIZE 4

class BVertex : public ReadPos, public GraphVertex {
 public:
  int index;
	static int WriteFixedDegree;
  std::vector<int> out;
  std::vector<int> in;
  int length;
  unsigned char flagged: 1;

  BVertex() {
    out.resize(ALPHABET_SIZE);
    std::fill(out.begin(), out.end(), -1);
    in.resize(ALPHABET_SIZE);
    std::fill(in.begin(), in.end(), -1);
    index = -1;
    length = 0;
  }
  BVertex& operator=(const BVertex &b) {
    index = b.index;
    out   = b.out;
    in    = b.in;
    length = b.length;
    return *this;
  }

  int FirstOut() {
    return NextOut(-1);
  }
  int FirstIn() {
    return NextIn(-1);
  }

  int EndOut() {
    return out.size();
  }

  int EndIn() {
    return in.size();
  }
  int LookupOutIndex(int outEdgeIndex) {
    int outEdge = EndOut()-1;
		int firstOut = FirstOut();
    while (outEdge >= firstOut and out[outEdge] != outEdgeIndex)
      outEdge = PrevOut(outEdge);
		if (outEdge >= firstOut)
      return outEdge;
    else
      return -1;
  }
  int EraseOutEdge(int outEdgeIndex ) {
    int outEdge = LookupOutIndex(outEdgeIndex);
    assert(outEdge < EndOut());
    assert(outEdge >= 0);
    out[outEdge] = -1;
  }

  int LookupInIndex(int inEdgeIndex) {
    int inEdge = EndIn()-1;
		int firstIn = FirstIn();
    while (inEdge >= firstIn and in[inEdge] != inEdgeIndex)
      inEdge = PrevIn(inEdge);
    if (inEdge >= firstIn) 
      return inEdge;
    else 
      return -1;
  }
	
  int EraseInEdge(int inEdgeIndex ) {
    int inEdge = LookupInIndex(inEdgeIndex);
    assert(inEdge < EndIn());
    assert(inEdge >= 0);
    in[inEdge] = -1;
  }
    
	int PrevOut(int curEdge) {
		if (curEdge < 0)
			return curEdge;
		while (--curEdge >= 0 and out[curEdge] == -1);
		return curEdge;
	}

	int PrevIn(int curEdge) {
		if (curEdge < 0)
			return curEdge;
		while (--curEdge >= 0 and in[curEdge] == -1);
		return curEdge;
	}

  int NextOut(int curEdge) {
    int size = out.size();
    if (curEdge >= size)
      return size;
    while (++curEdge < size and out[curEdge] == -1 ); 
    return curEdge;
  }

  int NextIn(int curEdge) {
    int size = in.size();
    if (curEdge >= size) {
      return size;
    }
    while (++curEdge < size and in[curEdge] == -1);
    return curEdge;
  }

  
  int InDegree() {
    int i, d;
    d = 0;
    for (i = 0; i < EndIn(); i++ ) {
      if (in[i] != -1) d++;
    }
    return d;
  }
  int OutDegree() {
    int i, d;
    d = 0;
    for (i = 0; i < EndOut(); i++ ) {
      if (out[i] != -1) d++;
    }
    return d;
  }

	template<typename E>
		void GetSortedOutEdgeList(std::vector<E> &edges, 
															std::vector<int> &edgeList) {
		int curLength = 0;
		std::vector<std::pair<int, int> > lengthIndexList;
		int outEdge, outEdgeIndex;
		for (outEdgeIndex = FirstOut(); outEdgeIndex < EndOut(); outEdgeIndex = NextOut(outEdgeIndex)) {
			outEdge = out[outEdgeIndex];
			lengthIndexList.push_back(std::pair<int, int>(edges[outEdge].length, outEdge));
		}
		std::sort(lengthIndexList.begin(), lengthIndexList.end());
		int i;
		for (i = 0; i < lengthIndexList.size(); i++) {
			edgeList.push_back(lengthIndexList[i].second);
		}
	}

	template<typename E>
		void GetSortedInEdgeList(std::vector<E> &edges,
														 std::vector<int> &edgeList) {
		int curLength = 0;
		std::vector<std::pair<int, int> > lengthIndexList;
		int inEdge, inEdgeIndex;
		for (inEdgeIndex = FirstIn(); 
				 inEdgeIndex < EndIn(); 
				 inEdgeIndex = NextIn(inEdgeIndex)) {
			inEdge = in[inEdgeIndex];
			lengthIndexList.push_back(std::pair<int, int>(edges[inEdge].length, inEdge));
		}
		std::sort(lengthIndexList.begin(), lengthIndexList.end());
		int i;
		for (i = 0; i < lengthIndexList.size(); i++) {
			edgeList.push_back(lengthIndexList[i].second);
		}
	}
	
	int AddOutEdge(int edgeIndex) {
		out.push_back(edgeIndex);
		return out.size()-1;
	}
	
	int AddInEdge(int edgeIndex) {
		in.push_back(edgeIndex);
		return in.size()-1;
	}
	int AddOutEdge(int edgeIndex,
								 char *edgeSeq,
								 int edgeLength,
								 int vertexSize) { 
		assert(edgeLength > vertexSize + 1);
		int outIndex = unmasked_nuc_index[(int) edgeSeq[vertexSize + 1]];
		// Make sure we are not overwriting anythign
		assert(out[outIndex] == -1);
		out[outIndex] = edgeIndex;
		return outIndex;
	}

	int AddInEdge(int edgeIndex,
								char *edgeSeq,
								int edgeLength,
								int vertexSize) {
		assert(edgeLength > vertexSize + 1);
		int inIndex = unmasked_nuc_index[(int) edgeSeq[edgeLength - vertexSize - 1]];
		assert(in[inIndex] == -1);
		in[inIndex] = edgeIndex;
		return inIndex;
	}
	
  friend std::ostream &operator<<(std::ostream &out, BVertex &vertex);
  friend std::istream &operator>>(std::istream &in, BVertex &vertex);

	int CheckUniqueEdges(std::vector<int> &edgeList) {
		std::vector<int> tmpOut;
		unsigned int i;
		for (i = 0; i < edgeList.size(); i++ ){ 
			if (edgeList[i] != -1) {
				tmpOut.push_back(edgeList[i]);
			}
		}
		std::sort(tmpOut.begin(), tmpOut.end());
		unsigned int endOut = tmpOut.size();
		if (tmpOut.size() == 0)
			return 1;
		endOut--;
		for (i = 0; i < endOut; i++) {
			if (tmpOut[i] == tmpOut[i+1]) {
				return 0;
			}
		}
		return 1;
	}
	int CheckUniqueOutEdges() {
		return CheckUniqueEdges(out);
	}
	int CheckUniqueInEdges() {
		return CheckUniqueEdges(in);
	}

	void CondenseEdgeLists() {
		int curPos = 0;
		unsigned int o, i;
		for (o = 0; o < out.size(); o++ ){
			if (out[o] != -1) {
				out[curPos] = out[o];
				curPos++;
			}
		}
		out.resize(curPos);
		curPos = 0;
		for (i = 0; i < in.size(); i++) {
			if (in[i] != -1) {
				in[curPos] = in[i];
				curPos++;
			}
		}
		in.resize(curPos);
	}

	void Write(ostream &outs) {
		if (WriteFixedDegree) {
			outs << index << " ";
			outs << out[0] << " ";
			outs << out[1] << " ";
			outs << out[2] << " ";
			outs << out[3] << " ";
			outs << in[0] << " ";
			outs << in[1] << " ";
			outs << in[2] << " ";
			outs << in[3];
		}
		else {
			outs << index << " ";
			outs << "out " << out.size() << " ";
			unsigned int o, i;
			for (o = 0; o < out.size(); o++) {
				outs << out[o] << " ";
			}
			outs << "in " << in.size() << " ";
			for (i = 0; i < in.size(); i++ ){
				outs << in[i] << " ";
			}
		}
	}
};

typedef std::vector<BVertex> BVertexList;

#endif
