/***************************************************************************
 * Title:          RuleList.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "RuleList.h"
#include <fstream>
#include <iostream>
#include "utils.h"
#include "ParseTitle.h"

using namespace std;


int IsComment(string &line) {
	int i = 0;
	while (i < line.size() and isspace(line[i])) { i++; }
	if (i < line.size() and line[i] == '#')
		return 1;
	else 
		return 0;
}
		

void ParseRuleFile(std::string &ruleFile, std::vector<Rule> &rules) {
	std::ifstream ruleIn;
	openck(ruleFile,ruleIn, std::ios::in);
	std::string ruleLine;
	int length;
	int cloneVar;
	Rule rule;
	while(std::getline(ruleIn, ruleLine)) {
		if (IsComment(ruleLine)) {
			continue;
		}
		rule.regexpStr = "";
		ExtractQuotedString(ruleLine, rule.regexpStr);
		if (rule.regexpStr == "")
			break;
		ParseKeyword(ruleLine, "CloneLength", rule.cloneLength);
		ParseKeyword(ruleLine, "CloneVar", rule.cloneVar);
		if (ParseKeyword(ruleLine, "Type", rule.type) == 0) {
			rule.type = rules.size();
		}
		/*
			ParseKeyword(ruleLine, "Forward", rule.forward);
			ParseKeyword(ruleLine, "Reverse", rule.reverse);
		*/
		cout << "got rule: " << rule.regexpStr << endl;
		rules.push_back(rule);
		if (regcomp(&rules[rules.size()-1].compRegex, rule.regexpStr.c_str(), REG_EXTENDED)) {
			cout << "ERROR Parsing " << rule.regexpStr << endl;
			exit(1);
		}

	}
	int i;
	for (i= 0; i < rules.size(); i++) { 
		//		rules[i].regexp = rules[i].regexpStr;
	}
}


int GetReadRule(RuleList &rules, std::string &readTitle, int &readType) {

	int r;
	const char* readTitleStr = readTitle.c_str();
	size_t nmatch = 4;
	regmatch_t pmatch[4];
	for (r = 0; r < rules.size(); r++ ){
		int mv;
		if (!(mv = regexec(&rules[r].compRegex, readTitleStr, nmatch, pmatch, 0))) {
			readType = r;
			return 1;
		}
	}
	return 0;
}
