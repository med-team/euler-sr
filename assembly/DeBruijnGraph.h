/***************************************************************************
 * Title:          DeBruijnGraph.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  08/19/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef DE_BRUIJN_GRAPH_H_
#define DE_BRUIJN_GRAPH_H_

#include "DNASequence.h"
#include "SimpleSequence.h"
#include "SeqReader.h"
#include "ReadPos.h"
#include "SeqUtils.h"
#include "utils.h"
#include "graph/GraphAlgo.h"
#include "graph/MSTAlgo.h"
#include "SortedTupleList.h"


template<typename ReadPosType>
std::ostream &operator<<(std::ostream &out, std::vector<ReadPosType> &rp) {
  out << rp.size() << std::endl;
  int i;
  for (i = 0; i < rp.size(); i++ ) {
    out << rp[i] << std::endl;
  }
	return out;
}


template<typename ReadPosType>
std::istream &operator>>(std::istream &in, std::vector<ReadPosType> &rp) {
  int size;
  in >> size;
  int i;
  rp.resize(size);
  for (i = 0; i < size; i++ ) {
    in >> rp[i];
    if (! in ) {
      std::cout << "Error reading position " << i << " of " << size << std::endl;
      exit(1);
    }
  }
  return in;
}

template<typename V>
std::istream &ReadVertexList(std::istream &in, std::vector<V> &vertices) {
  int size;
  std::string word;
  in >> word >> size;
  int index;
  vertices.resize(size);
  int i;
  for (i = 0; i < size; i++ ) {
    in >> index;
    ReadVertex(in, vertices[i]);
		vertices[i].index = index;
  }
  return in;
}

template<typename V>
std::ostream &WriteVertexList(std::ostream &out, std::vector<V> &vertices) {
  int i;
  int numVertices = 0;
  for (i = 0; i < vertices.size(); i++ ) {
    if  (vertices[i].OutDegree() > 0 or
				 vertices[i].InDegree() > 0) {
      numVertices++;
    }
  }
 
  out << "Number_of_vertices " << numVertices << std::endl;

  for (i = 0; i < vertices.size(); i++ ) {
    if (vertices[i].OutDegree() > 0 or
				vertices[i].InDegree() > 0) {
      out << i << " ";
      WriteVertex(out, vertices[i]);
      out << std::endl;
    }
  }
  return out;
}

template<typename V>
std::ostream &WriteVertex(std::ostream &outs, V &vertex) {
	vertex.Write(outs);
  return outs;
}

template<typename V>
void CondenseEdgeLists(std::vector<V> &vertexList) {
	int v;
	for (v = 0; v < vertexList.size(); v++) {
		vertexList[v].CondenseEdgeLists();
	}
}

template<typename V>
std::istream &ReadVertex(std::istream &ins, V &vertex) {
  ins >> vertex.index;
	std::string token;
	ins >> token;
	int outIndex, inIndex;
	int i;
	if (token == "out") {
		int nOut;
		ins >> nOut;
		for (i = 0; i < nOut; i++ ){
			ins >> outIndex;
			vertex.out.push_back(outIndex);
		}
		ins >> token;
		if (token != "in") {
			std::cout << "ERROR reading vertex, expected 'in'."<< std::endl;
			exit(1);
		}
		int nIn;
		ins >> nIn;
		for(i = 0; i < nIn; i++ ) {
			ins >> inIndex;
			vertex.in.push_back(inIndex);
		}
	}
	else {
		vertex.out[0] = atoi(token.c_str());
		ins >> vertex.out[1] >> vertex.out[2] >> vertex.out[3]
				>> vertex.in[0] >> vertex.in[1] >> vertex.in[2] >> vertex.in[3];
	}
  return ins;
}

template<typename E>
std::ostream &WriteEdgeList(std::ostream &out, std::vector<E> &edges) {
  out << "Number_of_edges " << edges.size() << std::endl;
  int i;
  for (i = 0; i < edges.size(); i++) { 
    if (! edges[i].IsNullified() )
      out << i << " " << edges[i] << std::endl;
  }
  return out;
}
template<typename E>
std::istream &ReadEdgeList(std::istream &in, std::vector<E> &edges ) {
  int size, index;
  std::string word;
  in >> word >> size;
  edges.resize(size);
  int i;
  for (i = 0; i < size; i++) {
		edges[i].Init();
    in >> index >> edges[i];
		edges[i].index = index;
		//    edges[i].index = index;
  }
  return in;
}


template<typename V>
int StoreZeroDegreeVertexIndices(std::vector<V> &vertices, std::vector<int> &indices ) {
  int v;
  for (v = 0; v < vertices.size(); v++ ) {
    if (vertices[v].InDegree() == 0 and
				vertices[v].OutDegree() == 0) 
      indices.push_back(v);
  }
  return indices.size();
}

template<typename V, typename E>
	int RemoveZeroDegreeVertices(std::vector<V> &vertices, std::vector<E> &edges) {
  int nZero = 0;
  std::vector<int> removedIndices;
  nZero = StoreZeroDegreeVertexIndices(vertices, removedIndices);

  // If there is nothing to fix, end now
  if (nZero == 0) {
    return 0;
  }
  DeleteElementsFromList(vertices, removedIndices);
  UpdateRemovedVertexIndices(edges, removedIndices);
  
  return nZero;
}

template<typename V>
int DeleteElementsFromList(std::vector<V> &list, std::vector<int> &removedIndices) {
  // Now copy the removed vertices from the list
  if (removedIndices.size() == 0) 
    return 0;
  int curRemoved = 0;
  int v;
  for (v = 0; v < list.size(); v++ ) {
    //    stopfn(v);
    if (curRemoved < removedIndices.size() and
				v == removedIndices[curRemoved]) {
      curRemoved++;
    }
    else {
      //      std::cout << "prev: " << v << " new: " << v - curRemoved << std::endl;
      list[v - curRemoved] = list[v];
    }
  }
  list.resize(list.size() - removedIndices.size());
  return removedIndices.size();
}

template<typename E>
int UpdateRemovedEdgeBalancedIndices(std::vector<E> &edges,
																		 std::vector<int> &removedEdges) {
  // Once edges have been removed from the list, 
  // the indices of the balanced edges need to be updated
  // to the new ordering of edges.
  // In the case of updating vertex in/out edge indices and edge src/dest
  // vertex indices, we don't expect to find an index in the removedIndices 
  // list because only unremoved indices remain.  However, since we're simply
  // re-packing the edges here (indices 1 2 X ALPHABET_SIZE X X 5 6 X 7 -> 
  // 1 2 3 4 5 6), the balanced edges have nothing to do with whether or not
  // they are from a removed edge.

  int e;
  int balancedEdge;
  if (removedEdges.size() == 0) 
    return 0;

  std::vector<int>::iterator edgeIt;
  int numLowerRemovedEdges;
  for (e = 0; e < edges.size(); e++ ) {
    balancedEdge = edges[e].balancedEdge;
    assert(balancedEdge != -1);
    if (std::binary_search(removedEdges.begin(),
													 removedEdges.end(), balancedEdge) != 0) {
      std::cout << "edge: " << balancedEdge 
								<< " should not be in the edge list " << std::endl;
      assert(0);
    }
    if (edges[e].balancedEdge >= removedEdges[0]) {
      if (balancedEdge > removedEdges[removedEdges.size()-1]) {
				numLowerRemovedEdges = removedEdges.size();
      }
      else {
				edgeIt = std::lower_bound(removedEdges.begin(),
																	removedEdges.end(),
																	balancedEdge);
				numLowerRemovedEdges = edgeIt - removedEdges.begin();
      }
      edges[e].balancedEdge -= numLowerRemovedEdges;
    }
  }
  return 0;
}

template<typename V>
void UpdateRemovedEdgeIndices(std::vector<V> &vertices,
															std::vector<int> &removedIndices) {
  int v;
  int i, o;
  int edgeIndex;
  int numberRemovedEdgesBelow;
  std::vector<int>::iterator nextEdgeIndex;
  for (v = 0; v < vertices.size(); v++ ) {
    //squeeze edge indices back down after removing them
    for (i = 0; i < vertices[v].EndIn(); i++ ) {
      edgeIndex = vertices[v].in[i];
      if (std::binary_search(removedIndices.begin(), 
														 removedIndices.end(), edgeIndex) != 0) {
				std::cout << "edge index: " << edgeIndex 
									<< " should not be part of vertex "
									<< v << std::endl;
				assert(0);
      }
      if (edgeIndex >= removedIndices[0]) {
				nextEdgeIndex = std::lower_bound(removedIndices.begin(),
																				 removedIndices.end(),
																				 edgeIndex);
				if(nextEdgeIndex == removedIndices.end())
					numberRemovedEdgesBelow = removedIndices.size();
				else
					numberRemovedEdgesBelow = nextEdgeIndex - removedIndices.begin();
				vertices[v].in[i] -= numberRemovedEdgesBelow;
      }
    }
    // repeated code, except using 'out'
    for (o = 0; o < vertices[v].EndOut(); o++ ) {
      edgeIndex = vertices[v].out[o];
      if (std::binary_search(removedIndices.begin(), 
														 removedIndices.end(), edgeIndex) != 0) {
				std::cout << "out edge: " << edgeIndex 
									<< " should not be a part of " 
									<< v << std::endl;
				assert(0);
      }
      if (edgeIndex >= removedIndices[0]) {
				nextEdgeIndex = std::lower_bound(removedIndices.begin(),
																				 removedIndices.end(),
																				 edgeIndex);
				if(nextEdgeIndex == removedIndices.end())
					numberRemovedEdgesBelow = removedIndices.size();
				else
					numberRemovedEdgesBelow = nextEdgeIndex - removedIndices.begin();
				vertices[v].out[o] -= numberRemovedEdgesBelow;
      }
    }
  }
}

template<typename E>
void UpdateRemovedVertexIndices(std::vector<E> &edges, std::vector<int> &removedIndices) {
  // Now offset the vertex indices stored in the edge list
  int e;
  int vertex;
  int numberRemovedVerticesBelow;
  std::vector<int>::iterator nextIndex;
  
  for (e = 0; e < edges.size(); e++) {
    vertex = edges[e].src;

    // No edge should reference a removed vertex.
    if (std::binary_search(removedIndices.begin(), removedIndices.end(), vertex) != 0) {
      std::cout << "vertex " << vertex << " should not be src of  " << e << std::endl;
      assert(0);
    }

    // If the vertex has vertices with indices lower than it that 
    // have been removed, we need to fix the index so that only
    // present indices remain.
    if (vertex >= removedIndices[0]) {
      nextIndex = lower_bound(removedIndices.begin(), removedIndices.end(), vertex);
      if (nextIndex == removedIndices.end())
				numberRemovedVerticesBelow = removedIndices.size();
      else 
				numberRemovedVerticesBelow = nextIndex - removedIndices.begin();
      edges[e].src -= numberRemovedVerticesBelow;
    }

    // Now the same thing with the destination vertex
    vertex = edges[e].dest;
    if (binary_search(removedIndices.begin(), removedIndices.end(), vertex) != 0) {
      std::cout << "vertex " << vertex  << " should not be dest of " << e << std::endl;
      assert(0);
    }
    if (vertex >= removedIndices[0]) {
      nextIndex = lower_bound(removedIndices.begin(), removedIndices.end(), vertex);
      if (nextIndex == removedIndices.end())
				numberRemovedVerticesBelow = removedIndices.size();
      else 
				numberRemovedVerticesBelow = nextIndex - removedIndices.begin();
      edges[e].dest -= numberRemovedVerticesBelow;
    }
  }
}



template<typename T, typename E>
	void PrintBGraph(std::vector<T> &vertices, std::vector<E> &edges, int vertexSize, std::ostream &out) {
  WriteVertexList(out, vertices);
  WriteEdgeList(out, edges);
	out << vertexSize << std::endl;
}

template<typename T, typename E>
	void PrintBGraph(std::vector<T> &vertices, std::vector<E> &edges, int vertexSize, std::string &outName) {
  std::ofstream out;
  openck(outName, out, std::ios::out);
  PrintBGraph(vertices, edges, vertexSize, out);
  out.close();
}


template<typename V, typename E>
	int ReadBGraph(std::string &inName, std::vector<V> &vertices, std::vector<E> &edges) {
  std::ifstream in;
  openck(inName, in, std::ios::in);
  ReadVertexList(in, vertices);
  ReadEdgeList(in, edges);
	int vertexSize;
	if (!(in >> vertexSize)) {
		vertexSize = 1;
	}
  in.close();
	return vertexSize;
}

template<typename V, typename E>
	void CheckVertices(std::vector<V> &vertices, std::vector<E> &edges) {
  int v;
  int nEdges = edges.size();
  for (v = 0; v < vertices.size(); v++ ) {
    assert(vertices[v].in[0] < nEdges);
    assert(vertices[v].in[1] < nEdges);
    assert(vertices[v].in[2] < nEdges);
    assert(vertices[v].in[3] < nEdges);
    assert(vertices[v].out[0] < nEdges);
    assert(vertices[v].out[1] < nEdges);
    assert(vertices[v].out[2] < nEdges);
    assert(vertices[v].out[3] < nEdges);
  }
}

template<typename V, typename E>
	int CheckEdges(std::vector<V> &vertices, std::vector<E> &edges) {
  int e;
	int src, dest, srcEdgeIndex, destEdgeIndex;
  for (e = 0; e < edges.size(); e++) {
		if (edges[e].src >= 0 and edges[e].dest >= 0) {
			src = edges[e].src;
			srcEdgeIndex = vertices[src].LookupOutIndex(e);
			assert(srcEdgeIndex >= 0);
			dest = edges[e].dest;
			destEdgeIndex = vertices[dest].LookupInIndex(e);
			assert(destEdgeIndex >= 0);
		}
  }
  return 1;
}


template<typename E>
int CheckBalancedEdgeList(std::vector<E> &edges, std::vector<int> &indices) {
  int i;
  for (i = 0; i < indices.size(); i++ ) {
    if (!std::binary_search(indices.begin(), indices.end(), edges[indices[i]].balancedEdge)) {
      std::cout << "edge " << indices[i] << " is in the list but not " << edges[indices[i]].balancedEdge << std::endl;

      return 0;
    }
  }
  return 1;
}


template<typename E>
void CheckBalance(std::vector<E> &edges) {
  int e;
  int balancedEdge;
  for (e = 0; e < edges.size(); e++ ) {
    balancedEdge = edges[e].balancedEdge;
    assert(balancedEdge >= 0);
    assert (edges[balancedEdge].balancedEdge == e);
  }
}

template<typename Vertex_t, typename Edge_t>
	class PrintComponentFunctor {
 public:
  int size;
  int degree;
  int component;
  int edgeLength;
  std::vector<Vertex_t> *vertices;
  std::vector<Edge_t> *edges;

  PrintComponentFunctor() {
    component = 0;
  }
  void Reset() {
    size = 0; 
    degree = 0;
    edgeLength = 0;
  }
  void operator()(int vertexIndex) {

    int i;
    for (i = 0; i < (*vertices)[vertexIndex].EndOut(); i++ ) {
      if ((*vertices)[vertexIndex].out[i] >= 0) {
				edgeLength += (*edges)[(*vertices)[vertexIndex].out[i]].length;
				/*
					std::cout << " e: " << (*vertices)[vertexIndex].out[i] << " (";
					std::cout << (*edges)[(*vertices)[vertexIndex].out[i]].length << " b: ";
					std::cout << (*edges)[(*vertices)[vertexIndex].out[i]].balancedEdge << ") ";
				*/
      }
    }
    size ++;
    degree += (*vertices)[vertexIndex].OutDegree();
  }
};

template<typename Vertex_t, typename Edge_t>
	void PrintComponents(std::vector<Vertex_t> &vertices,
											 std::vector<Edge_t> &edges) {
  PrintComponentFunctor<Vertex_t, Edge_t> printComponent;

  printComponent.vertices = &vertices;
  printComponent.edges = &edges;
  int v;
  printComponent.component = 0;
  for (v = 0; v < vertices.size(); v++ ) {
    if (vertices[v].marked == GraphVertex::NotMarked) {
      printComponent.Reset();
      TraverseDFS(vertices,edges,v,printComponent);
      std::cout << printComponent.component << " " 
								<< printComponent.size << " " 
								<< printComponent.degree << " "
								<< printComponent.edgeLength << std::endl;
      printComponent.component++;
    }
  }
}

template<typename Edge_t>
void ReadSequences(std::string edgeFileName, std::vector<Edge_t> &edges) {
  SimpleSequenceList sequences;
  ReadSimpleSequences(edgeFileName, sequences);
  assert(sequences.size() == edges.size());
  int e;
  for (e = 0; e < sequences.size(); e++ ) {
    edges[e].seq.seq = sequences[e].seq;
    edges[e].seq.length = sequences[e].length;
    edges[e].length  = sequences[e].length;
  }
}

template<typename Vertex_t, typename Edge_t>
	void PrintEdges(std::vector<Vertex_t> &vertices, std::vector<Edge_t> &edges, std::string &edgeFileName) {
  int v, e;
  DNASequence tmpSeq;
  std::string blank;
  std::ofstream seqOut;
  openck(edgeFileName, seqOut, std::ios::out);
  int edgeIndex;
	for (e = 0; e < edges.size(); e++) {
		
		//  for (v = 0; v < vertices.size(); v++ ) {
		//    for (e = 0; e < vertices[v].EndOut(); e++ ) {
		//      if (vertices[v].out[e] >= 0) {
		//				edgeIndex = vertices[v].out[e];
		//		tmpSeq.seq = edges[edgeIndex].seq.seq;
		tmpSeq.seq = edges[e].seq.seq;
		tmpSeq.length = edges[e].seq.length;
		tmpSeq.ClearName();
		*tmpSeq.titlestream << edges[e].src << " -> " << edges[e].dest 
												<< " (" << e << ")";
		tmpSeq.PrintSeq(seqOut);
		seqOut << std::endl;
		//	}
		//    }
  }  
}

template<typename Vertex_t, typename Edge_t>
	void PrintGraph(std::vector<Vertex_t> &vertices, std::vector<Edge_t> &edges, std::string graphOutName) {
  std::ofstream graphOut;
  openck(graphOutName, graphOut, std::ios::out);
  int v, e;
  int numVertices = 0;
  for (v = 0; v < vertices.size(); v++ ){
    if (vertices[v].OutDegree() != 0 or
				vertices[v].InDegree() != 0 )
      numVertices++;
  }
  graphOut << "Number_of_Vertex " << numVertices << std::endl;

  int balEdge;
  int curVertex = 0;
  for (v = 0; v < vertices.size(); v++ ) {
    if (vertices[v].InDegree() != 0 or 
				vertices[v].OutDegree() != 0) {
      graphOut << "Vertex " << curVertex << " "  << vertices[v].OutDegree() << " "
							 << vertices[v].InDegree() << std::endl;
      graphOut << "Last_edge";
      for (e = 0; e < vertices[v].EndIn(); e++) { 
				if (vertices[v].in[e] >= 0) {
					graphOut << " " << vertices[v].in[e];
					balEdge = edges[vertices[v].in[e]].balancedEdge;
					graphOut << " " << balEdge;
				}
      }
      graphOut << std::endl;
      graphOut << "Next_edge";
      for (e = 0; e < vertices[v].EndOut(); e++) { 
				if (vertices[v].out[e] >= 0) {
					graphOut << " " << vertices[v].out[e];
					balEdge = edges[vertices[v].out[e]].balancedEdge;
					graphOut << " " << balEdge;
				}
      }
      graphOut << std::endl;
      curVertex++;
    }
  }
  graphOut.close();
}

template<typename V, typename E>
	void GVZPrintBGraph(std::vector<V> &vertices, std::vector<E> &edges, std::string &graphOutName) {
  std::ofstream graphOut;
  openck(graphOutName, graphOut, std::ios::out);
  
  int v, e;
  graphOut << "digraph G {"<<std::endl
					 << "\tsize=\"8,8\";"<<std::endl
					 << "\toverlap=\"false\";" << std::endl;
	

  int edgeIndex;
  for (v = 0; v < vertices.size(); v++ ) {
    for (e = 0; e < vertices[v].EndOut(); e++ ) {
      if (vertices[v].out[e] != -1) {
				edgeIndex = vertices[v].out[e];
				graphOut << "\t" << v << " -> " << edges[edgeIndex].dest << " [label=\"" ;
				graphOut << edgeIndex <<" {" << edges[edgeIndex].index << "} (" << edges[edgeIndex].seq.length << ")\""; 
				if (edges[edgeIndex].flagged == GraphEdge::Marked)
					graphOut << " ,color=red ";
	    
				graphOut <<  "] weight=\"0.5\" fontsize=\"10\" len=\"3\";" << std::endl;
      }
    }
    graphOut << "\t" << v << " [label=\"";
    graphOut << " " << v << " : " << vertices[v].index << "\" ";
    if (vertices[v].flagged == GraphVertex::Marked)
      graphOut << " " << ", color=\"gray\",style=filled  ";
    graphOut << "];" << std::endl;
  }
  graphOut << "}" << std::endl;
}


template<typename Vertex_t, typename Edge_t> 
	void CalcBalancedMST(std::vector<Vertex_t> &vertices, std::vector<Edge_t> &edges) {
  Unmark(vertices);
  Unmark(edges);
    
  std::vector<Edge_t*> edgePtrs;
  edgePtrs.resize(edges.size());
  int e;
  for (e = 0; e < edges.size(); e++ )
    edgePtrs[e] = &(edges[e]);

  GraphAlgo::CompareEdgePtrs<Edge_t> compareEdgePtrs;
  std::sort(edgePtrs.begin(), edgePtrs.end(), compareEdgePtrs);

  std::vector<DisjointSet::DJVertex<Vertex_t > > djForest;
  DisjointSet::CreateForest(vertices, djForest);
  
  int srcVertex, destVertex;
  Edge_t *edge;
  int balEdge, balSrc, balDest;
  int balancedEdge;
  for (e = 0; e < edgePtrs.size(); e++ ) {
    // add e
    edge = edgePtrs[e];
    srcVertex = edge->src;
    destVertex = edge->dest;
    if (DisjointSet::Find(&(djForest[srcVertex])) != 
				DisjointSet::Find(&(djForest[destVertex]))) {
      DisjointSet::Union(&(djForest[srcVertex]), 
												 &(djForest[destVertex]));
      std::cout << "adding " << edge->index << " to mst " << std::endl;
      edge->mst = GraphAlgo::MSTIn;

      balancedEdge = edge->balancedEdge;
      balSrc = edges[balancedEdge].src;
      balDest = edges[balancedEdge].dest;
      if (balancedEdge != edge->index) {
				// the balanced edge should not have been added yet to the graph
				if (DisjointSet::Find(&(djForest[balSrc])) != 
						DisjointSet::Find(&(djForest[balDest]))) {
					DisjointSet::Union(&(djForest[balSrc]), 
														 &(djForest[balDest]));
					std::cout << "adding balanced edge: " << balancedEdge << " to mst " << std::endl;
					edges[balancedEdge].mst = GraphAlgo::MSTIn;
				}
				else {
					if (edges[balancedEdge].balancedEdge != edge->index) {
						std::cout << "Is there something strange about this? added a balanced edge," << std::endl;
						std::cout << " but not the normal edge " << std::endl;
					}
				}
      }
    }
  }
}

template<typename V, typename E>
	void RemoveEdge(std::vector<V> &vertices, std::vector<E> &edges, int edge,
									int &srcIndex, int &destIndex) {
  int src, dest;
  src = edges[edge].src;
  dest = edges[edge].dest;
  srcIndex = vertices[src].LookupOutIndex(edge);
  assert(srcIndex >=0);
	assert(dest >= 0);
	assert(dest <= vertices.size());
  destIndex = vertices[dest].LookupInIndex(edge);
  assert(destIndex >=0);
  vertices[src].out[srcIndex] = -1;
  vertices[dest].in[destIndex] = -1;
  edges[edge].src = -1;
  edges[edge].dest = -1;
}
#endif
