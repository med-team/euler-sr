/***************************************************************************
 * Title:          ListSpectrum.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/29/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef LIST_SPECTRUM_H_
#define LIST_SPECTRUM_H_

#include "Spectrum.h"
#include "utils.h"

template <typename T>
class ListSpectrum : public Spectrum<T> {

 public:
	std::vector<T> tupleList;
	int size() {
		return tupleList.size();
	}
	int Resize(int size) {
		tupleList.resize(size);
	}

	T &operator[](int i) {
		return tupleList[i];
	}

	int Read(std::string &fileName, int minMult = 0) {
		std::ifstream kmerFile;
		openck(fileName, kmerFile, std::ios::in);
		int mult;
		int numPastThresh = 0;
		int numTuples;
		if (!(kmerFile >> numTuples)) {
			std::cout << "Error, the spectrum must begin with a number." << std::endl;
			exit(1);
		}
		
		T tempTuple;
		int line = 0;
		while(kmerFile && line < numTuples) {
			//			tempTuple.ReadLine(kmerFile);
			line++;
			if (tempTuple.ReadLine(kmerFile, minMult)) {
				if (tempTuple.Valid()) {
					numPastThresh++;
				}
			}
		}
		kmerFile.close();
		kmerFile.clear();
		openck(fileName, kmerFile, std::ios::in);
		kmerFile >> numTuples;
		Resize(numPastThresh);
		int index = 0;
		line = 0;
		while(kmerFile and line < numTuples) {
			if (tempTuple.ReadLine(kmerFile, minMult)) {
				if (tempTuple.Valid()) {
					//					std::cout << "storing tuple at " << index << std::endl;
					//					Pause();
					tupleList[index] = tempTuple;
					index++;
				}
			}
			line++;
		}
		this->tupleSize = tempTuple.tupleSize;
		index++;
	}

	void Write(std::string &fileName, int minMult = 0) {
		int numSpectra = 0;
		int i;
		for (i =0 ; i < tupleList.size(); i++ ) {
			if (tupleList[i].GetMult() >= minMult) numSpectra++;
		}
		std::ofstream spectOut;
		openck(fileName, spectOut, std::ios::out);
		spectOut << numSpectra << std::endl;
		for (i = 0;i < tupleList.size(); i++) {
			if (tupleList[i].GetMult() >= minMult) {
				spectOut << tupleList[i] << std::endl;
			}
		}
		spectOut.close();
	}

	int IncrementMult(T &tuple) {
		int listIndex;
		listIndex = FindTuple(tuple);
		if (listIndex != -1) {
			return tupleList[listIndex].IncrementMult();
		}
		return 0;
	}

	int FindTuple(std::string &tupleStr) {
		T tempTuple;
		tempTuple = tupleStr;
		return FindTuple(tempTuple);

	}
	int FindTuple(T &tuple) {

		// Do a simple binary search for value in spectrum
		int minIndex = 0;
		int maxIndex = tupleList.size();

		int index = (maxIndex + minIndex) / 2;

		if (tuple < tupleList[0] or 
				tuple > tupleList[tupleList.size()-1]) {
			return -1;
		}

		while (index < maxIndex and tupleList[index] != tuple ) {
			if (maxIndex -1 == minIndex) 
				// minIndex is as close to the value is as possible, so it 
				// is not going to be found.
				return -1;

			if (tupleList[index] < tuple) {
				minIndex = index;
			}
			else if (tuple < tupleList[index]) {
				maxIndex = index;
			}
			else {
				std::cout << "case that shouldn't exist!!!!\n";
				exit(0);
			}
			index = (maxIndex + minIndex) / 2;
		}
		return index;
	}

};

#endif
