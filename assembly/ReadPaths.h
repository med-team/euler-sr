/***************************************************************************
 * Title:          ReadPaths.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/18/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef READ_PATHS_H_
#define READ_PATHS_H_

#include <istream>
#include <fstream>
#include <assert.h>
#include "utils.h"

class PathInterval {
 public:
	int edge;
	int index:31;
	int set:1;
	PathInterval() { edge = -1; index = -1; set = 0;}
	PathInterval(int e, int i) {edge= e; index = i; }
};


class Path {
 public:
	PathInterval *interval;
	int length;
	int index;
	Path() {
		length = 0;
		interval = NULL;
		index = -1;
	}
	Path& operator=(const Path rhs) {
		interval = rhs.interval;
		length   = rhs.length;
		return *this;
	}
};

typedef std::vector<PathInterval*> PathIntervalList;
typedef std::vector<Path*> PathList;
typedef std::vector<int> PathLengthList;
typedef std::vector<std::vector<int> > PathEdgeList;


class ComparePaths {
 public:

	int operator()(const Path *pathA , const Path *pathB) {
		assert(pathA);
		assert(pathB);
		int length = pathA->length;
		if (pathA->length > pathB->length)
			length = pathB->length;

		int i;
		for (i = 0; i < length; i++ ) {
			if (pathA->interval[i].edge < pathB->interval[i].edge)
				return 1;
		}
		return 0;
	}
};

int GetLastEdgeIndex(PathIntervalList &paths, PathLengthList &pathLengths, int pathIndex,
										 int &lastEdge, int &lastIntv);

int GetFirstEdgeIndex(PathIntervalList &paths, PathLengthList &pathLengths, int pathIndex,
										 int &lastEdge, int &lastIntv);

void ReadReadPaths(std::string &pathInFile, PathIntervalList &paths, PathLengthList &pathLengths);
void WriteReadPaths(std::string &pathOutFile, PathIntervalList &paths, PathLengthList &pathLengths);
void SortPaths(PathList &paths);
void PathIntervalListToPathList(PathIntervalList &pathIntervals,
																PathLengthList &pathLengths,
																PathList &paths);

void PathIntervalListToPathEdgeList(PathIntervalList &pathIntervals,
																		PathLengthList &pathLengths,
																		PathEdgeList &paths);

int ArePathsEqual(PathInterval *pathA, int lengthA, PathInterval *pathB, int lengthB);

int SplicePath(PathIntervalList &paths, PathLengthList &pathLengths, 
							 int path, int start, int end);

void FreePaths(PathIntervalList &paths, PathLengthList &pathLengths);

#endif
