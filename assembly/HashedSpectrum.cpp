/***************************************************************************
 * Title:          HashedSpectrum.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/05/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "HashedSpectrum.h"
#include "utils.h"





int HashedSpectrum::HashSequence(SimpleSequenceList &seqListPtr, int curSeq,
																 int trimFront, int trimEnd) {
  int p, p2;
  CountedReadPos readPos;
  int newTupleStored = 0;
  int valid = 1;
  readPos.read = curSeq;
  for (p = trimFront; p < seqListPtr[curSeq].length - CountedReadPos::hashLength + 1 - trimEnd; p++) {
    readPos.pos = p;
    valid = 1;
    for (p2 = 0; p2 < CountedReadPos::hashLength and valid; p2++) {
      if (unmasked_nuc_index[seqListPtr[curSeq].seq[p+p2]] >= 4)
				valid = 0;
    }
    if (valid and hashTable.Store(readPos, hashFunction)) 
      newTupleStored = 1;
  }
  return newTupleStored;
}

void HashedSpectrum::StoreSpectrum(SimpleSequenceList &seqList,
																	 int trimFront, int trimEnd ) {
	int s;
	int storeSeq;
	int seqNumber = 0;

	for (s = 0; s < seqList.size(); s++ ) {
		storeSeq = HashSequence(seqList, s, trimFront, trimEnd);
			
		seqNumber++;
		PrintStatus(seqNumber, 100000);
	}
}

void HashedSpectrum::HashToReadPositionList(SimpleSequenceList &sequences,
																						ReadPositions &readPos ) {
	int i;
	int nReadPos = 0;
  ReadPosHashTable::iterator it, end;
	ReadPosHashTable::HashPage *page;
	ReadPosHashTable::Data *data, *pageEnd;
  for (i = 0; i < hashTable.size; i++ ) {

		page = hashTable.table[i].head;
		while (page != NULL) {
			pageEnd = &(*page).page[page->size];
			for (data = &(*page).page[0]; data != pageEnd; ++data) {
				++nReadPos;
			}
			page = page->next;
		}
		/*    end = hashTable.table[i].End();
    it  = hashTable.table[i].Begin();
    while (it < end) {
      ++nReadPos;
      ++it;
    }
		*/
  }
	readPos.resize(nReadPos);
	int rp = 0;
	for (i = 0; i < hashTable.size; i++ ) {
		page = hashTable.table[i].head;
		while (page != NULL) {
			pageEnd = &(*page).page[page->size];
			for (data = &(*page).page[0]; data != pageEnd; ++data) {

				/*    end = hashTable.table[i].End();
    it  = hashTable.table[i].Begin();
    while (it < end) {
				*/
				readPos[rp].read = data->read; //(*it).read;
				readPos[rp].pos  = data->pos;  // (*it).pos;
				rp++;
				++it;
			}
			page = page->next;
		}
	}
	// Set up the functor to compare the tuples
  CompareTuples<SimpleSequenceList> comp;
  comp.sequencesPtr = &sequences;
  comp.length = hashLength;

	  // Do quicksort on the read positions
  std::sort(readPos.begin(), readPos.end(), comp);
}


HashedSpectrum::HashedSpectrum(HashValueFunctor &hashFunctionP) {
	hashFunction = hashFunctionP;
	hashTable.Init(hashFunction);
}

	
