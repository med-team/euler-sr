#ifndef BEDGE_H_
#define BEDGE_H_

#include "graph/GraphAlgo.h"

class BEdge : public GraphEdge {
 public:
  SimpleSequence seq;
  //  ReadIntervalList readIntervals;
  int src;
  int dest;
  int multiplicity;
  int index;
  int balancedEdge;
  int length;
  unsigned char flagged      : 1;

  friend std::ostream &operator<<(std::ostream &out, BEdge &edge) {
    out << edge.src << " " << edge.dest << " " << edge.multiplicity 
				<< " " << edge.index << " " << edge.balancedEdge << " " << edge.length;
    return out;
  }
  friend std::istream &operator>>(std::istream &in, BEdge &edge) {
    in >> edge.src >> edge.dest 
       >> edge.multiplicity >> edge.index >> edge.balancedEdge >> edge.length;
    return in;
  }
	void Init() {}
  BEdge() {
    dest = -1;
    src  = -1;
    seq.seq = NULL;
    seq.length = 0;
    multiplicity = 0;
    balancedEdge = -1;
    flagged = GraphVertex::NotMarked;
  }
  void Copy(const BEdge &e) {
    src  = e.src;
    dest = e.dest;
    multiplicity = e.multiplicity;
    index = e.index;
    balancedEdge = e.balancedEdge;
    length  = e.length;
  }

  BEdge& ShallowCopy(const BEdge &e) {
    seq.length = e.seq.length;
    seq.seq = e.seq.seq;
    Copy(e);
    return *this;
  }
  BEdge& operator=(const BEdge &e) {
    /*
      seq.length = e.seq.length;
      seq.seq = new unsigned char[seq.length];
      memcpy(seq.seq, e.seq.seq, seq.length);
      // copy the rest
      Copy(e);
    */
    // for now, just do a shallow copy, hopefully that will speed
    // things up
    return ShallowCopy(e);
  }
  int Cost() {
		assert(multiplicity > 0);
		return -multiplicity;
  }
  void Cost(int cost) {
    multiplicity = -cost;
  }

  void Nullify(){ 
    dest = -1;
    src = -1;
    if (seq.seq != NULL) {
      delete [] seq.seq;
      seq.seq = NULL;
      seq.length = 0;
    }
    multiplicity = 0;
    length = 0;
  }
  int IsNullified() {
    if (dest == -1 and src == -1 and multiplicity == 0 and length == 0)
      return 1;
    else
      return 0;
  }
};

typedef std::vector<BEdge> BEdgeList;


#endif
