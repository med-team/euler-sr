/***************************************************************************
 * Title:          ReadPos.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  03/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/

#include "ReadPos.h"
#include "DNASequence.h"
#include "SeqUtils.h"

int ReadPos::hashLength;
SimpleSequenceList* ReadPos::sequences;

void PrintTuple(SimpleSequenceList &sequences, 
								ReadPos &readPosition, int tupleSize, std::ostream &out) {
  DNASequence tmpSeq;
  tmpSeq.seq = &(sequences[readPosition.read].seq[readPosition.pos]);
  tmpSeq.length = tupleSize;
  tmpSeq.PrintSeq(out);
}

void PrintTuple(int tuple, int tupleSize, std::ostream &out) {
  int i;
  std::string tupleStr = "";
  int mask = 3;
  char c;
  char nuc;
  for (i = 0; i < tupleSize; i++) {
    nuc = tuple & mask;
    c = nuc_char[nuc];
    tupleStr = c + tupleStr;
    tuple  >>= 2;
  }
  out << tupleStr;
}

