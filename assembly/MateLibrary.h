#ifndef MATE_LIBRARY_H_
#define MATE_LIBRARY_H_

#include "MateTable.h"
#include "RuleList.h"
#include "IntervalGraph.h"
#include "Trace.h"
#include "PathBranch.h"
#include "PathLib.h"
#include <list>
#include <set>
using namespace std;
class MatePathStatistics {
public:
	int numPaths;
	int meanPathLength;
	int pathCount;
	MatePathStatistics(int np, int mpl, int pc) {
		numPaths = np;
		meanPathLength = mpl;
		pathCount = pc;
	}
};

class EdgePair {
public:
	int edge1, edge2;
	int mateType;
	int operator==(const EdgePair &p) const {
		return (p.edge1 == edge1 and p.edge2 == edge2);
	}
	int operator<(const EdgePair &p) const {
		if (p.edge1 == edge1) {
			if (p.edge2 == edge2) {
				return mateType < p.mateType;
			}
			else {
				return edge2 < p.edge2;
			}
		}
		else {
			return edge1 < p.edge1;
		}
	}
	EdgePair &operator=(const EdgePair &p) {
		edge1 = p.edge1;
		edge2 = p.edge2;
		mateType = p.mateType;
		return *this;
	}
};


class MatePairMap {
public:
	int count;
	int edge1, edge2;
	int meanEdge1End, meanEdge2Start;
	int numPaths;
	int averageNumEdges;
	int read1Length, read2Length;
};

typedef std::map<EdgePair, MatePairMap> EdgePairMap;

class MatePathInterval {
 public:
	int edge;
	MatePathInterval(int e) {
		edge = e;
	}
};

typedef std::list<MatePathInterval> MatePathList;

void ReadMateTable(std::string &MateTableName,
									 ReadMateList &mateList);

int CountValidMatePaths(IntervalGraph &g,
												int curEdge, int curEdgePos,
												int endEdge, int endEdgePos,
												int curLength,
												int minLength, int maxLength,
												int maxDepth, int maxValid, int &numValid,
												int &storePath, MatePathList &matePath, 
												int print, int &totalPathLength,
												map<int,int> &visited);

void MarkEdgesOnValidMatePaths(IntervalGraph &g,
															 int curEdge, int curEdgePos,
															 int endEdge, int endEdgePos,
															 int curLength, int minLength, int maxLength,
															 int maxDepth,	std::vector<int> &path, int curDepth, 
															 std::vector<int> &edgeOnPathCount, 
															 int &numPaths, int &totalLength, int &totalEdges,
															 std::vector<int> &edgeTraversalCount);

int AreEdgesPaired(IntervalGraph &g,
									 int curEdge, int pairedEdge,
									 ReadMateList &mateList,
									 int curInterval);

void RemoveLowFrequencyEdgePairs(IntervalGraph &graph, 
																 EdgePairMap &matePairs,
																 ReadMateList &mateList, 
																 int minMatePairCount,
																 int ruleType=-1);

int StoreEdgePairMap(IntervalGraph &graph, 
										 ReadMateList &matePairs,
										 EdgePairMap &edgePairs,
										 int ruleType = -1);

void AssignMateOrder(int p, int mateIndex, int &mp, int&fisrtMate, int&secondMate);


void StoreMeanEdgePosition(EdgePairMap &matePairs);

void StoreUniqueMatePaths(IntervalGraph &graph, TVertexList &vertices, TEdgeList &edges,
													RuleList &rules,
													EdgePairMap &matePairs, PathBranch &pathTree);

class MatePairLocation {
 public:
	int count;     // count including read intervals
	int avgEndPos, avgStartPos;
	int cloneCount; // count of clones starting on corresponding edge
	MatePairLocation() {
		count = 0;
		avgEndPos = avgStartPos = 0;
		cloneCount = 0;
	}
	MatePairLocation(int c, int s, int a, int cc) {
		count = c;
		avgStartPos = s;
		avgEndPos = a;
		cloneCount = cc;
	}
};

typedef std::map<int, MatePairLocation> MateEdgeMap;

int StoreTreeDepth(IntervalGraph &g, int srcEdge, MateEdgeMap &mateEdges,
									 std::vector<int>  &distToEnd);

int SearchForMateEdge(IntervalGraph &g, int rootEdge, int srcEdge, 
											int maxSearchLength, int maxSearchDepth, 
											int mateEdge, std::list<int> &path);

int SearchForMateEdge(IntervalGraph &g, int rootEdge, int srcEdge, 
											int maxSearchLength, int maxSearchDepth, 
											MateEdgeMap &mateEdges, std::list<int> &path, int &altDestEdge);

int FindTreeDepth(IntervalGraph &g, int srcEdge, 
									MateEdgeMap &mateEdges, std::set<int> &extraEdges,
									std::vector<int> &distToSrc);

void CollectMateEdges(IntervalGraph &g, ReadMateList &readMates, int edge, 
											MateEdgeMap &mateEdges, int mateType, int strand=0);

int GetAverageMateStartPos(IntervalGraph &g, ReadMateList &readMates, 
													 int srcEdge, int destEdge, int &avgSrcEnd, int &avgDestEnd);

/*int MarkPathTree(IntervalGraph &g, int srcEdge,
								 MateEdgeMap &srcMateEdges, 
								 std::set<int> &additionalEdges );
*/

void ClearDistances(MateEdgeMap &readMates, std::set<int> &extraEdges,
										std::vector<int> distToSrc);

void UnmarkMateEdges(IntervalGraph &g, MateEdgeMap &readMates,
										 std::set<int> &extraEdges);

void UntraverseMateEdges(IntervalGraph &g, MateEdgeMap &readMates,
												 std::set<int> &extraEdges);

void RemoveLowCountMateEdges(MateEdgeMap &mateEdges, int minCount);

int ExtractMin(std::map<int, int> &distMap, std::set<int> &removed);

int FindClosestVertex(IntervalGraph &g, int startVertex, std::set<int> &destVertices);

int FindMatePath(IntervalGraph &g, int srcEdge,
								 MateEdgeMap &srcMateEdges,
								 std::list<int> &srcEdgePath);

void FindScaffoldPaths(IntervalGraph &g, 
											 ReadMateList &mateTable,
											 RuleList &mateRules, int mateType, int minScaffoldLength,
											 PathIntervalList &paths,
											 PathLengthList &pathLengths);

int FindPairedOutEdge(IntervalGraph &g, MateEdgeMap &pairedEdges, int curVertex, int &pairedOutEdge);


int CollectPairedOutEdges(IntervalGraph &g, MateEdgeMap &pairedEdges, int curVertex, 
													set<int> &pairedOutEdges);


int AdvancePathAlongPairedEdges(IntervalGraph &g, int curEdge, MateEdgeMap &pairedEdges, 
																int stopEdge, 
																std::list<int> &pairedPath, int &pathSeqLength,
																map<int,int> &edgeTarversalCount,
																int &lastEdge, int &numPairedOutEdges);

int FindPairedPath(IntervalGraph &g, MateEdgeMap &pairedEdges, 
									 int curEdge, int curEdgePos, int destEdge, int destEdgePos,
									 int curLength, int minLength, int maxLength,
									 std::list<int> &curPath, int &numCurPathPairedEdges,
									 std::list<int> &optimalPath, int &numOptPathPairedEdges, int &numOptPaths);

void GetLastStartPosition(MateEdgeMap &mateEdgeMap, int &startPos, int &pairedEdge);


int IsLengthValid(int length, int min, int max);

int MarkScaffoldEdges(IntervalGraph &g, int minCoverage, int minLength);

int FindPairedScaffoldEdges(IntervalGraph &g, ReadMateList &readMates, int edge, 
														std::set<int> &pairedScaffoldEdges);

int FindMaximallySupportedPath(IntervalGraph &g, ReadMateList &mateTable, 
															 RuleList &mateRules, int mateType,
															 int srcEdge, int destEdge,
															 std::list<int> &optimalPath) ;

int FindMaximallySupportedPath(IntervalGraph &g, MateEdgeMap &pairedEdges, 
															 int curEdge, int curEdgePos, int destEdge, int destEdgePos,
															 int curLength, int minPathLength, int maxPathLength,
															 int maxSearchDepth,
															 std::list<int> &curSupportedPath, int curSupportedPathScore,
															 std::list<int> &maxSupportedPath, int &maxSupportedPathScore,
															 map<int,int> &edgeTraversalCount,
															 int &numOptPaths);

int QueryUpstreamEdgesForDownstreamConnection(IntervalGraph &g, ReadMateList &mateTable,
																							int curVertex, int radius, 
																							std::set<int> &dsEdgeSet, int minPairedEdges,
																							std::set<int> &traversedVertices);


void StoreDownstreamEdges(IntervalGraph &g, int curVertex, int radius, 
													std::set<int> &dsEdgeSet);

int RemoveBadStartEndMatePairs(IntervalGraph &g, ReadMateList &mateTable, RuleList &rules, int ruleType);

int ComputeMatePairLengthDistribution(IntervalGraph g, ReadMateList &readMates,
																			int mateType, int &meanSep, float &stddevSep);


int  PathsMayOverlap(Path &forPath, int forPathLength,
										 Path &revPath, int revPathLength,
										 int minCloneSize, int maxCloneSize );

int  FindOverlappingMatePaths(Path &forPath, int forPathLength,
															Path &revPath, int revPathLength,
															Path &overlappingPath, int &overlappingPathLength);

#endif
