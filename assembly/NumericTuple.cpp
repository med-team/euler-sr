/***************************************************************************
 * Title:          NumericTuple.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/25/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "NumericTuple.h"

int NumericTuple::tupleSize = -1;
int NumericTuple::numBytes = -1;
