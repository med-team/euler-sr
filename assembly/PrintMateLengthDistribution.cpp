/***************************************************************************
 * Title:          PrintMateLengthDistribution.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "IntervalGraph.h"
#include "MateLibrary.h"
#include "MateTable.h"
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;

void PrintUsage() {
	std::cout << "usage: printMateLengthDistribution graphName mateTableName" << std::endl;
}
int main(int argc, char* argv[]) {

	std::string graphFileName, mateTableName;
	if (argc < 3) {
		PrintUsage();
		exit(0);
	}
	graphFileName = argv[1];
	mateTableName = argv[2];

	IntervalGraph graph;
	int vertexSize;
	ReadIntervalGraph(graphFileName, graph, vertexSize);

	ReadMateList  mateTable;
	ReadMateTable(mateTableName, mateTable);

	int e, i;
	std::map<int,int> lengthCount;
	TEdgeList &edges = graph.edges;
	PathLengthList &pathLengths = graph.pathLengths;
	PathIntervalList &paths = graph.paths;

	for (e = 0; e < edges.size(); e++ ){
		for (i = 0; i < (*edges[e].intervals).size(); i++) {
			int pathPos;
			int path;
			path = (*edges[e].intervals)[i].read;
			pathPos = (*edges[e].intervals)[i].pathPos;

			if (path % 2 == 0) {
				// This is the foward read.
				int readIndex = path / 2;
				int mateIndex = mateTable[readIndex].mateIndex;
				int mateReadIndex = mateIndex * 2 + 1;

				if (pathLengths[path] == 0 or pathLengths[mateReadIndex] == 0) {
					continue;
				}
			
				int pathLength = pathLengths[path];
				int readEndEdge = paths[path][pathLength-1].edge;
				int readEndIntv = paths[path][pathLength-1].index;
				int readEndPos  = (*edges[readEndEdge].intervals)[readEndIntv].edgePos + 
					(*edges[readEndEdge].intervals)[readEndIntv].length;
			
				int readEndEdgeLength = edges[readEndEdge].length;
			
				int mateBeginEdge = paths[mateReadIndex][0].edge;
				int mateBeginIntv = paths[mateReadIndex][0].index;
				int mateBeginPos  = (*edges[mateBeginEdge].intervals)[mateBeginIntv].edgePos;

				if (readEndEdge == mateBeginEdge ) {
					lengthCount[mateBeginPos - readEndPos]++;
				}
			}
		}
	}

	std::map<int, int>::iterator lenIt;
	for (lenIt = lengthCount.begin(); lenIt != lengthCount.end(); ++lenIt) {
		std::cout << (*lenIt).first << " " << (*lenIt).second << std::endl;
	}
}
