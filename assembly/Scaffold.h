#ifndef SCAFFOLD_H_
#define SCAFFOLD_H_
#include "MateLibrary.h"
#include "IntervalGraph.h"

class MateCount {
public:
	int count;
	int beginPos;
	int endPos;
	int overlap;
	int expOverlap;
	int alignScore;
	int beginAlignPos, endAlignPos;
	MateCount() {
		count = 0;
		beginPos = 0;
		endPos   = 0;
		overlap  = 0;
		expOverlap = 0;
		beginAlignPos = endAlignPos = -1;
		alignScore = 0;
	}
};

int IsEndEdge(IntervalGraph &g, int edgeIndex);
int IsBeginEdge(IntervalGraph &g, int edgeIndex);
int IsEndVertex(IntervalGraph &g, int vertexIndex);
int IsBeginVertex(IntervalGraph &g, int vertexIndex);
void GrowMatrices(IntMatrix &scoreMat, IntMatrix &pathMat, int nrows, int ncols);
void GrowEdge(TEdgeList &edges, int destEdge, int sourceEdge, int sourceOffset, int sourceLength);

void GrowEdge(IntervalGraph &graph,
							int edge,
							int length,
							unsigned char *seq);


void PrefixSuffixAlign(SimpleSequence &sourceSeq,
											 SimpleSequence &sinkSeq,
											 int &maxScoreSourcePos,
											 int &maxScoreSinkPos,
											 int &maxScore, int &nMisMatch, int &nIndel,
											 IntMatrix &scoreMat, IntMatrix &pathMat, IntMatrix &matchMat,
											 int printAlign=0);


void InsertGap(TEdgeList &edges,  int e, int gapLength);


void MatePairScaffoldJoinEdges(IntervalGraph &graph,
															 ReadMateList  &mateList,
															 int           mateType,
															 int           minMateCount,
															 IntMatrix     &scoreMat,
															 vector<int>   &vToRemove);

void InitScaffoldMatchMatrix(IntMatrix &matchMat);

#endif
