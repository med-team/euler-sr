#ifndef VOTE_UTILS_H_
#define VOTE_UTILS_H_
#include "DNASequence.h"
#include "mctypes.h"
 
int PrepareSequence(DNASequence &read);
int InitVotingMatrix(DNASequence &read, IntMatrix &votes);
int InitSolidVector(DNASequence &read, IntVector &solid);

#endif
