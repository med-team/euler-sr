/***************************************************************************
 * Title:          HashedTuple.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/17/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef INTEGRAL_TUPLE_H_
#define INTEGRAL_TUPLE_H_


#define MAX_TUPLE_SIZE 31
#include <iostream>
#include <string>
#include <sstream>

#include "SeqUtils.h"
#include "utils.h"

using namespace std;

typedef unsigned long long int LongTuple;

class IntegralTuple  {
 public:
	// tuple location
	static int tupleSize;
	static LongTuple MASK_OFF_FIRST;
	static LongTuple MASK_OFF_LAST;
	static LongTuple VERTEX_MASK;

	// the actual tuple
	LongTuple tuple:56;
	int count:8;


	int GetMult() {
		return 0;
	}

	static void SetTupleSize(int ts) {
		tupleSize = ts;
		MASK_OFF_FIRST = 3;
		int i;
		for (i = 0; i < tupleSize - 2; i++) {
			MASK_OFF_FIRST = MASK_OFF_FIRST << 2;
			MASK_OFF_FIRST += 3;
		}
		MASK_OFF_FIRST <<= 2;
		MASK_OFF_LAST  = 0x3;
		for (i = 0; i < tupleSize - 2; i++) {
			MASK_OFF_LAST = MASK_OFF_LAST << 2;
			MASK_OFF_LAST += 3;
		}
		
		VERTEX_MASK = 3;
		for (i = 0; i < tupleSize -1; i++ ) {
			VERTEX_MASK = VERTEX_MASK << 2;
			VERTEX_MASK += 3;
		}
	}
	
	IntegralTuple() {
		tuple = 0;
	}

	int Length() {
		return tupleSize;
	}

	int Valid() {
		// This may only store valid tuples.
		return 1;
	}

	int SetMult(int count) {
		return 0;
	}
	
	int ReadLine(std::istream &in, int minMult =0) {
		std::string tuple;
		if (!(in >> tuple)) {
			std::cout << "Error reading tuple." << std::endl;
			exit(1);
		}
		if (tupleSize == -1) {
			// determine the tuple size from the word that was read.
			SetTupleSize(tuple.size());
		}
		StringToTuple(tuple);
		std::string line;
		std::getline(in, line);
		// Parse the multiplicity
		std::stringstream linestrm(line);
		int mult = 0;
		linestrm >> mult;
		if (mult >= minMult) 
			return 1;
		else
			return 0;
	}
	int StringToTuple(unsigned char *s) {
		// Store the tuple so that lowest two bits correspond to the first
		// nucleotide, and the upper k*2 (k is the tuple size) bits
		// corresponds to the last nucleotide.
		unsigned long long nuc;
		int p;
		tuple = 0;
		for (p = 0; p < IntegralTuple::tupleSize; p++ ) {
#ifdef NDEBUG
			if (unmasked_nuc_index[s[p]] >= 4)
				return 0;
#endif
			nuc = unmasked_nuc_index[s[p]];
			nuc <<= (2*p);
			tuple |= nuc;
		}
		return 1;
	}
	int StringToTuple(std::string &s) {
		return StringToTuple((unsigned char*) s.c_str());
	}

	int operator<(const IntegralTuple rhs) const {
		return tuple < rhs.tuple;
	}

	int operator>(const IntegralTuple &rhs) const {
		return tuple > rhs.tuple;
	}
	
	int operator==(const IntegralTuple &rhs) const {
		return tuple == rhs.tuple;
	}
	
	int operator!=(const IntegralTuple &rhs) const { 
		return tuple != rhs.tuple;
	}
	
	IntegralTuple& operator=(const IntegralTuple &rhs) {
		tuple = rhs.tuple;
	}

	IntegralTuple &operator=(std::string &tupleString) {
		this->StringToTuple(tupleString);
	}
	void MakeRC(IntegralTuple &rc) {
		/*
		unsigned long long mask = 0x3;
		unsigned long long rcNuc;
		rc.tuple = 0;
		int i;
		mask = 3;
		for (i = 0; i < tupleSize; i++) {
			rcNuc = tuple & mask;
			rcNuc = rcNuc >> (i * 2);
			rc.tuple += comp_bin[rcNuc];
			if (i < tupleSize - 1)
				rc.tuple = rc.tuple << 2;
			mask = mask << 2;
		}
		*/

		unsigned long long mask = 0x3;
		unsigned long long rcNuc;
		unsigned long long tmpTuple = tuple;
		unsigned long long revRC = ~tuple;
		rc.tuple = 0;
		int i;
		mask = 3;
		assert(tupleSize > 0);
		for (i = 0; i < tupleSize - 1; i++) {
			rcNuc = revRC & mask;
			//			rcNuc = rcNuc >> (i * 2);
			rc.tuple = rc.tuple + rcNuc;
			rc.tuple = rc.tuple << 2;
			revRC = revRC >> 2;
			//			tmpTuple = tmpTuple >> 2;
			//			rc.tuple += comp_bin[rcNuc];
			//			rc.tuple = rc.tuple << 2;
			//			mask = mask << 2;
		}
		rcNuc = revRC & mask;		
		//		rc.tuple += //comp_bin[rcNuc];
		rc.tuple = rc.tuple + rcNuc;
	}

	void ToString();

	void ToString(std::string &tupleString) {
		tupleString = "";
		tupleString.reserve(tupleSize);
		int byteIndex, bytePos;
		int i;
		byteIndex = 0;
		unsigned long mask = 0x3;
		//		mask <<= ((unsigned long) (tupleSize-1)*2);
		unsigned long m2 = mask << ((tupleSize-1)*2);
		mask = 0x3;
		for (i = 0; i < tupleSize; i++) {
			// translate binary into nuc
			m2 = tuple & mask;
			m2 >>= (i*2);
			tupleString.push_back(nuc_char[m2]);
			mask <<=2;
		}
	}

	friend std::ostream &operator<<(std::ostream &out, IntegralTuple &tup) {
		std::string tupleString;
		tup.ToString(tupleString);
		out << tupleString << " 0" << std::endl;
		return out;
	}

	friend std::istream &operator>>(std::istream &in, IntegralTuple &tup) {
		tup.ReadLine(in);
		return in;
	}
	
	int IncrementMult() {
		// No-op
		return 0;
	}
	
};

class CountedIntegralTuple : public IntegralTuple {
 public:
 CountedIntegralTuple() : IntegralTuple() {
		count = 1;
	}
	int IncrementMult() {
		if (count < 127)
			return ++count;
		else
			return (int) count;
	}
	int GetMult() {
		return (int) count;
	}
	int SetMult(int mult) {
		return (count = mult);
	}
	CountedIntegralTuple& operator=(const CountedIntegralTuple &rhs) {
		this->tuple = rhs.tuple;
		this->count = rhs.count;
	}
};

class EdgePosIntegralTuple : public IntegralTuple {
 public:
	int edge;
	int pos;
 EdgePosIntegralTuple() : IntegralTuple() {
	}
	EdgePosIntegralTuple& operator=(const EdgePosIntegralTuple &rhs) {
		this->tuple = rhs.tuple;
		this->edge  = rhs.edge;
		this->pos   = rhs.pos;
		return *this;
	}
};


template<typename T_Tuple>
int LookupBinaryTuple(T_Tuple *tupleList, int listLength, T_Tuple &value) {

	T_Tuple *ptr;

	int begin = 0, end = listLength;
	int cur = (end + begin) / 2;
	while (begin < end and tupleList[cur] != value) {
		if (value < tupleList[cur]) {
			end = cur;
		}
		else {
			begin = cur + 1;
		}
		cur = (end + begin) / 2;
	}
	if (begin == end or tupleList[cur] != value)
		return -1;
	else
		return cur;

	/*

	ptr = std::lower_bound(tupleList, tupleList + listLength, value);
	//ptr = std::find(tupleList, tupleList + listLength, value);
	if (ptr != tupleList + listLength and *ptr == value)
		return ptr - tupleList;
	else
		return -1;
	*/
}


template<typename T_Tuple> 
int LookupBinaryTuple(std::vector<T_Tuple> &tupleList, T_Tuple &value) {
	typename std::vector<T_Tuple>::iterator tupleIt;
	tupleIt = std::lower_bound(tupleList.begin(), tupleList.end(), value);
	if (tupleIt != tupleList.end() and *tupleIt == value)
		return (tupleIt - tupleList.begin());
	else
		return -1;
}

template<typename T_Tuple>
void ReadBinaryTupleList(std::string &fileName, 
												 T_Tuple **tupleList,
												 int &listLength, int minMult = 0) {
	std::ifstream in;
	openck(fileName, in, std::ios::in | std::ios::binary);
	// read the number of tuples

	if (minMult == 0) {
		in.read((char*) &listLength, sizeof(int));
		*tupleList = new T_Tuple[listLength];
		in.read((char*) *tupleList, sizeof(T_Tuple) * listLength);
	}
	else {
		T_Tuple tuple;
		int totalTuples;
		listLength = 0;
		in.read((char*) &totalTuples, sizeof(int));
	 
		while(in) {
			if (in.read((char*) &tuple, sizeof(T_Tuple))) {
				if (tuple.GetMult() >= minMult) {
					//tupleList.push_back(tuple);
					listLength++;
				}
			}
		}
		if (listLength == 0 ) {
			*tupleList = NULL;
			return;
		}
		cout << "Placing " << listLength << " " << T_Tuple::tupleSize << "-mers into a dictionary." << endl;
		in.clear();
		in.seekg(std::ios::beg);
		*tupleList = new T_Tuple[listLength];
		int i = 0;
		
		in.read((char*) &totalTuples, sizeof(int));
		while(in) {
			if (in.read((char*) &tuple, sizeof(CountedIntegralTuple))) {
				if (tuple.GetMult() >= minMult) {
					//tupleList.push_back(tuple);
					//				std::string tupleStr;
					//				tuple.ToString(tupleStr);
					(*tupleList)[i].tuple = tuple.tuple;
					(*tupleList)[i].SetMult(tuple.GetMult());
					i++;
				}
			}
		}
	}
}


int ReadMultBoundedBinaryTupleList(std::string &fileName,
																	 int minMult,
																	 std::vector<CountedIntegralTuple> &tupleList);

template<typename T_Tuple>
void WriteBinaryTupleList(std::string &fileName,
													T_Tuple *tupleList,
													 int &listLength) {

	std::ofstream out;
	openck(fileName, out, std::ios::out | std::ios::binary);
	out.write((const char*) &listLength, sizeof(int));
	out.write((const char*) tupleList, sizeof(T_Tuple) * listLength);
	out.close();
}


template<typename T_Tuple, int IndexN>
class DictionaryTupleList{
 public:
	int *startIndex, *endIndex;
	int indexLength;
	T_Tuple *list;
	int listLength;
	LongTuple mask;
	int offset;
	void Initialize(T_Tuple *listp, int listLengthp) {
		list = listp;
		listLength = listLengthp;
		this->CreateIndex();
	}

	LongTuple ComputeIndex(LongTuple tuple) {
		LongTuple i;
		i = tuple & mask;
		i = (i >> (2*offset));
		return i;
	}

	void IndexList() {	Pause();
		if (IndexN == 0) {
			indexLength = 0;
			return;
		}

		indexLength = 1;
		indexLength <<= (2*IndexN);
		startIndex = new int[indexLength];
		endIndex   = new int[indexLength];
		
		// Create a mask to grab the upper bits of the tuple.
		//
		offset = T_Tuple::tupleSize - IndexN;
		mask = 0;
		LongTuple low  = 3;
		LongTuple shift = 2; //long to keep all types the same.
		int i;
		for (i = 0; i < offset; i++) {
			mask = mask << shift;
			mask += low;
		}
		mask = ~mask;
		
		LongTuple upperIndex;
		
		int indexPos, listPos;
		indexPos = listPos = 0;
		
		// Loop invariant: 
		//  index[indexPos] is the first value i such that (list[i] & mask) == indexPos <= i

		long int listStart = -1;
		listPos = indexPos = 0;
		while ((listPos < listLength) and 
					 (indexPos < indexLength)) {
			
			upperIndex = ComputeIndex(list[listPos].tuple);

			while (indexPos < indexLength and indexPos != upperIndex ) {
				// The index does not exist in the list
				assert(indexPos < indexLength);
				startIndex[indexPos] = -1;
				endIndex[indexPos]   = -1;
				indexPos++;
			}
			if (indexPos >= indexLength)
				break;

			// the beginning index where list[i] & mask == upperIndex
			listStart = listPos;

			while (indexPos == upperIndex) {
				// Advance forward in the list.
				listPos++;
				if (listPos >= listLength)
					break;
				upperIndex = ComputeIndex(list[listPos].tuple);
			}
			// The array from 'indexStart .. indexPos-1' has indexPos as it's uper 2*n bits.
			startIndex[indexPos] = listStart;
			endIndex[indexPos]   = listPos;
			
			// Searched all entries beginning with 'indexPos'
			indexPos++;
		}
	}
	
	int DictLookupBinaryTuple(T_Tuple& query) {

		LongTuple index = ComputeIndex(query.tuple);
		// Look if this tuple is indexed or not
		if (startIndex[index] == -1) {
			return -1;
		}

		int offsetIndex;
		offsetIndex = LookupBinaryTuple((T_Tuple*) &list[startIndex[index]], 
																		endIndex[index] - startIndex[index], query); 
		if (offsetIndex == -1)
			return -1;
		else
			return offsetIndex + startIndex[index];

	}
	
	void InitFromFile(std::string &fileName, int minMult = 0) {
		ReadBinaryTupleList(fileName, &list, listLength, minMult);
		IndexList();
	}
};


typedef std::vector<CountedIntegralTuple> CountedIntegralTupleList;
typedef std::vector<EdgePosIntegralTuple> EdgePosIntegralTupleList;

typedef DictionaryTupleList<CountedIntegralTuple,9> CountedIntegralTupleDict;
typedef DictionaryTupleList<IntegralTuple,9> IntegralTupleDict;


template <typename T_Tuple>
void ForwardNuc(T_Tuple curVertex, char nextNuc, 
								T_Tuple &nextVertex) {

	nextVertex.tuple = curVertex.tuple;
	nextVertex.tuple = nextVertex.tuple >> 2;
	T_Tuple nextNucL;
	nextNucL.tuple = nextNuc;
	nextNucL.tuple = nextNucL.tuple << (IntegralTuple::tupleSize * 2 - 2);
	nextVertex.tuple += nextNucL.tuple;
	assert(nextNuc < 4);
}

template <typename T_Tuple>
void BackwardsNuc(T_Tuple &curVertex, char nextNuc,
									T_Tuple &prevVertex) {
	prevVertex.tuple = curVertex.tuple;
	prevVertex.tuple = prevVertex.tuple << 2;
	prevVertex.tuple &= IntegralTuple::VERTEX_MASK;
	prevVertex.tuple += nextNuc;
}


template <typename T_Tuple>
void MutateTuple(T_Tuple curTuple, unsigned char newNuc, int pos, T_Tuple nextTuple) {
	T_Tuple mutatedNuc;
	T_Tuple mutatedMask;
	mutatedMask.tuple = 3;
	mutatedMask.tuple = mutatedMask.tuple << (2 * pos);
	mutatedMask.tuple = ~mutatedMask.tuple;
	curTuple.tuple = curTuple.tuple & mutatedMask.tuple;

	T_Tuple shiftedNewNuc;
	shiftedNewNuc.tuple = newNuc;
	shiftedNewNuc.tuple = shiftedNewNuc.tuple << (2 * pos);
	nextTuple.tuple = curTuple.tuple + shiftedNewNuc.tuple;
}



template <typename TupleType>
class UpdateIntegralTupleCountFunctor {
 public:
	int operator()(TupleType &tuple) {
		tuple.IncrementMult();
	}
};

template<int T_HashLength>
class IntegralTupleHashValueFunctor {
 public:
	long int mask;
	IntegralTupleHashValueFunctor() {
		mask = 0;
		unsigned char nucMask = 3;
		int i;
		// prepare the mask
		for (i = 0; i < T_HashLength-1; i++ ){
			mask |= nucMask;
			mask <<=2;
		}
		mask |= nucMask;
	}
	int operator()(const IntegralTuple &tuple) const {
		assert(mask != 0);
		return (int) (tuple.tuple & mask);
	}
	int operator()(const unsigned long long longTuple) const {
		return (int) (longTuple & mask);
	}

	int MaxHashValue() {
		int maxHashValue = 1;
		int i;
		for (i = 0; i < T_HashLength; i++) 
			maxHashValue <<=2;
		return maxHashValue;
	}
};

																	 
#endif
