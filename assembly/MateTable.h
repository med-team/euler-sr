#ifndef MATE_TABLE_H_
#define MATE_TABLE_H_
#include <string>
#include <vector>


class Clone {
public:
	int ai, bi;
	int type;
};

class ReadMate {
 public:
	int mateIndex;
	int mateType;
	char marked;
	ReadMate() {
		marked = 0;
		mateIndex = -1;
		mateType = -1;
	}
};

typedef std::pair<std::string, Clone> NameClonePair;
typedef std::vector<ReadMate> ReadMateList;


#endif
