/***************************************************************************
 * Title:          PrintReadIntervals.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  12/02/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <iostream>
#include <string>
#include <vector>
#include "DeBruijnGraph.h"
#include "ReadIntervals.h"
#include "SeqReader.h"
#include "SeqUtils.h"
#include "SimpleSequence.h"
#include "IntegralTuple.h"
#include "IntervalGraph.h"
int IntegralTuple::tupleSize = 0;


using namespace std;

void PrintUsage() {
  std::cout << "usage: integralPrintReadIntervals sequences "
						<< "-mapGappedReads    Try and map reads even when not every l+1-mer in"<<endl
						<< "                   the read maps." << endl
						<< "-printCoverage bgraphName Just print the multiplicity of edges to the branching graph." << endl;
}


class MappedRead {
public:
	int readIndex : 32;
	int readPos : 32;
	MappedRead(int i, int p) {
		readIndex = i;
		readPos   = p;
	}
	MappedRead &operator=(const MappedRead &rhs) {
		readIndex = rhs.readIndex;
		readPos   = rhs.readPos;
		return *this;
	}
};

int ThreadReadExact(DNASequence &read, int &readPos, IntervalGraph &g, int edgeIndex, int edgePos,
										int &curIntv,
										std::vector<int> &readPosList, 
										std::vector<int> &edgeIndexList, 
										std::vector<int> &edgePosList,
										std::vector<int> &lengthList);


typedef vector<MappedRead> MappedReadList;
typedef vector<MappedReadList> MappedReadMatrix;

int main(int argc, char* argv[] ) {

  std::string overlapFileName;
	std::string edgeFileName;
  std::string sequenceListFileName;
  std::string readIntervalFileName;
	std::string pathFileName;
	std::string bgraphFileName;
  int vertexSize, overlapSize;

  if (argc < 2) {
    PrintUsage();
    exit(1);
  }

  int argi = 1;
  sequenceListFileName = argv[argi++];



	bgraphFileName = sequenceListFileName + ".bgraph";
	overlapFileName = sequenceListFileName + ".iovp";
	edgeFileName = sequenceListFileName + ".edge";
	readIntervalFileName = sequenceListFileName + ".intv";
	pathFileName  = sequenceListFileName + ".path";
	
	int mapGappedReads = 0;
	int printCoverageOnly = 0;
	while (argi < argc) {
		if (strcmp(argv[argi], "-mapGappedReads") == 0){ 
			mapGappedReads = 1;
		}
		else if (strcmp(argv[argi], "-printCoverage") == 0) {
			printCoverageOnly = 1;
		}
		else {
			PrintUsage();
			cout << "bad option: " << argv[argi] << endl;
			exit(0);
		}
		++argi;
	}
	
	std::string reportFileName = sequenceListFileName + ".report";
	std::ofstream reportFile;
	std::ifstream readsIn;
	std::ifstream edgeIn;
	openck(reportFileName, reportFile, std::ios::app);
	openck(sequenceListFileName, readsIn, std::ios::in);
	openck(edgeFileName, edgeIn, std::ios::in);

	BeginReport(argc, argv, reportFile);
	
	SimpleSequenceList edges;
	IntervalGraph graph;
	int skipIntervals = 1; // Flag to skip reading intervals since they are stored here.

	ReadIntervalGraph(sequenceListFileName, graph, vertexSize, skipIntervals);
  overlapSize = vertexSize + 1;

  ReadSequences(edgeFileName, graph.edges);
  cout << "read edges." << endl;
  // Read in the vertex list
  std::ifstream overlapIn;
	EdgePosIntegralTuple *edgePosTupleList;
	

	// Read the overlaps.
	int numOverlaps;

	openck(overlapFileName, overlapIn, std::ios::in | std::ios::binary);
	overlapIn.read((char*) &numOverlaps, sizeof(int));
	edgePosTupleList = new EdgePosIntegralTuple[numOverlaps];
	overlapIn.read((char*) edgePosTupleList, sizeof(EdgePosIntegralTuple)*numOverlaps);
	std::cout << "read: " << numOverlaps << " overlaps." << std::endl;
	PathIntervalList paths;
	PathLengthList pathLengths;
	std::vector<BareReadIntervalList> edgeReadIntervals;
	vector<MappedReadMatrix> edgeReadMap;

	int e;
	if (!printCoverageOnly) {
		edgeReadIntervals.resize(graph.edges.size());
		//		edgeFullReadIntervals.resize(edges.size());
		edgeReadMap.resize(graph.edges.size());
		for (e = 0; e < graph.edges.size(); e++ ){ 
			edgeReadMap[e].resize(graph.edges[e].length - vertexSize);
		}
	}
	else {
	}
	
	std::cout << "storing read intervals. " << std::endl;
	int skipGapped = 1;


	DNASequence read, readRC;
	PathIntervalList path;
	int readIndex;
	DNASequence* readPair[2], *readPtr;
	readPair[0] = &read;
	readPair[1] = &readRC;
	readIndex = 0;

	EdgePosIntegralTuple readTuple;
	EdgePosIntegralTuple::tupleSize = overlapSize;
	int rn = 0;
	std::vector<int> edgeList, lengthList, readPosList, edgePosList;
	while (SeqReader::GetSeq(readsIn, read, SeqReader::noConvert)) {

		if (skipGapped) {
			int seqIsGapped = 0;
			int curPos;
			for (curPos = 0; curPos < read.length; curPos++ ){ 
				if (unmasked_nuc_index[read.seq[curPos]] >= 4) {
					seqIsGapped = 1;
					break;
				}
			}
			if (seqIsGapped) {
				paths.push_back(NULL);
				pathLengths.push_back(0);
				paths.push_back(NULL);
				pathLengths.push_back(0);				
				rn++;
				continue;
			}
		}

		MakeRC(read, readRC);
		int edgeIndex, edgePos;
		int r, s;
		int overlapIndex;
		int nextN;
		ReadIntervalList readPath;
		int pathPos;


		// simplify the code by an RC
		int spacing = 100000;
		PrintStatus(rn, spacing);

		int forPathLength  = -1;
		
		if (read.length < overlapSize) {
			// add placeholders for forward/reverse strand
			paths.push_back(NULL);
			pathLengths.push_back(0);
			paths.push_back(NULL);
			pathLengths.push_back(0);
			rn++;
			continue;
		}
		for (r = 0; r < 2; r++) {
			//    std::cout << "processing sequence " << s << std::endl;
			readPath.clear();
			readPtr = readPair[r];
			nextN   = -1;
			int p;
			int prevEdge = -1;
			int fullReadMapped = 1;
			int gapSpansEdges = 0;

			// 
			int readPos;
			int npos = 0;
			int nEdges = 0;
			if (readPosList.size() < readPtr->length - overlapSize + 1) {
				readPosList.resize(readPtr->length - overlapSize + 1);
				edgePosList.resize(readPtr->length - overlapSize + 1);
				edgeList.resize(readPtr->length - overlapSize + 1);
				lengthList.resize(readPtr->length - overlapSize + 1);
			}

			for (p = std::min(overlapSize-1, readPtr->length); p >= 0; p--) {
				if (numeric_nuc_index[read.seq[p]] >= 4) {
					nextN = p;
					fullReadMapped = 0;
					break;
				}
			}

			int numIntervals, curIntv;
			readPos = 0;
			numIntervals = 0;
			curIntv = 0;
			while (readPos < readPtr->length - overlapSize + 1) {
				
				// 
				// Locate a position on the read starting at readPos that maps
				// to an edge in the graph.
				//
				edgeIndex = -1;
				for (; readPos < readPtr->length - overlapSize + 1; readPos++ ) {
					if (numeric_nuc_index[readPtr->seq[readPos + overlapSize - 1]] >= 4) {
						nextN = readPos + overlapSize - 1;
						fullReadMapped = 0;
					}
					if (readPos > nextN) {
						readTuple.StringToTuple(&readPtr->seq[readPos]);
						overlapIndex = LookupBinaryTuple(edgePosTupleList, numOverlaps, readTuple);
						if (overlapIndex < 0) {
							fullReadMapped = 0;
						}
						else {
							edgeIndex = edgePosTupleList[overlapIndex].edge;
							edgePos   = edgePosTupleList[overlapIndex].pos;
							break;
						}
					}
				}

				if (edgeIndex != -1) {
					// An anchoring edge was found. 
					// Thread the rest of the read through the graph.
					//				readPosList.clear(); edgeList.clear(); edgePosList.clear(); lengthList.clear();
					int startReadPos = readPos;
					numIntervals += ThreadReadExact(*readPtr, readPos, graph, edgeIndex, edgePos,
																					curIntv,
																					readPosList, edgeList, edgePosList, lengthList);
				}
				//
				// This threads to the end of a match, or the end of a read.
				// If the whole read is mapped, done anyway.  Otherwise, part
				// of the read may be missing due to an error in it.
				// Move forward with the read pos to attempt to thread the rest of the read.
				//
				++readPos;
			}
			// end trying to fit the entire read into the graph.
			
			if (numIntervals > 0) {
				// Create the edge and path intervals
				
				paths.push_back(new PathInterval[numIntervals]);
				pathLengths.push_back(numIntervals);
				readIndex = paths.size() - 1;
				int i;
				for (i =0 ; i < numIntervals; i++) {
					edgeReadIntervals[edgeList[i]].push_back(BareReadInterval(readIndex,
																																		readPosList[i],
																																		edgePosList[i],
																																		lengthList[i]));
					paths[readIndex][i].edge  = edgeList[i];
					paths[readIndex][i].index = edgeReadIntervals[edgeList[i]].size() - 1;//readPosList[i];
				}
			}
			else {
				// The read could not be mapped back to the
				// graph.  Add a placeholder for this.
				paths.push_back(NULL);
				pathLengths.push_back(0);	
			}
		}
		++rn;
		assert(paths.size() == rn*2);

		read.Reset();
		readRC.Reset();
	}
	if (!printCoverageOnly) {
		std::ofstream readIntervalOut;
		openck(readIntervalFileName, readIntervalOut, std::ios::out);

		int edgeIndex;
		int intervalIndex;
		intervalIndex = 0;
		int ri;
		for (edgeIndex = 0; edgeIndex < graph.edges.size();edgeIndex++) {
			readIntervalOut << "EDGE " << edgeIndex 
											<< " Length " << graph.edges[edgeIndex].length
											<< " Multiplicity " << edgeReadIntervals[edgeIndex].size()
											<< std::endl;
			for (ri = 0; ri < edgeReadIntervals[edgeIndex].size(); ri++) {
				readIntervalOut << "INTV " << edgeReadIntervals[edgeIndex][ri].read 
												<< " " << edgeReadIntervals[edgeIndex][ri].readPos 
												<< " " << edgeReadIntervals[edgeIndex][ri].length
												<< " " << edgeReadIntervals[edgeIndex][ri].edgePos << std::endl;
			}
		}
		readIntervalOut.close();
		// Read intervals are fixed, now determine the index of each path interval
		// into the edge interval list.
		/*
		int pathIndex, readPos;
		for (e = 0; e < edgeReadIntervals.size(); e++) {

			int allFound = 1;
			for (ri = 0; ri < edgeReadIntervals[e].size(); ri++) {
				pathIndex = edgeReadIntervals[e][ri].read;
				// The index of every path is temporarily set to 
				// equal the read position.  Since there is only
				// one read position per path and per edge, the order
				// of the read positions correponds to the order of
				// the intervals on the edge (since the read positions 
				// are assigned in the order that intervals are added).
				//
				readPos   = edgeReadIntervals[e][ri].readPos;
				int pi;
				for (pi = 0; pi < pathLengths[pathIndex]; pi++) {
					if (paths[pathIndex][pi].index == readPos and
							paths[pathIndex][pi].set == 0) {
						assert(paths[pathIndex][pi].edge == e);
						paths[pathIndex][pi].index = ri;
						paths[pathIndex][pi].set = 1;
						break;
					}
				}
				if (pi == pathLengths[pathIndex]) {
					allFound = 0;
					assert(0);
				}
			}
		}
		*/
		cout << "writing to paths: " << pathFileName << endl;
		WriteReadPaths(pathFileName, paths, pathLengths);
	}
	EndReport(reportFile);
	return 0;
}


int ThreadReadExact(DNASequence &read, int &readPos, IntervalGraph &g, int edgeIndex, int edgePos,
										int &curIntv,
										std::vector<int> &readPosList, 
										std::vector<int> &edgeIndexList, 
										std::vector<int> &edgePosList,
										std::vector<int> &lengthList) {
	
	// This begins assuming that read[readPos... readPos + tupleSize -1] ==
	// edges[edgeIndex][edgePos... edgePos+tuplesize]
	
	int intvLength;
	int numNewIntv = 0;
	while (readPos < read.length) {
		intvLength = 0;
		int edgeStartPos = edgePos;
		int readStartPos = readPos;
		while((readPos < read.length - IntegralTuple::tupleSize) and
					(edgePos < g.edges[edgeIndex].length - IntegralTuple::tupleSize) and
					(read.seq[readPos+IntegralTuple::tupleSize] == 
					 g.edges[edgeIndex].seq.seq[edgePos+IntegralTuple::tupleSize])) {
			++readPos;
			++edgePos;
			++intvLength;
		}
		readPosList[curIntv]   = readStartPos;
		edgeIndexList[curIntv] = edgeIndex;
		edgePosList[curIntv]   = edgeStartPos;
		lengthList[curIntv]    = intvLength + IntegralTuple::tupleSize;
		++curIntv;
		++numNewIntv;
		if ((readPos == read.length - IntegralTuple::tupleSize  ) or
				(edgePos < g.edges[edgeIndex].length - IntegralTuple::tupleSize))
			return numNewIntv;

		// This is done when the read is fully matched (first case)
		// or there is an error and the read is not fully mapped (second)

		edgeStartPos = 0;
		intvLength   = 0;

		// Determine the next edge.
		int outEdge, outEdgeIndex;
		int dest;
		dest = g.edges[edgeIndex].dest;
		int foundOut = 0;
		for (outEdgeIndex = g.vertices[dest].FirstOut();
				 outEdgeIndex != g.vertices[dest].EndOut();
				 outEdgeIndex = g.vertices[dest].NextOut(outEdgeIndex)) {
			outEdge= g.vertices[dest].out[outEdgeIndex];
			assert(g.edges[outEdge].length >= IntegralTuple::tupleSize);
			if (read.seq[readPos + IntegralTuple::tupleSize] == 
					g.edges[outEdge].seq.seq[IntegralTuple::tupleSize - 1]) {
				foundOut = 1;
				break;
			}
		}

		if (!foundOut) {
			// No possible extension worked. stop.
			return numNewIntv;
		}
		else {
			// Next interval starts at the beginning of the edge
			readPos++; 
			edgePos = 0;
			edgeIndex = outEdge;
		}
	}
	return numNewIntv;
}
