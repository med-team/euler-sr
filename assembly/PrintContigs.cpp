/***************************************************************************
 * Title:          PrintContigs.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  09/16/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "DeBruijnGraph.h"
#include "IntervalGraph.h"
#include "SeqReader.h"
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;



int main(int argc, char* argv[]) {

	BVertexList vertices;
	BEdgeList edges;
  std::string baseInName, bGraphName, contigOutName, edgeFileName;

	int argi = 1;
	if (argc <= 1) {
		std::cout << "usage: graphBase" << std::endl;
		std::cout << "prints the contigs out to graphBase.contig" << std::endl;
		exit(0);
	}
	baseInName       = argv[argi++];
  
	bGraphName   = baseInName  + ".bgraph";
  edgeFileName = baseInName  + ".edge";

	contigOutName = baseInName + ".contig";

	ReadBGraph(bGraphName, vertices, edges);

  ReadSequences(edgeFileName, edges);


	std::ofstream contigOut;
	openck(contigOutName, contigOut, std::ios::out);

	int e;
	Unmark(edges);
	std::stringstream titleStrm;
	for (e = 0; e < edges.size(); e++ ) {
		if (edges[e].marked != GraphEdge::Marked) {
			titleStrm.str("");
			titleStrm << e << " " << edges[e].length << " " << edges[e].multiplicity;
			edges[e].seq.PrintSeq(contigOut, titleStrm.str());
			contigOut << std::endl;
			edges[e].marked = GraphEdge::Marked;
			edges[edges[e].balancedEdge].marked = GraphEdge::Marked;
		}
	}
	return 0;
}
