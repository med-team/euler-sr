/***************************************************************************
 * Title:          MultTuple.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef MULT_TUPLE_H_
#define MULT_TUPLE_H_

#include "DNASequence.h"
#include "SeqUtils.h"
#include "Tuple.h"

class MultTuple : public Tuple {
	public:
	int mult;
	int Valid() {
		int i;
		for ( i = 0; i < this->size(); i++) {
			if (numeric_nuc_index[(*this)[i]] >= 4)
				return 0;
		}
		return 1;
	}
	int GetMult() {
		return mult;
	}
 MultTuple(const char *s) : Tuple(s) {
		mult = 1;
	}
	MultTuple() {}
	void assign(const char *charPtr, int length=tupleSize) {
		((std::string*)this)->assign((char*) charPtr, tupleSize);
		this->mult = 1;
	}
	int ReadLine(std::istream &in, int minMult=0) {
		if ((in >> *((std::string*) this) >> mult)) {
			std::string line;
			std::getline(in, line);
			if (mult >= minMult)
				return 1;
		}
		return 0;
	}
	int IncrementMult() {
		mult++;
		return mult;
	}
	MultTuple &operator=(std::string &str) {
		*((std::string*) this) = str;
		return *this;
	}
};



#endif
