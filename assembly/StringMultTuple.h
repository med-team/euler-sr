/***************************************************************************
 * Title:          StringMultTuple.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef STRING_MULT_TUPLE_H_
#define STRING_MULT_TUPLE_H_

#include "StringTuple.h"

class StringMultTuple : public StringTuple {
 public:
	int mult;
	int GetMult() {
		return mult;
	}
	char *GetString() {
		return s;
	}

 StringMultTuple() : StringTuple() {
		mult = 0;
	}

 StringMultTuple(char *sp) : StringTuple(sp){
		mult = 0;
	}
	
	StringMultTuple &operator=(StringMultTuple rhs) {
		if (s == NULL)
			s = new char[tupleSize+1];
		strncpy(s, rhs.s, tupleSize);
		mult = rhs.GetMult();
	}
	
	void ToString(std::string &str) {
		str.copy(s, tupleSize);
	}

	int IncrementMult(int inc = 1) {
		return mult+= inc;
	}
		
	int ReadLine(std::ifstream &in, int minMult = 0) {	
		if (tupleSize == -1) {
			std::string tuple;
			in >> tuple;
			tupleSize = tuple.size();
			if (s == NULL) {
				s = new char[tupleSize+1];
				s[tupleSize] = 0;
			}
			strncpy(s, tuple.c_str(), tupleSize);
			in >> mult;
		}
		else {
			if (s == NULL) {
				s = new char[tupleSize+1];
				s[tupleSize] = 0;
			}
			in >> s >> mult;
		}
		std::string remainder;
		std::getline(in, remainder);
		if (mult >= minMult)
			return 1;
		else
			return 0;
	}

	int operator<(StringMultTuple &rhs) {
		return (strncmp(s, rhs.s, tupleSize) < 0);
	}

	int operator>(StringMultTuple &rhs) {
		return (strncmp(s, rhs.s, tupleSize) > 0);
	}
	
	int operator==(StringMultTuple &rhs) {
		return (strncmp(s, rhs.s, tupleSize) == 0);
	}
	
	int operator !=(StringMultTuple &rhs) {
		return (!(*this == rhs));
	}

	friend std::ostream &operator<<(std::ostream &out, StringMultTuple &rhs) {
		out << rhs.s << " " << rhs.mult << std::endl;
		return out;
	}
	friend std::istream &operator>>(std::istream &in, StringMultTuple &rhs) {
		if (rhs.s == NULL)
			rhs.s = new char[StringMultTuple::tupleSize];
		in >> rhs.s >> rhs.mult;
	}
	void CleanUp() {
		if (s != NULL ){
			delete[] s;
		}
	}
	char operator[](int pos) {
		return s[pos];
	}
};


#endif
