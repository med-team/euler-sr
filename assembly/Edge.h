#ifndef EDGE_H_
#define EDGE_H_

class Edge {
 public:
  int dest;
  Edge() {
    dest = -1;
  }
  Edge& operator=(const Edge &e) {
    dest = e.dest;
  }
};

#endif
