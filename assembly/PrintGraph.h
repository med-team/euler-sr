#include "IntervalGraph.h"
#include "DeBruijnGraph.h"

#include <set>
#include <iostream>

int PrintGraphSubset(IntervalGraph &g, 
										 std::set<int> &vertexSet,
										 int minEdgeLength,
										 std::ostream &graphOut);
