#ifndef FIX_ERRORS_STATS_H_
#define FIX_ERRORS_STATS_H_

class Stats {
public:
  int numEdge;
  int numIns;
  int numDel;
  int numMut;
  int numNotSolid;
	int numTies;
  int numNoPathFound;
  int numMultiplePaths;
	int numErrorAtEnd;
  Stats() {
    Reset();
  }
  void Reset() {
    numEdge = numIns = numDel = numMut = 0;
    numNotSolid = 0;
    numNoPathFound = 0;
    numMultiplePaths = 0;
		numErrorAtEnd = 0;
		numTies = 0;
  }
	Stats &Append(Stats &s) {
    numEdge += s.numEdge;
    numIns += s.numIns;
    numDel += s.numDel;
    numMut += s.numMut;
		numTies += s.numTies;
    numNotSolid += s.numNotSolid;
    numNoPathFound  += s.numNoPathFound;
    numMultiplePaths += s.numMultiplePaths;
		numErrorAtEnd += s.numErrorAtEnd;
    return *this;
	}
  Stats &operator+=(Stats &s) {
		Append(s);
  }
	friend std::ostream &operator<<(std::ostream &strm, Stats &s) {
		strm << s.numIns << "\t" 
				 << s.numDel << "\t" 
				 << s.numMut << "\t" 
				 << s.numNotSolid << "\t" 
				 << s.numNoPathFound << "\t" 
				 << s.numMultiplePaths << std::endl;
				return strm;
	}
	static void PrintHeader(std::ostream &strm) {
		strm << "ins\tdel\tnumMut\tbad\tnoPath\tmultiplePath" << std::endl;
	}
};


#endif
