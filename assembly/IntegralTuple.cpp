/***************************************************************************
 * Title:          IntegralTuple.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "IntegralTuple.h"
#include "utils.h"

LongTuple IntegralTuple::MASK_OFF_FIRST = 0;
LongTuple IntegralTuple::MASK_OFF_LAST  = 0;
LongTuple IntegralTuple::VERTEX_MASK    = 0;
#include <iomanip>

void IntegralTuple::ToString() {
		std::string st;
		ToString(st);
		std::cout << st << std::endl;
	}



int ReadMultBoundedBinaryTupleList(std::string &fileName,
																	 int minMult,
																	 std::vector<CountedIntegralTuple>&  tupleList) {
	std::ifstream in;
	openck(fileName, in, std::ios::binary);
	int numTuples;
	in.read((char*) &numTuples, sizeof(int));
	std::cout << "reading through " << numTuples << std::endl;
	CountedIntegralTuple tuple;
	int numFreqTuples = 0;
	while(in) {
		if (in.read((char*) &tuple, sizeof(CountedIntegralTuple))) {
			if (tuple.count >= minMult) {
				//tupleList.push_back(tuple);
				numFreqTuples++;
			}
		}
	}
	in.clear();
	in.seekg(std::ios::beg);

	std::cout << "about to store " << numFreqTuples << " tuples." << std::endl;
	tupleList.resize(numFreqTuples);
	int i = 0;
	in.read((char*) &numTuples, sizeof(int));
	while(in) {
		if (in.read((char*) &tuple, sizeof(CountedIntegralTuple))) {
			if (tuple.count >= minMult) {
				//tupleList.push_back(tuple);
				//				std::string tupleStr;
				//				tuple.ToString(tupleStr);
				tupleList[i].tuple = tuple.tuple;
				tupleList[i].count = tuple.count;
				i++;
			}
		}
	}
	return numFreqTuples;
}
