/***************************************************************************
 * Title:          HashedSpectrum.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/05/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef HASHED_SPECTRUM_H_
#define HASHED_SPECTRUM_H_

#include "Spectrum.h"
#include "ReadPos.h"
#include "SimpleSequence.h"
#include "IntegralTuple.h"
#include "hash/VectorHashTable.h" // this defines the storage



template <typename T_TupleType, int T_HashLength, int BUFFER_SIZE>
class VectorTupleHash  {
 public:
	// use for computing a hash
	IntegralTupleHashValueFunctor<T_HashLength>  hashFunction;
	UpdateIntegralTupleCountFunctor<T_TupleType> updateFunction;

	// store here
	typedef VectorHashTable<T_TupleType,
		IntegralTupleHashValueFunctor<T_HashLength>,
		UpdateIntegralTupleCountFunctor<T_TupleType>, BUFFER_SIZE > HashTable;

	HashTable hashTable;

	void Flush() {
		hashTable.Flush(hashFunction, updateFunction);
	}

	void FlushDirected() {
		hashTable.FlushDirected(hashFunction, updateFunction);
	}

	VectorTupleHash() {
		hashTable.Init(hashFunction);
	}

	int HashSequence(SimpleSequence &seq,
									 int trimFront = 0,
									 int trimEnd   = 0) {
		int p;
		int newTupleStored = 0;
		int nextInvalid = -1;
		// Find the first position that is not valid
		for (p = trimFront; p < seq.length - T_TupleType::tupleSize; p++) {
			if (unmasked_nuc_index[seq.seq[p]] >= 4)
				nextInvalid = p;
		}
		T_TupleType tuple;
		int tupleSet = 0;
		for (p = trimFront; p < seq.length - T_TupleType::tupleSize + 1 - trimEnd; p++) {
			if (unmasked_nuc_index[seq.seq[p + T_TupleType::tupleSize -1]] >= 4) {
				nextInvalid = p + T_TupleType::tupleSize -1;
				tupleSet = 0;
			}
			if (p > nextInvalid) {
				if (tupleSet == 0) {
					tuple.StringToTuple(&(seq.seq[p]));
					tupleSet = 1;
				}
				else {
					T_TupleType nextTuple;
					ForwardNuc(tuple, 
										 unmasked_nuc_index[seq.seq[p+IntegralTuple::tupleSize - 1]], 
										 nextTuple);
					tuple.tuple = nextTuple.tuple;
				}
				if (hashTable.BufferedStore(tuple, hashFunction, updateFunction))
					newTupleStored = 1;
			}
		}
		return newTupleStored;
	}

	int HashSequenceUndirected(SimpleSequence &seq,
														 int trimFront = 0,
														 int trimEnd   = 0) {
		int p;
		int newTupleStored = 0;
		int nextInvalid = -1;
		T_TupleType* forStrand, *revStrand;
		forStrand = 0;
		// Find the first position that is not valid
		for (p = trimFront; p < seq.length - T_TupleType::tupleSize; p++) {
			if (unmasked_nuc_index[seq.seq[p]] >= 4)
				nextInvalid = p;
		}
		T_TupleType tuple, query, queryRC, tupleCopy, queryRCCopy;
		for (p = trimFront; p < seq.length - T_TupleType::tupleSize + 1 - trimEnd; p++) {
			
			if (unmasked_nuc_index[seq.seq[p + T_TupleType::tupleSize -1]] >= 4)
				nextInvalid = p + T_TupleType::tupleSize -1;
			if (p > nextInvalid) {
				tuple.StringToTuple(&(seq.seq[p]));
				hashTable.BufferedStore(tuple, hashFunction, updateFunction);

				tupleCopy = tuple;
				//				std::cout << "original tuple: " << tuple.tuple << std::endl;
				query = tuple;
				tuple.MakeRC(queryRC);
				queryRCCopy = queryRC;
				forStrand = hashTable.Find(query, hashFunction);
				//				revStrand = hashTable.Find(queryRC, hashFunction);
				//				std::cout << query.tuple << " " << forStrand << " " << revStrand << std::endl;
				if (forStrand) {
					// The key already exists in the forward direction, update that one
					updateFunction(*forStrand);
					//					if (hashTable.BufferedStore(tuple, hashFunction, updateFunction))
					//					if (hashTable.Store(tuple, hashFunction, updateFunction))
					newTupleStored = 1;
				}
			}
		}
		return newTupleStored;
	}
};

#endif
