#ifndef PATH_TRACE_H_
#define PATH_TRACE_H_

#include <vector>
#include <stdlib.h>
#include <assert.h>

class PathTrace {
 public:
	// Make resizing the matrix fast.
	std::vector<int> *edges;
	int parent;
	int count;
	int size() {
		assert(edges != NULL);
		return edges->size();
	}
	PathTrace() {
		count = 0;
		edges = new std::vector<int>;
		parent = -1;
	}
	PathTrace& operator=(const PathTrace &pt) {
		count = pt.count;
		edges = pt.edges;
		parent = pt.parent;
		return *this;
	}
	~PathTrace() {
		// just don't delete the edge list.
	}
};


typedef std::vector<PathTrace> PathTraceList;
class TraceMap {
 public:
	int trace;
	int pos;
	TraceMap(int t, int p) {
		trace    = t; pos = p;
	}
};

class TraceMapList {
 public:
	std::vector<TraceMap> traces;
	int numStart;
	int numInternal;
	int numEnd;
	int outResolved, inResolved;
	int startEdge, endEdge;
	int inAdjacent, outAdjacent;
	int GetFirstStartTraceIndex() {
		unsigned int t;
		for (t = 0; t < traces.size(); t++) { 
			if (traces[t].pos == 0)
				return t;
		}
		assert(t < traces.size());
	}
	int GetFirstEndTraceIndex(PathTraceList &pathTraces) {
		unsigned int t;
		for (t = 0; t < traces.size(); t++) {
			if ((int) pathTraces[traces[t].trace].edges->size() 
					== 
					traces[t].pos + 1) {
				return t;
			}
		}
		assert(t < traces.size());
	}

	
	TraceMapList() {
		numStart = 0;
		numInternal = 0;
		numEnd = 0;
		outResolved = 0;
		inResolved = 0;
		inAdjacent = 0;  outAdjacent = 0;
		startEdge = endEdge = -1;
	}
};
	
typedef std::vector<TraceMapList> TraceMapMatrix;

#endif
