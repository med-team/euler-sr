/***************************************************************************
 * Title:          BitSpectrum.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  02/05/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef BIT_SPECTRUM_H_
#define BIT_SPECTRUM_H_

#include <string>
#include "assert.h"
#include <iostream>
#include <stdlib.h>
//#include <malloc.h>
#include "utils.h"
#include "Spectrum.h"

template<typename T>
class BitSpectrum : public Spectrum<T> {
 private:
	int GetMultiplicity(unsigned int pos, int slot) {/*
		std::cout << "gm: " << std::hex << (int) tupleList[pos] << " mo ";
		std::cout << slot << " " << std::oct << (int) includeMask[slot] << " slot  " << slot*2 << " maksed " ;
		std::cout << std::oct << (int) (tupleList[pos] & includeMask[slot]) ;
		std::cout << " " << (int) ((tupleList[pos] & includeMask[slot]) >> (slot*2)) << std::endl;
																					*/
		/*
		std::cout << (int) tupleList[pos] << " " << (int) includeMask[slot] ;
		std::cout << " " << (int) (tupleList[pos] & includeMask[slot]);
		std::cout << " " << (int) ((tupleList[pos] & includeMask[slot]) >> (slot * 2)) 
							<< std::endl;
		*/
		return (tupleList[pos] & includeMask[slot]) >> (slot * 2);
	}

 public:
	int findOnlySolid;
	int size() {
		return listLength;
	}
	BitSpectrum(int ts) {
		this->tupleSize = ts;
		T::tupleSize = ts;
		InitList();
		findOnlySolid = 0;
	}
	
	void FindOnlySolid() {
		findOnlySolid = 1;
	}

	BitSpectrum(std::string &spectrumFile) {
		Read(spectrumFile);
	}
	
	unsigned int listLength;
	unsigned long int numTuples;
	unsigned char* tupleList;

	static unsigned char excludeMask[4];
	static unsigned char includeMask[4];

	void InitList() {
		numTuples = 1;
		numTuples <<= (this->tupleSize)*2;		

		//		std::cout << "numtuples: " << numTuples << std::endl;
		listLength = numTuples / 4;
		//		std::cout << "list length: " << listLength << std::endl;
		// Allocate space for the last tuple
		if (numTuples % 4 != 0) 
			listLength++;
		tupleList = new unsigned char[listLength];
	}


	int GetMultiplicity(unsigned int hashValue) {
		unsigned int pos = hashValue / 4;
		assert(pos < listLength);
		int slot = hashValue % 4;
		return GetMultiplicity(pos, slot);
	}

	void Write(std::string &fileName, int minMult=0) {
		// ignore the min mult for now.
		std::ofstream listOut;
		openck(fileName, listOut, std::ios::out);
		listOut << listLength << std::endl;
		listOut << numTuples  << std::endl;
		listOut.write((const char*) tupleList, listLength);
	}

	int Read(std::string &fileName, int minMult=0) {
		// ignore min mult for now
		std::ifstream listIn;
		openck(fileName, listIn, std::ios::in);
		listIn >> listLength;
		listIn >> numTuples;
		tupleList = new unsigned char[listLength];
		memset(tupleList, listLength, 0);
		std::string line;
		std::getline(listIn, line);
		if (!listIn.read((char*) tupleList, listLength)) {
			return 0;
		}
		else {
			return 1;
		}
	}

	int FindTuple(T &tuple) {
		unsigned int hashValue;
		if (tuple.GetHashValue(hashValue) < 0)
			return -1;
		int mult = GetMultiplicity(hashValue);
		if (findOnlySolid && mult >= 3)
			return 1;
		if (!findOnlySolid && mult != 0)
			return 1;
		
		// otherwise, the tuple isn't found.  It is 
		// either too low multiplicity and we are
		// only looking for solid, or there are no mult limits
		// and it is simply not found.
		return -1;
	}


	int IncrementMult(T &tuple) {
		unsigned int hashValue;
		int result = tuple.GetHashValue(hashValue);
		// Don't store hash values that have
		// masked nucleotides
		if (result < 0) return -1;
		unsigned int pos = hashValue / 4;
		int slot = hashValue % 4;
		assert(pos < listLength);
		int curMult = GetMultiplicity(pos, slot);
		//		std::cout << "location: "  << pos << " " << slot << " cur mult " << curMult << std::endl;
		if (curMult < 3) {
			curMult++;
			curMult <<= (slot*2);
			// clear the multiplicity at this spot
			tupleList[pos] &= excludeMask[slot];

			// put back in the new multiplicity
			tupleList[pos] |= curMult;
		}
		return curMult;
	}

	int CountSolid() {
		unsigned int pos, slot;
		int numSolid = 0;
		for (pos = 0; pos < listLength-1; pos++) {
			for (slot = 0; slot < 4; slot++) {
				int mult = GetMultiplicity(pos, slot);
				if (mult >= 3) {
					numSolid++;
				}
			}
		}
		return numSolid;
	}

	int CountAll(unsigned int freq[]) {
		unsigned int pos, slot;
		int numSolid = 0;
		freq[0] = freq[1] = freq[2] = freq[3];
		for (pos = 0; pos < listLength-1; pos++) {
			for (slot = 0; slot < 4; slot++) {
				int mult = GetMultiplicity(pos, slot);
				freq[mult]++;
			}
		}
		int last = numTuples % 4;
		if (last == 0)
			last = 4;
		for (slot = 0; slot < last; slot++) {
			int mult = GetMultiplicity(pos,slot);
			if (mult >= 0) 
				freq[mult]++;
		}
		return numSolid;
	}

};


template<typename T>
unsigned char BitSpectrum<T>::excludeMask[4] = {0xfc, 0xf3, 0xcf, 0x3f};

template<typename T>
unsigned char BitSpectrum<T>::includeMask[4] = {0x03, 0x0c, 0x30, 0xc0};

#endif
