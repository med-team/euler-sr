/***************************************************************************
 * Title:          CreateMateScaffold.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "IntervalGraph.h"
#include "MateLibrary.h"
#include "PathLib.h"
#include <vector>
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;


typedef std::vector<std::list<int > > ScaffoldList;


	
int SearchContainedScaffold(ScaffoldList &scaffoldList, int src, int dest, int maxDepth) {
	if (maxDepth == 0)
		return 0;

	if (std::find(scaffoldList[src].begin(), 
								scaffoldList[src].end(), dest) != scaffoldList[src].end()) {
		return 1;
	}
	else {
		std::list<int>::iterator it;
		for (it = scaffoldList[src].begin(); it != scaffoldList[src].end(); ++it) {
			if (SearchContainedScaffold(scaffoldList, *it, dest, maxDepth - 1)) {
				// return this path early 
				return 1;
			}
		}
		return 0;
	}
}

void RemoveContainedScaffolds(ScaffoldList &scaffoldList) {
	int e;
	std::list<int>::iterator it, erased;
	for (e = 0; e < scaffoldList.size(); e++) {
		it = scaffoldList[e].begin();
		while (it != scaffoldList[e].end()) {
			if (*it == e) {
				erased = it;
				++it;
				scaffoldList[e].erase(erased);
			}
			else {
				++it;
			}
		}
	}
	for (e = 0; e < scaffoldList.size(); e++) { 
		std::list<int>::iterator it1, it2, erasedIt;

		if (scaffoldList[e].size() > 1) {
			it1 = scaffoldList[e].begin();
			while (it1 != scaffoldList[e].end()) {
				it2 = scaffoldList[e].begin();
				while(it2 != scaffoldList[e].end()) {
					if (*it1 == *it2) {
						++it2;
						continue;
					}

					int removedIt1 = 0;
					if (SearchContainedScaffold(scaffoldList, *it2, *it1, 5)) {
						erasedIt = it2;
						++it2;
						std::cout << "erasing " << e << " " << *erasedIt << " ---> " << *it1 << std::endl;
						scaffoldList[e].erase(erasedIt);
					}
					else {
						++it2;
					}
				}
				++it1;
			}
		}
	}
}


void PrintUsage() {
	std::cout << " usage: createMateScaffold graphIn mateTable graphOut " << std::endl
						<< "  [-minPathCount c]  Remove paths with count less than 'c' " << std::endl
						<< "  [-minMatePairCount c] Remove paths with mate count less than 'c'" << std::endl;
}

int main(int argc, char* argv[]) {
	
	std::string graphFileName, mateTableName, graphOutName;
	if (argc < 4) {
		PrintUsage();
		exit(0);
	}
	graphFileName = argv[1];
	mateTableName = argv[2];
	graphOutName  = argv[3];
	int minPathCount = 2;
	int minMatePairCount = 2;
	int argi = 4;
	while (argi < argc) {
		if (strcmp(argv[argi], "-minPathCount") == 0) {
			minPathCount = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-minMatePairCount") == 0) {
			minMatePairCount = atoi(argv[++argi]);
		}
		else {
			PrintUsage();
			std::cout << "bad option: " << argv[argi] << std::endl;
			exit(0);
		}
		++argi;
	}

	IntervalGraph graph;
	ReadMateList  mateTable;
	std::cout << "Reading interval graph." << std::endl;
	int vertexSize;
	ReadIntervalGraph(graphFileName, graph, vertexSize);
	std::cout << "Reading mate table." << std::endl;
	ReadMateTable(mateTableName, mateTable);

	PathIntervalList &paths       = graph.paths;
	PathLengthList   &pathLengths = graph.pathLengths;
	TEdgeList        &edges       = graph.edges;
	TVertexList      &vertices    = graph.vertices;

	/*
	 * Now collect the mate-edges from each edge to see 
	 * if there is a unique edge to scaffold it with.
	 *
	 */
	TEdgeList connectingEdges;
	int newEdgeIndex = edges.size();
	int newEdgeCount = 0;
	int v;
	// Now disconnect all vertices.  They will be scaffolded later on.
	TVertexList newVertices;
	for (v = 0; v < vertices.size(); v++ ){
		int vi, vo;
		for (vi = 0; vi < vertices[v].in.size(); vi++ ){
			/*			if (vertices[v].in[vi] != -1) {
				edges[vertices[v].in[vi]].dest = -1;
			*/
				vertices[v].in[vi] = -1;
				//			}
		}
		for (vo = 0; vo < vertices[v].out.size(); vo++ ){
			/*			if (vertices[v].out[vo] != -1) {
				edges[vertices[v].out[vo]].src = -1;
			*/
				vertices[v].out[vo] = -1;
				//			}
		}
	}

	vertices.resize(edges.size() * 2);
	v = 0;
	std::vector<std::list<int> > scaffold;
	scaffold.resize(edges.size());
	int e;
	for (e = 0; e < edges.size(); e++) {
		vertices[v].AddOutEdge(e);
		vertices[v+1].AddInEdge(e);
		edges[e].src = v;
		edges[e].dest = v + 1;
		v+=2;
	}

	for (e = 0; e < edges.size(); e++) {
		MateEdgeMap mateEdges;
		CollectMateEdges(graph, mateTable, e, mateEdges, 0);

		MateEdgeMap::iterator matesIt, matesEnd, deletedIt;
		matesEnd =  mateEdges.end();
		
		// Remove low count mate edges.
		std::cout << "for edge " << e << " (" << edges[e].index << ", " 
							<< edges[e].length  << ") collected: " << mateEdges.size() << " edges."
							<< std::endl;
		for (matesIt = mateEdges.begin(); matesIt != mateEdges.end(); ++matesIt) {
			std::cout << (*matesIt).first << " (" << (*matesIt).second.count << ", " 
								<< edges[e].length - (*matesIt).second.avgStartPos << ", "
								<< (*matesIt).second.avgEndPos << ", "
								<< edges[(*matesIt).first].length << ") ";
			int mateSep = ((edges[e].length - (*matesIt).second.avgStartPos) + 
										 (*matesIt).second.avgEndPos);
			if (mateSep < 350) {
				scaffold[e].push_back((*matesIt).first);
			}
		}
		std::cout << std::endl;
	}

	int nscaffolds = 0;
	for (e = 0; e < edges.size(); e++)  {
		if (scaffold[e].size() > 0) {
			std::list<int>::iterator sit;
			std::cout << "scaffold " << e << " " << edges[e].length << " : ";
			for (sit = scaffold[e].begin(); sit != scaffold[e].end(); ++sit) {
				std::cout << *sit << " " ;
				++nscaffolds;
			}
			std::cout << std::endl;
		}
	}
	std::cout << "before transitive edge removal found: " << nscaffolds << " scaffolds."
						<< std::endl;

	RemoveContainedScaffolds(scaffold);
	
	int nremaining = 0;
	for (e = 0; e < edges.size(); e++)  {
		if (scaffold[e].size() > 0) {
			std::list<int>::iterator sit;
			std::cout << "scaffold " << e << " " << edges[e].length << " : ";
			for (sit = scaffold[e].begin(); sit != scaffold[e].end(); ++sit) {
				std::cout << *sit << " " ;
				++nremaining;
			}
			std::cout << std::endl;
		}
	}

	for (e = 0; e < edges.size(); e++) {
		MateEdgeMap mateEdges;

		// These two edges should be joined by a new edge.
		TEdge newEdge;
		//		if (mateEdges.size() == 1 or mateEdges.size() == 2) {

		int numSpanning, avgSrcEnd, avgDestBegin;

		/*			
						MateEdgeMap::iterator firstIt, secondIt;
						firstIt = secondIt = mateEdges.begin();
						int srcEdge = e;
						int destEdge = (*firstIt).first;
						int avgStartPos;
						if (mateEdges.size() == 2) {
						++secondIt;
						if ((*firstIt).first == srcEdge) {
						destEdge = (*secondIt).first;
						avgStartPos = (*secondIt).second.avgStartPos;
						}
						}
						else {
						destEdge = (*firstIt).first;
						avgStartPos = (*firstIt).second.avgStartPos;
						}
		*/
		/*			GetAverageMateStartPos(graph, mateTable, srcEdge, destEdge, 
						avgSrcEnd, avgDestBegin);
		*/

		std::list<int>::iterator pairedEdgeIt;
		int srcEdge, destEdge;
		srcEdge = e;
		for (pairedEdgeIt = scaffold[e].begin();
				 pairedEdgeIt != scaffold[e].end();
				 ++pairedEdgeIt) {
			destEdge = *pairedEdgeIt;
				
			//			if (avgDestBegin < 150 and 150 > (edges[e].length - avgStartPos) ) {
			
			//				int gapLength = 300 - (avgDestBegin + (edges[e].length - avgStartPos));
			/*				std::cout << "edges: " << srcEdge << " (" << edges[srcEdge].length 
								<< ") " << destEdge  << " (" << edges[destEdge].length << ") "
								<< " separated by: " << gapLength << std::endl;
			*/
			int gapLength = 100;
			connectingEdges.push_back(newEdge);
			connectingEdges[newEdgeCount].src = edges[srcEdge].dest;
			connectingEdges[newEdgeCount].dest = edges[destEdge].src;
			connectingEdges[newEdgeCount].Init();
			// Initialize the intervals to NULL.

					
			connectingEdges[newEdgeCount].length = vertices[srcEdge].vertexSize + gapLength;
			int newEdgeLength = connectingEdges[newEdgeCount].length;				
			connectingEdges[newEdgeCount].seq.seq = new unsigned char[newEdgeLength];
			int s;


			for (s = 0; s < newEdgeLength; s++ )
				connectingEdges[newEdgeCount].seq.seq[s] = 'N';
			connectingEdges[newEdgeCount].seq.length = newEdgeLength;
			vertices[edges[srcEdge].dest].AddOutEdge(newEdgeIndex);
			vertices[edges[destEdge].src].AddInEdge(newEdgeIndex);
			++newEdgeIndex;
			++newEdgeCount;
		}
	}
	
	std::cout << "after transitive edge removal: " << nremaining << std::endl;

	std::cout << "old number of edges: " << edges.size() << " new: " << connectingEdges.size() << std::endl;
	int n;
	for (n =0; n < connectingEdges.size(); n++ ) {
		edges.push_back(connectingEdges[n]);
		edges[edges.size() - 1].flagged = GraphEdge::Marked;
	}



	std::string coloredGvzName = graphOutName + ".colored.gvz";
  GVZPrintBGraph(graph.vertices, graph.edges, coloredGvzName);
	
	graph.CondenseSimplePaths();
	
	std::string bGraphOutName = graphOutName + ".bgraph";
	std::string intvOutName = graphOutName + ".intv";
	std::string gvzOutName  = graphOutName + ".gvz";
	std::string pathOutName = graphOutName + ".path";
	std::string edgeOutName = graphOutName + ".edge";
	std::string euGraphOutName = graphOutName + ".graph";
	CheckEdges(graph.vertices, graph.edges);
  PrintEdges(graph.vertices, graph.edges, edgeOutName);

	graph.PrintIntervalGraph(bGraphOutName, intvOutName);


  PrintGraph(graph.vertices,graph.edges, euGraphOutName);
  GVZPrintBGraph(graph.vertices, graph.edges, gvzOutName);
	WriteReadPaths(pathOutName, graph.paths, graph.pathLengths);

}
