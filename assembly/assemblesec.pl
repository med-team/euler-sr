#!/usr/bin/env perl
 
if ($#ARGV < 0) {
  print "usage: assemblesec.pl readsFile [-vertexSize v] [-exeDir e] [-minKmerCount m] \n";
	print "assembleSEC: assembly Sans Error Correction.  A quick way of binnign reads.\n";
	print "This simply builds a de bruijn graph on raw reads, and does not process it\n";
	print "further.  For certain applications, this is sufficient, but for most de novo\n";
	print "assemblies, this will not assemble any meaningful contigs.\n";
  exit(0);
}
$vertexSize = 20;
$machtype = $ENV{"MACHTYPE"};
$srcDir = $ENV{"EUSRC"};
$exeDir = "$srcDir/assembly/$machtype";
$readsFile = shift @ARGV;
$curdir = "";
if (! -e $readsFile ){
  print "$readsFile does not exist\n";
  exit(1);
}
$minKmerCount = 1;
while ($#ARGV >= 0) {
  $option = shift @ARGV;
  if ($option eq "-vertexSize") {
			$vertexSize = shift @ARGV;
  }
  elsif ($option eq "-dir") {
			$curdir = shift @ARGV;
  }
	elsif ($option eq "-minKmerCount") {
			$minKmerCount = shift @ARGV;
	}
}

if ($curdir != "") {
  chdir($curdir);
}

# create all the commands at the top of the script so 
# I don't have to look all over the place to find them

if ($vertexSize > 27) {
		$buildVertexCmd = 
				"$exeDir/countSpectrum $readsFile $readsFile.v -tupleSize $vertexSize -printPos";
		$sortVertexCmd =
				"$exeDir/sortVertexList $readsFile.v $readsFile $vertexSize $readsFile.sv";
		$buildGraphCmd = 
				"$exeDir/debruijn $readsFile $readsFile.sv $readsFile.dot -vertexSize $vertexSize";
		$edgesToOverlapListCmd =
				"$exeDir/edgesToOverlapList $readsFile.edge $vertexSize $readsFile.ovp";
		$printReadIntervalsCmd =
				"$exeDir/printReadIntervals $readsFile.ovp $readsFile.edge $readsFile $vertexSize $readsFile.intv $readsFile.path";
}
else {
#		$buildVertexCmd =
#				"$exeDir/integralCountSpectrum $readsFile $vertexSize $readsFile.spect -binary -skipGapped -minMult 2";
		$buildVertexCmd =
				"$exeDir/readsToSpectrum $readsFile $vertexSize $readsFile.spect -minMult $minKmerCount";
    $sortVertexCmd = 
				"$exeDir/sortIntegralTupleList $readsFile.spect";
		$trim = $vertexSize * 3;
    $buildGraphCmd =
				"$exeDir/svdeBruijn $readsFile $vertexSize -trimShort $trim";
		$removeFilesCmd = "";
		
		$edgesToOverlapListCmd = 
				"$exeDir/integralEdgesToOverlapList $readsFile.edge $vertexSize $readsFile.iovp";

		$printReadIntervalsCmd = 
				"$exeDir/integralPrintReadIntervals $readsFile";
}


$removeFilesCmd = "$srcDir/assembly/CleanUpIntermediates.pl $readsFile";

print "$buildVertexCmd\n";
$res = system($buildVertexCmd);  #1
if ($res != 0) {
  print "building vertices failed: $res\n";
  exit(1);
}
print "sorting vertices\n";
$res = system($sortVertexCmd);  #2
if ($res != 0) {
  print "sorting vertices failed: $res\n";
  print "$sortVertexCmd\n";
  exit(1);
}
print "building graph\n";
$res = system($buildGraphCmd); #3
if ($res != 0) {
  print "building the graph failed: $res\n";
  print "$buildGraphCmd\n";
  exit(1);
}

print "mapping read intervals\n";
$res = system($edgesToOverlapListCmd);  #4
if ($res != 0) {
  print "creating the overlap list failed: $res\n";
  exit(1);
}
$res = system($printReadIntervalsCmd); #5
if ($res != 0) {
  print "printing the read intervals failed: $res\n";
	print "$printReadIntervalsCmd\n";
  exit(1);
}

