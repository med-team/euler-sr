#include "SeqReader.h"
#include "DNASequence.h"
#include "IntegralTuple.h"
#include "utils.h"

#include <vector>
#include <string>

class CompareWithCombine {
public:
	int operator()(CountedIntegralTuple &a, CountedIntegralTuple &b) const {
		if (a.tuple == b.tuple) {
			a.count = b.count = a.count + b.count;
		}
		return a.tuple < b.tuple;
	}
};

class CompareCountedIntegralTuples {
public:
	int operator()(const CountedIntegralTuple &a, const CountedIntegralTuple &b) const {
		return a.tuple < b.tuple;
	}
};

using namespace std;
int IntegralTuple::tupleSize = 0;

typedef std::vector<CountedIntegralTuple> TupleVector;

void CompressReverseComplements(TupleVector &tupleList) {
	std::sort(tupleList.begin(), tupleList.end(), CompareCountedIntegralTuples());
	int i;
	CountedIntegralTuple rc;
	i = 0;

	// Combine rev comp multiplicities.
	for (i = 0; i < tupleList.size(); i++ ) {
		if (tupleList[i].count < 0)
			continue;
		tupleList[i].MakeRC(rc);
		if (rc.tuple > tupleList[i].tuple) {
			int rcTupleIndex;
			rcTupleIndex = LookupBinaryTuple(&tupleList[i+1], tupleList.size() - i - 1, rc);
			if (rcTupleIndex != -1) {
				tupleList[i].count = tupleList[i].count + tupleList[i+1+rcTupleIndex].count;
				tupleList[rcTupleIndex + i + 1].count = -1;
			}
		}
	}
	// Remove the rc from the list.
	int cur = 0;
	for (i = 0, cur = 0; i < tupleList.size(); i++) {
		if (tupleList[i].count >= 0) {
			tupleList[cur].tuple = tupleList[i].tuple;
			tupleList[cur].count = tupleList[i].count;
			cur++;
		}
	}
	cout << "removed " << tupleList.size() - cur << " reverse complements of " << tupleList.size() << endl;
	tupleList.resize(cur);
}

void Compress(TupleVector &tupleList) {
	std::sort(tupleList.begin(), tupleList.end(), CompareCountedIntegralTuples());
	int i, c, n;
	i = 0; c = 0;
	int numRemoved = 0;
	int indexOfLast = tupleList.size() - 1;
	int listSize = tupleList.size();
	int curI;
	int count;
	for (i = 0; i < listSize ; i++ ){ 
		count = 1;
		curI = i;
		while (i < indexOfLast and 
					 tupleList[curI] == tupleList[i+1]) {
			i++;
			count++;
		}
		tupleList[c] = tupleList[i];
		tupleList[c].count = count;
		c++;
		numRemoved += (count - 1);
	}
	
	//	cout << "Removed: " << numRemoved << " during compression." << endl;
	tupleList.resize(c);
}


void Join(TupleVector &srcA, 
					TupleVector &srcB, 
					TupleVector &dest) {
	int a = 0, b = 0;

	int nA = srcA.size(), nB = srcB.size();
	int d;

	//	back_insert_iterator<TupleVector> mergeOutput(dest);
	//	set_union(srcA.begin(), srcA.end(), srcB.begin(), srcB.end(), mergeOutput, CompareWithCombine() );
	while (a < nA and b < nB) {
		if (srcA[a] < srcB[b]) {
			dest.push_back(srcA[a]);
			a++;
		}
		else if (srcB[b] < srcA[a]) {
			dest.push_back(srcB[b]);
			b++;
		}
		else {
			assert(srcA[a].tuple == srcB[b].tuple);
			int prevA = srcA[a].count;
			srcA[a].count = srcA[a].count + srcB[b].count;
			dest.push_back(srcA[a]);
			int lastd = dest.size() - 1;
			a++;
			b++;
		}
	}
	while (a < nA) {
		dest.push_back(srcA[a]);
		a++;
	}
	while (b < nB) {
		dest.push_back(srcB[b]);
		b++;
	}

	//	CompressReverseComplements(dest);
}

void Update(TupleVector &src,
						TupleVector &dest) {
	int nSrc = src.size(), nDest = dest.size();
	
	TupleVector tmp;
	Compress(src);
	Join(src,dest,tmp);
	dest = tmp;
	/*	cout << "Update: " << nSrc << " " << nDest << " " << src.size() 
			 << " " << dest.size() << endl;
	*/
}


// Define data structures and operations for a balanced binomial sorted list tree

class BinomialVertexNode {
public:
	int totalSize;
	BinomialVertexNode *left, *right, *parent;
	int leftSize, rightSize;
	TupleVector tupleList;
	string pagedOutFileName;
	int isPaged;
	static int pageIndex;
	static int pageSize;
	BinomialVertexNode() {
		leftSize = rightSize = 0;
		left = right = parent = 0;
		isPaged = 0;
	}
	void AddList(TupleVector &newList) {
		tupleList = newList;
		Compress(tupleList);
		//CompressReverseComplements(tupleList);
		totalSize = tupleList.size();
	}

	void MergeChildren() {
		assert(left != NULL);
		assert(right != NULL);
		Join(left->tupleList, right->tupleList, tupleList);
		delete left;
		delete right;
		left = right = NULL;
	}
	
	void PageOut(string pageFileName) {
		cout << "NOT DONE" << endl;
		exit(0);
		std::ofstream pageOut;
		openck(pageFileName, pageOut, std::ios::out|std::ios::binary);
		pageOut.write((const char *) &tupleList[0], sizeof(CountedIntegralTuple) * tupleList.size());
		int listSize = tupleList.size();
		isPaged = 1;
		pageOut.close();
	}

	void MapPage(string pageFileName) {
		cout << "MapPage not done" << endl;
		exit(0);
		//		int fildes = open(pageFileName.c_str(), );
		/*
		CountedIntegralTuple *mappedList;
		mappedList = (CountedIntegralTuple*) mmap(0, 
																							sizeof(CountedIntegralTuple) * listSize , 
																							PROT_READ, MAP_PRIVATE, 
																						fildes, 0);
		*/
	}
};


int BinomialVertexNode::pageIndex = 0;
int BinomialVertexNode::pageSize  = 0;

BinomialVertexNode* AddNewList(BinomialVertexNode *&root, TupleVector &newList) {
	if (root == NULL) {
		root = new BinomialVertexNode;
		root->AddList(newList);
		return root;
	}
	else {
		if (root->left == NULL) {

			// Nodes are always added as a sibling to the current node. 
			// A new parent is created, and the current parent is added 
			// as the right child to the current node.
			// The current node is demoted to a child

			// Make the current node children.
			BinomialVertexNode *newRoot = new BinomialVertexNode, *leftChild, *rightChild;

			// the left child is new.
			leftChild = newRoot->left   = new BinomialVertexNode;
			leftChild->parent = newRoot;
			leftChild->AddList(newList);

			// the right child is the root, demoted.
			rightChild = newRoot->right = root;

		 	// Link the new root back up.
			newRoot->parent = root->parent;

			// Move the root down.
			root->parent = newRoot;
			newRoot->totalSize = root->totalSize + newRoot->left->totalSize;


			// Reassign whatever was pointing to the root to be the new root.
			root = newRoot;
			return leftChild;
		}
		else {
			// left is not null
			assert(root->left != NULL);
			assert(root->right != NULL);
			if (root->left->totalSize < root->right->totalSize) {
				return AddNewList(root->left, newList);
			}
			else {
				return AddNewList(root->right, newList);
			}
		}
	}
}

int Close(int a, int b, float percent) {
	if ( abs(a - b) < percent*a or 
			 abs(a - b) < percent*b) 
		return 1;
	else
		return 0;
}

BinomialVertexNode *BalanceTree(BinomialVertexNode *leaf) {

	// Cannot balance the root, since if we get here, there is just 
	// one vertex.

	BinomialVertexNode *parent = leaf->parent, *root;
	if (parent == NULL)
		root = leaf;
	else
		root = parent;

	while (parent != NULL and 
				 Close(parent->left->totalSize, 
							 parent->right->totalSize, 0.15)) {
		/*		cout << "merging children of size: " << parent->left->totalSize << " " 
				 << parent->right->totalSize << endl;
		*/
		parent->MergeChildren();
		parent->totalSize = parent->tupleList.size();
		root   = parent;
		parent = parent->parent;
	}
	return root;
}

BinomialVertexNode *CollapseEntireTree(BinomialVertexNode *root) {
	if (root->left != NULL) {
		CollapseEntireTree(root->left);
		CollapseEntireTree(root->right);

		root->MergeChildren();
	}
	return root;
}

void PrintUsage() {
	cout << "usage: readsToSpectrum reads tupleSize spectrumOut" << endl;
	cout << "  -minMult M (0) Only output tuples with multiplicity >= M " << endl << endl;
	cout << "  -bufferSize B (1000000) Read tuples into buffers of size B" << endl << endl;
	cout << "  -printCount    Print the number of times each tuple appears" << endl << endl;
	cout << "  The tuple list is output in a binary format, with the tuple first and " <<endl
			 << "  the count next if printCount is given." << endl
			 << "  -usePaging  S  Page out buffers when they pass 'S' in size." << endl;
}

int main(int argc, char* argv[]) {

	cout << "sizeof countedintegraltuple: " << sizeof(CountedIntegralTuple) << " "
			 << "sizeof it: " << sizeof(IntegralTuple) << endl;

	if (argc < 4) {
		cout << "usage: readsToSpectrum reads tupleSize spectrumOut" << endl;
		exit(1);
	}
	string readsFileName         = argv[1];
	int tupleSize                = atoi(argv[2]);
	string spectrumOutFileName   = argv[3];
	int argi = 4;
	int minMult = 0;
	int bufferSize = 1000000;
	int printCount = 0;
	while (argi < argc) {
		if (strcmp(argv[argi], "-minMult") == 0) {
			minMult = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-bufferSize") == 0) {
			bufferSize = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-printCount") == 0) {
			printCount = 1;
		}
		else {
			PrintUsage();
			cout << " bad option: " << argv[argi] << endl;
			exit(1);
		}
		++argi;
	}
	CountedIntegralTuple::tupleSize = tupleSize;
	TupleVector tupleList, buffer;
	DNASequence seq;
	ifstream readsIn;
	openck(readsFileName, readsIn, std::ios::in);
	int r = 0;
	long total = 0;
	BinomialVertexNode *listRoot = NULL, *listLeaf = NULL, *balancedRoot;

	string reportFileName;
	std::ofstream report;
	std::ofstream spectrumOut;
	reportFileName       = readsFileName + ".report";
	openck(reportFileName, report, std::ios::out);
	BeginReport(argc, argv, report);
	openck(spectrumOutFileName, spectrumOut, std::ios::out | std::ios::binary);
	buffer.resize(bufferSize);
	int bufCur = 0;
	while (SeqReader::GetSeq(readsIn, seq, SeqReader::noConvert)) {
		int i;
		CountedIntegralTuple t;
		i = 0;

		// advance to first valid.
		while(i < seq.length - tupleSize + 1 and
					!t.StringToTuple(&seq.seq[i])) 
			i++;

		char nuc;
		while (i < seq.length - tupleSize + 1) {

			if (i < seq.length - tupleSize + 1) {
				buffer[bufCur] = t;
				++bufCur;
				//				buffer.push_back(t);
			}

			nuc = seq.seq[i+tupleSize];
			if (nuc != 'A' and
					nuc != 'C' and
					nuc != 'T' and
					nuc != 'G') {
				i+= tupleSize;
				while (i < seq.length - tupleSize + 1 and
							 !t.StringToTuple(&seq.seq[i]))
					i++;
			}
			else {
				CountedIntegralTuple next;
				ForwardNuc(t, numeric_nuc_index[seq.seq[i+tupleSize]], next);
				t = next;
			}
			i++;
			total++;
			if (bufCur == bufferSize) {
				listLeaf = AddNewList(listRoot, buffer);
				balancedRoot = BalanceTree(listLeaf);
				bufCur = 0;
			}
		}
				
		/*
		for (i = 0; i < seq.length - tupleSize + 1; i++ ){ 
			if (t.StringToTuple(&seq.seq[i]))
				tupleList.push_back(t);
		}
		*/
		r++;
		if (r % 500000 == 0) {
			cout << r << endl;
		}
	}
	// only one buffer has been created.
	buffer.resize(bufCur);
	cout << "adding last list of size " << buffer.size();
	listLeaf = AddNewList(listRoot, buffer);
	
	balancedRoot = CollapseEntireTree(listRoot);

	std::sort(listRoot->tupleList.begin(), listRoot->tupleList.end(), CompareCountedIntegralTuples());
	
	cout << "before compression: " << listRoot->tupleList.size() << endl;
	CompressReverseComplements(listRoot->tupleList);
	
	int i;
	long nTuples = listRoot->tupleList.size();


	// First count the number above threshold.
	int freqListSize = 0;
	cout << "root list has: " << listRoot->tupleList.size() << " tuples." << endl;
	int numPalindrome = 0;
	CountedIntegralTuple rc;
	for (i = 0; i < listRoot->tupleList.size(); i++) {
		if (listRoot->tupleList[i].count >= minMult) {
			freqListSize++;
			listRoot->tupleList[i].MakeRC(rc);
			if (rc.tuple == listRoot->tupleList[i].tuple)
				numPalindrome++;
		}
	}	
	freqListSize *=2;
	freqListSize -= numPalindrome;
	cout << "excluding " << numPalindrome << " palindromes. " << endl;
	spectrumOut.write((const char*) &freqListSize, sizeof(int));
	cout << "writing " << freqListSize << " tuples of " << tupleList.size() << endl;
	for (i = 0; i < listRoot->tupleList.size(); i++) {
		if (listRoot->tupleList[i].count >= minMult) {
			spectrumOut.write((const char*) &listRoot->tupleList[i], sizeof(CountedIntegralTuple));
			listRoot->tupleList[i].MakeRC(rc);
			if (listRoot->tupleList[i].tuple != rc.tuple) {
				rc.count = listRoot->tupleList[i].count;
				LongTuple tuple = rc.tuple;
					spectrumOut.write((const char*) &rc, sizeof(CountedIntegralTuple));		
			}
		}
	}
	spectrumOut.close();
	EndReport(report);
	return 0;
}
