#ifndef PATH_LIB_H_
#define PATH_LIB_H_
#include <list>
#include "ReadPaths.h"
#include "PathBranch.h"
#include "mctypes.h"
#include "IntervalGraph.h"
#include "MateTable.h"
#include "Trace.h"


typedef std::pair<int, int> InOutEdgePair;

void CollectPathTree(PathIntervalList &paths,
										 PathLengthList &pathLengths,
										 PathBranch &pathTree);

void CollectPathTreeOnPath(PathInterval* path, int pathLength,
													 PathBranch &pathTree);

void PrintPathTree(PathBranch &pathTree);
void PrintPathTree(PathBranch &pathTree, std::list<int> &curPath);
void PrintEdgeTraces(PathTraceList &pathTraces, TraceMapMatrix &traceMaps, int edge);

int FindPathCoverage(PathInterval *path, int pathLength,
										 PathBranch &pathTree);

void DeletePathTraceList(PathTraceList &list);

int TrimLowCoverageBranches(PathBranch &pathTree, int minCount);

int RemoveLowCountPaths(IntervalGraph    &g,
												PathIntervalList &paths,
												PathLengthList   &pahtLengths,
												PathBranch       &removedPathTree,
												int minPahtCount);

int RemoveLowCountTraces(PathTraceList &pathTraces, int minCount, PathBranch &removedPathTree);

void PathTreeToPathList(PathBranch &pathTree, PathTraceList &pathTraces);

void PathTreeToPathList(PathBranch &pathTree, 
												std::list<int> &curPath,
												PathTraceList &pathTraces);

void RemoveEnclosedPaths(PathTraceList &pathTraces);
int LocatePathStart(PathTraceList &pathTraces, int startEdge);

void MarkEnclosedPaths(PathTraceList &pathTraces);
void RemoveEnclosedPaths(PathTraceList &pathTraces);

void MarkExInternal(PathTraceList &pathTraces, TraceMapMatrix &traceMap);

void PrintPathTraces(PathTraceList &pathTraces, IntervalGraph &graph,
										 std::ostream &out);


int CheckForwardConsistency(PathTrace& path1, int pos1, PathTrace &path2, int pos2,
														int minMatchLength = 1);

int CheckReverseConsistency(PathTrace &path1, int pos1, PathTrace &path2, int pos2,
														int minMatchLength = 1);


// Store a map from an edge to the paths that go through it.

void StoreTraceMaps(PathTraceList &pathTraces, 
										TraceMapMatrix & traceMap  );

// Count how many edges are adjacent to this one in a postman traversal.
void CountAdjacencies(PathTraceList &pathTraces, TraceMapMatrix &traceMap);

// Checks to see if an edge is contained as the center
// of other paths.

int IsEdgePathContained(PathTraceList &pathTraces,
												TraceMapMatrix &traceMap,
												int edgeIndex);


// Looks to see if there is a 1-1 correspondence of entrance and exit edges
// for a repeat.
int AreEntranceAndExitEdgesPaired(PathTraceList &pathTraces,
																	TraceMapMatrix &traceMap,
																	TVertexList &vertices,
																	TEdgeList &edges,
																	int centerEdgeIndex, std::set<InOutEdgePair> &edgePairs);

void CollectPathSequence(TVertexList &vertices, 
												 TEdgeList &edges,
												 PathTrace &path, 
												 SimpleSequence &seq);


int  ArePathAndTraceReverseConsistent(PathInterval* path, int pathPos, int pathLength,
																			PathTrace &trace, int tracePos, int &pathEnd);
int  ArePathAndTraceForwardConsistent(PathInterval* path, int pathPos, int pathLength,
																			PathTrace &trace, int tracePos, int &pathBegin);

int FindLongestPathTraceReverseConsistency(PathInterval *path,
																					 int pathPos, int pathLength,
																					 PathTrace &trace, int tracePos,
																					 int &pathBegin);

int  FindLongestPathTraceForwardConsistency(PathInterval *path, 
																						int pathPos, int pathLength,
																						PathTrace &trace, int tracePos, 
																						int &pathEnd);

int FindLongestPathTraceOverlap(PathInterval *path, int pathLength, int &pathBegin, int &pathEnd,
																PathTrace &trace, 
																int &traceBegin, int &traceEnd, int findFirst);

int TraceContainsDuplications(PathTrace &trace );


int CountForwardConsistent(PathTrace &refPath, int refPos, int matchLength,
													 PathTraceList &pathTraces,
													 TraceMapMatrix &traceMap,
													 int edgeIndex, int &forwardConsistentTrace);

int CountReverseConsistent(PathTrace &refPath, int refPos, int matchLength,
													 PathTraceList &pathTraces,
													 TraceMapMatrix &traceMap,
													 int edgeIndex, int &reverseConsistentTrace);

int IsPathEndResolved(PathTrace &trace, TraceMapMatrix &traceMap);

int AreInternalPathEdgesResolved(PathTraceList &pathTraces,
																 TraceMapMatrix &traceMap,
																 int pathIndex);

int IsTangleEdgeResolved(PathTraceList &pathTraces,
												 TraceMapMatrix &traceMap,
												 int edgeIndex);

int MarkResolvedPaths(PathTraceList &pathTraces, TraceMapMatrix &traceMaps, int notStrict = 0 );

void PrintPathTraceList(PathTraceList &pathTraces);

int CheckPathTraceListBalance(TEdgeList &edges, PathTraceList &pathTraces);

int CheckPathBalance(TEdgeList &edges, PathTraceList &pathTraces, TraceMapMatrix &traceMaps);

int AreTracesForwardConsistent(PathTrace &traceA, int traceAPos, 
															 PathTrace &traceB, int traceBPos);
int AreTracesReverseConsistent(PathTrace &traceA, int traceAPos, 
															 PathTrace &traceB, int traceBPos);
int AreTracesConsistent(PathTrace &traceA, int traceAPos, 
												PathTrace &traceB, int traceBPos);

int FindCompatibleTrace(PathTraceList &pathTraces,
												TraceMapMatrix &traceMap,
												PathTrace &trace, int traceIndex, int tracePos,
												int edgeIndex,
												int &compatibleTrace, int &compatibleTraceIndex);

int ExtendPath(PathTraceList &pathTraces,
							 TraceMapMatrix &traceMap,
							 PathTrace &trace,
							 int traceIndex, int tracePos,
							 PathTrace &newTrace);

int PrintCandidatePaths(PathTraceList &pathTraces,
												TraceMapMatrix &traceMaps, TEdgeList &edges);



// Provide easy common interface to detach path, one with mate pairs,
// one without.

void DetachPath(PathTraceList &pathTraces,
								TraceMapMatrix &traceMap,
								IntervalGraph &graph,
								TVertexList &vertices, TEdgeList &edges,
								PathIntervalList &paths, PathLengthList &pathLengths,
								PathTrace &trace, int traceIndex, int removeInconsistentPaths, int doPrint);

void DetachPath(PathTraceList &pathTraces,
								TraceMapMatrix &traceMap,
								IntervalGraph &graph,
								TVertexList &vertices, TEdgeList &edges,
								PathIntervalList &paths, PathLengthList &pathLengths,
								PathTrace &trace, int traceIndex, 
								ReadMateList &mateList, int removeInconsistentPaths, int doPrint);


void DetachPath(PathTraceList &pathTraces,
								TraceMapMatrix &traceMap,
								IntervalGraph &graph,
								TVertexList &vertices, TEdgeList &edges,
								PathIntervalList &paths, PathLengthList &pathLengths,
								PathTrace &trace, int traceIndex, 
								int isMatePath, ReadMateList &mateList, int removeInconsistentPaths, int doPrint);

int PathContainsEdge(PathInterval *path, int pathLength, int edgeIndex, int &pathIndex);

void PrintTracesAsReads(TVertexList &vertices, TEdgeList &edges, 
												PathTraceList &traces, int endEdgeLength,
												std::string pathSeqName);

void PrintPathTraceResolution(TEdgeList &edges,
															PathTraceList &pathTraces, 
															TraceMapMatrix &traceMaps);

int TraceContainsDuplications(PathTrace &trace );

void BalancedDetachPaths(PathTraceList &pathTraces,
												 TraceMapMatrix &traceMap,
												 IntervalGraph &graph, 
												 TVertexList &vertices, TEdgeList &edges,
												 PathIntervalList &paths, PathLengthList &pathLengths,
												 PathTrace &trace, int traceIndex,
												 PathTrace &balTrace, int balTraceIndex);


void BalancedDetachPaths(PathTraceList &pathTraces,
												 TraceMapMatrix &traceMap,
												 IntervalGraph &graph, 
												 TVertexList &vertices, TEdgeList &edges,
												 PathIntervalList &paths, PathLengthList &pathLengths,
												 PathTrace &trace, int traceIndex,
												 PathTrace &balancedTrace, int balancedTraceIndex,
												 ReadMateList &mateList, 
												 int removeInconsistentPathIntervals,
												 int isMatePath);


/*
class PathBranch {
 public:
	int edge;
	std::vector<int> branches;
	PathBranch(int e) {
		edge = e;
	}
};


typedef std::vector<PathBranch> PathTree;
*/
void AddBranch(int trunk, int branch);
#endif
