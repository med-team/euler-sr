/***************************************************************************
 * Title:          BinSpectToAscii.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "IntegralTuple.h"
#include "utils.h"
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;

int main(int argc, char *argv[]) {
	std::string binListName, asciiListName;
	int tupleSize;
	if (argc < 4) {
		std::cout << "usage: binSpectToAscii binListName tupleSize asciiListName" << std::endl;
		std::cout << "   -fasta        Prints the tuples as a fasta file so they" << std::endl
							<< "                 be assembled." << std::endl;
		std::cout << "   -printCount   Prints the count of each tuple." << std::endl;
		exit(1);
	}
	binListName = argv[1];
	tupleSize   = atoi(argv[2]);
	asciiListName = argv[3];
	int printFasta = 0;
	int printCount = 0;
	int argi = 4;
	while (argi < argc) {
		if(strcmp(argv[argi], "-fasta") == 0) {
			printFasta = 1;
		}
		else if (strcmp(argv[argi], "-printCount") == 0) {
			printCount = 1;
		}
		argi++;
	}
	
	std::ifstream binIn;
	std::ofstream asciiOut;
	IntegralTuple::tupleSize = tupleSize;
	openck(binListName, binIn, std::ios::in | std::ios::binary);
	openck(asciiListName, asciiOut, std::ios::out);
	int nTuples;
	binIn.read((char*) &nTuples, sizeof(int));
	int i;
	IntegralTuple tuple;
	CountedIntegralTuple cTuple;
	std::string tupleStr;
	int seqNumber = 0;
	if (!printFasta)
		asciiOut << nTuples << std::endl;

	for (i = 0; i < nTuples ; i++ ) {
		if (printFasta) {
			asciiOut << ">" << seqNumber << std::endl;
		}
		if (printCount == 0) {
			binIn.read((char*) &tuple, sizeof(IntegralTuple));
			tuple.ToString(tupleStr);
			asciiOut << tupleStr << std::endl;	
		}
		else {
			binIn.read((char*) &cTuple, sizeof(CountedIntegralTuple));
			cTuple.ToString(tupleStr);
		 asciiOut << tupleStr << " " << cTuple.count << std::endl;
		}
		seqNumber++;
	}
	return 0;
}
