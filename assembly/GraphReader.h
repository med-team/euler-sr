/***************************************************************************
 * Title:          GraphReader.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef GRAPH_READER_H_
#define GRAPH_READER_H_
#include "IntervalGraph.h"
#include "utils.h"
#include <iostream>

int GetDimensions(std::string &graphFileName, int &numVertices, int &numEdges);
int ReadGraph(std::string &graphFileName, IntervalGraph &graph);

#endif
