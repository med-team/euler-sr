#!/usr/bin/env perl
BEGIN {
		unshift(@INC, $ENV{"EUSRC"} . "/assembly/");
}

use strict;
use POSIX;
use RunCmd;


my $machtype = $ENV{"MACHTYPE"};
my $srcDir = $ENV{"EUSRC"};
my $exeDir = "$srcDir/assembly/$machtype";

if ($#ARGV < 1) {
		print "usage: Assemble.pl readsFile vertexSize [-ruleFile file] [-minMult m] [-noFixErrors] [-numCpu n]\n";
		
		exit(1);
}
my $readsFile  = shift @ARGV;
my $vertexSize = shift @ARGV;
my $ruleFile = "";
my $minMult = 0;
my $opt;
my $fixErrors = 1;
my $minMultOpt = "";
my $numCpu = 1;
my $numCpuOpt = "";
my $minKmerCountOpt = "";
while ($#ARGV >= 0) {
		$opt   = shift @ARGV;
		if ($opt =~ /^\-/) {
				if ($opt eq "-minMult") {
						$minMult = shift @ARGV;
						$minMultOpt = " -minMult $minMult ";
						$minKmerCountOpt = " -minKmerCount $minMult ";
				}
				elsif ($opt eq "-noFixErrors") {
						print "skipping fix errors\n";
						$fixErrors = 0;
				}
				elsif ($opt eq "-ruleFile") {
						$opt = shift @ARGV;
						$ruleFile = $opt;
				}
				elsif ($opt eq "-numCpu") {
						$numCpu = shift @ARGV;
						$numCpuOpt = " -numProc $numCpu ";
				}
				else {
					print "bad option: $opt\n";
					exit(1);
				}
		}
}


#
# Commands to correct errors.
#

#my $countSpectCmd =
#		"mkdir -p fixed; $exeDir/integralCountSpectrum $readsFile $vertexSize fixed/$readsFile.spect -printCount -binary";
my $countSpectCmd = 
		"mkdir -p fixed; $exeDir/readsToSpectrum $readsFile $vertexSize fixed/$readsFile.spect -printCount $minMultOpt";

my $estErrDistCmd = 	
	"$exeDir/estErrorDist fixed/$readsFile.spect -binary";

my $sortSpectCmd =
		"$exeDir/sortIntegralTupleList fixed/$readsFile.spect -printCount -minMult $minMult";
my $fixErrorsCmd = "";
if ($fixErrors) {
		RunCmd::RunCommand($countSpectCmd);  # 1
		if ($minMult == 0) {
				# have to estimate the minimum multiplcity manually
				my $estErrMinMult = 0;
				print "$estErrDistCmd\n";
				$estErrMinMult = `$estErrDistCmd`;         # 3
				if ($estErrMinMult <= 1) {
						$minMult = 2;
				}
				else {
						$minMult = $estErrMinMult;
						chomp $minMult;
				}
		}
		RunCmd::RunCommand($sortSpectCmd);   # 2
		print "Using minimum multiplicity: $minMult\n";
		$fixErrorsCmd  = "$exeDir/fixErrors $readsFile fixed/$readsFile.spect $vertexSize fixed/$readsFile -minMult $minMult -maxScore 3 -startScore 2 -stepScore 1 -minVotes 2 -edgeLimit 3 -replaceN $numCpuOpt";
		
		
}
else {
		$fixErrorsCmd = "mkdir -p fixed; cd fixed; ln -sf ../$readsFile ./$readsFile";
}
RunCmd::RunCommand("$fixErrorsCmd"); # 4


my $assembleCmd =
		"$srcDir/assembly/assemblesec.pl fixed/$readsFile -vertexSize $vertexSize $minKmerCountOpt";

my $minEdgeLength = $vertexSize * 4;
my $bulgeLength   = $vertexSize * 4;
my $simplifyCmd = 
		"mkdir -p simple; $exeDir/simplifyGraph fixed/$readsFile simple/$readsFile -minEdgeLength $minEdgeLength -removeBulges $bulgeLength -removeLowCoverage 5 3";

RunCmd::RunCommand($assembleCmd);   #5
RunCmd::RunCommand($simplifyCmd);   #6

#
# Customize read transformation.
# 
my $erodePathsOpt = "";
if ($ruleFile eq "") {
  $erodePathsOpt = " -erodeShortPaths $vertexSize ";
}

my $transformCmd = 
		"mkdir -p transformed; $exeDir/transformGraph simple/$readsFile transformed/$readsFile -minPathCount 3 -notStrict $erodePathsOpt";

RunCmd::RunCommand($transformCmd);  #7

if ($ruleFile ne "") {
		my $buildMateListCmd = 
				"$exeDir/buildMateTable fixed/$readsFile $ruleFile $readsFile.mates";
		my $mateTransformCmd = 
				"mkdir -p matetransformed; $exeDir/mateTransformGraph transformed/$readsFile $readsFile.mates $ruleFile matetransformed/$readsFile -notStrict -minMatePairCount 5 -onlyMatePaths -scaffold";
		my $printMateContigsCmd =
				"$exeDir/printContigs matetransformed/$readsFile; cp matetransformed/$readsFile.contig .";
		
		RunCmd::RunCommand($buildMateListCmd); #8 
		RunCmd::RunCommand($mateTransformCmd); #9
		RunCmd::RunCommand($printMateContigsCmd);
}
else {
		my $printTransContigsCmd = 
				"$exeDir/printContigs transformed/$readsFile; cp transformed/$readsFile.contig .";
		RunCmd::RunCommand($printTransContigsCmd);
}

