/***************************************************************************
 * Title:          CountIntegralTuples.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "IntegralTuple.h"
#include "SeqReader.h"
#include "DNASequence.h"
#include "SeqUtils.h"
#include "utils.h"
#include "IntegralTuple.h"
int IntegralTuple::tupleSize = 0;


int  ValidTuple(unsigned char* seq, int pos, int seqLength, int tupleLength);
void AdvanceToValid(unsigned char *seq, int curPos, int seqLength, int tupleLength,
										CountedIntegralTuple &tuple, int &nextPos);

void PrintUsage() {
		std::cout << "usage: countIntegralTuples tupleList tupleSize seqFile [-skipGapped]" << std::endl;
}
int main(int argc, char* argv[]) {

	std::string tupleListName, outTupleListName;
	std::string seqFileName;
	int tupleSize;
	if (argc < 4) {
		PrintUsage();
		exit(1);
	}
	tupleListName = argv[1];
	tupleSize     = atoi(argv[2]);
	seqFileName   = argv[3];
	int argi = 4;
	int skipGapped = 0;

	while (argi < argc) {
		if (strcmp(argv[argi], "-skipGapped") == 0) {
			skipGapped = 1;
		}
		else {
			PrintUsage();
			std::cout << "bad option: " << argv[argi] << std::endl;
		}
		argi++;
	}

	std::ifstream listIn, seqIn;

	openck(tupleListName, listIn, std::ios::in | std::ios::binary);

	int nTuples;
	listIn.read((char*) &nTuples, sizeof(int));
	std::cout << "reading: " << nTuples << " tuples." << std::endl;
	std::cout << "they will require: " << sizeof(CountedIntegralTuple) * nTuples << " bytes." << std::endl;
	long int i;
	CountedIntegralTupleList tupleList;
	tupleList.resize(nTuples);

	for (i = 0; i < nTuples; i++) {
		if (i % 100000 == 0) 
			std::cout << i << std::endl;
		LongTuple tuple;
		listIn.read((char*) &tuple, sizeof(LongTuple));
		tupleList[i].tuple = tuple;
	}
	
	openck(seqFileName, seqIn, std::ios::in);

	DNASequence seq, seqRC;
 	CountedIntegralTuple tuple;
	tuple.tupleSize = tupleSize;
	int nextPos;
	CountedIntegralTupleList::iterator listIt;
	CountedIntegralTuple nextTuple;
	int seqIndex = 0;
	DNASequence *seqs[2];
	seqs[0] = &seq;
	seqs[1] = &seqRC;
	while(SeqReader::GetSeq(seqIn, seq, SeqReader::noConvert)) {
		if (seqIndex % 100000 == 0 && seqIndex) {
			std::cout << seqIndex << std::endl;
		}
		++seqIndex;
		int curPos;

		if (seq.length < tupleSize)
			continue;
		
		
		if (skipGapped) {
			int seqIsGapped = 0;
			for (curPos = 0; curPos < seq.length; curPos++ ){ 
				if (unmasked_nuc_index[seq.seq[curPos]] >= 4) {
					seqIsGapped = 1;
					break;
				}
			}
			if (seqIsGapped) {
				continue;
			}
		}
				
		MakeRC(seq, seqRC);
		int s;
		DNASequence *seqPtr;
		for (s = 0; s < 2; s++ ) {
			seqPtr = seqs[s];
			if (ValidTuple(seqPtr->seq, 0, seqPtr->length, IntegralTuple::tupleSize)) {
				tuple.StringToTuple(&seqPtr->seq[0]);
				curPos = 0;
			}
			else {
				// find the last invalid nuc in the first tupleSize positions.
				int lastInvalidPos = 0;
				int p;
				for (p = 0; p < IntegralTuple::tupleSize; p++) {
					if (unmasked_nuc_index[seqPtr->seq[p]] >= 4)
						lastInvalidPos = p;
				}
				AdvanceToValid(seqPtr->seq, lastInvalidPos, 
											 seqPtr->length, IntegralTuple::tupleSize, tuple, curPos);
			}

			while (curPos + IntegralTuple::tupleSize <= seqPtr->length) {
				// assert that the next position
				// Better find this tuple			
				// 
				listIt = std::lower_bound(tupleList.begin(), tupleList.end(), tuple);
				assert(listIt != tupleList.end());
				(*listIt).IncrementMult();
			
				// the next one to store is from cur pos + 1 to curPos + tupleSize
				// make sure this position is ok to store
				if (curPos + IntegralTuple::tupleSize == seqPtr->length)
					break;

				if (unmasked_nuc_index[seqPtr->seq[curPos + IntegralTuple::tupleSize]] >= 4) {
					AdvanceToValid(seqPtr->seq, curPos + IntegralTuple::tupleSize,
												 seqPtr->length, IntegralTuple::tupleSize, tuple, curPos);
				}
				else {
					
					ForwardNuc(tuple,  unmasked_nuc_index[seqPtr->seq[curPos + IntegralTuple::tupleSize]], nextTuple);
				}
				++curPos;
				tuple.tuple = nextTuple.tuple;
			}
		}
	}
	// There was an extra count for each tuple (due to the constructor), get rid of that.
	for (i = 0; i < nTuples; i++ ) 
		tupleList[i].count--;

	listIn.close();
	std::ofstream listOut;
	openck(tupleListName, listOut,std::ios::out | std::ios::binary);
	listOut.write((char*) &nTuples, sizeof(int));
	listOut.write((char*) &tupleList[0], sizeof(CountedIntegralTuple)*nTuples);

	return 0;
}

int ValidTuple(unsigned char* seq, int pos, int seqLength, int tupleLength) {
	int p;
	if (pos + tupleLength > seqLength)
		return 0;

	for (p = pos; p < pos + tupleLength ; p++) {
		if (numeric_nuc_index[seq[p]] >= 4)
			return 0;
	}
}

void AdvanceToValid(unsigned char *seq, int curPos, int seqLength, int tupleLength,
										CountedIntegralTuple &tuple, int &nextPos) {
	// assert that curPos is invalid.
	int pi;
	int nextInvalidPos = curPos;
	int p = curPos + 1;
	int validFound = 0;
	do {
		p = nextInvalidPos + 1;
		// If there is no room for the next valid pos, bail out.
		if (p + tupleLength > seqLength) {
			nextPos = seqLength + 1;
			return;
		}
		
		for( pi = p; pi < p + tupleLength; pi++ ) {
			if (numeric_nuc_index[seq[pi]] >= 4) {
				nextInvalidPos = pi;
			}
		}
	} while (p < nextInvalidPos and p + tupleLength <= seqLength);
	if (p + tupleLength >= seqLength) {
		nextPos = seqLength + 1;
	}
	else {
		nextPos = p;
	}
}

