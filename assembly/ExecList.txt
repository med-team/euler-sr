SmallVertexDeBruijn.cpp,svdeBruijn, $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lcommon -lassemble -lalign

testdictionary.cpp, testdict, $(LIBCOMON), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lcommon -lassemble -lalign

FixErrorsI.cpp, fixErrors, $(LIBCOMMON) $(LIBASSEMBLE) $(MACHTYPE)/VoteUtils.o $(MACHTYPE)/SAP.o, $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) $(MACHTYPE)/SAP.o -lassemble -lcommon  -lalign 

SplitVertices.cpp, splitVertices, $(LIBCOMMON) $(LIBASSEMBLE),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon  -lalign

PrintPath.cpp,printPaths, $(LIBCOMMON) $(LIBASSEMBLE),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

GVZPrintBGraph.cpp,gvzPrintBGraph, $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lcommon 

SimulateMatePairsFromReads.cpp,simMPR,$(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lcommon 

FindEdgeSpanningReads.cpp,findESRs, $(LIBPARSEBLAST)  $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lparseblast  -lcommon

PrintLocalGraph.cpp,printLocalGraph, $(LIBCOMMON) $(MACHTYPE)/libassemble.$(LIB_EXT),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

JoinSourcesAndSinks.cpp,joinsas, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBPATH) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lpath -lassemble -lcommon -lalign

CompareAssemblies.cpp,cmpAssemblies, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

FindRarePaths.cpp,findRarePaths, $(LIBCOMMON) $(MACHTYPE)/libassemble.$(LIB_EXT),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

CombineTupleLists.cpp,combineTupleLists, $(LIBCOMMON),	$(CPP) -O3 -o $@ $< $(LIBDIR) -lcommon 

ClipReads.cpp,clipReads, $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lcommon 

TruncateHomopolymericRegions.cpp,truncateHPR, $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lcommon 

ReorderIntervals.cpp,reorderIntervals, $(LIBASSEMBLE)  $(LIBCOMMON) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

PrintReadPosAsSpectrum.cpp,printReadPosAsSpectrum,$(LIBASSEMBLE) $(LIBCOMMON)	$(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

SortTupleList.cpp,sortTupleList,$(LIBASSEMBLE) $(LIBCOMMON)	$(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

PrintReadPaths.cpp,printReadPaths,$(LIBASSEMBLE) $(LIBCOMMON)	$(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

PrintReadNumbers.cpp,printReadNumbers,$(LIBASSEMBLE) $(LIBCOMMON) $(LIBALIGN)	,	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

PrintSourceSinks.cpp,printSourceSinks,$(LIBASSEMBLE) $(LIBCOMMON) $(LIBALIGN)	,	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

GraphToBGraph.cpp,graphToBGraph, $(LIBASSEMBLE)	$(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble  -lcommon -lalign

Printervals.cpp,printervals,$(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble  -lcommon -lalign

UpdateReadsAndIntervals.cpp,updateReadsAndIntervals, $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

TestGraph.cpp,testGraph, $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble  -lcommon -lalign

BGraphToEULERGraph.cpp,bGraphToEuler, $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble  -lcommon -lalign

CheckGraph.cpp,checkGraph, $(LIBASSEMBLE) $(LIBCOMMON) $(LIBALIGN),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR) -lassemble  -lcommon -lalign

PrintGraphSummary.cpp,printGraphSummary, ${MACHTYPE}/RepeatSearch.o $(LIBALIGN) $(LIBASSEMBLE) $(LIBCOMMON),		$(CPP) $(CPPOPTS) -o $@ $<  ${MACHTYPE}/RepeatSearch.o $(LIBDIR) -lassemble  -lcommon -lalign

SimplifyGraph.cpp,simplifyGraph, $(LIBALIGN) $(LIBASSEMBLE) $(LIBCOMMON) ,	$(CPP) $(CPPOPTS)  -o $@ $< $(LIBDIR) -lassemble  -lcommon -lalign ${MACHTYPE}/RepeatSearch.o

CalcGraphStatistics.cpp,calcGraphStatistics, $(LIBASSEMBLE) $(LIBCOMMON) $(LIBALIGN)	,	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign

PrintReadIntervals.cpp, printReadIntervals, $(LIBCOMMON) ${LIBASSEMBLE} $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $<  -o $@  $(LIBDIR) -lassemble -lcommon $(LIBS) -lalign

IntegralPrintReadIntervals.cpp, integralPrintReadIntervals, $(LIBCOMMON) ${LIBASSEMBLE} $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $<  -o $@  $(LIBDIR) -lassemble -lcommon $(LIBS) -lalign

EdgesToOverlapList.cpp,edgesToOverlapList, ${LIBASSEMBLE} $(LIBCOMMON) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon $(LIBS) -lalign

IntegralEdgesToOverlapList.cpp, integralEdgesToOverlapList, ${LIBASSEMBLE} $(LIBCOMMON) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon $(LIBS) -lalign

PairReads.cpp,pairReads, ${LIBCOMMON},	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBS)

SortVertexList.cpp,sortVertexList, ${LIBASSEMBLE} $(LIBCOMMON) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR)  -lassemble -lcommon $(LIBS) -lalign

DeBruijn.cpp,debruijn, ${LIBASSEMBLE} ${LIBCOMMON} $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon $(LIBS) -lalign

ReadSimulator.cpp,readsimulator, ${LIBCOMMON},	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lcommon $(LIBS)

QualityTrimmer.cpp,qualityTrimmer, ${LIBCOMMON},	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lcommon $(LIBS)

PrintReadKmerCoverage.cpp,printReadKmerCoverage, $(LIBHASH) $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) -lhash -lassemble  -lcommon -lalign

FixErrorsSAP.cpp,fixErrorsSAP, $(LIBHASH) $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $<  -o $@ $(LIBDIR) $(LIBS) -lhash -lassemble -lcommon -lalign

FixErrorsIVoting.cpp,fixErrorsIVoting, $(LIBHASH) $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $<  -o $@ $(LIBDIR) $(LIBS) -lhash -lassemble  -lcommon -lalign

FixErrorsVoting.cpp,fixErrorsVoting, $(LIBHASH) $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(INCLUDEDIR) $<  -o $@ $(LIBDIR)$(LIBS) -lhash -lassemble  -lcommon  -lalign

CountSpectrum.cpp,countSpectrum, $(LIBHASH) $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBASSEMBLE) $(LIBHASH),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) -lhash -lassemble  -lcommon -lalign

PrintDMST.cpp,printDMST, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) -lassemble  -lcommon -lalign

TrimPrimer.cpp,trimPrimer,$(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) -lassemble  -lcommon -lalign 

TruncateUncertainIntervals.cpp,truncateUncertain,$(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) -lassemble  -lcommon -lalign

PrintContigs.cpp,printContigs,$(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) -lassemble -lcommon -lalign 

FilterLowComplexityReads.cpp,filterLCReads, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) -lassemble  -lcommon  -lalign

ThreadReads.cpp,threadReads, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR)$(LIBS) -lassemble  -lcommon  -lalign

ThreadReads2.cpp,threadReads2,$(MACHTYPE)/ThreadUtils.o $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) ,	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS) $(MACHTYPE)/ThreadUtils.o -lassemble -lcommon -lalign 

EstimateErrorDistribution.cpp,estErrorDist,$(LIBCOMMON) ${LIBASSEMBLE} $(LIBALIGN) ,	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) $(LIBS)  -lcommon -lassemble -lalign

ContigMapper.cpp,contigMapper, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) ,	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR)  -lassemble -lcommon -lalign

CompareTupleLists.cpp,compareTupleLists, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) ,	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon  -lalign

CountSmallSpectrum.cpp,countSmallSpectrum, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

SplitLinkedClones.cpp,splitLinkedClones, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign 

CountSmallSolid.cpp,countSmallSolid, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

ShortSpectrumToLong.cpp,shortSpectrumToLong, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

CheckReads.cpp,checkReads,$(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

FindExactReadMatches.cpp,findExactReadMatches,$(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

PrintAllReadExactMatches.cpp,printAllReadExactMatches,$(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

PrintFullReads.cpp,printFullReads, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

AddReadsToGraph.cpp,addReadsToGraph, $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

GenerateEdgeCoveringReads.cpp,$(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

PathsToReads.cpp,pathsToReads, $(MACHTYPE)/ThreadUtils.o	$(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN), 	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR)  $(MACHTYPE)/ThreadUtils.o -lassemble -lcommon -lalign

LastChanceReads.cpp,lastChanceReads, $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN), 	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

PrintEdgeEnds.cpp,printEdgeEnds, $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN), 	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

PrintShortEdgeReads.cpp,printShortEdgeReads, $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN), 	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign 

IntegralCountSpectrum.cpp,integralCountSpectrum, IntegralTuple.h NumericHashedSpectrum.h $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon  -lalign

SortIntegralTupleList.cpp,sortIntegralTupleList, IntegralTuple.h NumericHashedSpectrum.h $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

BinSpectToAscii.cpp,binSpectToAscii, $(LIBCOMMON), 	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lcommon 

PrintEdgePairs.cpp,printEdgePairs, $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

TransformSimpleRepeats.cpp,transformSimpleRepeats, $(LIBCOMMON)  $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

BuildMateTable.cpp,buildMateTable, $(LIBPATH) $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lpath -lalign

FilterValidMatePaths.cpp,filterValidMatePaths, $(LIBPATH) $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon $(MACHTYPE)/MateLibrary.o -lpath -lalign

CountIntegralTuples.cpp,countIntegralTuples, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN), 	$(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@  $(LIBDIR) -lassemble -lcommon -lalign

PrintPathTree.cpp, printPathTree, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon -lpath -lalign

TransformGraph.cpp, transformGraph, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon -lpath -lalign


FindSplitEdges.cpp, findSplitEdges, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon -lalign

ThreadGenomeThroughGraph.cpp, threadGenome, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon -lalign


MateTransformGraph.cpp, mateTransformGraph, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH) , $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon   -lpath -lalign


CountMatesOnSameEdge.cpp, cmose, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH) , $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon   -lpath -lalign

PrintMatedReadSubset.cpp, printMatedReadSubset, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH) , $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lpath -lassemble -lcommon -lalign

MergeAdjacentEdges.cpp, mergeAdjacentEdges, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) , $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon -lalign

GraphExplorer.cpp, graphExplorer, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lcommon -lpath -lalign

PrintAdjacentMatePairs.cpp, printAdjacentMatePairs, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR)  -lpath -lassemble -lcommon  -lalign

CreateMateScaffold.cpp, createMateScaffold, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lpath -lcommon -lalign


CleanGraphWithMates.cpp, cleanGraphWithMates, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lpath -lcommon  -lalign


TestFindMatePath.cpp, testFindMatePath, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lpath -lcommon  -lalign

PrintDisjointEdges.cpp, printDisjointEdges, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) , $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lcommon  -lalign

PrintMateLengthDistribution.cpp, printMateLengthDistribution,  $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN) $(LIBPATH), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lcommon  -lpath -lalign


PrintEdgeCoverage.cpp, printEdgeCoverage,  $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lcommon  -lalign

SFF2Fasta.cpp, sff2fasta,  $(LIBCOMMON), $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lcommon

MapReads.cpp, mapReads, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),  $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lcommon -lalign


AlignedReadExtender.cpp, alignedReadExtender, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN),  $(CPP) $(CPPOPTS) $(INCLUDEDIR)  $< -o $@ $(LIBDIR) -lassemble -lcommon -lalign

IntegralPrintReadKmerCoverage.cpp, iPrintReadKmerCov, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon -lalign


PrintVariants.cpp, printVariants, $(LIBCOMMON) $(LIBASSEMBLE) $(LIBALIGN), $(CPP) $(CPPOPTS) $(INCLUDEDIR) $< -o $@ $(LIBDIR) -lassemble -lcommon -lalign


CombineIntegralTupleLists.cpp,combineIntegralTupleLists, $(LIBCOMMON),	$(CPP) -O3 -o $@ $< $(LIBDIR) -lassemble -lcommon -lalign

FilterIlluminaReads.cpp, filterIlluminaReads, $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign


FilterFailedEndReads.cpp, filterFailedEndReads, $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign


ReadsToSpectrum.cpp, readsToSpectrum, $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign


ElandToFastq.cpp, elandToFastq, $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign

PrintRedRepeatEdges.cpp, printRedRepeatEdges,  $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign

SummarizeRepeatStructure.cpp, summarizeRepeatStructure,  $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign

SplitPairedFastq.cpp, splitPairedFastq,  $(LIBCOMMON),	$(CPP) $(CPPOPTS) -o $@ $< $(LIBDIR)  -lassemble -lcommon -lalign