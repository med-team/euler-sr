#ifndef THREAD_PATH_H_
#define THREAD_PATH_H_

class ThreadPathInterval {
public:
	int edge;
	int length;
	int pos;
	ThreadPathInterval(int e, int l, int p) : edge(e), length(l), pos(p) {};
	const ThreadPathInterval & operator=(const ThreadPathInterval &rhs) {
		edge = rhs.edge; length = rhs.length; pos = rhs.pos;
		return *this;
	}
};

typedef std::list<ThreadPathInterval> ThreadPath;
typedef std::vector<ThreadPathInterval> ThreadPathVector;

#endif
