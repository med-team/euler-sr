/***************************************************************************
 * Title:          TransformGraph.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "IntervalGraph.h"
#include "PathLib.h"
#include "MateLibrary.h"
#include "IntegralTuple.h"

using namespace std;
int IntegralTuple::tupleSize = 0;

void PrintUsage() {
	cout << "usage: transformGraph graphName graphOutName " << endl;
	cout << "  -notStrict   (FALSE) Resolve any particular instance of a repeat even if not all " << endl
						<< "               instances of that repeat are resolved." << endl << endl
						<< "  -minPathCount c(0) " << endl
						<< "               Remove paths if they occur less than c times." << endl << endl
						<< "  -erodeShortPaths E Erode the ends of paths if they are less than 'E'"<<endl<<endl
						<< "               into an edge." << endl << endl;
	
}

int main(int argc, char* argv[]) {
	string graphFileName, graphOutName;
	int vertexSize;
	if (argc <= 2) {
		PrintUsage();
		exit(0);
	}

	graphFileName = argv[1];
	graphOutName = argv[2];
	int argi= 3;
	int notStrict = 0;
	int minPathCount = 0;
	int erodeShortPaths = 0;
	while (argi < argc) {
		if (strcmp(argv[argi], "-notStrict") == 0) {
			notStrict = 1;
		}
		else if (strcmp(argv[argi], "-minPathCount") == 0) {
			minPathCount = atoi(argv[++argi]);
		}
		else if (strcmp(argv[argi], "-erodeShortPaths") == 0 ){
			erodeShortPaths = atoi(argv[++argi]);
		}
		else {
			PrintUsage();
			exit(0);
		}
		++argi;
	}

	string reportFileName = graphFileName + ".report";
	ofstream report;
	openck(reportFileName, report, ios::out);
	BeginReport(argc, argv, report);


	IntervalGraph graph;
	ReadIntervalGraph(graphFileName, graph, vertexSize);
	cout << "got " << graph.edges.size() << " edges." << endl;

	PathIntervalList &paths       = graph.paths;
	PathLengthList   &pathLengths = graph.pathLengths;
	TEdgeList        &edges       = graph.edges;
	TVertexList      &vertices    = graph.vertices;
	//	ReadReadPaths(pathFileName, paths, pathLengths);


	int changeMade;

	do {
		PathBranch pathTree, removedPathTree;

	CollectPathTree(paths, pathLengths, pathTree);

	cout << "Done collecting paths.  Looking for redundant paths." << endl;
	// Make the list of paths more easy to search.
	list<int> curPath;


	TrimLowCoverageBranches(pathTree, minPathCount);

	// PrintPathTraceList(pathTraces);
	// Find paths that are subpaths of others.


	PathTraceList pathTraces;
	PathTreeToPathList(pathTree, pathTraces);
	PrintPathTraces(pathTraces, graph, cout);

	MarkEnclosedPaths(pathTraces);
	RemoveEnclosedPaths(pathTraces);
	//CheckPathTraceListBalance(graph.edges, pathTraces);
	cout << "removing low count paths." << endl;
	RemoveLowCountPaths(graph, paths, pathLengths, pathTree, minPathCount);
	//	PrintPathTraces(pathTraces, graph, cout);
	cout << "removing low count traces. " << endl;
	RemoveLowCountTraces(pathTraces, minPathCount, removedPathTree);
	//	#ifndef NDEBUG
		cout << "removed paths: " << endl;
		PrintPathTraces(pathTraces, graph, cout);
		cout << "done." << endl;
		//#endif
		//	assert(CheckPathTraceListBalance(edges, pathTraces));
		//		CheckPathBalance(pathTraces, tra

	int p, pi;

  assert(graph.CheckAllPathsBalance(1));

		changeMade = 0;
		TraceMapMatrix traceMaps;
		traceMaps.resize(edges.size());
		StoreTraceMaps(pathTraces, traceMaps);
	
		MarkResolvedPaths(pathTraces, traceMaps, notStrict);
		//		PrintCandidatePaths(pathTraces, traceMaps, graph.edges);

		// Make sure each path has it's own balance, but don't bother 
		// doing this in debug mode.
		assert(CheckPathBalance(edges, pathTraces, traceMaps));

	
		// Now check the balance of the resolved traces.
		int e;
		int resolvedTraceIndex;
		int endEdgeIndex, beginEdgeIndex;
		int traceMapIndex;

		vector<int> balancedEdges;
		balancedEdges.resize(edges.size());
		for (e = 0; e < edges.size(); e++ ) {  
			balancedEdges[e] = edges[e].balancedEdge;
		}
		int numResolvedRepeats = 0;
		for (p = 0; p < pathTraces.size(); p++ ){
			int firstEdge = (*pathTraces[p].edges)[0];
			int traceLength = pathTraces[p].edges->size();
			int lastEdge = (*pathTraces[p].edges)[traceLength -1];
			int balancedEdge = balancedEdges[lastEdge];
			int balancedLastEdge = balancedEdges[firstEdge];

			if (traceMaps[firstEdge].outResolved and traceMaps[lastEdge].inResolved) {

				int resolvedLength = (*pathTraces[p].edges).size();
				int lastEdge = (*pathTraces[p].edges)[resolvedLength-1];
				int resolvedTraceEndBal = balancedEdges[(*pathTraces[p].edges)[resolvedLength-1]];
				int resolvedTraceBeginBal = balancedEdges[(*pathTraces[p].edges)[0]];
				//			assert((*pathTraces[resolvedTraceIndex].edges)[0] == traceMaps[e].startEdge);
				int b = resolvedTraceEndBal;
				assert (traceMaps[b].outResolved);
				//					cout << "detaching bal  starting at: " << b << endl; 
				traceMapIndex       = traceMaps[b].GetFirstStartTraceIndex();
				resolvedTraceIndex  = traceMaps[b].traces[traceMapIndex].trace;

				
				if (resolvedTraceBeginBal != firstEdge and
						resolvedTraceBeginBal != lastEdge and
						resolvedTraceEndBal != firstEdge and 
						resolvedTraceEndBal != lastEdge)  {

				int pe;
				cout << "detaching path " << p << " starting at: " 
						 << firstEdge << " ["  << edges[firstEdge].index << "]" << " ";
				

				for (pe = 0; pe < pathTraces[p].edges->size(); pe++) {
					cout << (*pathTraces[p].edges)[pe] << " ";
				}
				cout << endl;

				cout << "detaching (bal) path " << resolvedTraceIndex << " starting at: " 
						 <<  balancedEdge << " [" << edges[balancedEdge].index << "] " << " ";
				for (pe = 0; pe < pathTraces[resolvedTraceIndex].edges->size(); pe++) {
					cout << (*pathTraces[resolvedTraceIndex].edges)[pe] << " ";
				}
				cout << endl;
				



				assert(pathTraces[p].edges->size() == pathTraces[resolvedTraceIndex].edges->size());

				for (pe = 0; pe < pathTraces[p].edges->size(); pe++) {
					assert(edges[(*pathTraces[p].edges)[pe]].balancedEdge == 
								 (*pathTraces[resolvedTraceIndex].edges)[resolvedLength - 1 - pe]);
				}

					//					assert(graph.CheckAllPathsBalance(1));
					changeMade = 1;
					BalancedDetachPaths(pathTraces, traceMaps, graph, vertices, edges,
															paths, pathLengths,
															pathTraces[p], p,
															pathTraces[resolvedTraceIndex], resolvedTraceIndex);
					
					traceMaps[firstEdge].outResolved = 0;
					traceMaps[lastEdge].inResolved = 0;
					traceMaps[b].outResolved = 0;
					traceMaps[resolvedTraceBeginBal].inResolved = 0;
					++numResolvedRepeats;

					//					assert(graph.CheckAllPathsBalance(1));
				}
			}
		}


		// Somehow some dirt ends up left over. Clean it here.
		for (e = 0; e < edges.size(); e++ ){ 
			if (edges[e].src == -1 and edges[e].dest == -1 and edges[e].intervals->size() > 0)
				edges[e].intervals->clear();
		}
			
		graph.RemoveMarkedIntervalsNoPaths();
		//		graph.RemoveEmptyEdges();
		graph.RemoveUnlinkedEdges();
		int nremoved = graph.RemoveEmptyVertices();
		cout << "resolved: " << numResolvedRepeats << " repeats and removed: " << nremoved << " vertices." << endl;
		graph.CondenseSimplePaths();

		if (!changeMade and erodeShortPaths) {
			graph.ErodeShortEndIntervals(erodeShortPaths);
			changeMade = 1;
			erodeShortPaths = 0;
		}
	}
	while (changeMade);
	//	while (0);

	string bGraphOutName = graphOutName + ".bgraph";
	string intvOutName = graphOutName + ".intv";
	string gvzOutName  = graphOutName + ".gvz";
	string pathOutName = graphOutName + ".path";
	string edgeOutName = graphOutName + ".edge";
	string euGraphOutName = graphOutName = ".graph";
	CheckEdges(graph.vertices, graph.edges);

	graph.CondenseEdgeLists();
	graph.PrintIntervalGraph(bGraphOutName, intvOutName);


  PrintGraph(graph.vertices,graph.edges, euGraphOutName);
  PrintEdges(graph.vertices, graph.edges, edgeOutName);
  GVZPrintBGraph(graph.vertices, graph.edges, gvzOutName);
	WriteReadPaths(pathOutName, graph.paths, graph.pathLengths);

	return 0;

}
