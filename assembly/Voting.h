#ifndef VOTING_H_
#define VOTING_H_

#include "SimpleSequence.h"
#include "SeqReader.h"
#include "SeqUtils.h"
#include "utils.h"
#include "hash/HashUtils.h"
#include "BufferedSeqReader.h"
#include "FixErrorsStats.h"
#include "mctypes.h"
#include "VoteUtils.h"

#include <vector>
#include <iostream>
#include <iomanip>
#include <ext/hash_map>
#include <map>
#include <deque>
#include "IntegralTuple.h"
#include "VoteUtils.h"

int FindSolidSubsequence(DNASequence &seq, CountedIntegralTupleDict &spectrum, 
												 int tupleSize, int minMult, int &seqStart, int &seqEnd);

int TrimSequence(DNASequence &seq, CountedIntegralTupleDict &spectrum,	int tupleSize, 
								 int minMult, int &seqStart, int &seqEnd, int maxTrim, int printMap);

int FixSequence(DNASequence &seq, 
								//								T_Spectrum &spectrum,
								CountedIntegralTupleDict &spectrum,
								IntMatrix &votes, IntVector &alreadySolid,
								int tupleSize, int minMult, int voteThreshold, Stats &stats, int numSearch,
								int &fixPos);

int VoteSequence(DNASequence &seq, CountedIntegralTupleDict &spectrum, int tupleSize,
								 int minMult, int startPos,
								 IntMatrix &votes, IntVector &solid, 
								 int numSearch, int runFast,
								 int checkInsertions, int checkDeletions, int earlyEnd,
								 std::deque<int> &history);

int VoteHistory(IntMatrix &votes, std::deque<int> &history);

int CheckSolid(DNASequence &seq, CountedIntegralTupleDict &spectrum, int tupleSize, int minMult);

int SolidifySequence(DNASequence &read, 
										 CountedIntegralTupleDict &spectrum, int tupleSize, int minMult,
										 IntMatrix &votes, IntVector &solid,
										 int minVotes, Stats &stats, int numSearch, 
										 int DoDeletion, int DoInsertion, int earlyEnd);

#endif
