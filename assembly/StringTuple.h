/***************************************************************************
 * Title:          StringTuple.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/15/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef STRING_TUPLE_H_
#define STRING_TUPLE_H_

#include "DNASequence.h"
#include "Tuple.h"
#include "SeqUtils.h"

class StringTuple {
 public:
	static int tupleSize;
	char *s;
	int GetMult() { 
		return 0;
	}

	StringTuple() {
		s = NULL;
	}

	StringTuple(char* sp) {
		s = sp;
	}

	int Length() {
		return tupleSize;
	}
	char * ToString() {
		return (char*) s;
	}
	
	void assign(char *str) {
		s = str;
	}

	int IncrementMult() {
		return 0;
	}
		
	int ReadLine(std::ifstream &in, int minMult=0) {
		int mult;
		if (tupleSize == -1) {
			std::string temp;
			in >> temp;
			tupleSize = temp.size();
			if (s != NULL) delete[] s;
			s = new char[tupleSize];
			strncpy(s, temp.c_str(), tupleSize);
		}
		else {
			if (s == NULL) 
				s = new char[tupleSize];
			in >> s;
			std::string line;
			// pitch the remainder of the line since it may have a multiplicity
			// or other junk.
			std::getline(in, line); 
			std::stringstream linestrm(line);
			int mult = 0;
			linestrm >> mult;
			if (mult >= minMult) 
				return 1;
			else
				return 0;
		}
	}

	int Valid() {
		int i;
		if (s == NULL)
			return 0;
		for (i = 0; i < tupleSize; i++) {
			if (numeric_nuc_index[s[i]] >= 4)
				return 0;
		}
		return 1;
	}

	int GetHashValue(unsigned int &hashValue) {
		int i;
		if (s == NULL) {
			return -1;
		}
		hashValue = 0;
		// method 1
		for (i = 0; i < tupleSize; i++) {
			hashValue <<=2;
			if (unmasked_nuc_index[s[i]] >= 4)
				return -1;
			else
				hashValue += unmasked_nuc_index[s[i]];
			/*			
			if (i < tupleSize-1)
			hashValue <<=2;*/
		}

		/*
		unsigned int h2 = 0;
		for (i = 0; i < tupleSize; i++) {
			h2 <<=2;
			if (unmasked_nuc_index[s[i]] >= 4)
				return -1;
			else
				h2 += unmasked_nuc_index[s[i]];
		}
		
		if (h2 != hashValue) {
			std::cout << "error: " << h2 << " != " << hashValue << std::endl;
			}*/
		return 1;
	}

	friend std::istream& operator>>(std::istream &in, StringTuple &tuple) {
		std::string templine;
		if (tupleSize == -1) {
			in >> templine;
			tupleSize = templine.size();
			if (tuple.s == NULL) {
				tuple.s = new char[tupleSize];
			}
			strncpy(tuple.s, templine.c_str(), tupleSize);
		}
		else {
			in >> tuple.s;
		}
		std::getline(in, templine);
	}
	
	friend std::ostream &operator<<(std::ostream &out, StringTuple &tuple) {
		if (tupleSize != -1 and tuple.s != NULL) {
			std::string strtup(tuple.s,tupleSize);
			out << strtup << std::endl;
		}
	}
	
	int operator<(StringTuple &rhs) {
		return strncmp(s, rhs.s, tupleSize) < 0;
	}
	
	int operator>(StringTuple &rhs) {
		return strncmp(s, rhs.s, tupleSize) > 0;
	}
	int operator==(StringTuple &rhs) {
		return strncmp(s, rhs.s, tupleSize) == 0;
	}
	int operator!=(StringTuple &rhs) {
		return ! (*this==rhs);
	}
	unsigned char operator[](int pos) {
		assert(s != NULL);
		return s[pos];
	}
	StringTuple& copy(StringTuple &rhs) {
		if (s == NULL) {
			s = new char[tupleSize];
		}
		memcpy(s, rhs.s, tupleSize);
		return *this;
	}

	StringTuple& operator=(StringTuple &rhs) {
		this->copy(rhs);
		return *this;
	}
			
};

//int  GetHashValue(DNASequence &seq, int pos, int length, StringTuple &tuple);


#endif
