# common stuff
SHELL=/bin/sh

CPP = g++

ifneq (, $(filter $(MAKECMDGOALS), debug))
  CPPOPTS := -g # -DNDEBUG #@CPPOPTS Flag to replace
  ifeq (, $(filter %_debug, $(MACHTYPE)))
    MACHTYPE := $(join ${MACHTYPE},_debug)
  endif
else
	CPPOPTS := -O3 -DNDEBUG
endif

ifneq (, $(filter $(MAKECMDGOALS), parallel))
  CPPOPTS += -g -fopenmp
endif



ifneq (, $(filter $(MAKECMDGOALS), debugclean))
  ifeq (, $(filter %_debug, $(MACHTYPE)))
    MACHTYPE := $(join ${MACHTYPE},_debug)
  endif
endif


SRC_DIR=$(EUSRC)
BIN_DIR=$(SRC_DIR)/$(MACHTYPE)/bin

MYSQL_INC = -I${MYSQL_INC_DIR}
MYSQL_LIB = -L${MYSQL_LIB_DIR} -lmysqlclient -lz

#where to look
INCLUDEDIR = -I${SRC_DIR}/lib 

ARCH_OBJS  = $(addprefix $(MACHTYPE)/,$(OBJS))
#ARCH_OBJS  = $(OBJS)


LIBBASE    = $(SRC_DIR)/lib
LIBPATH    = $(SRC_DIR)/lib/$(MACHTYPE)
LIBDIR     = -L$(SRC_DIR)/lib/$(MACHTYPE)


define MAKE_SUBDIR
$(1):
				cd $(1) && ${MAKE} ${MAKECMDGOALS}
endef

define MAKE_DIR_CLEAN
$(join clean, $(1)):
			 cd $(1) && $${MAKE} $${MAKECMDGOALS} clean
CLEAN_TARGETS += $(join clean, $(1))
endef


#
# Architecture specific options.
#
ifeq ($(MACHTYPE), LINUX)
	CPPOPTS = -D_3_3_ 
	ARCH_LIBS   = $(LIBS) 
	LIB_EXT = so
	LIB_CREATE = -shared
endif

ifeq ($(MACHTYPE), i686)
	LIB_EXT = a
	LIB_CREATE = rcs
	LIB_TOOL = ar 
endif

ifeq ($(MACHTYPE), i386)
	ARCH_LIBS = $(LIBS)
	LIB_EXT = so
	LIB_CREATE = -shared
endif

ifeq ($(MACHTYPE), DARWIN)
	ARCH_LIBS = $(LIBS)
	LIB_EXT = dylib
	LIB_CREATE = -dynamic
endif

ifeq ($(MACHTYPE), powerpc)
  CPPOPTS += -D_3_3_ -D_POWERPC_
	ARCH_LIBS = $(LIBS)
	LIB_EXT = a
	LIB_CREATE = -static -o
	LIB_TOOL = libtool
endif

ifeq ($(MACHTYPE), x86_64)
		 LIB_EXT = a
		 LIB_CREATE = rcs
		 LIB_TOOL = ar 
endif

ifeq ($(MACHTYPE), x86_64_debug)
		 LIB_EXT = a
		 LIB_CREATE = rcs
		 LIB_TOOL = ar 
endif


ifeq ($(MACHTYPE), ALPHA)
	CPPOPTS = -O3 -DNDEBUG
endif

MKDEP       = ${EUSRC}/mkdep.pl
MKFILES     = ${EUSRC}/mkfiles.pl

.PHONY: clean


MACHEXECS = $(addprefix $(MACHTYPE)/, $(NOARCH_EXECS))


# define some library locations

LIBCOMMON = $(SRC_DIR)/lib/$(MACHTYPE)/libcommon.${LIB_EXT} 
LIBLAV    = $(SRC_DIR)/lib/$(MACHTYPE)/liblav.${LIB_EXT}
LIBAXT    = $(SRC_DIR)/lib/$(MACHTYPE)/libaxt.${LIB_EXT}
LIBNET    = $(SRC_DIR)/lib/$(MACHTYPE)/libnet.${LIB_EXT}
LIBALIGN =  $(SRC_DIR)/lib/$(MACHTYPE)/libalign.${LIB_EXT} 
LIBBLOCKDB = $(SRC_DIR)/lib/$(MACHTYPE)/libblockdb.${LIB_EXT}
LIBBLOCKS  = $(SRC_DIR)/lib/$(MACHTYPE)/libblocks.${LIB_EXT}
LIBEMBOSS = $(SRC_DIR)/lib/$(MACHTYPE)/libemboss.${LIB_EXT}
LIBHASH    = $(SRC_DIR)/lib/$(MACHTYPE)/libhash.${LIB_EXT}
LIBPARSEBLAST = $(SRC_DIR)/lib/$(MACHTYPE)/libparseblast.${LIB_EXT}
LIBBBBWT = $(SRC_DIR)/lib/$(MACHTYPE)/libbbbwt.${LIB_EXT}
LIBCHAIN = $(SRC_DIR)/lib/$(MACHTYPE)/libchain.${LIB_EXT}
LIBNET = $(SRC_DIR)/lib/$(MACHTYPE)/libnet.${LIB_EXT}
LIBTREE = $(SRC_DIR)/lib/$(MACHTYPE)/libtree.${LIB_EXT}
LIBASSEMBLE = $(SRC_DIR)/lib/$(MACHTYPE)/libassemble.${LIB_EXT}
LIBREGEXP = $(SRC_DIR)/lib/$(MACHTYPE)/libboost_regex.${LIB_EXT}
