#!/usr/bin/env perl

$execList = shift @ARGV;
@cppFiles = glob("*.cpp");

open(EX, $execList) or die "cannot open $execList\n";
open(MT, ">Makefile.targets") or die "cannot open Makefile.targets\n";
open(ME, ">Makefile.execs") or die "cannot open Makefile.execs\n";

while(<EX>) {
		if ($_ =~ /([^,]+)\s*,\s*([^,]+)\s*,\s*([^,]+)\s*,\s*([^,]+)\s*/) {
				push @reqCpps, $1;
				push @targets, $2;
				push @prereqs, $3;
				push @commands, $4;
		}
}

chomp @cppFiles;

for ($i =0 ;$i <= $#reqCpps; $i++ ) {
		$foundTarget = 0;
		for ($j = 0; $j <= $#cppFiles; $j++) {
				if ($cppFiles[$j] eq $reqCpps[$i]) {
						$foundTarget = 1;
						last;
				}
		}
		if ($foundTarget) {
				push @targetIndices, $i;
		}
}

print MT "EXECS = ";
for ($i = 0; $i < $#targetIndices; $i++) { 
		print MT "$targets[$targetIndices[$i]] \\\n ";
}
# print the last if necessary
if ($#targetIndices >= 0) {
		print MT "$targets[$targetIndices[$i]]\n";
}

for ($i = 0; $i <= $#targetIndices; $i++) { 
		$index = $targetIndices[$i];
		print ME "$targets[$index]: arch_dir \${MACHTYPE}/$targets[$index]\n";
}

for ($i = 0; $i <= $#targetIndices; $i++ ) {
		$index = $targetIndices[$i];
		$reqCpps[$index] =~ /(.*).cpp/;
		$reqObj = "$1.o";
		print ME "\${MACHTYPE}/$targets[$index]: \${MACHTYPE}/$reqObj $prereqs[$index]\n\t$commands[$index]\n\n";
}

