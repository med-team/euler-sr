/***************************************************************************
 * Title:          SeqReader.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/09/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef SEQ_READER
#define SEQ_READER

#include <fstream>
#include <vector>
#include "DNASequence.h"

class SeqReader {
private:
  char *_buffer;
  std::ifstream *_in;
  int _buffFull;
  int _buffLen;
  static int GetSeqName(std::ifstream &in, std::string &name);
  static int _maskRepeats;
	static int lineNumber;
  int _lineNumber;
	static int _removeRepeats;
public: 
  static int noConvert;
 SeqReader(std::ifstream *in) :  _in(in), _buffFull(0), _buffLen(0), _lineNumber(0) {};
  
  // Public utility functions
  static int GetSeq(std::string seqName, DNASequence &sequence, int c=0);
  static int GetSeq(std::ifstream &in, DNASequence &sequence, int c=1);
  static int GetSeq(std::ifstream &in, DNASequence *&seqPtr, int c=1);
  // Functions for working on one stream per seqreader
  void SetSource(std::ifstream *source) { _in = source; _lineNumber = 0; _buffLen = 0; }
  //  int GetSeq(char* & seq, char* &name, long & len, int convert=1);
  int GetSeq(DNASequence &s, int convert=1);
  int GetSeq(DNASequence* &s, int convert=1);
	static int GetRead(std::istream &in, DNASequence &sequence);
	int GetRead(DNASequence &sequence);
	
  int GetLine(char* &line);
  static void MaskRepeats() { SeqReader::_maskRepeats = 1;}
	static void UnmaskRepeats() { SeqReader::_maskRepeats = 0;}
	void KeepRepeats() {_removeRepeats = 0;}
};

int ReadSequences(std::string &inFileName, DNASequenceList &sequences);
int ReadDNASequences(std::string &inFileName, DNASequenceList &sequences, int numToRead = -1 );
int ReadNamedSequences(std::string &inFileName, NamedSequenceList &sequences);
int ReadSimpleSequences(std::string &inFileName, std::vector<SimpleSequence> &sequences);
#endif
