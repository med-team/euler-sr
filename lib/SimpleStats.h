/***************************************************************************
 * Title:          SimpleStats.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  12/01/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef _STATS_H_
#define _STATS_H_

#include <vector>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>

void SeedRandom();

int Random(int randMax);
float Uniform();
	  
float NormalRandom(float mean, float var);

template <typename T>
T Max(std::vector<T> &values) {
  int i;
  T max = -99999999;
  for (i = 0; i < values.size(); i++) 
    if (max < values[i]) max = values[i];
  
  return max;
}

template <typename T>
T Min(std::vector<T> &values) {
  int i;
  T min = 99999999;
  for (i = 0; i < values.size(); i++) 
    if (min > values[i]) min = values[i];
  return min;
}

template <typename T>
float Integrate(std::vector<T> bins, float &width) {
  int i;
  int nbins;
  float area;
  nbins = bins.size();
  area = 0;
  for (i = 0; i < nbins; i++)
    area += bins[i] * width;
  return area;
}

template <typename T>
void Bin(std::vector<T> &values, std::vector<int> &bins, int nbins, T min, T max) {
  int i;
  bins.resize(nbins);

  // initialize bins
  for (i = 0; i < nbins; i++) {
    bins[i] = 0;
  }

  // calculate bins
  int binIndex;
  float ratio;
  float lenth;
  float length;
  length = max - min * 1.0;
  for (i = 0; i < values.size(); i++) {
    if (values[i] == max)
      bins[nbins-1]++;
    else {
      ratio = (values[i] - min) / length;
      binIndex = (int) floor(ratio*nbins);
      bins[binIndex]++;
    }
  }
}


template <typename T>
void GetTail(std::vector<T> &values, std::vector<int> &tailIndices, float tailPct) {
  int i;
  float width, area;
  T min, max;
  std::vector<int> bins;
  int nbins = 100;

  min = Min(values);
  max = Max(values);
  Bin(values, bins, 100, min, max);
  width = (max - min *1.0 ) / nbins;
  area = Integrate(bins, width);
  float tailArea = 0.0;
  i = nbins  - 1;
  // how much of the tail makes tailPct volume? 
  while ( i > 0 && (tailArea / area) < tailPct) {
    tailArea += width*bins[i];
    i--;
  }
  float minTailValue;
  
  minTailValue = i*width + min;
  for (i = 0; i < values.size(); i++) 
    if (values[i] > minTailValue) 
      tailIndices.push_back(i);
}


template <typename T>
void GetMeanVar(std::vector<T> &values, T &mean, T &var) {

  int i;
  float total;
  float totalSSE;

  if (values.size() == 0) {
    mean = 0;
    var = -1; // n'est pas possible!
  }

  total = 0;
  totalSSE = 0;
  int length;
  for (i = 0; i < values.size(); i++) {
    total += values[i];
    totalSSE += values[i] * values[i];
  }

  mean = total / values.size();
  var  = (values.size()*1.0)/(values.size()-1)*(totalSSE / values.size() - mean*mean);
}

#endif
