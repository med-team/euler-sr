/***************************************************************************
 * Title:          AlignmentPrinter.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef ALIGNMENT_PRITNER_H_
#define ALIGNMENT_PRITNER_H_
#include <string>
#include "DNASequence.h"

void MakeAlignmentString(DNASequence &refSeq, 
			 DNASequence &qrySeq,
			 int refStartPos, int qryStartPos,
			 int *alignment, int alignmentLength,
			 std::string &refAlignString, 
			 std::string &qryAlignString, 
			 std::string &alignString,
			 int &refAlignStart, int &qryAlignStart);

void PrintAlignmentString(std::string refAlignString, 
			  std::string qryAlignString,
			  std::string alignString,
			  int refPos, int qryPos,
			  std::ostream &out,
			  int lineLength);


void PrintAlignment(DNASequence &refSeq, 
		    DNASequence &qrySeq,
		    int refStartPos, int qryStartPos,
		    int *alignment, int alignmentLength,
		    std::ostream &out, int lineLength=50 );

#endif
