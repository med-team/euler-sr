/***************************************************************************
 * Title:          SimpleSequence.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef SIMPLE_SEQUENCE_H_
#define SIMPLE_SEQUENCE_H_

#include <vector>
#include <string>
#include <iostream>
class SimpleSequence {
 public:
  unsigned char* seq;
  int length;
  SimpleSequence() {
    seq = NULL;
    length = 0;
  }
  SimpleSequence operator=(SimpleSequence s) {
    seq    = s.seq;
    length = s.length;
    return *this;
  }
  std::ostream &PrintSeq(std::ostream &out, std::string title,
												 int lineLength = 50) {
    // simple sequences do not have titles, we have to supply then
    int i;
    out << ">" << title << std::endl;
    for (i = 0; i < length; i++ ) {
      if (i % lineLength == 0 and i > 0 and i < length-1)
				out << std::endl;
      out << seq[i];
    }
    out << std::endl;
		return out;
  }
};
typedef std::vector<SimpleSequence> SimpleSequenceList;
#endif
