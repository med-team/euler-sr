/***************************************************************************
 * Title:          SimpleStats.cpp 
 * Author:         Mark Chaisson
 * Created:        2008
 * Last modified:  12/10/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "SimpleStats.h"


void SeedRandom() {
	time_t t;
  srandom((unsigned) time(&t));
}

int Random(int randMax) {
  return (int) (randMax * ((1.0*random()) / RAND_MAX));
}

float Uniform() {
  return (1.0*random()) / RAND_MAX;
}
	  
float NormalRandom(float mean, float var) {

  float x1, x2, w, y1, y2;
  // Do Box-Muller transformation to generate N(0,1)
  do {
    x1 = 2.0 * Uniform() - 1.0;
    x2 = 2.0 * Uniform() - 1.0;
    w = x1 * x1 + x2 * x2;
  } while ( w >= 1.0 );
  w = sqrt( (-2.0 * log( w ) ) / w );
  y1 = x1 * w;
  return y1*var + mean;
}
