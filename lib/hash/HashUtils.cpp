/***************************************************************************
 * Title:          HashUtils.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "HashUtils.h"

#include "SeqUtils.h"
#include "align/alignutils.h"

int HashValue::size = 13;
int HashValue::indexSize = 11;
int HashValue::convertSize = 5;

int nCreated, nStored, nFilledBuckets;
Chain* HashTable::Find(HashValue &hashValue, Chain* &chainPtr) {
  int bucketPos;
  chainPtr= NULL;
  assert(hashValue.GetIndex() < nBuckets);
  Chain *lastChainPtr;
  lastChainPtr = NULL;
  chainPtr = buckets[hashValue.GetIndex()];
  while (chainPtr != NULL and
	 chainPtr->hashValue != hashValue) {
    lastChainPtr = chainPtr;
    chainPtr = chainPtr->next;
  }
  if (chainPtr != NULL) {
    /*
      std::cout << "found " << chainPtr->hashValue.hashValue << " " 
      << hashValue.hashValue << " " << hashValue.GetIndex() << " " << chainPtr <<  std::endl;
    */
  }
  return lastChainPtr;
}

CountChain* HashTable::StoreCount(HashValue &hashValue) {
  assert(hashValue.GetIndex() < nBuckets);
  CountChain *chainPtr, *lastChainPtr;
  chainPtr = lastChainPtr = NULL;
  ++nStored;
  lastChainPtr = (CountChain*) Find(hashValue, (Chain*&)chainPtr);
  if (chainPtr == NULL ) {
    // This value was not found, append it to the last chain ptr
    // should have found a last chain ptr;
    chainPtr = new CountChain;
    if (lastChainPtr == NULL) 
      buckets[hashValue.GetIndex()] = chainPtr;
    else
      lastChainPtr->next = chainPtr;
    ++nCreated;
  }
  assert(chainPtr != NULL);
  chainPtr->count++;
}


PositionChain* HashTable::StorePos(HashValue &hashValue, HashPos &p) {
  assert(hashValue.GetIndex() < nBuckets);
  PositionChain *chainPtr, *lastChainPtr;
  chainPtr = lastChainPtr = NULL;
  ++nStored;
  Chain *tmpPtr;
  lastChainPtr = (PositionChain*) Find(hashValue, (Chain*&) chainPtr);
  if (chainPtr == NULL ) {
    // This value was not found, append it to the last chain ptr
    // should have found a last chain ptr;
    chainPtr = new PositionChain(hashValue);
    if (lastChainPtr == NULL) 
      buckets[hashValue.GetIndex()] = chainPtr;
    else
      lastChainPtr->next = chainPtr;
    ++nCreated;
  }
  assert(chainPtr != NULL);
  chainPtr->positions.push_back(p);
}

int CountHashGenome(DNASequence &seq, char seqNumber, 
		    HashPattern &hashPattern,
		    int maskRepeats,
		    HashTable &hashTable) {
  assert(hashTable.nBuckets > 0);
  nCreated = 0; nStored = 0;
  int p;
  int lastPos = seq.length - hashPattern.mask.size();
  HashValue hashValue;
  for (p = 0; p < lastPos; p++) {
    if (GetHashValue(seq, p, hashPattern, maskRepeats, hashValue)) {
      hashTable.StoreCount(hashValue);
    }
  }
  return 1;
}

int HashGenome(DNASequence &seq, char seqNumber, 
	       HashPattern &hashPattern,
	       int maskRepeats,
	       HashTable &hashTable) {
  assert(hashTable.nBuckets > 0);
  nCreated = 0; nStored = 0;
  int p;
  int lastPos = seq.length - hashPattern.mask.size();
  HashValue hashValue;
  for (p = 0; p < lastPos; p++) {
    if (GetHashValue(seq, p, hashPattern, maskRepeats, hashValue)) {
      HashPos pos(0,p);
      hashTable.StorePos(hashValue,pos);
    }
  }
  std::cout << "genome statistics: " << nCreated << " " << nStored << std::endl;
}



int GetHashValue(DNASequence &seq, int pos, 
		 HashPattern &hashPattern,
		 int maskRepeats,
		 HashValue &hashValue) {
  int i;
  unsigned int value;
  value = 0;
  // Can't mask this position
  assert(pos + hashPattern.mask.size() <= seq.length);
  assert(hashPattern.maskIndices.size() > 0);
  unsigned char val;
  unsigned char nuc;
  DNASequence frag;
  for (i = 0; i < hashPattern.maskIndices.size()-1; i++) {
    val = seq.seq[pos+hashPattern.maskIndices[i]];
    if ( (!maskRepeats and unmasked_nuc_index[val] > 3) or 
	 (maskRepeats and numeric_nuc_index[(unsigned char)val] < 0)) {
      return 0;
    }
    value += unmasked_nuc_index[val];
    value <<= 2;
  }
  val = seq.seq[pos+hashPattern.maskIndices[i]];
  if ( (!maskRepeats and unmasked_nuc_index[val] > 3 ) or 
       (maskRepeats and numeric_nuc_index[(unsigned char)val] < 0)) {
    return 0;
  }
  value += unmasked_nuc_index[val];
  hashValue = value;
  return 1;
}


void QueryHash(DNASequence &refSeq,
	       HashPattern &hashPattern,
	       HashTable &qryHashTable) {

  int pos;
  HashValue hashValue;
  int lastPos = refSeq.length - hashPattern.size();
  Chain *chain;
  for (pos = 0; pos < lastPos; pos++) {
    if (GetHashValue(refSeq, pos, hashPattern, 1, hashValue)) {
      // Try to find the reference in query
      qryHashTable.Find(hashValue, chain);
    }
  }
}
