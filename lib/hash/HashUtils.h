/***************************************************************************
 * Title:          HashUtils.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef HASHUTILS_H_
#define HASHUTILS_H_

#include "mctypes.h"
#include "DNASequence.h"
#include <vector>


class HashValue {
public:
  static int size;
  static int indexSize;
  static int convertSize;
  unsigned int hashValue;
  int GetIndex() {
    return hashValue >> (convertSize*2);
  }
  HashValue & operator=(HashValue &hv) {
    hashValue = hv.hashValue;
    return *this;
  }
  HashValue & operator=(int val) {
    hashValue = val;
    return *this;
  }
  int operator==(HashValue &val) {
    return val.hashValue == hashValue;
  }
  int operator!=(HashValue &val) {
    return val.hashValue != hashValue;
  }
};

class HashPattern {
public:
  BitVector mask;
  IntVector maskIndices;
  int nChars;
  int length;
  void SetNChars() {
    int i;
    nChars = 0;
    for (i = 0; i < mask.size(); i++) {
      if (mask[i]) ++nChars;
    }
    HashValue::size = 2*nChars;
    HashValue::convertSize = HashValue::size - HashValue::indexSize;
    if (HashValue::convertSize < 0) 
      HashValue::convertSize = 0;
  }
  void SetUngappedSize(int size) {
    int i;
    mask.clear();
    maskIndices.clear();
    for (i = 0; i < size; i++ ) {
      mask.push_back(1);
      maskIndices.push_back(i);
    }
    length = maskIndices.size();
  }
  int size() { return mask.size(); }
};

class HashPos {
public:
  char sequence;
  int pos;
  HashPos() {
    sequence = -1;
    pos = -1;
  }
  HashPos(char s, int p) {
    sequence = s;
    pos = p;
  }
  HashPos &operator=(HashPos &p2) {
    sequence = p2.sequence;
    pos = p2.pos;
    return *this;
  }
};


class Chain {
public:
  HashValue hashValue;
  Chain *next;
  Chain(HashValue &hv) {
    hashValue = hv;
    next = NULL;
  }
  Chain() {
    next = NULL;
    hashValue = -1;
  }
};

class PositionChain : public Chain {
public:
  std::vector<HashPos> positions;
  PositionChain(HashValue &hv) : Chain(hv) {};
  PositionChain() : Chain() {};
};


class CountChain : public Chain {
public:
  int count;
  CountChain() : Chain(), count(0) {}
};
  
class HashTable {
public:
  int nBuckets;
  std::vector<Chain*> buckets;
  HashTable(int nBuckets) {
    nBuckets = CalcNBuckets(nBuckets);
    Init(nBuckets);
  }
  HashTable() {
    nBuckets = CalcNBuckets(HashValue::indexSize);
    Init(nBuckets);
  }
  void Init(int nb) {
    nBuckets = nb;
    buckets.resize(nBuckets);
    int i;
    for (i = 0; i < nBuckets; i++) {
      buckets[i] = NULL;
    }
  }
  static int CalcNBuckets(int nChars) {
    return 1 << (2*nChars);
  }

  PositionChain* StorePos(HashValue &hashValue, HashPos &p);
  CountChain* StoreCount(HashValue &hashValue);
  Chain* Find(HashValue &hashValue, Chain* &chainPtr);
};


int GetHashValue(DNASequence &seq, int pos, 
			  HashPattern &hashPattern,
			  int maskRepeats,
			  HashValue &hashValue);

int CountHashGenome(DNASequence &seq, char seqNumber, 
		    HashPattern &hashPattern,
		    int maskRepeats,
		    HashTable &hashTable);

int HashGenome(DNASequence &seq, char seqNumber, 
	       HashPattern &hashPattern,
	       int maskRepeats,
	       HashTable &hashTable);

void QueryHash(DNASequence &refSeq,
	       HashPattern &hashPattern,
	       HashTable &qryHashTable);
	       


#endif
