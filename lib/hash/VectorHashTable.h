/***************************************************************************
 * Title:          VectorHashTable.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/09/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef VECTOR_HASH_TABLE
#define VECTOR_HASH_TABLE

#include <vector>



template<typename T, typename HashFunct, typename UpdateFunct, int BUFFER_SIZE>
class VectorHashTable {
public:
  int size;
	int count;
  vector<T> **table;
	T buffer[BUFFER_SIZE];
	int bufCur;

	VectorHashTable() { count = 0;};
  VectorHashTable(HashFunct &hashFunct) {
		count = 0;
		Init(hashFunct);
  }
	void Init(HashFunct &hashFunct) {
    size = hashFunct.MaxHashValue();
    table = new vector<T>*[size];
		fill(table, table + size, (vector<T>*) NULL);
		bufCur = 0;
	}

  int CountSize() {
    int i;
    int total = 0;
    for (i = 0; i < size; i++ ) {
			if (table[i] != NULL) {
				total += table[i]->size();
			}
		}
    return total;
  }
	class SortOnHash {
	public:
		HashFunct hash;
		int operator()(const T a, const T b) const {
			return this->hash(a) < this->hash(b);
		}
	};

	class SortOnNHash {
	public:
		int hashLength;
		unsigned long long mask;
		HashFunct & hashFunct;
		int operator()(const unsigned  long a, const unsigned long b) const {
			return (hashFunct(a) < hashFunct(b));
		}
	};
			
	int FlushDirected(HashFunct &hashFunct, UpdateFunct &update) {
		//		cout << "Flushing " << bufCur << endl;
		int i = 0;
		SortOnHash sortOnHash;
		sortOnHash.hash = hashFunct;
		std::sort(&buffer[0], &buffer[bufCur], sortOnHash);
		//		std::sort(&buffer[0], &buffer[bufCur]);
		// Compress the buffer
		if (bufCur == 0)
			return 1;
		/*
		 * Compute some performance diagnostics.
		int numCacheHits = 0;
		int curIndex = 0;
		for (i = 0; i < bufCur; i++) {
			curIndex = i;
			while (i < bufCur and hashFunct(buffer[curIndex]) == hashFunct(buffer[i].tuple)) {
				i++;
			}
			if (i > curIndex) {
				numCacheHits++;
			}
		}		
		cout << "num cache hits: " << numCacheHits << std::endl;

		int numCompressed = 0;
		T cur;
		int curIndex = 0;
		for (i = 0; i < bufCur; i++) {
			cur = buffer[i];
			curIndex = i;
			while (i < bufCur and cur.tuple == buffer[i].tuple) {
				update(cur);
				i++;
			}
			buffer[curIndex] = cur;
			curIndex++;
			if (curIndex > i) numCompressed++;
		}
		bufCur = curIndex;
		cout << "Compressed " << numCompressed << " / " << bufCur << endl;
		*/
		for (i = 0; i < bufCur; i++) {
			T* queryLookupAddr;
			if ((queryLookupAddr = Find(buffer[i])) != NULL) {
				update(*queryLookupAddr);
				//				Store(buffer[i], hashFunct, update);
			}
			else {
				T queryRC, queryRCCopy;
				buffer[i].MakeRC(queryRC);
				queryRCCopy = queryRC;
				if ((queryLookupAddr = Find(queryRC)) != NULL)
					update(*queryLookupAddr);
				else
					StoreNew(buffer[i], hashFunct);
			}
		}
		bufCur = 0;
		return 1;
	}

	int Flush(HashFunct &hashFunct, UpdateFunct &update) {
		//		cout << "Flushing " << bufCur << endl;
		int i = 0;
		std::sort(&buffer[0], &buffer[bufCur]);
		// Compress the buffer
		if (bufCur == 0)
			return 1;
		T cur;
		cur = buffer[0];
		int numCompressed = 0;
		for (i = 1; i < bufCur; i++) {
			if (cur.tuple == buffer[i].tuple) {
				numCompressed++;
				while (i < bufCur and cur.tuple == buffer[i].tuple) i++;
			}
			cur = buffer[i];
		}
		cout << "Compressed " << numCompressed << " / " << bufCur << endl;
		for (i = 0; i < bufCur; i++) {
			Store(buffer[i], hashFunct, update);
		}
		bufCur = 0;
		return 1;
	}

	int BufferedStore(T &value, HashFunct &hashFunct, UpdateFunct &update) {
		buffer[bufCur] = value;
		bufCur++;
		if (bufCur >= BUFFER_SIZE) {
			FlushDirected(hashFunct, update);
		}
	}

	int StoreNew(T &value, HashFunct &hashFunct) {
    int index = hashFunct(value);
    if (index < 0) 
     return 0;
		
		// If this is new, simply return here.
		if (table[index] == NULL) {
			table[index] = new vector<T>;
		}
		
		table[index]->push_back(value);
		++count;
		return 1;
	}
		
		
  int Store(T &value, HashFunct &hashFunct, UpdateFunct &update) {
    int index = hashFunct(value);
    if (index < 0) 
     return 0;
		
		// If this is new, simply return here.
		if (table[index] == NULL) {
			table[index] = new vector<T>;
			table[index]->push_back(value);
			++count;
			return 1;
		}

		// The table must have a vector here
		assert(table[index] != NULL);
		typename vector<T>::iterator it;
		it = std::find(table[index]->begin(), table[index]->end(), value);
		if (it != table[index]->end() and *it == value) {
			update(*it);
			return 0;
		}
		else {
			table[index]->push_back(value);
			//			std::sort(table[index]->begin(), table[index]->end());
			count++;
			return 1;
		}
  }
	
	T* Find(T &value, HashFunct hashFunct=  HashFunct()) {
		
		int index = hashFunct(value);
		assert(index < size);
		//		cout << "size: " << size << " index: " << index << endl;
		if (table[index] == NULL) {
			return NULL;
		}
		else {
			typename vector<T>::iterator it;
			it = std::find(table[index]->begin(), table[index]->end(), value);
			if (it != table[index]->end() and *it == value) {
				value = *it;
				return (T*) &*it;//table[index]->begin() + (it - table[index]->begin()); // strange math needed to go back from iterator to addresss
			}
			else {
				return NULL;
			}
		}
	}
		
	void Summarize() {
		int nChains = 0;
		int chainLength, totalChainLength = 0;
		int i;
		int maxLen = 200;
		int bins[maxLen];
		int b;

		for (b = 0; b < maxLen; b++ ) bins[b] = 0;

		for (i =0 ; i < size; i++) {
			if (table[i] != NULL) {
				nChains++;
				chainLength = table[i]->size();
				if (chainLength  > maxLen-1) 
					bins[maxLen-1]++;
				else 
					bins[chainLength]++;
				totalChainLength += table[i]->size();
			}
		}
		for (b = 0; b < maxLen; b++ )
			std::cout << bins[b] << " ";
		cout << endl;
		std::cout << nChains << " vectors." << std::endl;
		std::cout << totalChainLength / nChains << " average vector length. " << std::endl;
	}

	void Free() {
		int i;
		for (i = 0; i < size; i++) {
			if (table[i] != NULL)
				delete table[i];
		}
	}
};
    

#endif
