/***************************************************************************
 * Title:          Alignment.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef ALIGNMENT_H_
#define ALIGNMENT_H_



class Alignment {
public:
  int refPos;
  int qryPos;
  int refEnd, qryEnd;
  int *locations;
  int alignmentLength;
  Alignment() {
    refPos = qryPos = -1;
    refEnd = -1; qryEnd = -1;
    locations = NULL;
  }
  Alignment &operator=(const Alignment &copy) {
    refPos = copy.refPos;
    qryPos = copy.qryPos;
    refEnd = copy.refEnd;
    refPos = copy.refPos;
    locations = copy.locations;
  }
};
 
class T_Alignment : public Alignment {
public:
  int refEnum;
  int qryEnum;
  int length;
  int *enumerations;
  int level;
  int hashLength;
  T_Alignment *parent;
  T_Alignment *next;
  T_Alignment *child;

  T_Alignment() : Alignment() {
    refPos = 0;
    qryPos = 0;
    refEnum = -1;
    qryEnum = -1;
    length = 0;
    level  = 1;
    hashLength = 0;
    enumerations = NULL;
    locations    = NULL;
    next         = NULL;
    child        = NULL;
    parent       = NULL;
  }
};

void ComputeAlignmentStatistics(int *alignment, int alignLength, char *qry, char* ref,
																int &nMatch, int &nMismatch, int &nGapQry, int &nGapRef);

#endif
