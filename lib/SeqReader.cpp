/***************************************************************************
 * Title:          SeqReader.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  11/09/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <sstream>
#include <iostream>
#include "SeqReader.h"
#include "utils.h"

int  SeqReader::_maskRepeats   = 0;
int  SeqReader::_removeRepeats = 1;
int  SeqReader::noConvert = 0;
int  SeqReader::lineNumber = 0;
#define BUF_LEN 1024

#ifdef _INTEL_
#define CROPE std::crope
#else
#define CROPE __gnu_cxx::crope
#endif


int SeqReader::GetRead(DNASequence &sequence) {
	return GetRead(*_in, sequence);
}

int SeqReader::GetRead(std::istream &in, DNASequence &sequence) {
	std::string title;
	if (!in || !in.good())
		return 0;
	while (in and in.peek() != '>') in.get();

	std::getline(in, title);
	title = title.substr(1);
	std::string line;
	std::string read;
	char ws;
	while(in and in.peek() != '>' and !in.eof()) {
		std::getline(in, line);
		lineNumber++;
		read.append(line);
		while (in and ((ws = in.peek()) == ' ' or
									 ws == '\n' or
									 ws == '\t')) {
			in.get();
		}
		//		in.get(); // discard '\n'
	}
	
	int pos;
	int noGapPos;
	// Look for gaps in the reads.
	/*	noGapPos = 0;
	for (pos = 0; pos < read.size(); pos++) {
		if (read[pos] != ' ' and 
				read[pos] != '\t' and
				read[pos] != '\r' and
				read[pos] != '\n') {
			read[noGapPos] = read[pos];
			noGapPos++;
		}
		}*/
	sequence.Reset(read.size());
	int p;

	memcpy(sequence.seq, read.c_str(), read.size());
	sequence.length = read.size();
	for (p = 0; p < sequence.length; p++) {
		sequence.seq[p] = toupper(sequence.seq[p]);
	}
	sequence.namestr = title;
	while (in and in.peek() != '>' and !in.eof())
		in.get();
	return 1;
}

int SeqReader::GetSeq(DNASequence &sequence, int convert) {
  return GetSeq(*_in, sequence, convert);
}
int SeqReader::GetSeq(std::string seqName, DNASequence &sequence, int convert) {
  std::ifstream in;
  in.open(seqName.c_str(), std::ios::binary | std::ios::in);
  if (!in.good()){
    std::cout << "cold not open sequence file " << seqName << std::endl;
    return 0;
  }
  int retval = GetSeq(in, sequence, convert);
  in.close();
  in.clear();
  return retval;
}

int SeqReader::GetSeq(std::ifstream &in, DNASequence *&seqPtr, int convert) {
  seqPtr = NULL;
  if (!in or !in.good() or in.peek() == EOF) {
    return 0;
  }
  else {
    seqPtr = new DNASequence;
    return GetSeq(in, *seqPtr, convert);
  }
}
    

int SeqReader::GetSeq(std::ifstream &in, DNASequence &sequence, int convert){
  // early exit if this is the end of the file.
  if (!in or !in.good() or in.peek() == EOF) return 0;

  // Read the '>' part of the fasta file.
  if (!GetSeqName(in, sequence.namestr)) {
		return 0;
	}

  // Read in the fasta file. 
  //  std::string *line;
  char *line;
  int tlc = 0;
  int l,l1;
  char c;
  std::streampos strmPos, endOfTitlePos;
  endOfTitlePos = strmPos = in.tellg();
  char p;
  while ( (p = in.peek()) != EOF and p != '>') in.get();
  if (p == EOF) {
    in.clear(); // want to continue reading although
    // fail bit is set on eof
  }

  int length;
  length = in.tellg() - endOfTitlePos;
  
  line = new char[length];
  in.seekg(endOfTitlePos, std::ios::beg);
  in.read(line, length);
    
  int curp, wsp;
  curp = wsp = 0;
  while (wsp < length) {
    if (line[wsp] == '\n' or 
				line[wsp] == ' ' or 
				line[wsp] == '\0') {
			if (line[wsp] == '\n')
				lineNumber++;
      wsp++;
		}
    else {
      line[curp] = line[wsp];
			// a hack for solexa sequences.
			if (line[curp] == '.')
				line[curp] = 'N';
      curp++;
      wsp++;
    }
  }
  sequence.Reset(curp);
  memcpy(&sequence.seq[0], line, curp);
  sequence.length = curp;
  delete[] line;

  int i;
  // It is possible to mask different things. 
  // use repeat masking.
  if (_maskRepeats)
    sequence.StoreAsMasked(REPEAT_MASKED);
	else 
		sequence.RemoveRepeats();
  
  // Store sequence as binary representation rather than character.
  if (convert) {
    for (i = 0; i < sequence.length; i++) {
      sequence.seq[i] = sequence.CharToNumeric(sequence.seq[i]);
    }
    sequence._ascii = 0;
  }
  else
    sequence._ascii = 1;
  return 1;
}

int SeqReader::GetSeq(DNASequence* &sequence, int convert ) {
  sequence = new DNASequence;
  int worked;
  worked =  GetSeq(*_in, *sequence, convert);
  if ( ! worked ) {
    delete sequence;
    sequence = NULL;
  }
  return worked;
}

int SeqReader::GetSeqName(std::ifstream &in, std::string &name) {
  char *s;
  std::stringbuf strbuf;
	if (in.peek() != '>') {
		std::cout << "ERROR, titles in FASTA format must begin with a \">\"."<<std::endl;
		std::cout << "at line: " << lineNumber << std::endl;
		std::string line;
		std::getline(in, line);
		std::cout << "The offending line is: " << std::endl;
		std::cout << line << std::endl;
		exit(1);
	}
  if (in.peek() == '>') in.get();
	std::getline(in, name);
	lineNumber++;
  return in.peek() != EOF;
}

int ReadDNASequences(std::string &inFileName, DNASequenceList &sequences, int numSeq ) {
  DNASequence seq;
  std::ifstream seqIn;
  openck(inFileName, seqIn, std::ios::in);
	int count = 0;
  while(SeqReader::GetSeq(seqIn, seq, SeqReader::noConvert) and 
				(numSeq == -1 or count < numSeq) ) {
    sequences.push_back(seq);
		count++;
  }
  return sequences.size();
}

int ReadNamedSequences(std::string &inFileName, NamedSequenceList &sequences) {
  std::ifstream seqIn;
  openck(inFileName, seqIn, std::ios::in);
  DNASequence seq;
	NamedSequence namedSeq;
  while(SeqReader::GetSeq(seqIn, seq, SeqReader::noConvert)) {
		namedSeq.seq = new unsigned char[seq.length];
		memcpy(namedSeq.seq, seq.seq, seq.length);
		namedSeq.length= seq.length;
		namedSeq.namestr = seq.namestr;
		sequences.push_back(namedSeq);
	}
}

int ReadSimpleSequences(std::string &inFileName, std::vector<SimpleSequence> &sequences) {
  DNASequence seq;
  SimpleSequence simpleSeq;
  std::ifstream seqIn;
  openck(inFileName, seqIn, std::ios::in);
  while(SeqReader::GetSeq(seqIn, seq, SeqReader::noConvert)) {
    simpleSeq.seq = new unsigned char[seq.length];
    memcpy(simpleSeq.seq, seq.seq, seq.length);
		int s;
		for (s = 0; s < seq.length; s++) {
			simpleSeq.seq[s] = toupper(simpleSeq.seq[s]);
		}
    simpleSeq.length = seq.length;
    sequences.push_back(simpleSeq);
  }
  return sequences.size();
}
