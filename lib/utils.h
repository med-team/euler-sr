/***************************************************************************
 * Title:          utils.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  03/11/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef _UTILS_H_
#define _UTILS_H_
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#define CheckedOpen

template <typename t>
int openck(std::string fileName, t &file, 
	    std::ios_base::openmode flags= (std::ios::in),
            int severe=1) {

  file.open(fileName.c_str(), flags);
  if (!file.good()) {
    std::cout << "could not open " << fileName << std::endl;
    if (severe) 
      exit(0);
    else
     return 0;
  }
  return 1;
}

template <typename T>
int Convert(std::string &inString, T &value) {
   std::stringstream instream;
   instream.str(inString);
   instream >> value;
   return (!instream.fail());
}

int PeekInput(std::ifstream &in, std::string str); 

void ParseFileName(std::string fileName, 
		   std::string &refName, 
		   std::string &qryName,
		   std::string &seqName);

int OnFwgridNode();

class Pos {
 public:
  int rBegin, rEnd, qBegin, qEnd;
  float eValue;
};

int Tokenize(std::string &line, std::vector<std::string> &values);
int RunBlast(std::string &queryName, std::string &sbjctName, Pos &position);   
void MakeTempName(std::string &fileName, std::string ext = "");
std::string CommandLineToString(int argc, char* argv[]);
void BeginReport(int argc, char *argv[], std::ofstream &out);
void EndReport(std::ofstream &out);

template<typename T>
std::string NumToStr(T number) {
	std::stringstream numStrStrm;
	numStrStrm << number;
	return numStrStrm.str();
}

int IsOption(char* arg);
int IsOption(char* arg, char* option);

void PrintStatus(int pos, int spacing=1000);
	
void Pause();
void WaitLock(std::string &lockFileName, int &fileDes);
void ReleaseLock(int fileDes);

#endif
