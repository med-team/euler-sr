/***************************************************************************
 * Title:          StatUtils.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef _STAT_UTILS_H_
#define _STAT_UTILS_H_

template<typename T>
hist(T* data, int count, int *&bins, int nbins) {
  bins = new ine[nbins];
  int i;
  for (i = 0; i < nbins; i++) {
    bins[i] = 0;
  }
  T min;
  T max;
  min = 99999999;
  max = -min;
  // Find shift of data
  for (i = 0; i < count; i++) {
    if (min > data[i])
      min = data[i];
    if (max < data[i])
      max = data[i];
  }
  
  // Bin data
  int index;
  for (i = 0; i < count; i++) {
    index = std::min(std::floor(((data[i] - min) / (1.0*max))* nbins), nbins-1);
    bins[index]++;
  }
}
    
    
	       
  
  
  

}

#endif
