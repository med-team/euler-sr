/***************************************************************************
 * Title:          GraphAlgo.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/25/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef GRAPH_ALGO_H_
#define GRAPH_ALGO_H_

#include <vector>
#include <iostream>
#include <set>
#include <map>
#include "rbtree.h"
#include "utils.h"

class GraphVertex {
public:
  enum Mark_e { Marked, NotMarked};
  unsigned char marked: 1;
  void Unmark() { marked = NotMarked;}
  virtual int FirstOut() = 0;
  virtual int FirstIn() = 0;
  virtual int EndOut() = 0;
  virtual int EndIn() = 0;
  virtual int NextOut(int curEdge) = 0;
  virtual int NextIn(int curEdge) = 0;
  virtual int InDegree() = 0;
  virtual int OutDegree() = 0;
};

class GraphEdge {
public:
  enum Mark_e { Marked, NotMarked};
  unsigned char marked: 1;
  unsigned char mst: 1;
  void Unmark() { marked = NotMarked;}
};


template<typename V, typename E, typename Funct>
void TraverseRandomAccess(std::vector<V> &vertices, std::vector<E> &edges,
			  Funct &funct) {
  int v;
  for (v = 0; v < vertices.size(); v++) {
    funct(v);
  }
}

template<typename V, typename E, typename Funct> 
void TraverseDFS(std::vector<V> &vertices, std::vector<E> &edges, int vertexIndex, Funct &funct) {
  int edgeIndex;
  int e;
  if (vertices[vertexIndex].marked == GraphVertex::Marked) 
    return;
  
  // the vertex is new to the dfs, run 'f' on it.
  funct(vertexIndex);

  // Don't want to visit this vetex again, so mark it
  vertices[vertexIndex].marked = GraphVertex::Marked;

  // search back
  int srcIndex;
  for (e = vertices[vertexIndex].FirstIn(); 
       e < vertices[vertexIndex].EndIn(); 
       e = vertices[vertexIndex].NextIn(e)) {
    edgeIndex = vertices[vertexIndex].in[e];
    srcIndex = edges[edgeIndex].src;
    if (vertices[srcIndex].marked == GraphVertex::NotMarked) {
      TraverseDFS(vertices, edges, srcIndex, funct);
    }
  }

  // search forward
  int destIndex;
  for (e = vertices[vertexIndex].FirstOut(); 
       e < vertices[vertexIndex].EndOut(); 
       e = vertices[vertexIndex].NextOut(e)) {
    edgeIndex = vertices[vertexIndex].out[e];
    destIndex = edges[edgeIndex].dest;
    if ( vertices[destIndex].marked == GraphVertex::NotMarked ) {
      TraverseDFS(vertices, edges, destIndex, funct);
    }
  }
}

template<typename V, typename E, typename Funct> 
void TraverseGraphDFS(std::vector<V> &vertices, std::vector<E> &edges, Funct &funct) {
  int v;
  for (v = 0; v < vertices.size(); v++) {
    if ( vertices[v].marked == GraphVertex::NotMarked ) {
      TraverseDFS(vertices, edges, v, funct);
    }
  }
}

template<typename T>
void Unmark(std::vector<T> &elements) {
  int i;
  for (i = 0; i < elements.size(); i++) {
    elements[i].Unmark();
  }
}


template<typename T, typename S>
class IndexedScorePriorityQueue {
public:
  S* scores;
  std::set<T> indices;
};

template<typename T> 
class SearchablePriorityQueue {
public:
  T min;
  std::set<T> heap;
  int size() {
    return heap.size();
  }
  void push(const T &value) {
    heap.insert(value);
  }
  T pop() {
    // adjust the bounds if T is the min value
    typename std::set<T>::iterator last, nextToLast;

    last = heap.begin();
    T value = *last;
    heap.erase(last);
    return value;
  }

  int find(const T &key) {
    typename std::set<T>::iterator heapIt;
    for (heapIt = heap.begin(); heapIt != heap.end(); ++heapIt) {
      if (*heapIt == key)
				return 1;
    }
    return 0;
  }
  T&operator[](const T &key) {
    int i;
    typename std::set<T>::iterator it, end;
    end = heap.end();
    for (it = heap.begin(); it != heap.end(); ++it) {
      if (*it == key) {
				return (T&) *it;
      }
    }
    assert(it != heap.end());
  }
};

template<typename V>
class ScoredVertex {
public:
  // This class adds a score component to a vertex
  // It's convenient when only a small component of a graph will be
  // scored, so that the entire graph does not need to have an extra field 
  // added.
  
  V vertex; // a way of indexing a vertex
  int score; // score of the vertex
  ScoredVertex() {
    vertex = -1;
    score = 0;
  }

  ScoredVertex(const ScoredVertex<V> &v) {
    *this = v;
  }
  ScoredVertex(int v, int s) {
    vertex = v;
    score  = s;
  }
  ScoredVertex<V> &operator=(const ScoredVertex<V> &v) {
    vertex = v.vertex;
    score  = v.score;
    return *this;
  };
  int operator>(const ScoredVertex<V> &v) const {
    return score > v.score;
  }
  int operator<(const ScoredVertex<V> &v) const {
    return score < v.score;
  }
  int operator==(const ScoredVertex<V> &v) const {
    return vertex == v.vertex;
  }
};
template<typename T>
std::ostream & operator<<(std::ostream& out, ScoredVertex<T> &sv) {
  out << sv.vertex << " " << sv.score << " ";
  return out;
}

/*
  template<typename T>
  class CompareScoredVertex {
  public:
  int operator<(const ScoredVertex<T> &A, const ScoredVertex<T> &B) {
  return A.score < B.score;
  }
  };
*/

typedef RBTreeNode<ScoredVertex<int> > HeapScoredVertex;


template<typename V, typename E>
void BellmanFordMaximumPath(std::vector<V> &vertices,
														std::vector<E> &edges,
														int source, std::vector<int> &predecessor) {

  std::cout << "THIS IS NOT FINISHED NOR TESTED" << std::endl;
  assert(0);
  int v, ei, e;

  if (edges.size() < 1) return;
  // Preprocessing step, find the maximum cost edge in the graph
  int maxCost = edges[0].Cost();
  for (e = 0; e < edges.size(); e++ ) {
    if (maxCost < edges[e].Cost())
      maxCost = edges[e].Cost();
  }
  
  int infinity = 999999999;
  std::vector<int> distances;
  distances.resize(vertices.size());
  predecessor.resize(vertices.size());
  std::fill(distances.begin(), distances.end(), infinity);
  std::fill(predecessor.begin(), predecessor.end(), -1);

  distances[source] = 0;
  int dest, src;
  for (v = 0; v < vertices.size(); v++ ) {
    for (e = 0; e < edges.size(); e++ ) {
      dest = edges[e].dest;
      src  = edges[e].src;

      if (distances[dest] > distances[src] + (maxCost - edges[e].Cost())) {
				distances[dest] = distances[src] + (maxCost - edges[e].Cost());
				predecessor[dest] = ei;
      }
    }
  }
  
  // Check for negative weight cycles
  for (e = 0; e < edges.size(); e++ ) {
    src = edges[e].src;
    dest = edges[e].dest;
    if (distances[dest] > distances[src] + (maxCost - edges[e].Cost())) {
      std::cout << "Error, negative weight cycle on edge " << edges[e] << std::endl;
      std::cout << distances[dest] << " " << distances[src] << " " << maxCost - edges[e].Cost() << std::endl;
      assert(0);
    }
  }
}


template<typename V, typename E>
void SingleSourceSubsetMaximumPath(std::vector<V> &vertices, std::vector<E> &edges, 
																	 int sourceVertex, int destVertex,
																	 std::set<int> vertexSubset, std::map<int,int> &shortestPathEdges) {
  int maxCost = 0;
  int v, e;
  std::set<int>::iterator vertexIt;
  int outEdge, outEdgeIndex;
  
  // Find the maximum cost vertex, but over only a subset of vertices
  for (vertexIt = vertexSubset.begin();
       vertexIt != vertexSubset.end();
       ++vertexIt) {
    if (*vertexIt != destVertex) {
      for (outEdgeIndex = vertices[*vertexIt].FirstOut();
					 outEdgeIndex != vertices[*vertexIt].EndOut();
					 outEdgeIndex = vertices[*vertexIt].NextOut(outEdgeIndex)) {
				outEdge = vertices[*vertexIt].out[outEdgeIndex];
				
				if (edges[outEdge].Cost() >maxCost) 
					maxCost = edges[outEdge].Cost();
      }
    }
  }
	
  // For now, the code is more-or-less copied from the 
  // singtle source max path. It just searches throug the vertex subset alone

  // prepare references to scores
  std::map<int, HeapScoredVertex*> references;

  // prepare the priority queue
  RBTree<ScoredVertex<int> > queue;
  HeapScoredVertex *minVertex;
  
  references[sourceVertex] = queue.Insert(ScoredVertex<int>(sourceVertex, 0));
  
  // Find One-to-all shortest path
  
  int dest;
  int first = 1;
  ScoredVertex<int> closestVertex;
  int vertex, closestScore;
  int inEdgeIndex;
  while (queue.size() > 0) {
    // fetch the highest 'closest' vertex

    if (queue.Pop(closestVertex) == 0) {
      std::cout << "INTERNAL ERROR: the size of the queue was " << queue.size() 
								<< " but nothing could be popped" << std::endl;
      assert(0);
    }
    if (closestVertex.vertex  == destVertex)
      return;

    // Relax vertices out of 'vertex'
    for (outEdgeIndex = vertices[closestVertex.vertex].FirstOut();
				 outEdgeIndex != vertices[closestVertex.vertex].EndOut();
				 outEdgeIndex = vertices[closestVertex.vertex].NextOut(outEdgeIndex)) {
      outEdge = vertices[closestVertex.vertex].out[outEdgeIndex];
      dest = edges[outEdge].dest;
      // If this vertex is already in the priority queue, 
      // We need to store a new score if the dest hasn't had a score
      // computed for it, or the score is worse than the new score
      if (references.find(dest) == references.end() or
					(references[dest]->data.score > closestVertex.score + (maxCost - edges[outEdge].Cost()))) {
				// Need to update the score on the dest vertex
				// First remove the old score from the priority queue

				if (references.find(dest) != references.end()) {
					queue.Delete(references[dest]);
				}

				// Update the optimal score in the queue
				int destScore;
				destScore        = closestVertex.score + (maxCost - edges[outEdge].Cost());
				int prevSize     = queue.size();
				references[dest] = queue.Insert(ScoredVertex<int>(dest, destScore));

				// Store the path information for the optimal path
				inEdgeIndex = vertices[dest].LookupInIndex(outEdge);
				assert(inEdgeIndex >= 0);
				shortestPathEdges[dest] = inEdgeIndex;
      }
    }
  }
}



template<typename V, typename E>
void SingleSourceMaximumPath(std::vector<V> &vertices, std::vector<E> &edges, 
														 int source, std::vector<int> &shortestPathEdges, int destVertex=-1) {

  int v, e;

  if (edges.size() < 1) return;
  // Preprocessing step, find the maximum cost edge in the graph
  int maxCost = edges[0].Cost();
  for (e = 0; e < edges.size(); e++ ) {
    if (maxCost < edges[e].Cost())
      maxCost = edges[e].Cost();
  }

  // Set all edges to not traversed
  shortestPathEdges.resize(vertices.size());
  for (v =0 ; v < vertices.size(); v++ ) shortestPathEdges[v] = -1;
  
  // prepare references to scores
  std::vector<HeapScoredVertex*> references;

#ifdef _DEBUG_
  std::vector<char> visited;
  visited.resize(vertices.size());
  std::fill(visited.begin(), visited.end(), 0);
#endif

  references.resize(vertices.size());
  for (v = 0; v < vertices.size(); v++) references[v] = NULL;
  
  // prepare the priority queue
  RBTree<ScoredVertex<int> > queue;
  HeapScoredVertex *minVertex;
  
  references[source] = queue.Insert(ScoredVertex<int>(source, 0));
  
  // Find One-to-all shortest path
  
  int outEdge, outEdgeIndex, dest;
  int first = 1;
  ScoredVertex<int> closestVertex;
  int vertex, closestScore;
  int inEdgeIndex;
  while (queue.size() > 0) {
    // fetch the highest 'closest' vertex
    /*
      std::cout << "cur queue " << queue.size() << std::endl;
      std::cout << queue << std::endl;
    */
    if (queue.Pop(closestVertex) == 0) {
      std::cout << "INTERNAL ERROR: the size of the queue was " << queue.size() 
								<< " but nothing could be popped" << std::endl;
      assert(0);
    }

		//      std::cout << "popped vertex: " << closestVertex.vertex << std::endl;
		//      std::cout << queue << std::endl;

#ifdef _DEBUG_
    if (visited[closestVertex.vertex] == 1) {
      std::cout << "ERROR, re-visiting " << closestVertex.vertex << std::endl;
      assert(0);
    }
    visited[closestVertex.vertex] = 1;
#endif
    // Relax vertices out of 'vertex'
    for (outEdgeIndex = vertices[closestVertex.vertex].FirstOut();
				 outEdgeIndex != vertices[closestVertex.vertex].EndOut();
				 outEdgeIndex = vertices[closestVertex.vertex].NextOut(outEdgeIndex)) {
      outEdge = vertices[closestVertex.vertex].out[outEdgeIndex];
      dest = edges[outEdge].dest;
      // If this vertex is already in the priority queue, 
      // We need to store a new score if the dest hasn't had a score
      // computed for it, or the score is worse than the new score
      if (references[dest] == NULL or 
					(references[dest] != NULL and
					 references[dest]->data.score > closestVertex.score + (maxCost - edges[outEdge].Cost()))) {
				// Need to update the score on the dest vertex
				// First remove the old score from the priority queue
	
				if (references[dest] != NULL) {
					//	  std::cout << "Deleting " << dest << " from queue " << std::endl;
					queue.Delete(references[dest]);
				}

				// Update the optimal score in the queue
				int destScore;
				destScore = closestVertex.score + (maxCost - edges[outEdge].Cost());
				//	std::cout << "Added  " << dest << " to the queue " << std::endl;
				references[dest] = queue.Insert(ScoredVertex<int>(dest, destScore));
				// Store the path information for the optimal path
				inEdgeIndex = vertices[dest].LookupInIndex(outEdge);
				assert(inEdgeIndex >= 0);
				shortestPathEdges[dest] = inEdgeIndex;
      }
    }
  }
}

template<typename V, typename E, typename List>
void TraceOptimalPath(std::vector<V> &vertices,
											std::vector<E> &edges,
											int sourceVertex, int destVertex,
											List pathList,
											std::vector<int> &edgePath) {

  assert(destVertex >= 0);
  int curVertex = destVertex;
  int edge;
  //  assert(std::find(pathList.begin(), pathList.end(), curVertex) != pathList.end());
  while(curVertex != sourceVertex) {
    edge = vertices[curVertex].in[pathList[curVertex]];
    //    std::cout << "edge: " << edge << " " << startToAll[curVertex] << std::endl;
    edgePath.insert(edgePath.begin(), edge);
    curVertex = edges[edge].src;
    
    // Some sanity checks. This will bomb if we are in an ininite loop
    assert(edgePath.size() < edges.size());
    //    assert(std::find(pathList.begin(), pathList.end(), curVertex) != pathList.end());
  }
}


template<typename V, typename E>
int StoreFinishingTimes(std::vector<V> &vertices, std::vector<E> &edges,
												std::vector<int> &finishingTimes, std::vector<int> &finishingOrder) {
  finishingTimes.clear();
  finishingTimes.resize(vertices.size());
  finishingOrder.clear();
  finishingOrder.resize(vertices.size());
  Pause();
  Unmark(vertices);
  int v;
  int time = 0;
  int lastVertex = -1;
  for (v = 0; v < vertices.size(); v++ ) {
    if (vertices[v].marked == GraphVertex::NotMarked) {
      lastVertex = StoreFinishingTimes(vertices, edges, 
																			 finishingTimes, finishingOrder,
																			 v, time, lastVertex);
    }
  }
  return lastVertex;
}

template<typename V, typename E>
int StoreFinishingTimes(std::vector<V> &vertices, std::vector<E> &edges,
												std::vector<int> &f, std::vector<int> &finishingOrder, 
												int curVertex, int &curTime, int &previous) {
  // OK, this is now written to be particular to the method for fining
  // strongly connected components, but it should be ok.
  
  int outEdge, outEdgeIndex;
  int lastVertex = -1;
  int destVertex = -1;
  assert(vertices[curVertex].marked != GraphVertex::Marked);
  vertices[curVertex].marked = GraphVertex::Marked;
  for (outEdgeIndex = vertices[curVertex].FirstOut();
       outEdgeIndex < vertices[curVertex].EndOut();
       outEdgeIndex = vertices[curVertex].NextOut(outEdgeIndex)) {
    outEdge    = vertices[curVertex].out[outEdgeIndex];
    destVertex = edges[outEdge].dest;
    // Only traverse edges that are part of the higest scoring tree
    // These are marked.
    // Only visit vertices that are not yet visited
    if (edges[outEdge].marked == GraphEdge::Marked and
				vertices[destVertex].marked != GraphVertex::Marked) {
      StoreFinishingTimes(vertices, edges, f, finishingOrder, destVertex, curTime, previous);
    }
  }
  ++curTime;
  finishingOrder[curVertex] = previous;
  vertices[curVertex].length = curTime;
  f[curVertex]    = curTime;
  previous        = curVertex;
  return curVertex;
}


template<typename V, typename E>
int DFSBackwards(std::vector<V> &vertices, std::vector<E> &edges,
								 int curVertex, std::vector<int> &traced, std::vector<int> &finishingTimes) {
  
  int inEdge, inEdgeIndex;
  int srcVertex;
  int last = -1;
  assert(vertices[curVertex].marked != GraphVertex::Marked);
  if (vertices[curVertex].marked != GraphVertex::Marked) {
    traced.push_back(curVertex);
    assert(curVertex >= 0 and curVertex < vertices.size());
    vertices[curVertex].marked = GraphVertex::Marked;
    /*
			std::cout << "dfsb " << curVertex << " " << finishingTimes[curVertex] << std::endl;
    */
    for (inEdgeIndex = vertices[curVertex].FirstIn();
				 inEdgeIndex < vertices[curVertex].EndIn();
				 inEdgeIndex = vertices[curVertex].NextIn(inEdgeIndex)) {
      inEdge = vertices[curVertex].in[inEdgeIndex];
      srcVertex = edges[inEdge].src;
      assert(srcVertex >= 0 and srcVertex < vertices.size());
      /*
				std::cout << inEdge << " " << (int) edges[inEdge].marked <<  " " 
				<< (int) vertices[srcVertex].marked << " " << (int) GraphVertex::Marked <<  std::endl;
      */
      if (vertices[srcVertex].marked == GraphVertex::NotMarked and
					edges[inEdge].marked  == GraphEdge::Marked) {
				last = DFSBackwards(vertices, edges, srcVertex, traced, finishingTimes);
      }
    }
  }
  if (last == -1)
    return curVertex;
  else
    return last;
}

template<typename V, typename E>
int FindStronglyConnectedComponents(std::vector<V> &vertices, std::vector<E> &edges,
																		std::vector<std::vector<int > > &sccs) {
  std::vector<int> finishingTimes;
  std::vector<int> finishingOrder;

  int curVertex, lastVertex;
  
  lastVertex = StoreFinishingTimes(vertices, edges,
																	 finishingTimes, finishingOrder);

  Unmark(vertices);
  int nProcessed= 0;
  std::vector<int> scc;
  
  curVertex = lastVertex;
  int v;
  /*
		std::cout << "graph traversal result:" << std::endl;
		for (v = 0 ; v < vertices.size(); v++ ) {
		std::cout << v << " " << finishingTimes[v] << " " << finishingOrder[v] << std::endl;
		}
  */
  int prevOrder = finishingTimes[curVertex] + 1;
  while (finishingOrder[curVertex] != -1) {
    /*
      std::cout << "backwards dfs for " << curVertex << " f: " << finishingTimes[curVertex] 
      << " next: " << finishingOrder[curVertex] << std::endl;
    */
    assert(prevOrder > finishingTimes[curVertex]);
    prevOrder = finishingTimes[curVertex];
    // All sorts of cases to do sanity checks onl
    // First, we should always be starting the dfs on a vertex that has not 
    // been touched
    assert(curVertex < vertices.size() and curVertex >= 0);
    assert(vertices[curVertex].marked == GraphVertex::NotMarked);


    // Next, the number of vertices processed can't be more than the number 
    // of vertices in the graph.  If it is, this is probably in some infinite loop.
    assert(nProcessed < vertices.size());

    scc.clear();
    lastVertex = DFSBackwards(vertices, edges, curVertex, scc, finishingTimes);
    

    // Finally, the dfs should have processed at least one vertex
    // and, to make sure the last assignment is workin properly, 
    // the last index returned should have been the last one added to the list as well.
    assert(scc.size() > 0);
    assert(lastVertex == scc[scc.size()-1]);

    // Move curVertex on to the next unprocessed vertex.
    // This should be the vertex preceeding the earliest finish
    /*
      int c;
      int soonestFinish = -1;
      int soonestFinishIndex = -1;
    */
    
    while (vertices[curVertex].marked == GraphVertex::Marked and
					 finishingTimes[curVertex] >= 0 and 
					 finishingOrder[curVertex] >= 0) {
      curVertex = finishingOrder[curVertex];
    }
    //    curVertex = finishingOrder[scc[soonestFinishIndex]];
    /*
      std::cout << "dfs started with " << prevOrder << " ended " << finishingTimes[curVertex] 
      << " " << scc.size() <<  std::endl;
    */
    if (scc.size() > 1) {
      sccs.push_back(scc);
    }
  }
  return sccs.size();
}


#endif
