/***************************************************************************
 * Title:          alignutils.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  09/12/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef _ALIGNUTILS_
#define _ALIGNUTILS_

#include "DNASequence.h"
#include "mctypes.h"
#include <iomanip>
#include <iostream>
#include <vector>

// Constants for storing the direction of optimal paths (not 
// used for computing the scores of paths!).

class Score {
public:
  Score(std::string &scoreMatName, float open, float extend);
  Score();
	Score(float match, float mismatch, float gapOpen, float gapExtend);
	void CalculateEditDistance();
  FloatMatrix scoreMat;
  float gapOpen, gapExtend;
};


static int MATCH  = 0;
static int GAP_A  = 1;
static int GAP_B  = 2;

static int GAP_EXTEND = 0;
static int GAP_OPEN   = 1;
static int CLOSE_GAP_A = 3;
static int CLOSE_GAP_B = 4;

static int BAND_MATCH  = 5;
static int BAND_GAP_A  = 6;
static int BAND_GAP_B  = 7;

static int LOCAL_START = 8;

static int DO_LOCAL_ALIGN = 1;
static int DO_GLOBAL_ALIGN = 0;
// Infinity enough!
static float INF = 9999999;

static char nuc_index[256] = {
  0,1,2,3,4,4,4,4,4,4,   // 0 9
  4,4,4,4,4,4,4,4,4,4,   // 10 19
  4,4,4,4,4,4,4,4,4,4,   // 20 29
  4,4,4,4,4,4,4,4,4,4,   // 30 39
  4,4,4,4,4,4,4,4,4,4,   // 40 49
  4,4,4,4,4,4,4,4,4,4,   // 50 59
  4,4,4,4,4,1,4,2,4,4,   // 60 69
  4,0,4,4,4,4,4,4,4,4,   // 70 79
  4,4,4,4,3,4,4,4,4,4,   // 80 89
  4,4,4,4,4,4,4,1,4,2,   // 90 99
  4,4,4,0,4,4,4,4,4,4,   // 100 109
  4,4,4,4,4,4,3,4,4,4,   // 110 119
  4,4,4,4,4,4,4,4,4,4,   // 120 129
  4,4,4,4,4,4,4,4,4,4,   // 130 139
  4,4,4,4,4,4,4,4,4,4,   // 140 149
  4,4,4,4,4,4,4,4,4,4,   // 150 159
  4,4,4,4,4,4,4,4,4,4,   // 160 169
  4,4,4,4,4,4,4,4,4,4,   // 170 179
  4,4,4,4,4,4,4,4,4,4,   // 180 189
  4,4,4,4,4,4,4,4,4,4,   // 190 199
  4,4,4,4,4,4,4,4,4,4,   // 200 209
  4,4,4,4,4,4,4,4,4,4,   // 210 219
  4,4,4,4,4,4,4,4,4,4,   // 220 229
  4,4,4,4,4,4,4,4,4,4,   // 230 239
  4,4,4,4,3,2,1,0,3,2,   // 240 249
  1,0,3,2,1,0};          // 250 255



/*
  static way of initializing this:
  
  int a, c, t, g;
  a => 0; c => 1; g => 2; t => 3;

  nuc_index[0] = g;
  nuc_index[1] = a;
  nuc_index[2] = c;
  nuc_index[3] = t;
  nuc_index[(unsigned char)-1] = 2;  255
  nuc_index[(unsigned char)-2] = 0;  254
  nuc_index[(unsigned char)-3] = 1;  253
  nuc_index[(unsigned char)-4] = 3;  252
  nuc_index[(unsigned char)-5] = 2;  251
  nuc_index[(unsigned char)-6] = 0;  250
  nuc_index[(unsigned char)-7] = 1;  249
  nuc_index[(unsigned char)-8] = 3;  248
  nuc_index[(unsigned char)-9] = 2;  247
  nuc_index[(unsigned char)-10] = 0; 246
  nuc_index[(unsigned char)-11] = 1; 245
  nuc_index[(unsigned char)-12] = 3; 244

  nuc_index['g'] = 0; 103
  nuc_index['G'] = 0;  71
  nuc_index['a'] = 1;  97
  nuc_index['A'] = 1;  65
  nuc_index['c'] = 2;  99
  nuc_index['C'] = 2;  67
  nuc_index['t'] = 3; 116
  nuc_index['T'] = 3;  84
  nuc_index['n'] = 4; 110
  nuc_index['N'] = 4;  78
  nuc_index['x'] = 4; 120
  nuc_index['X'] = 4;  88
*/


void ResizeScoreMat(FloatMatrix &scoreMat);

int CalcNumMatches(DNASequence &seqa, DNASequence &seqb, int *locations);

float CalcPercentIdentity(DNASequence &seqa, DNASequence &seqb, 
			  int *locations);

float CalcRepatPercentIdentity(DNASequence &seqa, DNASequence &seqb, 
			       int *locations);

float CalcNonRepeatPercentIdentity(DNASequence &seqa, DNASequence &seqb, 
				   int *locations);

float AffineAlign(DNASequence &seqa, DNASequence &seqb, 
		  float match, float mismatch, float gap, 
		  float gapOpen, float gapExtend,
		  int *&locations, FloatMatrix &scoreMat);

float AffineAlign(DNASequence &seqa, DNASequence &seqb, 
		  float match, float mismatch, float gap, 
		  float gapOpen, float gapExtend,
		  int *&locations);


float FitAlign(DNASequence &seqa, DNASequence &seqb, 
							 float match, float mismatch, float gap, 
							 int *&locations, FloatMatrix &scorMat,
							 FloatMatrix &score, IntMatrix &path);

float Align(DNASequence &seqa, DNASequence &seqb, 
	    float match, float mismatch, float gap, 
	    int *&locations, FloatMatrix & scoreMat);

float BandedAlign(DNASequence &seqa, DNASequence &seqb, 
									float match, float mismatch, float gap, 
									int k,
									int *&locations,  
									FloatMatrix &score,
									IntMatrix &path,
									FloatMatrix &scoreMat,
									float *optScores = NULL);


float BandedAlign(DNASequence &seqa, DNASequence &seqb, 
									float match, float mismatch, float gap, 
									int k,
									int *&locations, float *optScores=NULL);

float CoreAlign(DNASequence &seqa, DNASequence &seqb,
		float match, float mismatch, float gap, 
		int *&locations, FloatMatrix &score, IntMatrix &path,
		float alternative, // 0 for smith-waterman, +infty for needleman wunsch
		int &globalMinRow, int &globalMinCol);

float LocalAlign(DNASequence &seqa, DNASequence &seqb, 
		 float match, float mismatch, float gap, 
		 int *&locations,
		 FloatMatrix &scorMat);


float LocalAlign(DNASequence &seqa, DNASequence &seqb, 
		 Score &scoreMat, int *&locations);

float OverlapAlign(DNASequence &seqa, DNASequence &seqb,
									 float match, float mismatch, float gap, 
									 int *&locations, FloatMatrix &scoreMat);

float AffineLocalAlign(DNASequence &seqa, DNASequence &seqb, 
		       float match, float mismatch, float gap, 
		       float gapOpen, float gapExtend,
		       int *&locations, FloatMatrix &scoreMat);

float AffineLocalAlign(DNASequence &seqa, DNASequence &seqb,
		       Score &scoreMat, int *&locations);

void ParseScoreMatStr(std::string scoreMatStr, FloatMatrix &scoreMat);

void ReadScoreMatFile(std::string scoreMatFileName, FloatMatrix & scoreMat);

class AlignVertex;

void GetGraphAlignment(AlignVertex *vertex, 
		       int *&locations, int &length, 
		       int refStartPos, int qryStartPos, int extDir);


float ScoreBandedAffineAlign(DNASequence &refSeq, DNASequence &qrySeq, // sequences to compare 
			    float initialScore,  // score of seed
			    FloatMatrix &scoreMat, float gapOpen, float gapExtend, // scoring parameters
			    float maxScore,  // negative score is good.
			    int *&locations, int &length, 
			    int &refAlignStart, int &qryAlignStart, // used for storing the resulting map
			    int refStartPos=0, int qryStartPos=0, 
			    int extDir=1
			    );


void AssignScoreMat(FloatMatrix &sm1, FloatMatrix &sm2);


void DeleteGraph(std::vector<AlignVertex*> &graph);

void InitScoreMat(FloatMatrix &scoreMat, float match, float mismatch);

template<typename T>
T** CreateMatrix(int rows, int cols, int init=0) {
  int i, j;
  T **mat;
  mat = new T*[rows];
  for (i = 0; i < rows; i++) {
    mat[i] = new T[cols];
    for (j = 0; j < cols; j++) {
      mat[i][j] = init;
    }
  }
  return mat;
}


template <typename T>
void DeleteMatrix(T **mat, int rows) {
  int r;
  for (r = 0; r < rows; r++)
    delete [] mat[r];
  delete[] mat;
}


template <typename T>
void PrintMatrix(T **mat, int rows, int cols, int width=3) {
  int r, c;
  std::cout << std::setw(width) << 1;
  for (c = 0; c < cols; c++) 
    std::cout << std::setw(width) << c;
  std::cout << std::endl;
  for (r = 0; r < rows; r++) {
    std::cout << std::setw(width) << r;
    for (c = 0; c < cols; c++ ) {
      std::cout << std::setw(width) << mat[r][c];
    }
    std::cout << std::endl;
  }
}



#endif
