/***************************************************************************
 * Title:          graphalign.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef _GRAPH_ALIGN_H_
#define _GRAPH_ALIGN_H_

#include "DNASequence.h"
#include <limits.h>
#include <float.h>
#include <vector>
#include <mctypes.h>

#define FORWARD_DIR 1
#define REVERSE_DIR -1

float AlignRegion(DNASequence &refSeq, int refStart, int refEnd,
									DNASequence &qrySeq, int qryStart, int qryEnd,
									FloatMatrix &scoreMat, float gapOpen, float gapExtend,
									int &refAlignStart, int &qryAlignStart, 
									int *&alignment, int &length);


class AlignVertex {
public:
  // Use for storing optimal scores/pointers.
  float matchScore, refGapScore, qryGapScore;
  AlignVertex *matchPrev, *refGapPrev, *qryGapPrev;
  int matchIndex, refGapIndex, qryGapIndex;
  // Use for storing griddata structure.
  AlignVertex *north, *south, *east, *west, *northwest, *southeast;
  int scored;
  int refPos, qryPos;
  AlignVertex(int refPosP, int qryPosP) {
    refPos     = refPosP;
    qryPos     = qryPosP;
    north      = NULL;
    south      = NULL;
    northwest  = NULL;
    east       = NULL;
    west       = NULL;
    southeast  = NULL;
    matchPrev  = NULL;
    refGapPrev = NULL;
    qryGapPrev = NULL;
    matchIndex = none; 
    refGapIndex= none;
    qryGapIndex= none;
    matchScore = infty;
    refGapScore= infty;
    qryGapScore= infty;
    scored = 0;
  }
  AlignVertex* GetNorth();
  AlignVertex* GetWest();
  AlignVertex* GetNorthWest();
  AlignVertex* GetSoutheast();
  AlignVertex* GetSouth();
  AlignVertex* GetEast();

  float ComputeDiagonalScore(AlignVertex *diag, AlignVertex *afRef, AlignVertex *afQry, 
			     FloatMatrix &scoreMat, char refChar, char qryChar);

  float ComputeAffineGapScore(AlignVertex *afGap, AlignVertex *diag, 
			      float affineGapOpen,
			      float affineGapExtend);

  float ScoreQryGap(float gapOpenCost, float gapExtendCost, float maxScore, float minScore);
  float ScoreRefGap(float gapOpenCost, float gapExtendCost, float maxScore, float minScore);
  float ScoreMatch(DNASequence &refSeq, DNASequence &qrySeq, 
		   int refPos, int qryPos,
		   int refStartPos, int qryStartPos, int extDir,
		   FloatMatrix &scoreMat, float maxScore, float minScore);

  // use these to ensure a path is not taken
  void StopMatchAlign() { matchScore = FLT_MAX; }
  void StopRefGapAlign() { refGapScore = FLT_MAX; }
  void StopQryGapAlign() { qryGapScore = FLT_MAX; }

  AlignVertex* InitializeSouthEast(AlignVertex *southeast);
  AlignVertex* InitializeSouth(AlignVertex *south);
  AlignVertex* InitializeEast(AlignVertex *east);

  int Perfect(float maxScore);
  int Capable(float maxScore);
  int ValidRefGap(float gapOpenScore, float maxScore) {
    return refGapScore < maxScore || gapOpenScore + matchScore < maxScore;
  }

  int ValidQryGap(float gapOpenScore, float maxScore) {
    return qryGapScore < maxScore || gapOpenScore + matchScore < maxScore;
  }

  int ValidMatch(float maxScore) {
    return matchScore < maxScore;
  }
  
  void Score(DNASequence &refSeq, DNASequence &qrySeq, 
	     int refPos, int qryPos, 
	     int refStartPos, int qryStartPos, int extDir,
	     FloatMatrix &scoreMat, float gapOpen, float gapExtend, float maxScore, float minScore) {
    ScoreRefGap(gapOpen, gapExtend, maxScore, minScore);
    ScoreQryGap(gapOpen, gapExtend, maxScore, minScore);
    ScoreMatch(refSeq, qrySeq, 
	       refPos, qryPos, 
	       refStartPos, qryStartPos, extDir,
	       scoreMat, maxScore, minScore);
    scored = 1;
  }

  static int match, refGap, qryGap, none;
  static float infty;
};


#endif
