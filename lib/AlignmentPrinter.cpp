/***************************************************************************
 * Title:          AlignmentPrinter.cpp 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  05/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include "AlignmentPrinter.h"

#include "DNASequence.h"
#include "SeqUtils.h"
#include "align/alignutils.h"

char GetAlignChar(unsigned char refChar, unsigned char qryChar) {
  if (nuc_index[refChar] < 4 and
      nuc_index[qryChar] < 4 and
      nuc_index[refChar] == nuc_index[qryChar]) {
    return '|';
  }
  else {
    return ' ';
  }
}


void PrintAlignment(DNASequence &refSeq, 
		    DNASequence &qrySeq,
		    int refStartPos, int qryStartPos,
		    int *alignment, int alignmentLength,
		    std::ostream &out, int lineLength ) {
  std::string refAlignString, qryAlignString, alignString;
  int refAlignStart, qryAlignStart;
  MakeAlignmentString(refSeq, qrySeq, refStartPos, qryStartPos, 
		      alignment, alignmentLength,
		      refAlignString, qryAlignString, alignString, 
		      refAlignStart, qryAlignStart);

  PrintAlignmentString(refAlignString, qryAlignString, alignString,
		       refAlignStart, qryAlignStart, out, lineLength);
  out << std::endl;
}

void MakeAlignmentString(DNASequence &refSeq, 
			 DNASequence &qrySeq,
			 int refStartPos, int qryStartPos,
			 int *alignment, int alignmentLength,
			 std::string &refAlignString, 
			 std::string &qryAlignString, 
			 std::string &alignString,
			 int &refAlignStart, int &qryAlignStart) {

  // Find the first aligned position in locations.
  int r, p, a;
  int alignStartIndex, alignEndIndex;
  for (a = 0; a < alignmentLength and alignment[a] == -1; a++);
  alignStartIndex = a;
  for (a = alignmentLength-1; a >= 0 and alignment[a] == -1; --a);
  alignEndIndex = a;

  std::string refStr, qryStr, alignStr;
  int refPos, qryPos;
  a = alignStartIndex;
  refPos = refStartPos + alignStartIndex;
  qryPos = qryStartPos + alignment[a];
  refAlignStart = a;
  qryAlignStart = qryPos;
  unsigned char refNuc, qryNuc;
  refAlignString = qryAlignString = alignString = "";
  while (a <= alignEndIndex) {
    refNuc = (unsigned char) refSeq.seq[refPos];
    qryNuc = (unsigned char) qrySeq.seq[qryPos];
    refAlignString += nuc_char[refNuc];
    qryAlignString += nuc_char[qryNuc];
    alignString    += GetAlignChar(refNuc, qryNuc);
    refPos++;
    qryPos++;
    a++;
    while (a < alignEndIndex and alignment[a] == -1) {
      refPos++;
      a++;
      refNuc = (unsigned char) refSeq.seq[refPos];
      refAlignString += nuc_char[refNuc];
      alignString += ' ';
      qryAlignString += '-';
    }
    if (a < (alignmentLength-1)) {
      int g;
      for (g = 0; g < (alignment[a+1] - alignment[a] - 1); ++g) {
				qryPos++;
				qryNuc = (unsigned char) qrySeq.seq[qryPos];
				refAlignString += '-';
				alignString += ' ';
				qryAlignString += nuc_char[qryNuc];
      }
    }
  }
}


void PrintAlignmentString(std::string refAlignString, 
			  std::string qryAlignString,
			  std::string alignString,
			  int refPos, int qryPos,
			  std::ostream &out,
			  int lineLength) {
  assert(refAlignString.size() == qryAlignString.size());
  assert(qryAlignString.size() == alignString.size());

  int linePos, refStringPos, qryStringPos, alignStringPos;
  int i;
  refStringPos= qryStringPos = alignStringPos = 0;
  while (refStringPos < refAlignString.size()) {
    out << "ref: "; out.width(8); out << refPos << " ";
    for (linePos = 0; linePos < lineLength; linePos++) {
      if (refStringPos >= refAlignString.size()) 
	break;
      out << refAlignString[refStringPos];
      refStringPos++;
      ++refPos;
    }
    out << std::endl;
    out << "              ";
    for (linePos = 0; linePos < lineLength; linePos++) {
      if (alignStringPos >= alignString.size())
	break;
      out << alignString[alignStringPos];
      alignStringPos++;
    }
    out << std::endl;
    out << "qry: "; out.width(8); out << qryPos << " ";
    for (linePos = 0; linePos < lineLength; linePos++) {
      if (qryStringPos >= qryAlignString.size())
	break;
      out << qryAlignString[qryStringPos];
      qryStringPos++;
      ++qryPos;
    }
    out << std::endl << std::endl;

  }
}
