/***************************************************************************
 * Title:          Mapping.h 
 * Author:         Mark Chaisson
 * Created:        2007
 * Last modified:  01/08/2008
 *
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#ifndef _MAPPING_H_
#define _MAPPING_H_
#include <iostream>
#include <ostream>
#include <istream>
#include <fstream>

void PrintGlue( int *locations, int lenLocations, std::ostream &out );
#endif
